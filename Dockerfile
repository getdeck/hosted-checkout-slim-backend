FROM 702020459620.dkr.ecr.us-east-1.amazonaws.com/stickyio-hco-php:7.4.16-0.0.1

COPY . /var/www/html
COPY .env.docker /var/www/html/.env

WORKDIR /var/www/html
RUN composer install --no-interaction --no-progress --no-suggest

RUN sed -ri -e 's!/var/www/html!/var/www/html/public!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!/var/www/html/public!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

RUN a2enmod rewrite

WORKDIR /var/www/html/public
