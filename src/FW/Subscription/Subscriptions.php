<?php
namespace App\FW\Subscription;
use App\FW\Subscription\Helper\Config;

define("INVALID_SUBSCRIPTION_ID", "Invalid subscription ID");
define("INVALID_DATE", "Invalid recurring date");
define("INVALID_ORDER_ID", "Invaild order id");
define("INVALID_ORDER_ACTION", "Invalid order action");
define("INVALID_BILLING", "Invalid billing model");
define("INVALID_PRODUCT_ID", "Invalid product ID");
define("INVALID_DATES", "Invalid dates");

define("DATE_FORMAT", "m/d/Y");

class Subscriptions extends Config{
    protected $filters;

    public function __construct($request){
        $this->request = $request;
        parent::__construct();

        $this->exceptionMessage = [
            "subscription_id" => INVALID_SUBSCRIPTION_ID,
            "recur_at" => INVALID_DATE,
            "order_id" => INVALID_ORDER_ID,
            "orderAction" => INVALID_ORDER_ACTION,
            "billing_model_id" => INVALID_BILLING,
            "product_id" => INVALID_PRODUCT_ID,
            "dates" => INVALID_DATES
        ];
    }

    public function orderList(){
        try{
            $this->manageRequestFilter();
            $this->filters['formType'] = 'order_find';
            $this->filters['formMethod'] = 'POST';
            $orderResponse = $this->processEndPoint($this->filters);
            
            if(isset($orderResponse['responseData']['order_id'])){
                rsort($orderResponse['responseData']['order_id']);
            }

            if(isset($orderResponse['responseData']['data'])){
                rsort($orderResponse['responseData']['data']);

                foreach($orderResponse['responseData']['data'] as $orderKey => $order) {
                    foreach($order['products'] as $key => $product) {
                        $orderResponse['responseData']['data'][$orderKey]['products'][$key]['name'] = html_entity_decode($product['name']); 
                        $orderResponse['responseData']['data'][$orderKey]['products'][$key]['next_subscription_product'] = html_entity_decode($product['next_subscription_product']); 
                    }
                }
            }

            return $this->productDetailImage($orderResponse);
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
        
    }

    protected function productDetailImage($response){
        $temp = [];
        try{
            $orders = isset($response['responseData']['data']) ? $response['responseData']['data'] : [];
           
            
            if(empty($orders)) return $response;

            foreach($orders as $kay=>$val){
                $products = $val['products'];
                
                foreach($products as $proKey=>$proVal){
                    $temp=$proVal;
                    $orders[$kay]['products'][$proKey]['images'] = [];
                }
            }
            $response['responseData']['data'] = $orders;
            return $response;
        }catch(\Exception $ex){
            return $response;
        }
    }
    

    protected function getImageUrl($proVal = []){
        try{

            if(empty($proVal)) return [];

            
            $form = ['formType'=>"product_image", 
                        "formMethod"=>"GET",
                        "param"=> $proVal['product_id']
                    ];
            $productImages = $this->processEndPoint($form);
            return (isset($productImages['responseData']['data']) ? $productImages['responseData']['data'] : []);
        }catch(\Exception $ex){
            return [];
        }
    }


    protected function manageRequestFilter(){
        try{
            //date m/d/y
            $this->filters = [
                "campaign_id"=>"all",
                "start_date"=>isset($this->requestBody["filter"]["start_date"]) ? $this->requestBody["filter"]["start_date"] : "06/01/2000",
                "start_time"=>"",
                "end_date"=>isset($this->requestBody["filter"]["end_date"]) ? $this->requestBody["filter"]["end_date"] : date("m/d/yy"),
                "end_time"=>"",
                "date_type"=>"create",
                // "product_id"=>[],
                "criteria"=>[
                    // "new"=>isset($this->requestBody["filter"]["new"]) ? $this->requestBody["filter"]["new"] : 1,
                    // "rma"=>isset($this->requestBody["filter"]["rma"]) ? $this->requestBody["filter"]["rma"] : 0,
                    // "has_upsells"=>isset($this->requestBody["filter"]["has_upsells"]) ? $this->requestBody["filter"]["has_upsells"] : 0,
                    "email"=>isset($this->requestBody["filter"]["email"]) ? $this->requestBody["filter"]["email"] : "",
                    // "recurring" => isset($this->requestBody["filter"]["recurring"]) ? $this->requestBody["filter"]["recurring"] : 0,
                ],
                "member_token"=>"",
                "return_type"=>"order_view",
                "search_type"=>"all"
            ];

            if(empty($this->filters["criteria"]["email"])) throw new \Exception("Please provide valid email");

            $hold = isset($this->requestBody["filter"]["hold"]) ? $this->requestBody["filter"]["hold"] : 0;
            $ram = isset($this->requestBody["filter"]["ram"]) ? $this->requestBody["filter"]["ram"] : 0;
            $return = isset($this->requestBody["filter"]["return"]) ? $this->requestBody["filter"]["return"] : 0;
            $chargeback = isset($this->requestBody["filter"]["chargeback"]) ? $this->requestBody["filter"]["chargeback"] : 0;

            $createStatus = $hold==0 && $ram==0 && $return==0 && $chargeback ==0 ? true : false;

            if($hold==1){
                $this->filters['date_type'] = 'hold';
            }

            if($ram==1){
                $this->filters['date_type'] = 'rma';
            }

            if($return==1){
                $this->filters['date_type'] = 'return';
            }

            if($chargeback==1){
                $this->filters['date_type'] = 'chargeback';
            }
            
            if(isset($this->requestBody["filter"]["new"]) && $createStatus==true){
                $this->filters['criteria']['new'] = $this->requestBody["filter"]["new"];
            }


            if(isset($this->requestBody["filter"]["recurring"]) && $createStatus==true){
                $this->filters['criteria']['recurring'] = $this->requestBody["filter"]["recurring"];
            }

            if(isset($this->requestBody["filter"]["approved"]) && !empty($this->requestBody["filter"]["approved"]) && $createStatus==true ){
                $this->filters['criteria']['approved'] =1;
            }

            if(isset($this->requestBody["filter"]["fraud"]) && !empty($this->requestBody["filter"]["fraud"]) && $createStatus==true){
                $this->filters['criteria']['fraud'] =1;
            }

            if(isset($this->requestBody["filter"]["unshipped"]) && !empty($this->requestBody["filter"]["unshipped"]) && $createStatus==true){
                $this->filters['criteria']['unshipped'] =1;
            }

            if(isset($this->requestBody["filter"]["shipped"]) && !empty($this->requestBody["filter"]["shipped"]) && $createStatus==true){
                $this->filters['criteria']['shipped'] =1;
            }

            if(isset($this->requestBody["filter"]["confirmed"]) && !empty($this->requestBody["filter"]["confirmed"]) && $createStatus==true){
                $this->filters['criteria']['confirmed'] =1;
            }

            if(isset($this->requestBody["filter"]["void"]) && !empty($this->requestBody["filter"]["void"]) && $createStatus==true){
                $this->filters['criteria']['void'] =1;
            }

            if(isset($this->requestBody["filter"]["refunded"]) && !empty($this->requestBody["filter"]["refunded"]) && $createStatus==true){
                $this->filters['criteria']['all_refunds'] =1;
            }

            if(isset($this->requestBody["filter"]["declined"]) && !empty($this->requestBody["filter"]["declined"]) && $createStatus==true){
                $this->filters['criteria']['declines'] =1;
            }

            if(isset($this->requestBody["filter"]["pending"]) && !empty($this->requestBody["filter"]["pending"]) && $createStatus==true){
                $this->filters['criteria']['pending'] =1;
            }


        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    public function checkRequestParam($expectedFields){
        foreach ($expectedFields as $field) {
            if(!array_key_exists($field, $this->requestBody) || empty($this->requestBody[$field])) {
                throw new \InvalidArgumentException($this->exceptionMessage[$field]);
            }
        }
    }

    public function processOrderAction($apiRequest, $requestIds, $field='param') {
        $response = [];

        if(is_array($requestIds)) {
            if(!in_array("", $requestIds)){
                foreach($requestIds as $id) {
                    $apiRequest[$field] = $id;
                    $apiRequest['returnError'] = false;
                    $apiResponse = $this->processEndPoint($apiRequest);
                    $response[$id] = $this->setActionResponse($apiResponse);
                }
                return $response;
            } else {
                throw new \InvalidArgumentException($this->exceptionMessage[$field]);
            }
        } else {
            $apiRequest[$field] = $requestIds;
            return $this->processEndPoint($apiRequest);
        }  
    }

    public function setActionResponse($apiResponse) {
        $response = [];
        if(isset($apiResponse['responseData'])){
            $response = $apiResponse['responseData'];

            if( !isset($apiResponse['responseData']['data']['responseMessage']) &&
                isset($apiResponse['message']) &&
                $apiResponse['responseData']['status'] == "SUCCESS") 
            {
                $response['data']['responseMessage'] = $apiResponse['message'];
            }
        } else {
            $response = $apiResponse;
        }
        return $response;
    }

    public function orderRefund(){
        try{
            
            $this->checkRequestParam(['order_id']);
            $request = [];
            $request['formType'] = 'order_calculate_refund';
            $request['formMethod'] = 'POST';
            $request['order_id'] = $this->requestBody['order_id'];
            $orderResponse = $this->processEndPoint($request);

            return $orderResponse;

        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    public function orderCancel(){
        try{
            throw new \Exception("Cannot cancel an order", 500);
            //cancel subscription
            $refundResponse = $this->orderRefund(); 
            $refundAmount = isset($refundResponse['responseData']['amount']) ? $refundResponse['responseData']['amount'] : null;

            if($refundAmount == null) throw new \Exception("Invalid refund amount");

            $cancelSubscription = [
                'order_id' => $this->requestBody['order_id'],
                'amount' => $refundAmount,
                'keep_recurring' => 0,
                'note_id' => 3,
                'stop_shipping'=>1,
                'formType' => 'order_refund',
                'formMethod' => 'POST'
            ];

            //return $this->processEndPoint($cancelSubscription);
            
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    public function orderStartSubscription(){
        try{
            $this->checkRequestParam(['subscription_id']);
           
            $request['formType'] = 'subscription_start';
            $request['formMethod'] = 'POST';

            return $this->processOrderAction($request, $this->requestBody['subscription_id']);

        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    public function  orderStopSubscription(){
        try{
            $this->checkRequestParam(['subscription_id']);

            $request['formType'] = 'subscription_stop';
            $request['formMethod'] = 'POST';

            return $this->processOrderAction($request, $this->requestBody['subscription_id']);

        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    public function orderPauseSubscription(){
        try{
            $this->checkRequestParam(['subscription_id']);

            $request['formType'] = 'subscription_pause';
            $request['formMethod'] = 'PUT';

            return $this->processOrderAction($request, $this->requestBody['subscription_id']);
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    public function orderResetSubscription(){
        try{
            if(empty($this->requestBody['subscription_id'])) throw new \Exception(SUBSCRIPTION_ID);

            $request['formType'] = 'subscription_reset';
            $request['formMethod'] = 'POST';
            $request['param'] = $this->requestBody['subscription_id'];

            return $this->processEndPoint($request);
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    public function orderSkipSubscription(){
        try{
            $this->checkRequestParam(['subscription_id']);

            $request['formType'] = 'skip_next_billing';
            $request['formMethod'] = 'POST';
            $request['skip_next_product'] = 1;

            return $this->processOrderAction($request, $this->requestBody['subscription_id'], 'subscription_id');

        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    public function orderUpdateSubscriptionRecurringDate(){
        $response = [];
        try{
            if(isset($this->requestBody['api_version']) && $this->requestBody['api_version'] == "v1") {
                $this->checkRequestParam(['order_id', 'product_id', 'recur_at']);

                foreach($this->requestBody['product_id'] as $product){
                    $request = [
                        'formType'           => 'order_update',
                        'formMethod'         => 'POST',
                        'order_id'=>[$this->requestBody['order_id'] => [
                            'recurring_date' => date(DATE_FORMAT, strtotime($this->requestBody['recur_at'])),
                            'product_id'     => $product
                        ]]
                    ];
                    $apiResponse = $this->processEndPoint($request);
                    $response[$product] = $this->setActionResponse($apiResponse);
                }
                return $response;
            } else {
                $this->checkRequestParam(['subscription_id', 'recur_at']);

                $request = [
                    'formType'          => 'subscription_recur_at',
                    'formMethod'        => 'PUT',
                    'skip_next_product' => 1,
                    'recur_at'          => $this->requestBody['recur_at']
                ];

                return $this->processOrderAction($request, $this->requestBody['subscription_id']);
            }
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    public function orderUpdate(){
        try{
            
            $this->checkRequestParam(['order_id']);
            //shipping
            if(empty($this->requestBody['shipping_first_name'])) throw new \Exception("Invalid shipping first name");
            if(empty($this->requestBody['shipping_last_name'])) throw new \Exception("Invalid shipping last name");
            if(empty($this->requestBody['shipping_address1'])) throw new \Exception("Invalid shipping address");
            if(empty($this->requestBody['shipping_city'])) throw new \Exception("Invalid shipping city");
            if(empty($this->requestBody['shipping_zip'])) throw new \Exception("Invalid shipping zip");
            if(empty($this->requestBody['shipping_state'])) throw new \Exception("Invalid shipping state");
            if(empty($this->requestBody['shipping_country'])) throw new \Exception("Invalid shipping country");

            //Billing
            if(empty($this->requestBody['billing_first_name'])) throw new \Exception("Invalid billing first name");
            if(empty($this->requestBody['billing_last_name'])) throw new \Exception("Invalid billing last name");
            if(empty($this->requestBody['billing_address1'])) throw new \Exception("Invalid billing address");
            if(empty($this->requestBody['billing_city'])) throw new \Exception("Invalid billing city");
            if(empty($this->requestBody['billing_zip'])) throw new \Exception("Invalid billing zip");
            if(empty($this->requestBody['billing_state'])) throw new \Exception("Invalid billing state");
            if(empty($this->requestBody['billing_country'])) throw new \Exception("Invalid billing country");
            

            $request = [
                'order_id'=>[$this->requestBody['order_id'] => [
                    'shipping_first_name'=> $this->requestBody['shipping_first_name'],
                    'shipping_last_name'=> $this->requestBody['shipping_last_name'],
                    'shipping_address1'=> $this->requestBody['shipping_address1'],
                    'shipping_address2'=> $this->requestBody['shipping_address2'], //optional
                    'shipping_city'=> $this->requestBody['shipping_city'],
                    'shipping_zip'=> $this->requestBody['shipping_zip'],
                    'shipping_state'=> $this->requestBody['shipping_state'],
                    'shipping_country'=> $this->requestBody['shipping_country'],

                    'billing_first_name'=> $this->requestBody['billing_first_name'],
                    'billing_last_name'=> $this->requestBody['billing_last_name'],
                    'billing_address1'=> $this->requestBody['billing_address1'],
                    'billing_address2'=> $this->requestBody['billing_address2'],  //optional
                    'billing_city'=> $this->requestBody['billing_city'],
                    'billing_zip'=> $this->requestBody['billing_zip'],
                    'billing_state'=> $this->requestBody['billing_state'],
                    'billing_country'=> $this->requestBody['billing_country'],
                    ]
                ],
                "formType"=>"order_update",
                "formMethod"=>"POST"

            ];

            return $this->processEndPoint($request);

        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    public function activeMemberProfile(){
        try{
            if(empty($this->requestBody['email'])) throw new \Exception("Please enter valid email");
            if(empty($this->requestBody['current_member_password'])) throw new \Exception("Please enter valid password");
            if(empty($this->requestBody['new_password'])) throw new \Exception("Please enter valid new password");
            if(empty($this->requestBody['new_confirm_password'])) throw new \Exception("Please enter valid new confirm password");

            if($this->requestBody['new_password']!=$this->requestBody['new_confirm_password']) throw new \Exception("New password and confirmation password do not match");

            $request = [
                "email" => $this->requestBody['email'],
                "current_member_password" => $this->requestBody['current_member_password'],
                "new_member_password" => $this->requestBody['new_password'],
                "formType" => "member_update",
                "formMethod" => "POST"
            ];

            return $this->processEndPoint($request);
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    public function memberLogin(){
        try{

            $this->getMemberContact();
            //email
            if(empty($this->requestBody['email'])) throw new \Exception("Please enter valid email");
            if(empty($this->requestBody['password'])) throw new \Exception("Please enter valid password");

            $request = [
                "email" => $this->requestBody['email'],
                "member_password" =>$this->requestBody['password'],
                "formType"=>"member_login",
                "formMethod"=>"POST"
            ];
            $loginResponse = $this->processEndPoint($request);
            $contactDetail = $this->getMemberContact(); 
            $loginResponse['contacts'] = isset($contactDetail['responseData']['data']) ? $contactDetail['responseData']['data'] : [];
            
            return $loginResponse;
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    protected function getMemberContact(){
        try{
            $email = isset($this->requestBody['email']) ? $this->requestBody['email'] : "";

            if(empty($email)){
                return [];
            }

            $request = [
                "param"=>"?search[email]=".$email,
                "formType"=>"contacts_address",
                "formMethod"=>"GET"
            ];
            return $this->processEndPoint($request);
            
        }catch(\Exception $ex){
            die($ex->getMessage());
           return [];
        }
    }

    public function memberForgotPassword(){
        try{
            if(empty($this->requestBody['email'])) throw new \Exception("Please enter valid email");

            $memberDetails = $this->dbInstance->members_portals->findOne(["member_portal_login_url_slug"=>$this->requestBody['member_hash']]);
          
            if(!empty($memberDetails) && 
                isset($memberDetails['member_forgot_password_event_id']) && 
                isset($memberDetails['member_forgot_password_event_id']['event_id']))
            {
                $request = [
                    "email" => $this->requestBody['email'],
                    "event_id" => $memberDetails['member_forgot_password_event_id']['event_id'],
                    "formType"=>"member_forgot_password",
                    "formMethod"=>"POST"
                ];
            
                return $this->processEndPoint($request);
            } else {
                throw new \Exception('Email not sent due to an error. Please contact support.');
            }
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    public function memberResetPassword(){
        try{
            if(empty($this->requestBody['email'])) throw new \Exception("Please enter valid email");
            if(empty($this->requestBody['member_temp_password'])) throw new \Exception("Please enter valid reset password");
            if(empty($this->requestBody['new_password'])) throw new \Exception("Please enter valid new password");
            if(empty($this->requestBody['new_confirm_password'])) throw new \Exception("Please enter valid new confirm password");

            if($this->requestBody['new_password']!=$this->requestBody['new_confirm_password']) throw new \Exception("New password and confirmation password do not match");

            $request = [
                "email" => $this->requestBody['email'],
                "member_temp_password" => $this->requestBody['member_temp_password'],
                "member_new_password" => $this->requestBody['new_password'],
                "formType" => "member_reset_password",
                "formMethod" => "POST"
            ];

            return $this->processEndPoint($request);
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    public function validateLogin(){
        try{
            $request = [
                "formType" => "validate_token"
            ];
            
            return $this->validateLoginToken($request);
           
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    public function memberConfiguration(){
        try{
            return (array) $this->config['configuration'];
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    public function subscriptionFrequencyByOffer(){
        try{
            $offerID = $this->requestBody['offer_id'];
            
            if(empty($offerID)) throw new \Exception("Invalid offer-id");

            $request = [
                            "formType" => "billing_model_view",
                            "formMethod"=>"POST",
                            "offer_id"=> $offerID
                        ];
            
            return $this->processEndPoint($request);
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    
    public function buyNowProdcut(){
        try{
          
            $this->buyNowValidation();
            $request =  $this->prepareOrderOffer();
            $request['formType'] = 'onepage_checkout';
            $request['formMethod'] = 'POST';
            
            return $this->processEndPoint($request);
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    public function subscriptionOrderUpdate(){
        try{
            $this->checkRequestParam(['billing_model_id', 'order_id', 'product_id']);

            $billing_model_id = $this->requestBody['billing_model_id'];
            $order_id = $this->requestBody['order_id'];
            $product_id = $this->requestBody['product_id'];

            $request = [
                "formType"         => "subscription_order_update",
                "formMethod"       => "POST",
                "billing_model_id" => $billing_model_id,
                "order_id"         => $order_id,
            ];

            return $this->processOrderAction($request, $product_id, 'product_id');

        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    protected function buyNowValidation(){
        try{
            $requiredField= [
                "firstName",
                "lastName",
                "billingFirstName",
                "billingLastName",
                "billingAddress1",
                // "billingAddress2", //optional
                "billingCity",
                "billingState",
                "billingZip",
                "billingCountry",
                "email_address",
                "customers_telephone",
                // "creditCardType",
                "creditCardNumber",
                "expirationDate",
                "CVV",
                "shippingId",
                // "tranType" Sale will be static
                // "ipAddress",
                "campaignId",
                "offer_id",
                "product_id",
                "billing_model_id",
                "quantity",
                // "price", we will validate seperately 
                // "billingSameAsShipping" : No,
                "shippingAddress1",
                // "shippingAddress2", : optional
                "shippingCity",
                "shippingState",
                "shippingZip",
                "shippingCountry"
            ];

            
            foreach($requiredField as $key=>$val){
                if(!isset($this->requestBody[$val])){
                    
                    throw new \Exception("We are not able to process your request, Please contact to support.");
                }

                if(empty($this->requestBody[$val])){
                    
                    throw new \Exception("We are not able to process your request, Please contact to support.");
                }
            }

        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    public function memberLogout(){
        try{
            $token = isset($this->config["auth_token"]) ? $this->config["auth_token"] : '';
            if(empty($token)) throw new \Exception("Invalid header token");

            $request = [
                "formType" => "member_logout",
                "formMethod"=>"POST",
                "token"=> $token
            ];
            return $this->processEndPoint($request);
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    
    protected function prepareOrderOffer(){
        try{
            $orderRequest = [
                "firstName" => isset($this->requestBody["firstName"]) ? $this->requestBody["firstName"] : "",
                "lastName" => isset($this->requestBody["lastName"]) ? $this->requestBody["lastName"] : "",
                "billingFirstName" => isset($this->requestBody["billingFirstName"]) ? $this->requestBody["billingFirstName"] : "",
                "billingLastName" => isset($this->requestBody["billingLastName"]) ? $this->requestBody["billingLastName"] : "",
                "billingAddress1" => isset($this->requestBody["billingAddress1"]) ? $this->requestBody["billingAddress1"] : "",
                "billingAddress2"=> isset($this->requestBody["billingAddress2"]) ? $this->requestBody["billingAddress2"] : "", //optional
                "billingCity"=> isset($this->requestBody["billingCity"]) ? $this->requestBody["billingCity"] : "",
                "billingState"=> isset($this->requestBody["billingState"]) ? $this->requestBody["billingState"] : "",
                "billingZip"=> isset($this->requestBody["billingZip"]) ? $this->requestBody["billingZip"] : "",
                "billingCountry"=> isset($this->requestBody["billingCountry"]) ? $this->requestBody["billingCountry"] : "",
                "email"=> isset($this->requestBody["email_address"]) ? $this->requestBody["email_address"] : "",
                "phone"=> isset($this->requestBody["customers_telephone"]) ? $this->requestBody["customers_telephone"] : "",
                "creditCardType"=> "VISA",
                "creditCardNumber"=> isset($this->requestBody["creditCardNumber"]) ? $this->requestBody["creditCardNumber"] : "",
                "expirationDate"=> isset($this->requestBody["expirationDate"]) ? str_replace("/","",$this->requestBody["expirationDate"]) : "",
                "CVV"=> isset($this->requestBody["CVV"]) ? $this->requestBody["CVV"] : "",
                "shippingId"=> isset($this->requestBody["shippingId"]) ? $this->requestBody["shippingId"] : "",
                "tranType"=> "Sale", //Sale will be static
                "ipAddress" => isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : "127.0.0.1",
                "campaignId"=> isset($this->requestBody["campaignId"]) ? $this->requestBody["campaignId"] : "",
                "offers"=>[
                    [
                        "offer_id"=> isset($this->requestBody["offer_id"]) ? $this->requestBody["offer_id"] : "",
                        "product_id"=> isset($this->requestBody["product_id"]) ? $this->requestBody["product_id"] : "",
                        "billing_model_id"=> isset($this->requestBody["billing_model_id"]) ? $this->requestBody["billing_model_id"] : "",
                        "quantity"=> isset($this->requestBody["quantity"]) ? $this->requestBody["quantity"] : "",
                        "price"=> isset($this->requestBody["price"]) ? $this->requestBody["price"] : "",
                    ]
                ],
                
                
                 //we will validate seperately 
                "billingSameAsShipping"=>"NO",
                "shippingAddress1"=> isset($this->requestBody["shippingAddress1"]) ? $this->requestBody["shippingAddress1"] : "",
                "shippingAddress2"=> isset($this->requestBody["shippingAddress2"]) ? $this->requestBody["shippingAddress2"] : "", //: optional
                "shippingCity"=> isset($this->requestBody["shippingCity"]) ? $this->requestBody["shippingCity"] : "",
                "shippingState"=> isset($this->requestBody["shippingState"]) ? $this->requestBody["shippingState"] : "",
                "shippingZip"=> isset($this->requestBody["shippingZip"]) ? $this->requestBody["shippingZip"] : "",
                "shippingCountry"=> isset($this->requestBody["shippingCountry"]) ? $this->requestBody["shippingCountry"] : ""
            ];

            return $orderRequest;
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }
    
    public function getOrdersProdcutsImage(){
        try{
            $orders = $this->requestBody['orders'];

            if(empty($orders)) throw new \Exception("Invalid order data");

            $imageDetail  = [];
            foreach($orders as $kay=>$val){
                $products = $val['products'];

                $imageDetail[$val['order_id']] = [] ;
                
                foreach($products as $proKey=>$proVal){
                    $temp=$proVal;

                    //
                    $prodcutImage = $this->getProdcutImageFromLocalDB($proVal);
                    //
                    $imageDetail[$val['order_id']][$proVal['product_id']] =empty($prodcutImage) ?  $this->getImageUrl($proVal) : $prodcutImage;
                    // $orders[$kay]['products'][$proKey]['images'] = $this->getImageUrl($proVal);
                }
            }
            return $imageDetail;
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    public function getProductsBundle(){
        try{

            $product_id = isset($this->requestBody['product_id']) ? $this->requestBody['product_id'] : "";

            if(empty($product_id)) throw new \Exception("Please enter valid product-id");

            $request = [
                "product_id" => $product_id,
                "formType" => "product_bundle_view",
                "formMethod" => "POST"
            ];

            return $this->processEndPoint($request);

        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    protected function getProdcutImageFromLocalDB($pram){
        try{
            $product_id  =  isset($pram['product_id']) ? $pram['product_id'] : "";
            $member_portal_createdBy = isset($this->config['configuration']['member_portal_createdBy']) ? $this->config['configuration']['member_portal_createdBy'] : "";
            $member_portal_checkout_id = isset($this->config['configuration']['member_portal_checkout_id']) ? $this->config['configuration']['member_portal_checkout_id'] : "";

            $where = [
                'product_crm_id' => array('$in' => array((int) $product_id, strval($product_id))),
                'product_createdBy' => $member_portal_createdBy
            ];
            
            $prodcut  = $this->dbInstance->products->findOne($where);
            $product_img_url = isset($prodcut['product_img_url']) ? $prodcut['product_img_url'] : "";

            if(empty($product_img_url)) return [];

            return [
                [
                    "alias"=>"product_image",
                    "created_at"=>"",
                    "id"=>$product_id,
                    "path"=>$product_img_url,
                    "updated_at"=>"",
                    "uuid"=>""
                ]
            ];

        }catch(\Exception $ex){
            return [];
        }
    }

    public function getADProducts(){
        try{ 
            
            $this->validateLoginToken(['formType'=>'ad_prodcuts']);
            $prodcutsList = [];
            $adProdcuts = isset($this->config['configuration']['member_portal_ad_products']) ? $this->config['configuration']['member_portal_ad_products'] : [];
            $index = 0;
            if(empty($adProdcuts)) return [];
            
            foreach($adProdcuts as $key=>$val){
                $product  = $this->dbInstance->products->findOne([
                    '_id'=> new \MongoDB\BSON\ObjectID($val)
                ]);
                
                if(!empty($product)){
                    $product['product_images'] = isset($product['product_images']) && !empty($product['product_images']) ? $product['product_images'] : ( isset($prodcut['product_img_url']) && !empty($prodcut['product_img_url']) ? [$prodcut['product_img_url']] :[] ) ; 
                    $prodcutsList[$index++] = $product;
                } 
                
            }
            
            return $prodcutsList;

        }catch(\Exception $ex){
           throw new \Exception($ex->getMessage());
        }
    }

    public function getCatalogProducts(){
        try{ 
            
            $this->validateLoginToken(['formType'=>'ad_prodcuts']);
            $prodcutsList = [];
            $offerProducts = [];
            $shopProdcuts = isset($this->config['configuration']['member_portal_catalog_products']) ? $this->config['configuration']['member_portal_catalog_products'] : [];
            $index = 0;

            if(empty($shopProdcuts)) {
                return [];
            }

            if(isset($this->requestBody["offer_id"])) {
                $request = [
                    "param" => $this->requestBody["offer_id"],
                    "formType" => "offer_view_v2",
                    "formMethod" => "GET"
                ];
    
                $offerDetails = $this->processEndPoint($request);
                
                if(isset($offerDetails['responseData']['data']['products'])){
                    foreach($offerDetails['responseData']['data']['products'] as $product){
                        $offerProducts[] = $product["id"];
                    }
                }
            }
            
            foreach($shopProdcuts as $val){
                $product  = $this->dbInstance->products->findOne([
                    '_id'=> new \MongoDB\BSON\ObjectID($val)
                ]);

                if(
                    (!empty($product) && isset($this->requestBody["offer_id"]) && in_array($product['product_crm_id'],$offerProducts)) ||
                    (!empty($product) && !isset($this->requestBody["offer_id"]))
                ){
                    $product['product_images'] = isset($product['product_images']) && !empty($product['product_images']) ? $product['product_images'] : ( isset($prodcut['product_img_url']) && !empty($prodcut['product_img_url']) ? [$prodcut['product_img_url']] :[] ) ; 
                    $prodcutsList[$index++] = $product;
                }
            }
            
            return $prodcutsList;

        }catch(\Exception $ex){
           throw new \Exception($ex->getMessage());
        }
    }


    public function getNotification(){
        try{
            $customer_id = isset($this->requestBody["customer_id"]) ? $this->requestBody["customer_id"] : "";

            //get all notification
            $_id = isset($this->config["configuration"]['_id']) ? (string) $this->config["configuration"]['_id'] : [];
            $notification = $this->dbInstance->members_portal_notifications->find(
                                    [
                                        "notification_deleted"=>0,
                                        "member_portal_id"=>$_id
                                    ],
                                    [
                                        "sort"=>[
                                                    "notification_createdDtm"=>-1
                                                ]
                                    ])->toArray();
            
            
            //get customer notification
            $userStatus = $this->getUSerNotificationStatus($customer_id);
            $notificationArray = [];

            foreach($notification as $val){

                $_notification_id = isset($val['_id']) ? (string) $val['_id'] : "";
                $user_notification_index = array_search($_notification_id,$userStatus["ids"]);
                $val['read_status'] = $user_notification_index > -1 && isset($userStatus['read_status'][$user_notification_index]) ? $userStatus['read_status'][$user_notification_index] : false;
                $val['delete_status'] = $user_notification_index > -1 && isset($userStatus['delete_status'][$user_notification_index]) ? $userStatus['delete_status'][$user_notification_index] : false;

                if($val['delete_status']!=true){
                    $notificationArray[] = $val;
                }
            
            }

            return $notificationArray;
        }catch(\Exception $ex){
            return [];
        }
    }

    protected function getUSerNotificationStatus($customer_id){
        $statusResponse = [
            "ids" =>[],
            "read_status"=>[],
            "delete_status"=>[],
        ];

        try{

            $notificationStatus = $this->dbInstance->members_portal_notifications_status->find(
                    [
                        "customer_id"=>$customer_id
                    ]
            )->toArray();

            if(!empty($notificationStatus)){
                foreach($notificationStatus as $val){
                    $notification_id = isset($val['notification_id']) ? $val['notification_id'] : "";
                    if(empty($notification_id)) continue;
                    $statusResponse["ids"][] = $notification_id;
                    $statusResponse["read_status"][] = isset($val['read_status']) ? $val['read_status'] : false;
                    $statusResponse["delete_status"][] = isset($val['delete_status']) ? $val['delete_status'] : false;
                }
            }
            
            return $statusResponse; 
                
        }catch(\Exception $ex){
            
            return $statusResponse;
        }
    }

    public function changeNotificationStatus(){
        try{
            $notification_status = isset($this->requestBody["notification_status"]) ? $this->requestBody["notification_status"] : [];
            $customer_id = isset($this->requestBody["customer_id"]) ? $this->requestBody["customer_id"] : "";
            
            

            if(empty($customer_id)) throw new \Exception("Customer-id invalid");

            foreach($notification_status as $key=>$val){
                $notification_id = isset($val['notification_id']) ? $val['notification_id'] :"";
                $read_status = isset($val['read_status']) ? $val['read_status'] :false;
                $delete_status = isset($val['delete_status']) ? $val['delete_status'] :false;
                
                if(empty($notification_id)) continue;

                if($delete_status ==true || $read_status==true){

                    $record = [
                        "notification_id"=>$notification_id,
                        "customer_id" => $customer_id,
                        "read_status" => $read_status,
                        "delete_status" => $delete_status

                    ];
                    //first try to update 

                    $notificationRecord = $this->dbInstance->members_portal_notifications_status->findOne([
                                    "notification_id"=>$notification_id,
                                    "customer_id" => $customer_id
                                ]
                    );

                    $__id = isset($notificationRecord['_id']) ? (string) $notificationRecord['_id'] : "";

                    if(empty($__id)){
                        $this->dbInstance->members_portal_notifications_status->insertOne($record);
                    }else{
                        $this->dbInstance->members_portal_notifications_status->updateOne([
                            "_id" => new \MongoDB\BSON\ObjectId($__id)
                        ],
                        ['$set' => $record]
                        );
                    }
                        
                }
            }

            return ["status"=>true, "message"=>"Request has been processed"];

        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    public function setAccountData($details){
        $order_shipping_addresses=strtolower(trim($details['shipping_street_address']).'_'.trim($details['shipping_street_address2']).'_'.trim($details['shipping_city']).'_'.trim($details['shipping_postcode']).'_'.trim($details['shipping_state']).'_'.trim($details['shipping_country']));

        $order_billing_addresses=strtolower(trim($details['billing_street_address']).'_'.trim($details['billing_street_address2']).'_'.trim($details['billing_city']).'_'.trim($details['billing_postcode']).'_'.trim($details['billing_state']).'_'.trim($details['billing_country']));
        
        $order_payments=strtolower(trim($details['credit_card_number']).'_'.trim($details['cc_number']).'_'.trim($details['cc_expires']).'_'.trim($details['cc_type']));
    
        return [
            'order_shipping_addresses'=>$order_shipping_addresses,
            'order_billing_addresses' => $order_billing_addresses,
            'order_payments' => $order_payments
        ];
    }

    public function checkRecurring($products) {
        foreach($products as $key => $product){
            if($product['is_recurring'] == 1 && $product['on_hold'] == 0) {
                return true;
            }
        }
        return false;
    }

    public function getAccountDetails(){
        try{
            set_time_limit(0);
            $orders_ids=array();
            $order_shipping_addresses=[];
            $shipping_addresses=[];

            $order_billing_addresses=[];
            $billing_addresses=[];

            $order_payments=[];
            $payments=[];

            $customer_email = isset($this->requestBody["customer_email"]) ? $this->requestBody["customer_email"] : "";

            $filters = [
                "campaign_id" => "all",
                "start_date" => date(DATE_FORMAT,strtotime(date(DATE_FORMAT).'-1 year')),
                "end_date" => date(DATE_FORMAT),
                "start_time" => "",
                "end_time" => "",
                "search_type" => "",
                "criteria" => ["email" => $customer_email],
                "return_type" => "customer_view",
                "formType" => "customer_find",
                "formMethod" => "POST"
            ];

            $orders= $this->processEndPoint($filters);
            
            if(isset($orders) && isset($orders['responseData']) && isset($orders['responseData']['data']) && !empty($orders['responseData']['data'])){
                foreach($orders['responseData']['data'] as $orderKey => $orderVal){
                    $orders_ids = array_merge($orders_ids,explode(",",$orderVal['order_list']));
                }

                $filters2['formType'] ='order_view';
                $filters2['formMethod'] = 'POST';
                $filters2['order_id'] = $orders_ids;
            
                $orderDetails = $this->processEndPoint($filters2);
              
                if(isset($orderDetails) && isset($orderDetails['responseData'])){
                    if(count($orders_ids)===1){
                        $is_recurring = $this->checkRecurring($orderDetails['responseData']['products']);

                        if($is_recurring == 1) {
                            $account_details = $this->setAccountData($orderDetails['responseData']);
                            $order_shipping_addresses[] = $account_details['order_shipping_addresses'];
                            $order_billing_addresses[] = $account_details['order_billing_addresses'];
                            $order_payments[] = $account_details['order_payments'];
                        }
                    }
                    else{
                        if(isset($orderDetails['responseData']['data']) && !empty($orderDetails['responseData']['data'])){
                            foreach($orderDetails['responseData']['data'] as $orderKey => $orderVal){
                                $is_recurring = $this->checkRecurring($orderVal['products']);
                                
                                if($is_recurring == 1) {
                                    $account_details = $this->setAccountData($orderVal);
                                    $order_shipping_addresses[] = $account_details['order_shipping_addresses'];
                                    $order_billing_addresses[] = $account_details['order_billing_addresses'];
                                    $order_payments[] = $account_details['order_payments'];
                                }
                            }
                        }
                    }

                    $unique_shipping_addresses = array_unique($order_shipping_addresses);

                    foreach($unique_shipping_addresses as $shippingKey => $shippingVal){
                        $shipping_addresses[]=explode('_',$shippingVal);
                    }

                    $unique_billing_addresses = array_unique($order_billing_addresses);

                    foreach($unique_billing_addresses as $billingKey => $billingVal){
                        $billing_addresses[]=explode('_',$billingVal);
                    }

                    $unique_payments = array_unique($order_payments);

                    foreach($unique_payments as $paymentKey => $paymentVal){
                        $payments[]=explode('_',$paymentVal);
                    }
                }
            }
            
            return ["shipping_addresses"=> $shipping_addresses, "billing_addresses" => $billing_addresses, "payments"=>$payments];
            
        } catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    public function orderPaymentUpdate() {
        try{
            set_time_limit(0);
            $card_response = [];
            $request = [
                "order_id"=>$this->requestBody,
                "formType"=>"order_update",
                "formMethod"=>"POST"
            ];
        
            $response = $this->processEndPoint($request);
           
            if($response['responseData']['response_code'] == 100) {
               return ["status" =>"true", "message" => "Order payment details have been updated successfully"];
            } else {
                foreach($response['responseData']['order_id'] as $key => $value) {
                    in_array($value['cc_number']['response_code'], [100,343]) ? "" : $card_response[] = 'card number';
                    in_array($value['cc_cvv']['response_code'], [100,343])  ? "" : $card_response[] = 'cvv';
                    in_array($value['cc_expiration_date']['response_code'], [100,343])  ? "" : $card_response[] = 'expiry date';
                    in_array($value['cc_payment_type']['response_code'], [100,343])  ? "" : $card_response[] = 'card type';
                    break;
                }
                
                if(empty($card_response)) {
                    return ["status" =>"true", "message" => "Order payments have been updated successfully"];
                } else {
                    return ["status" =>"false", "result" => $card_response, "message" => "Could not update order payment details"];
                }
            }
        } catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }
    
    public function swapSubscriptionProduct(){
        try{
            $this->checkRequestParam(['subscription_id', 'product_id']);
           
            $request = [
                "product_id" => $this->requestBody['product_id'],
                "formType" => "change_recurring_product",
                "formMethod" => "PUT"
            ];

            if(isset($this->requestBody['variant']) && !empty($this->requestBody['variant'])){
                $request['variant'] = $this->requestBody['variant'];
            }

            if(isset($this->requestBody['quantity']) && !empty($this->requestBody['quantity'])){
                $request['quantity'] = $this->requestBody['quantity'];
            }
           
            return $this->processOrderAction($request, $this->requestBody['subscription_id']);

        }catch(\Exception $ex){
           throw new \Exception($ex->getMessage());
        }
    }

}