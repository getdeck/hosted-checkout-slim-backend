<?php 
namespace App\FW\Subscription\Helper;
use App\FW\sdk\Config\Common;
use App\FW\sdk\Config\Curl;

class Config extends Common{
    public $requestBody;
    // public $request;
    public $config;
    
    public function __construct(){
        parent::__construct();
        $this->dbConnect();
        $this->getMemberConfiguration();
        
        // $this->requestBody  = $this->requestProcessBody();
        // $this->config = ['crmConfig'=>[
        //     "checkout_crm_admin"=>"sketchbrains",
        //     "checkout_api_username"=>"sketchbrains_3574",
        //     "checkout_api_password"=>"0bdbb27b03e56c"
        // ]
        // ];
    }

    protected function getMemberConfiguration(){
        try{
                $accessType="portal";
                $memberHashHeader = $this->request->getHeader('Member-Hash');
                $memberHashHeader = isset($memberHashHeader[0]) ? $memberHashHeader[0] : "";
                
                //custom domain
                $host = $this->request->getHeader('Member-Hash');
                $host = isset($host[0]) ? $host[0] : "";


                if(empty($memberHashHeader)) throw new \Exception("Service is not available, Please contact our support");
                //first we will get the configuration for shopify
                $returnconfig = $this->getCheckoutConfigurationForShopify($memberHashHeader);
                
                //for link based 
                if(empty($returnconfig)){
                    $memberConfiguration = $this->dbInstance->members_portals->findOne([
                        'member_portal_login_url_slug'=>(string) $memberHashHeader,
                        'member_portal_deleted'=>0
                    ]);

                    if(empty($memberConfiguration) ){
                        $domain = $this->dbInstance->domains->findOne([
                            'domain_name'=>$host,
                            'domain_deleted'=>0
                        ]);

                        $domain_id = isset($domain['_id']) ? (string) $domain['_id'] : "";
                        $memberConfiguration = $this->dbInstance->members_portals->findOne([
                            'member_domain_id'=> $domain_id,
                            'member_portal_deleted'=>0
                        ]);
                    }

                    
                    
                    $checkout_id = isset($memberConfiguration['member_portal_checkout_id']) ? $memberConfiguration['member_portal_checkout_id'] : '';
                   
                    if(empty($checkout_id)) throw new \Exception("Service is not available, Please contact our support");
                    
                    $checkoutConfiguration = $this->dbInstance->checkouts->findOne([
                        '_id' => new \MongoDB\BSON\ObjectID($checkout_id),
                        'checkout_deleted'=>0
                    ]);
                }else{
                    $checkoutConfiguration = $returnconfig['checkoutConfiguration'];
                    $memberConfiguration = $returnconfig['memberConfiguration'];
                    $accessType="shopify";
                }
                

                $checkout_ll_creds_id = isset($checkoutConfiguration['checkout_ll_creds_id']) ? $checkoutConfiguration['checkout_ll_creds_id'] : "";
                
                if(empty($checkout_ll_creds_id)) throw new \Exception("Service is not available, Please contact our support");
                
                $crmDetail =  $this->dbInstance->store_credentials->findOne([
                    '_id' => new \MongoDB\BSON\ObjectID($checkout_ll_creds_id),
                    'store_credential_deleted' => 0
                ]);
                    
                $checkout_crm_admin = isset($crmDetail['store_name']) ? $crmDetail['store_name'] : "";
                $checkout_api_username = isset($crmDetail['store_api_key']) ? $crmDetail['store_api_key'] : "";
                $checkout_api_password = isset($crmDetail['store_api_password']) ? $crmDetail['store_api_password'] : "";
                
                if(empty($checkout_crm_admin) || empty($checkout_api_username) || empty($checkout_api_password)) throw new \Exception("Service is not available, Please contact our support");
                


                $authHeader = $this->request->getHeader('Auth');
                $authHeaderValue = isset($authHeader[0]) ? $authHeader[0] : "";
                
                //push some checkout basic information
                $memberConfiguration['checkout_config'] = [
                    'checkout_label_url'   =>  isset($checkoutConfiguration['checkout_label_url']) ? $checkoutConfiguration['checkout_label_url'] : "",
                    'checkout_store_type'   =>  isset($checkoutConfiguration['checkout_store_type']) ? $checkoutConfiguration['checkout_store_type'] : "",
                    'checkout_campaign_id'  =>  isset($checkoutConfiguration['checkout_campaign_id']) ? $checkoutConfiguration['checkout_campaign_id'] : "",
                    'checkout_billing_model_id'  =>  isset($checkoutConfiguration['checkout_billing_model_id']) ? $checkoutConfiguration['checkout_billing_model_id'] : "",
                    'checkout_offer_id'  =>  isset($checkoutConfiguration['checkout_offer_id']) ? $checkoutConfiguration['checkout_offer_id'] : "",
                    'shipping_list'     =>  isset($checkoutConfiguration['checkout_crm_shipping_id']) ? $this->getShippingDetails($checkoutConfiguration['checkout_crm_shipping_id']) : [],
                    'checkout_domain_name'     =>  isset($checkoutConfiguration['checkout_domain_name']) ? $checkoutConfiguration['checkout_domain_name'] : "",
                ];

                $this->config = ['crmConfig'=>[
                                                    'checkout_crm_admin' => $checkout_crm_admin,
                                                    "checkout_api_username" => $checkout_api_username,
                                                    "checkout_api_password" => $checkout_api_password
                                            ],
                                 'configuration' => $memberConfiguration,
                                 'auth_token' => $authHeaderValue,
                                 'accessType' => $accessType
                                ];
                
                return $this->config;
        }catch(\Exception $ex){
            
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

     /**
     * Process form input   
     * @param Request $requestFormData
     * 
     * @throws Curl_Exception   
     * @author Sketchbrain TL
     * @return CRM_RESPONSE
     */
    public function processEndPoint($formData = []){
        try{
            
            $this->validateLoginToken($formData);
            
            $curl = new Curl($this->config['crmConfig'],$formData);
            return $curl->process();

        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    protected function validateLoginToken($formData = []){
        try{

            if($this->config['accessType'] == "shopify") return true;

            $exceptionRoute = ['member_login',
                                'member_update',
                                'member_forgot_password',
                                'member_reset_password',
                                'contacts_address',
                                'ad_prodcuts'
                            ];
            $routeName = isset($formData['formType']) ? $formData['formType'] : '';

            if (in_array($routeName,$exceptionRoute, TRUE)) return true;

            $authHeader = $this->request->getHeader('Auth');
            $authHeaderValue = isset($authHeader[0]) ? $authHeader[0] : "";

            if(empty($authHeaderValue)) throw new \Exception("Session has been expired");
                        
            $this->config["auth_token"] = $authHeaderValue;
            $formData = [
                "token" => $authHeaderValue,
                "formType"=>"member_check_session",
                "formMethod"=>"POST"
            ];
            $curl = new Curl($this->config['crmConfig'],$formData);
            return $curl->process();

        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), 401);
        }
    }

    protected function getCheckoutConfigurationForShopify($hash){
        try{
            if(empty($hash)) return [];

            $checkoutConfiguration = $this->dbInstance->checkouts->findOne([
                    'checkout_label_url' => $hash,
                    'checkout_deleted'=>0
                ],
                ['sort' => ['_id' => -1]]    
            );
            
            //find shopify configuration on the basis of custom domain
            if(empty($checkoutConfiguration)){

                $store = $this->dbInstance->store_credentials->findOne([
                    'store_custom_domain' => $hash,
                    'store_credential_deleted'=>0
                    ],
                    ['sort' => ['_id' => -1]]    
                );
                
                $hash = isset($store->store_name) ?$store->store_name : $hash;

                $checkoutConfiguration = $this->dbInstance->checkouts->findOne([
                    'checkout_label_url' => $hash,
                    'checkout_deleted'=>0
                    ],
                    ['sort' => ['_id' => -1]]    
                );
            } 
            
            if(empty($checkoutConfiguration)) return [];
            
            $memberConfiguration = $this->dbInstance->members_portals->findOne([
                                                        'member_portal_checkout_id'=>(string)$checkoutConfiguration->_id,
                                                        // 'member_portal_createdBy' => $checkoutConfiguration->checkout_createdBy,
                                                        'member_portal_deleted'=>0
                                                    ],
                                                    ['sort' => ['_id' => -1]]    
                                                );

            if(empty($memberConfiguration)) return []; 

            return ['checkoutConfiguration' =>$checkoutConfiguration, 'memberConfiguration'=>$memberConfiguration];

        }catch(\Exception $ex){
            return [];
        }
    }

    protected function getShippingDetails($idArray){
        try{

            if(empty($idArray)) return [];

            $shippingArray = [];
            foreach($idArray as $key=>$val){
                $shipping = $this->dbInstance->shipping_methods->findOne([
                                '_id' => new \MongoDB\BSON\ObjectID($val),
                                'shipping_method_deleted' => 0
                            ]);
                
                if(empty($shipping)) continue;

                $shippingArray[] = $shipping;
            }
            
            return $shippingArray;
        }catch(\Exception $ex){
            return [];
        }
    }

}