<?php
namespace App\FW\sdk\Limelight;
use GuzzleHttp\Client as GuzzleClient;
use App\FW\sdk\Config\Curl;

class ProductApi{

    public function __construct($request,$config,$dbInstance){

        $this->requestBody = $request;
        $this->config = $config;
        $this->dbInstance = $dbInstance;
    }

    public function getProductDetail(){
        try{
            $shopify_products = isset($this->requestBody['productId']) ? $this->requestBody['productId'] : [];
            $checkout_createdBy = isset($this->config['crmConfig']['checkout_createdBy']) ? $this->config['crmConfig']['checkout_createdBy'] : "";
            
            if(empty($shopify_products)){
                throw new \Exception("Please provide valid product ids");
            }

            $productArray = [];
            
            foreach($shopify_products as $key=>$val){
                $productDetail = $this->dbInstance->products->findOne([
                                'product_shopify_id'=> (string)$val,
                                'product_createdBy'=> $checkout_createdBy
                                ]);
        
                if(empty($productDetail)){
                    $productArray[] = [
                        "product_id"=>$val,
                        "exception"=>"product not configured"
                    ];
                    continue;
                }

                // get offer Detail
                $offer_id = isset($productDetail['product_offer_id']) ? $productDetail['product_offer_id'] : "";
                $offerDetails = $this->getOfferDetailFromCRM($offer_id);
                if(empty($offerDetails)){
                    $productDetail->offerDetails = ["exception"=>"Invalid offerId has been configured with product"];
                    $productDetail->billingModels = [];
                    $productArray[] = $productDetail;
                    continue;
                }

                $productDetail->billingModels = isset($offerDetails['billing_models']) ? $offerDetails['billing_models'] : [];
                $productDetail->billingModels = $this->calculateBillingDiscount($productDetail->price,$productDetail->billingModels);

                //Remove unwanted keys
                if(isset($offerDetails['created_at'])){
                    unset($offerDetails['created_at']);
                }

                if(isset($offerDetails['updated_at'])){
                    unset($offerDetails['updated_at']);
                }

                if(isset($offerDetails['products'])){
                    unset($offerDetails['products']);
                }

                if(isset($offerDetails['billing_models'])){
                    unset($offerDetails['billing_models']);
                }

                $variants = isset($productDetail['variants'])? (array)$productDetail['variants'] : [];

                if(!empty($variants)){
                    foreach($variants as $vKey=>$vVal){
                        $price = isset($vVal['price']) ? $vVal['price'] : 0.00;
                        $variants[$vKey]['billingModels'] = $this->calculateBillingDiscount($price,$productDetail->billingModels);
                    }
                    $$productDetail->variants = $variants;
                }

                $productDetail->offerDetails = $offerDetails;

                $productArray[] = $productDetail;
            }
            
            return ['products'=>$productArray];
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage());
        }
    }

    protected function getOfferDetailFromCRM($offerId){
        try{
            if(empty($offerId))
            {
                throw new \Exception("Invalid offer id");
            }

            $formData = [
                "formType"=>'offer_view_v2',
                "formMethod" => "GET",
                "param"=>$offerId
            ];

            $curl = new Curl($this->config['crmConfig'], $formData);

            $response = $curl->process();
            return (isset($response['responseData']['data']) ? $response['responseData']['data'] : [] );

            
        }catch(\Exception $ex){
            return [];
        }
    }

    protected function calculateBillingDiscount($price, $billingModels){
        try{

            if(empty($billingModels)){
                return $billingModels;
            }

            foreach($billingModels  as $key=>$val){
                $billingModels[$key]['discountedPrice'] = $price;
                $billingModels[$key]['discountValue'] = 0.00;
                $billingModels[$key]['discountType'] = "";

                if(isset($val['discount']) && empty($val['discount'])){
                    continue;
                }

                if(isset($val['discount']['percent']) && $val['discount']['percent']>0 ){
                    $billingModels[$key]['discountType'] = "percent";
                    $billingModels[$key]['discountValue'] = $val['discount']['percent'];
                    $billingModels[$key]['discountedPrice'] = $price - (($price * $val['discount']['percent']) /100);
                }

                if(isset($val['discount']['amount']) && $val['discount']['amount']>0){
                    $billingModels[$key]['discountType'] = "flat";
                    $billingModels[$key]['discountValue'] = $val['discount']['amount'];
                    $billingModels[$key]['discountedPrice'] = $price - $val['discount']['amount'];
                }

                $billingModels[$key]['discountValue'] = sprintf('%0.2f',$billingModels[$key]['discountValue']);
                $billingModels[$key]['discountedPrice'] = sprintf('%0.2f',$billingModels[$key]['discountedPrice']);
            }

            return $billingModels;
        }catch(\Exception $ex){
            return $billingModels;
        }
    }

}