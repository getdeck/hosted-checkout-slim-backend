<?php
namespace App\FW\sdk\Limelight\Order;
use App\FW\sdk\Config\Config;
use Psr\Http\Message\ServerRequestInterface as Request;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\RequestOptions as RequestOptions;
use App\FW\sdk\Limelight\Order\ShopifyCalculateDiscount;

/**
 * 
 */
class Shopify extends Config
{
	public $config;	

   public $requestBody;
   
   protected $couponDetail;

	public function __construct($config, $requestBody){
		$this->config = $config;
		$this->requestBody = $requestBody;
    }

	public function processOrder()
	{
		try{
         
            $checkout_enable_back_sync_shopify = isset($this->config['crmConfig']['checkout_enable_back_sync_shopify']) ? $this->config['crmConfig']['checkout_enable_back_sync_shopify'] : false;
			if(isset($this->config['crmConfig']['shopifyStore']) && $this->config['crmConfig']['shopifyStore']['store_type'] == 2 && $this->config['crmConfig']['shopifyStore'] && $checkout_enable_back_sync_shopify!=false){

            $this->subscriptionPriceSync();

            $products = [];
	            $orderData = array();
	            $draftOrder = array();
               $line_items = array();
               $discount = array();
               $totalCartPrice = 0.00;
               $removed_items = isset($this->requestBody['removed_items']) && is_array($this->requestBody['removed_items']) ? $this->requestBody['removed_items'] : [];
               foreach($this->requestBody['products'] as $key=>$val){
                  //skip products those are removed by user
                  if(!in_array($key, $removed_items)){
                  $line_items['line_items'][] = array( 
                                                         'title' => $val['product_name'], 
                                                         'price' => sprintf('%0.2f', $val['price']), 
                                                         'quantity' => $val['quantity'],
                                                         'variant_id'=> isset($val['variant_id']) ? $val['variant_id'] : '',
                                                      );
                  $totalCartPrice = $totalCartPrice + sprintf('%0.2f', $val['price']);
               }

               }
               if(isset($this->requestBody['promoCode']) && $this->requestBody['promoCode'] !== ''){
                                $discount['discount_codes'][] = array(
                                                                        "code"=> $this->requestBody['promoCode'],
                                                                        "amount"=> sprintf('%0.2f', $this->requestBody['discount']),
                                                                           "type"=> "fixed_amount"
                                                                     );
               }

               $refrence_discount = $this->getShopifyCouponDiscountDetail($totalCartPrice);
               $refStatus = isset($refrence_discount['status']) ? $refrence_discount['status'] : false;
               $discountAmount = isset($refrence_discount['discount_amount']) ? $refrence_discount['discount_amount'] : 0;
               $discountCode =  isset($refrence_discount['discount_code']) ? $refrence_discount['discount_code'] : "";
               $discountValue = isset($refrence_discount['discount_value']) ? $refrence_discount['discount_value'] : 0;
               $discountType = isset($refrence_discount['discount_type']) ? $refrence_discount['discount_type'] : "";


               if($refStatus && $discountValue>0 && !empty($discountCode) && !empty($discountType)){
                  $discount['discount_codes'][] = array(
                     "code"=> $discountCode,
                     "amount"=> sprintf('%0.2f', $discountValue),
                     "type"=> $discountType
                  );
               }

               $draftOrder = array_merge($line_items,$discount);
               $draftOrder['customer'] = array('email' => $this->requestBody['email']);
               $draftOrder['processing_method'] = "offsite";
               // $draftOrder['phone'] = $this->requestBody["phone"]; 
               //shipping

               if(isset($this->requestBody['shipping_method_name'])){
                  $draftOrder['shipping_lines'] = [
                     [
                        "code"=> "",
                        "price"=> $this->requestBody['shipping_amount'],
                        "discounted_price"=> "0.00",
                        "source"=> "",
                        "title"=> $this->requestBody['shipping_method_name'],
                        "tax_lines"=> [],
                        "carrier_identifier"=> "",
                        "requested_fulfillment_service_id"=> ""
                     ]
                  ];
               }
               


               $billingAddress = [];
               $shippingAddress = [];
               $billingAddress2 = isset($this->requestBody["billingAddress2"]) && $this->requestBody["billingAddress2"] !='' ? ', '.$this->requestBody["billingAddress2"] : '';
               $shippingAddress2 = isset($this->requestBody["address2"]) && $this->requestBody["address2"] !='' ? ', '.$this->requestBody["address2"] : '';               
               if(strtolower($this->requestBody['billingSameAsShipping']) == "yes"){
                  $billingAddress =   array( 
                                                         "first_name" => $this->requestBody["firstName"],
                                                         "last_name" => $this->requestBody["lastName"],
                                                         "address1" => $this->requestBody["address1"].$shippingAddress2,
                                                         "phone" => $this->requestBody["phone"],
                                                         "city" => $this->requestBody["city"],
                                                         "province" => $this->requestBody["state"],
                                                         "country" => $this->requestBody["country"],
                                                         "zip" => $this->requestBody["zip"]
                                                         );
                 $shippingAddress = $billingAddress;
               }else{
                  $billingAddress  =   array( 
                                                         "first_name" => $this->requestBody["billingFirstName"],
                                                         "last_name" => $this->requestBody["billingLastName"],
                                                         "address1" => $this->requestBody["billingAddress1"].$billingAddress2,
                                                         "phone" => $this->requestBody["phone"],
                                                         "city" => $this->requestBody["billingCity"],
                                                         "province" => $this->requestBody["billingState"],
                                                         "country" => $this->requestBody["billingCountry"],
                                                         "zip" => $this->requestBody["billingZip"]
                                                         );                 
                  $shippingAddress =  array( 
                                                         "first_name" => $this->requestBody["firstName"],
                                                         "last_name" => $this->requestBody["lastName"],
                                                         "address1" => $this->requestBody["address1"].$shippingAddress2,
                                                         "phone" => $this->requestBody["phone"],
                                                         "city" => $this->requestBody["city"],
                                                         "province" => $this->requestBody["state"],
                                                         "country" => $this->requestBody["country"],
                                                         "zip" => $this->requestBody["zip"]
                                                         );
               }
               $draftOrder["billing_address"] =  $billingAddress;
               $draftOrder["shipping_address"] = $shippingAddress;
               $is_recurring = isset($this->requestBody['is_recurring']) ? $this->requestBody['is_recurring'] : false;
               
               if($is_recurring == true){
                  $draftOrder['tags'] = "Subscription";
               }

               $draftOrder['note_attributes'] = array( "name" => "Sticky.io Order Id" , "value" => $this->requestBody['orderId']);   
               

               if(isset($this->requestBody['tax']) && $this->requestBody['tax'] != ""){
                  $draftOrder['total_tax'] = sprintf('%0.2f',$this->requestBody['tax']);

                  //tax_lines
                  $draftOrder['taxable'] = true;
                  $draftOrder['tax_lines'] = [
                                       [
                                       "title" => "Sales Tax",
                                       "price" => $draftOrder['total_tax'],  
                                       "rate" => isset($this->requestBody['tax_pct']) ? ($this->requestBody['tax_pct'] / 100) : ""
                                       ]
                                    ];
               }
             
               $draftOrder["processing_method"] ="offsite";
               $draftOrder["inventory_behaviour"] ="decrement_ignoring_policy";
               $draftOrder['send_receipt'] = true;
               
               $orderData['order'] = $draftOrder;
  
               
	            $client = new GuzzleClient([ "base_uri" => "https://".$this->config['crmConfig']['shopifyStore']['store_api_key'].":".$this->config['crmConfig']['shopifyStore']['store_api_password']."@".$this->config['crmConfig']['shopifyStore']['store_name']."/admin/api/"]);

	            $guzzleResponse = $client->request("post", "2020-01/orders.json", [RequestOptions::JSON => $orderData]);

	            $statusCode = $guzzleResponse->getStatusCode();

	            if($statusCode === 201 || $statusCode === 200){
	            	$resp = json_decode($guzzleResponse->getBody()->getContents(),true);
	            }else{
	            	throw new \Exception("Invalid order request");
               }

               //Need to create a externial transaction that will show on order total
               $transaction = $this->processTransaction($resp);
               $resp['transaction'] = $transaction;
               $resp['shopify_request'] = $orderData;
	            return $resp;
			}else{
				return false;
			}
		
        }catch(\Exception $ex){
            return ['status'=>false,'message'=>$ex->getMessage()];
            //throw new \Exception($ex->getMessage());
        }
   }

   protected function getShopifyCouponDiscountDetail($totalCartPrice){
      try{
         $return = [
                  "status"=>false,
                  "error"=>"", 
                  "refrence"=>"shopify",
                  "discount_code"=>"",
                  "discount_on"=>"", 
                  "discount_type"=>"",
                  "discount_amount"=>0,
                  "discount_value"=>0
            ];
          
            if($this->config['crmConfig']['checkout_store_type']!=2)  throw new \Exception("Discount not applicable");
            
            $refrence_discount = isset($this->config['crmConfig']['refrence_discount']) ? $this->config['crmConfig']['refrence_discount'] : [];
            $status  = isset($refrence_discount['status']) ? $refrence_discount['status'] : false;
            $discount_type = isset($refrence_discount['discount_type']) ? $refrence_discount['discount_type'] : "";
            $discount_value = isset($refrence_discount['discount_value']) ? $refrence_discount['discount_value'] : 0;
            $return['discount_type'] = $discount_type;

            if($discount_value==0 || $discount_value<0)  throw new \Exception("Discount not applicable");;
                
                
            $calculatedDiscount = 0;
            if($discount_type == 'fixed_amount'){
               $tempDiscount = $discount_value - $totalCartPrice;
               $calculatedDiscount = $tempDiscount<0 ? $totalCartPrice : $tempDiscount; 
               $return['discount_amount'] = $calculatedDiscount;
               $return['status'] = true;
               $return['discount_code'] = $refrence_discount['discount_code'];
               $return['discount_value'] = $calculatedDiscount;
            }
            
            if($discount_type == 'percentage'){
               $calculatedDiscount = ($totalCartPrice * $discount_value) / 100;
               $return['discount_amount'] = $calculatedDiscount;
               $return['status'] = true;
               $return['discount_code'] = $refrence_discount['discount_code'];
               $return['discount_value'] = $discount_value;
            }

            return $return;
   
      }catch(\Exception $ex){
         return [
            "status"=>false,
            "error"=>$ex->getMessage(), 
            "refrence"=>"shopify",
            "discount_code"=>"",
            "discount_on"=>"", 
            "discount_type"=>"",
            "discount_amount"=>0
        ];
      }
   }
   
   public function getCouponDiscouunt(){
      try{
         //ref_coupon
         //coupon_code

         // $this->config['crmConfig']['shopifyStore']['store_type'] = 2;
         // $this->config['crmConfig']['coupon_code']="38DN4CVVH0DE";
         // $this->config['crmConfig']['shopifyStore']['store_api_key'] = "f966077d3d25fe6065cf655218f9e6af";
         // $this->config['crmConfig']['shopifyStore']['store_api_password'] = "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";
         // $this->config['crmConfig']['shopifyStore']['store_name'] = "abc123-t1.myshopify.com";

         $store_type = isset($this->config['crmConfig']['shopifyStore']['store_type']) ? $this->config['crmConfig']['shopifyStore']['store_type'] : "";

         if($store_type!=2) throw new \Exception("Not a shopify store");

         $coupon = isset($this->config['crmConfig']['coupon_code_e']) ? $this->config['crmConfig']['coupon_code_e'] : "";
         $coupon = empty($coupon) && isset($this->config['crmConfig']['ref_coupon']) ? $this->config['crmConfig']['ref_coupon'] : $coupon;

         if(empty($coupon)) throw new \Exception("Invalid coupon");

         //get the coupon detail first
         
         $client = new GuzzleClient([ "base_uri" => "https://".$this->config['crmConfig']['shopifyStore']['store_api_key'].":".$this->config['crmConfig']['shopifyStore']['store_api_password']."@".$this->config['crmConfig']['shopifyStore']['store_name']."/admin/api/"]);
         
         $guzzleResponse = $client->request("get", "2020-07/price_rules.json");
         $statusCode = $guzzleResponse->getStatusCode();
         $priceDetail = $statusCode === 201 || $statusCode === 200 ?  json_decode($guzzleResponse->getBody()->getContents(),true) : [];

         $priceRuleArray = isset($priceDetail['price_rules']) ? $priceDetail['price_rules'] : [];
         
         foreach($priceRuleArray as $key=>$val){
            if($val['title'] == $coupon){
               $this->priceDetail = $val;
            break;
            }
         }

         if(empty($this->priceDetail)) throw new \Exception("Get invalid price rule");
         //now get the coupon detail

         $guzzleResponse = $client->request("get", "2020-07/price_rules/".$this->priceDetail['id']."/discount_codes.json");
         $statusCode = $guzzleResponse->getStatusCode();
         $couponDetail = $statusCode === 201 || $statusCode === 200 ?  json_decode($guzzleResponse->getBody()->getContents(),true) : [];

         $discountCodesArray = isset($couponDetail['discount_codes']) ? $couponDetail['discount_codes'] : [];

         foreach($discountCodesArray as $key=>$val){
            if($val['code'] == $coupon){
               $this->couponDetail = $val;
            break;
            }
         }

         
         $instance = new ShopifyCalculateDiscount($this->couponDetail,$this->priceDetail);
         return $instance->calculateDiscount();

      }catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
      }
   }

   public function validateCustomer(){
      try{
         $email = isset($this->requestBody['email']) ? $this->requestBody['email'] : "";
         $store_api_key = isset($this->config['crmConfig']['shopifyStore']['store_api_key']) ? $this->config['crmConfig']['shopifyStore']['store_api_key'] : "";
         $store_api_password = isset($this->config['crmConfig']['shopifyStore']['store_api_password']) ? $this->config['crmConfig']['shopifyStore']['store_api_password']:"";
         $store_name = isset($this->config['crmConfig']['shopifyStore']['store_name']) ?$this->config['crmConfig']['shopifyStore']['store_name']:"";

         if(empty($email)){
            throw new \Exception("Invalid customer email");
         }

         if(empty($store_api_key) || empty($store_api_password) || empty($store_name)){
            throw new \Exception("Invalid shopify store credential has been configured");
         }

         $endpoint = "https://".$store_api_key.":".$store_api_password."@".$store_name."/admin/api/";

         $client = new GuzzleClient([ "base_uri" => $endpoint]);

         $guzzleResponse = $client->request("get", "2020-01/customers/search.json?query=email:".$email);

         $statusCode = $guzzleResponse->getStatusCode();

         if($statusCode === 201 || $statusCode === 200){
            $resp = json_decode($guzzleResponse->getBody()->getContents(),true);
         }else{
            throw new \Exception("We are not able to search customer profile");
         }
         
         $customer = isset($resp['customers']) ? $resp['customers'] : [];
         $profile_found = false;
         foreach($customer as $key=>$val){
            $state = isset($val['state']) ?$val['state'] : "disabled";

            if(isset($val['email']) && $val['email'] == $email && $state!="disabled"){
               $profile_found = true;
            break;
            }
         }

         if($profile_found== false){
            throw new \Exception("We are not able to search customer profile");
         }
         
         return ['status'=>true, "message"=>"Customer profile has been found"];

      }catch(\Exception $ex){
         throw new \Exception($ex->getMessage());
      }
   }

   public function createShopifyCustomer(){
      try{

         $email = isset($this->requestBody['email']) ? $this->requestBody['email'] : "";
         $password = isset($this->requestBody['password']) ? $this->requestBody['password'] : "";

         $store_api_key = isset($this->config['crmConfig']['shopifyStore']['store_api_key']) ? $this->config['crmConfig']['shopifyStore']['store_api_key'] : "";
         $store_api_password = isset($this->config['crmConfig']['shopifyStore']['store_api_password']) ? $this->config['crmConfig']['shopifyStore']['store_api_password']:"";
         $store_name = isset($this->config['crmConfig']['shopifyStore']['store_name']) ?$this->config['crmConfig']['shopifyStore']['store_name']:"";

         if(empty($email)){
            throw new \Exception("Invalid customer email");
         }

         if(empty($password)){
            throw new \Exception("please provide valid password");
         }

         if(empty($store_api_key) || empty($store_api_password) || empty($store_name)){
            throw new \Exception("Invalid shopify store credential has been configured");
         }
         $customer = [];

         $customer["customer"] = [
                                 "first_name"=> isset($this->requestBody['firstName']) ? $this->requestBody['firstName'] : "",
                                 "last_name"=> isset($this->requestBody['lastName']) ?$this->requestBody['lastName'] : "",
                                 "email"=> isset($this->requestBody['email']) ?$this->requestBody['email'] : "",
                                 // "phone"=> isset($this->requestBody['phone']) ?$this->requestBody['phone'] : "",
                                 "verified_email"=> true,
                                 "addresses"=> [
                                    [
                                       "address1"=> isset($this->requestBody['address1']) ?$this->requestBody['address1'] : "",
                                       "city"=> isset($this->requestBody['city']) ?$this->requestBody['city'] : "",
                                       "province"=> isset($this->requestBody['state']) ?$this->requestBody['state'] : "",
                                       // "phone"=> isset($this->requestBody['phone']) ?$this->requestBody['phone'] : "",
                                       "zip"=> isset($this->requestBody['zip']) ?$this->requestBody['zip'] : "",
                                       "last_name"=> isset($this->requestBody['lastName']) ?$this->requestBody['lastName'] : "",
                                       "first_name"=> isset($this->requestBody['firstName']) ?$this->requestBody['firstName'] : "",
                                       "country"=> isset($this->requestBody['country']) ?$this->requestBody['country'] : "",
                                    ]
                                 ],
                                 "password"=> isset($this->requestBody['password']) ?$this->requestBody['password'] : "",
                                 "password_confirmation"=> isset($this->requestBody['password']) ?$this->requestBody['password'] : "",
                                 "send_email_welcome"=> true
         ];

         $endpoint = "https://".$store_api_key.":".$store_api_password."@".$store_name."/admin/api/";

         $client = new GuzzleClient([ "base_uri" => $endpoint]);

         $guzzleResponse = $client->request("post", "2020-10/customers.json",[RequestOptions::JSON => $customer]);

         $statusCode = $guzzleResponse->getStatusCode();
         $resp = [];
         if($statusCode === 201 || $statusCode === 200){
            $resp = json_decode($guzzleResponse->getBody()->getContents(),true);
         }else{
            throw new \Exception("We are not able to search customer profile");
         }
         
         return ['status'=>true, "message"=>"Customer profile has created", "customer"=>$resp];


      }catch(\Exception $ex){
         throw new \Exception($ex->getMessage());
      }
   }

   protected function subscriptionPriceSync(){
      try{
         //product 
         $products = isset($this->requestBody['products']) ? $this->requestBody['products'] : [];
         $order_success_products = isset($this->requestBody['order_success_products']) ? $this->requestBody['order_success_products'] : [];
         $price_sync = isset($this->requestBody['price_sync']) ? $this->requestBody['price_sync'] : false;

         if($price_sync!=true || empty($products) || empty($order_success_products)){
            return false;
         }

         //for one click upsell
         $oneClickUpsells = isset($this->requestBody['oneClickUpsells']) ? $this->requestBody['oneClickUpsells'] : [];
         if(!empty($oneClickUpsells) && !empty($products)){
            $this->requestBody['products'] = array_merge($products, $oneClickUpsells);
         }

         foreach($order_success_products as $key=>$val){
            $product_id = isset($val['id']) ? $val['id'] : "";
            $product_ids = array_map('trim' ,explode('-',$product_id));

            

            if(empty($product_id)) continue;

            foreach($this->requestBody['products'] as $inKey=>$inVal){
               $product_crm_id = isset($inVal['product_crm_id']) ? $inVal['product_crm_id'] : "";
               $variant_product_crm_id = isset($inVal['variant_product_crm_id']) ? $inVal['variant_product_crm_id'] : "";
               $product_crm_id = !empty($variant_product_crm_id) ? $variant_product_crm_id : $product_crm_id;

               if(in_array($product_crm_id,$product_ids)){

                  $this->requestBody['products'][$inKey]['price'] = $val['unit_price'];
               }
            }
         }

         return true;
      }catch(\Exception $ex){
         return false;
      }
   }

   protected function processTransaction($orderResponse){
      try{
         $totalPrice = isset($orderResponse['order']['total_price']) ? sprintf('%0.2f',$orderResponse['order']['total_price']) : 0.00;
         $id = isset($orderResponse['order']['id']) ? $orderResponse['order']['id'] : "";

         if(empty($id)) return false;

         $transaction = [
            "transaction"=>[
                  "kind"=> "capture",
                  "status"=> "success",
                  // "currency": "USD",
                  "gateway"=> "manual",
                  "amount"=> $totalPrice,
                  "source"=> "external"
            ]
            ];

         $client = new GuzzleClient([ "base_uri" => "https://".$this->config['crmConfig']['shopifyStore']['store_api_key'].":".$this->config['crmConfig']['shopifyStore']['store_api_password']."@".$this->config['crmConfig']['shopifyStore']['store_name']."/admin/api/"]);

         $guzzleResponse = $client->request("post", "2020-07/orders/".$id."/transactions.json", [RequestOptions::JSON => $transaction]);

         $statusCode = $guzzleResponse->getStatusCode();
         $resp = [];
         if($statusCode === 201 || $statusCode === 200){
            $resp = json_decode($guzzleResponse->getBody()->getContents(),true);
         }else{
            throw new \Exception("Invalid order request");
         }
         return $resp;
      }catch(\Exception $ex){
         return false;
      }
   }

}
