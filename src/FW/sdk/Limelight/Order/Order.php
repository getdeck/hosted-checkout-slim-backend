<?php
namespace App\FW\sdk\Limelight\Order;
use App\FW\sdk\Config\Config;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * LimeLight prospect managemnt  
 * Desc: Prospect extended with common config 
 * @copyright  2019 SketchBrain 
 * @license    SketchBrain   FW SDK License 1.0.0
 * @version    Release: V 1.0.0
 * @link       N/A
 * @since      Class available since Release 1.0.0
 */

class Order extends Config{
    // public $request;
    protected $response;
    public function __construct(Request $request){
        $this->request = $request;

        // $this->requestBody = $this.requestProcessBody($this->request); // json_decode($this->request->getBody()->getContents(),true);

        // print_r($this->requestBody);
        // die;
        parent::__construct();
        // $this->request->getBody()->rewind(true); 
    }

     /**
     * Create affiliate prospect 
     *
     * @param prospectData[]
     * 
     * @throws Exception If something interesting cannot happen
     * @author SketchBrain <sketchBrain>
     */
    public function createWithOrder(){
        try{
             $requestBody = $this->requestBody;
             $reqCustomFields =  isset($requestBody['custom_fields']) && count($requestBody['custom_fields']) > 0 ? $requestBody['custom_fields'] : [];

            // conditions 
            // 1. No Change
            // => user click pay now directly without changing anything just select existing shipping method and payment
            // => this case get last order id of that payment method 
            // use that order id and place order with new_order_card_on_file api call
            
            // 2. Payment method change => change billing adress too in this case
            // if shipping not change just pre fill form shipping details with selecte shipping address
            // pre fill billing address with selected billing address
            // user can change shipping/billing use the updated adresses to do normal checkout
            // => in this case we have prefilled shipping and billing address use those create prospect and place normal order with new_order_with_prospect api call
            
            // 3. Shipping information change => kept payment same
            // => in this case we will place order with new_order_card_on_file using same logic of using last order id of selected payment method
            // => after order placed get that order id and use api order_update to update repective extra/changed shiping details user added

            // 3. Shipping information change => payment change
            // => as payment method change we will treat this as normal order
            // pre fill shipping data to form + extra details added by user
            // billing details
            // place order with new_order_with_prospect

            //  1. blacklisted/fake payments/orders can not be used
            //  2. only user with email logged in, his previous order ids can be used when placing order with exsiting card details

            // 4. Billing information change
            // so such option to change billing address
          
            //create array or all order ids of same payment methods

            // save token into notes as well
          
            $token_user_data = isset($this->requestBody['token_user_data']) ? $this->requestBody['token_user_data'] : null;
            $token_user_email = $token_user_data && isset($this->requestBody['token_user_data']['email']) ? $this->requestBody['token_user_data']['email'] : null;

           
            // check if its a token based user + a new payment + previous order is is passed then only do card on file
            $token_based_checkout = $token_user_email && $requestBody['previousOrderId'] ? true : false;
         
            // if previousOrderId pass and is a token based checkout check 
            // if the previous order id was really linked with current email or not
            if($token_based_checkout && isset($requestBody['previousOrderId'])){
                $token_user_order_ids = $this->getCustomerOrderIds($token_user_email);
                if(is_array($token_user_order_ids) && count($token_user_order_ids) > 0){}else{
                    $token_user_order_ids = array($token_user_order_ids);
                }
                if(!in_array($requestBody['previousOrderId'], $token_user_order_ids)){
                    throw new \Exception("Existing Order not found for your email");
                }
                // for card on new file call we have to send shipping addresses manually as it not takes prospectid
                $requestBody['shippingAddress1']        = $requestBody['address1'];
                $requestBody['shippingAddress2']        = $requestBody['address2'];
                $requestBody['shippingZip']             = $requestBody['zip'];
                $requestBody['shippingCity']            = $requestBody['city'];
                $requestBody['shippingState']           = $requestBody['state'];
                $requestBody['shippingCountry']         = $requestBody['country'];
                $requestBody['billingSameAsShipping']   = 'NO';
                $requestBody['notes']                   = "Order placed using token based user using token ".$this->requestBody['token']." and email ".$this->requestBody['token_user_data']['email'];
            }

            // $requestBody = $this->request->getParsedBody();
            $requestBody['ipAddress']   = $this->getUserAgentIP($this->request);
            $requestBody['formType']    = $token_based_checkout ? 'new_order_with_user' : 'create_prospect_with_order';
            $requestBody['formMethod']  = 'POST';

            
            //Get the All detail from config file for processing the data
            $affiliateDetail = $this->getAffiliateDetail($requestBody);
            if(isset($affiliateDetail['offers']) && empty($affiliateDetail['offers'])){
                throw new \Exception("Invalid offer or product selection, Please contact our support");
            }
            
            $requestBody =  array_merge($requestBody, $affiliateDetail);
            $validation  = $this->validateFormInput($requestBody);
            $requestBody = $this->validateRequestAsperCRMconfig($requestBody);
            $response = $this->processEndPoint($requestBody);


            if($response['responseData']['order_id']){
             //check if user pass custom fields then update custom fields
                // form name is in format: custom_fields[51-13-1] => crm_field_type_id - link with(order/product/contact) - type id(text,int,date,enum,etc.)
                // link with: order => 2, product => 3, contact/prospect => 13
                // data type ids: Text => 1, Numeric => 2, Date => 3, Boolean => 4, Enumeration => 5 
                if(isset($reqCustomFields) && count($reqCustomFields) > 0){
                    $cust_fields = [];
                    foreach($reqCustomFields as $cust_key => $cust_value){
                        $extracted_data = explode('-', $cust_key);
                        if($extracted_data[1] == 2){
                            // for order
                            $cust_fields[] = array('id' => $extracted_data[0], 'value' => $cust_value);
                        }
                    }
                    if(count($cust_fields) > 0){
                        $body['order_id'] = $response['responseData']['order_id'];
                        $body['custom_fields'] = $cust_fields;
                        $this->updateOrderCustomFieldValue($body);
                    }  
                 }

                 // add custom fields to customers now, as once order placed prospect got deleted and a customer created
                 if(isset($response['responseData']['customerId'])){
                    if(isset($reqCustomFields) && count($reqCustomFields) > 0){
                        $cust_fields = [];
                        foreach($reqCustomFields as $cust_key => $cust_value){
                            $extracted_data = explode('-', $cust_key);
                            if($extracted_data[0] == 4 && $cust_value){
                                    $cust_value = true;
                            }else if($extracted_data[0] == 4 && !$cust_value){
                                    $cust_value = false;
                            }
                            if($extracted_data[1] == 13){
                                // for prospect
                                $cust_fields[] = array('id' => $extracted_data[0], 'value' => $cust_value);
                            }
                        }
                        if(count($cust_fields) > 0){
                            $body['customer_id'] = $response['responseData']['customerId'];
                            $body['custom_fields'] = $cust_fields;
                            $this->updateCustomerCustomFieldValue($body);
                        }  
                     }
                 }
            }

            $this->requestBody['orderId'] = $response['responseData']['order_id'];
            $this->requestBody['order_id'] = $response['responseData']['order_id'];
            $this->requestBody['tax'] = isset($this->requestBody['tax']) ? $this->requestBody['tax'] : $response['responseData']['orderSalesTaxAmount'];
            $this->requestBody['tax_pct'] = isset($this->requestBody['tax_pct']) ? $this->requestBody['tax_pct'] : $response['responseData']['orderSalesTaxPercent'];
            $this->requestBody['customerId'] = isset($response['responseData']['customerId']) ? $response['responseData']['customerId'] : '';
            $this->requestBody['price_sync'] = true;
            $orderDetail = $this->getOrderDetail();
            $this->requestBody['order_success_products'] = isset($orderDetail['order_total']['line_items']) ? $orderDetail['order_total']['line_items'] : [];
            $this->requestBody['shipping_method_name'] = isset($orderDetail['responseData']['shipping_method_name']) ? $orderDetail['responseData']['shipping_method_name'] : "";
            $this->requestBody['shipping_amount'] = isset($orderDetail['responseData']['shipping_amount']) ? $orderDetail['responseData']['shipping_amount'] : 0;
            
            $shopifyOrder = $this->processShopifyOrder();
            $response["shopifyOrder"] = ($shopifyOrder) ? $shopifyOrder : false;
            $response['requestBody'] =$requestBody;
            return $response;
        }catch(\Exception $ex){
            $this->pushErrorLog($ex);
            throw new \Exception($ex->getMessage());
        }
    }
    function updateOrderCustomFieldValue($body){
        try{
            if(!isset($body['order_id'])){
                throw new \Exception('order_id not found for updateOrderCustomFieldValue call');
            }
            $requestData = [
                "param"=> $body['order_id'],
                "formType"=>'update_order_custom_field_value',
                "formMethod"=>"POST",
                "custom_fields" => $body['custom_fields'],
            ];
            $returnData = $this->processEndPoint($requestData);
            $status =  isset($returnData['responseData']['status']) && $returnData['responseData']['status'] == 'SUCCESS' ? true : false;
            return $status;
            
        }catch(\Exception $ex){
            $this->pushErrorLog($ex);
        }
    }

    // add values to the custom fields for prospect/contact, which are already created using hco admin/crm
    function updateCustomerCustomFieldValue($body){
        try{
            if(!isset($body['customer_id'])){
                throw new \Exception('customer_id not found for updateCustomerCustomFieldValue call');
            }
            $requestData = [
                "param"=> $body['customer_id'],
                "formType"=>'update_customer_custom_field_value',
                "formMethod"=>"POST",
                "custom_fields" => $body['custom_fields'],
            ];
            $returnData = $this->processEndPoint($requestData);
            $status =  isset($returnData['responseData']['status']) && $returnData['responseData']['status'] == 'SUCCESS' ? true : false;
            return $status;

        }catch(\Exception $ex){
            $this->pushErrorLog($ex);
        }
    }

    /**
     * Process Affiliate upsell data 
     *
     * @param prospectData[]
     * 
     * @throws Exception If something interesting cannot happen
     * @author SketchBrain <sketchBrain>
     */
    public function processUpsell(){
        try{

            $requestBody = $this->requestBody;
            // $requestBody = $this->request->getParsedBody();
            $requestBody['ipAddress'] = $this->getUserAgentIP($this->request);

            $requestBody['formType'] ='create_upsell_order';
            $requestBody['formMethod'] = 'POST';
            $affiliateDetail = $this->getAffiliateDetail($requestBody);
          
            $param =  array_merge($requestBody, $affiliateDetail); 
            $validation  = $this->validateFormInput($param);
            $response = $this->processEndPoint($param);

            // get upsell order id and place order into shopify
            // here shipping price also sending for upsell order, need to remove that from crm
            if($response && isset($response['responseData']['order_id'])){
                $this->requestBody['order_id']  = $response['responseData']['order_id'];
                $shopifyOrder                   = $this->orderPostBack();
                $response["shopifyOrder"]       = ($shopifyOrder) ? $shopifyOrder : false;
            }

            return $response;


        }catch(\Exception $ex){
            $this->pushErrorLog($ex);
            throw new \Exception($ex->getMessage());
        }
    }

    //onepage_checkout


    public function fullPageCheckout($param = []){
        try{
            $requestBody = $this->requestBody;
            $requestBody['ipAddress'] = $this->getUserAgentIP($this->request);


            $requestBody['formType'] ='onepage_checkout';
            $requestBody['formMethod'] = 'POST';
            $affiliateDetail = $this->getAffiliateDetail($requestBody);
            $param =  array_merge($requestBody, $affiliateDetail); 
            $param['AFID'] = isset($param['AFID']) && !empty($param['AFID']) ? $param['AFID'] : 'AFID';
            $validation  = $this->validateFormInput($param);
            $param = $this->validateRequestAsperCRMconfig($param);
        
            $response = $this->processEndPoint($param);

            $this->requestBody['orderId'] = $response['responseData']['order_id'];
            $this->requestBody['order_id'] = $response['responseData']['order_id'];
            $this->requestBody['tax'] = isset($this->requestBody['tax']) ? $this->requestBody['tax'] : $response['responseData']['orderSalesTaxAmount'];
            $this->requestBody['tax_pct'] = isset($this->requestBody['tax_pct']) ? $this->requestBody['tax_pct'] : $response['responseData']['orderSalesTaxPercent'];
            $this->requestBody['price_sync'] = true;
            $orderDetail = $this->getOrderDetail();
            $this->requestBody['order_success_products'] = isset($orderDetail['order_total']['line_items']) ? $orderDetail['order_total']['line_items'] : [];
            $this->requestBody['shipping_method_name'] = isset($orderDetail['responseData']['shipping_method_name']) ? $orderDetail['responseData']['shipping_method_name'] : "";
            $this->requestBody['shipping_amount'] = isset($orderDetail['responseData']['shipping_amount']) ? $orderDetail['responseData']['shipping_amount'] : 0;
            
            $this->requestBody['customerId'] = isset($response['responseData']['customerId']) ? $response['responseData']['customerId'] : '';
            $shopifyOrder = $this->processShopifyOrder();

            $response["shopifyOrder"] = ($shopifyOrder) ? $shopifyOrder : false;
            return $response;
        }catch(\Exception $ex){
            $this->pushErrorLog($ex);
            throw new \Exception($ex->getMessage());
        }
    }


    //
    public function applyCoupon($param = []){
        try{
            $param = $this->requestBody;
            $param['formType'] ='coupon_validate';
            $param['formMethod'] = 'POST';
            $affiliateDetail = $this->getAffiliateDetail($param);
            $param =  array_merge($param, $affiliateDetail); 

            if(empty($param['products'])){
                throw new \Exception("Invalid product selection");
            }
            
            if(empty($param['shippingId'])){
                throw new \Exception("Please select a valid shipping method"); 
            }
            
            return $response = $this->processEndPoint($param);
        }catch(\Exception $ex){
            $this->pushErrorLog($ex);
            throw new \Exception($ex->getMessage());
        }
    }

    public function processShopifyOrderPaypal()
    {
        try{
            $shopifyOrder = $this->processShopifyOrder();

            $response["shopifyOrder"] = ($shopifyOrder) ? $shopifyOrder : false;
            return $response;
        }catch(\Exception $ex){
            $this->pushErrorLog($ex);
            throw new \Exception($ex->getMessage());
        }
    }

    protected function validateRequestAsperCRMconfig($requestData){
        try{

            //validate for 3dVerify
            $checkout_enable_3d_verify = isset($this->config['crmConfig']['checkout_enable_3d_verify']) ? $this->config['crmConfig']['checkout_enable_3d_verify'] : false;
            $checkout_enable_kount = isset($this->config['crmConfig']['checkout_enable_kount']) ? $this->config['crmConfig']['checkout_enable_kount'] : false;

            if($checkout_enable_3d_verify!=true){

                if(isset($requestData['3d_version'])){
                    unset($requestData['3d_version']);
                }

                if(isset($requestData['ds_trans_id'])){
                    unset($requestData['ds_trans_id']);
                }

                if(isset($requestData['acs_trans_id'])){
                    unset($requestData['acs_trans_id']);
                }
                //3d_version
                //ds_trans_id
                //acs_trans_id
            }

            //sessionID
            if($checkout_enable_kount!=true){
                if(isset($requestData['sessionID'])){
                    unset($requestData['sessionID']);
                }
            }
            
            return $requestData;
        }catch(\Exception $ex){
            return $requestData;
        }
    }

    public function orderDetail(){
        try{
            $param = $this->requestBody;
            $order_id = isset($param['order_id']) ? $param['order_id'] : '';
            
            if(empty($order_id)){
                throw new \Exception("Invalid order ID"); 
            }
            $request['formType'] ='order_view';
            $request['formMethod'] = 'POST';
            $request['order_id'] = [$order_id];
            
            return $response = $this->processEndPoint($request);
        }catch(\Exception $ex){
            $this->pushErrorLog($ex);
            throw new \Exception($ex->getMessage());
        }
    }

    public function orderPostBack(){
        try{
            $param = $this->requestBody;
            $order_id = isset($param['order_id']) ?$param['order_id'] : "";

            $orderDetail = $this->orderDetail();
            $responseData = isset($orderDetail['responseData']) ? $orderDetail['responseData'] : [];
            $checkoutCreatedBy = isset($this->config['crmConfig']['checkout_createdBy']) ? $this->config['crmConfig']['checkout_createdBy'] : "";
            
            if(empty($responseData)) throw new \Exception("Order is not available");
            
            $keyArray = [
               
                'billing_first_name'=>"billingFirstName",
                'billing_last_name'=>"billingLastName",
                'billing_street_address'=>"billingAddress1",
                'customers_telephone'=>"phone",
                'billing_city'=>"billingCity",
                'billing_state'=>"billingState",
                'billing_country'=>"billingCountry",
                'billing_postcode'=>"billingPostcode",
                'billing_postcode'=>"billingZip",
                
                'shipping_first_name'=>"firstName",
                'shipping_last_name'=>"lastName",
                'shipping_street_address'=>"address1",
                'shipping_city'=>"city",
                'shipping_state'=>"state",
                'shipping_country'=>"country",
                'shipping_postcode'=>"zip",
                "email_address"=>"email",
                "order_id"=>"orderId",
                "order_sales_tax_amount"=>"tax"
            ];

            foreach($keyArray as $key=>$val){
                if(!isset($responseData[$key])) continue;

                $responseData[$val] =  $responseData[$key];
            }

            $this->requestBody['products'] = [];


            $this->requestBody['promoCode'] = isset($responseData['promo_code']) ? $responseData['promo_code'] : "";
            $this->requestBody['discount'] = isset($responseData['coupon_discount_amount']) ? $responseData['coupon_discount_amount'] : 0.00;
            $this->requestBody['shipping_method_name'] = isset($responseData['shipping_method_name']) ? $responseData['shipping_method_name'] : "";
            $this->requestBody['shipping_amount'] = isset($responseData['shipping_amount']) ? $responseData['shipping_amount'] : 0;
            $this->requestBody['tax'] = isset($responseData['order_sales_tax_amount']) ? $responseData['order_sales_tax_amount'] : 0;
            $this->requestBody['tax_pct'] = isset($responseData['order_sales_tax']) ? $responseData['order_sales_tax'] : 0;

            $orderProdcut = isset($responseData['products']) ? $responseData['products'] : [];
            $allProducts = [];
            $is_recurring = false;

            foreach($orderProdcut as $key=>$val){
                $is_recurring = isset($val['is_recurring']) && $val['is_recurring']==1 ? true : $is_recurring;

                $product_id = isset($val['product_id']) ? $val['product_id'] : "";
                $sku = isset($val['sku']) ? $val['sku'] : "";
                
                $productDetail = $this->dbInstance->products->findOne([
                    'product_crm_id' => array('$in' => array((int) $product_id, strval($product_id))),
                    'product_createdBy'=>$checkoutCreatedBy,
                ]);
                
                if(empty($productDetail)) continue;
                
                $productDetail['variant_id'] = isset($productDetail['shopify_variant_id']) ? $productDetail['shopify_variant_id'] : "";
                $productDetail['price'] = isset($val['price']) ? $val['price'] : $productDetail['price'];
                $productDetail['quantity'] = $val['product_qty'];
                

                $variants = isset($productDetail['variants']) ? $productDetail['variants'] : [];

                if(!empty($variants)){
                    foreach($variants  as $varKey=>$varVal){
                        if( isset($varVal['sku_num']) && $varVal['sku_num'] == $sku){
                            // when crm sku and hco sku_num match take shopify variant id from hco
                            $productDetail['variant_id'] = isset($varVal['shopify_variant_id']) ? $varVal['shopify_variant_id'] : ( isset($varVal['id']) ? $varVal['id'] : $productDetail['variant_id']);
                        break;
                        }
                    }
                }
                $order_placed_variants = isset($val['variant']) && !empty($val['variant']) ? $val['variant'] : null;
                if(!empty($variants) && $order_placed_variants){
                    foreach($variants  as $varKey=>$varVal){
                        if( isset($varVal['id']) && $varVal['id'] == $order_placed_variants['id']){
                            $productDetail['variant_id'] = $varKey;
                        break;
                        }
                    }
                }

                $allProducts[] = $productDetail;

            }

            $this->requestBody = array_merge($this->requestBody,$responseData);
            $this->requestBody['products'] = $allProducts;
            $this->requestBody['is_recurring'] = $is_recurring;

            return $this->processShopifyOrder();
                
        }catch(\Exception $ex){
            $this->pushErrorLog($ex);   
            throw new \Exception($ex->getMessage());
        }
    }

    public function validateShopifyCustomer(){
        try{
            return $this->validateShopifyCustomerProcess();
        }catch(\Exception $ex){
            $this->pushErrorLog($ex);
            throw new \Exception($ex->getMessage());
        }
    }

    //createShopifyCustomer
    public function createShopifyCustomer(){
        try{
            return $this->createShopifyCustomerProcess();
        }catch(\Exception $ex){
            $this->pushErrorLog($ex);
            throw new \Exception($ex->getMessage());
        }
    }

    protected function getOrderDetail(){
        try{
            $orderDetails = $this->orderDetail();
            $shippingId = isset($this->requestBody['shippingId']) ? $this->requestBody['shippingId'] :"";
            //for the back sync
            $this->requestBody['shippingId'] = isset($orderDetails['responseData']['shipping_id']) && !empty($orderDetails['responseData']['shipping_id']) ? $orderDetails['responseData']['shipping_id']: $shippingId;
            $orderDetails = $this->manageShippingLabel($orderDetails);
            $orderTotalDetail = $this->getStickyOrderTotal();
            $orderDetails['order_total'] = isset($orderTotalDetail['responseData']['data']) ? $orderTotalDetail['responseData']['data'] : [];
            $order_sales_tax_amount = isset($orderDetail['responseData']['order_sales_tax_amount']) ? $orderDetail['responseData']['order_sales_tax_amount'] : 0;
            $orderDetail['responseData']['order_sales_tax_amount'] = isset($orderTotalDetail['responseData']['data']['tax']['total']) ? $orderTotalDetail['responseData']['data']['tax']['total'] : $order_sales_tax_amount;

            return $orderDetails;
         }catch(\Exception $ex){
            return [];
        }
    }

    protected function manageShippingLabel($orderDetails){
        try{
           $shippingId =  isset($this->requestBody['shippingId']) ? $this->requestBody['shippingId'] : "";
           $checkout_createdBy = isset($this->config['crmConfig']['checkout_createdBy']) ? $this->config['crmConfig']['checkout_createdBy'] : "";
           $detail = $this->dbInstance->shipping_methods->findOne(['shipping_method_crm_id'=>$shippingId, "shipping_method_createdBy"=>0, 'shipping_method_createdBy'=>$checkout_createdBy]);
           $shipping_method_name = isset($detail->shipping_method_name) ? $detail->shipping_method_name : "";

           if(isset($orderDetails['responseData']['shipping_method_name']) && !empty($shipping_method_name) ){
             $orderDetails['responseData']['shipping_method_name'] = $shipping_method_name;
           }
           
           return $orderDetails;
        }catch(\Exception $ex){
            return $orderDetails;
        }
    }

}
