<?php
namespace App\FW\sdk\Limelight\Order;

class ShopifyCalculateDiscount{
    public function __construct($couponDetail, $priceDetail){
		$this->couponDetail = $couponDetail;
		$this->priceDetail = $priceDetail;
    }

    public function calculateDiscount(){
        try{
            $return  = [
                "status"=>false,
                "error"=>"", 
                "refrence"=>"shopify",
                "discount_code"=>"",
                "discount_on"=>"", 
                "discount_type"=>"",
                "discount_amount"=>0
            ];

//            if($this->couponDetail['usage_count'] > $this->priceDetail['usage_limit']) throw new \Exception("Coupon limit has been crossed");
            
            $target_type = isset($this->priceDetail['target_type']) ? $this->priceDetail['target_type'] : "";
            
            if(empty($target_type)) throw new \Exception("Invalid action found for coupon process");

            if($target_type == 'shipping_line') $return = $this->shippingLine();

            if($target_type == 'line_item') $return = $this->lineItem();

            return $return;

        }catch(\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    //Shipping calculation 
    public function shippingLine(){
        try{

            $return = [
                "status"=>true,
                "error"=>"", 
                "refrence"=>"shopify",
                "discount_code"=>$this->priceDetail['title'],
                "discount_on"=>"shipping", 
                "discount_type"=>$this->priceDetail['value_type'],
                "discount_value"=> $this->priceDetail['value']<0 ? (-1 * $this->priceDetail['value']) : $this->priceDetail['value'] 
            ];

            return $return;
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage());
        }
    }

    public function lineItem(){
        try{

            $return = [
                "status"=>true,
                "error"=>"", 
                "refrence"=>"shopify",
                "discount_code"=>$this->priceDetail['title'],
                "discount_on"=>"cart", 
                "discount_type"=>$this->priceDetail['value_type'],
                "discount_value"=> $this->priceDetail['value']<0 ? (-1 * $this->priceDetail['value']) : $this->priceDetail['value'] 
            ];

            return $return;
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage());
        }
    }

}
