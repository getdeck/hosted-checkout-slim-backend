<?php
namespace App\FW\sdk\Limelight;
use App\FW\sdk\Config\Config;
use Psr\Http\Message\ServerRequestInterface as Request;
use GuzzleHttp\Client as GuzzleClient;
use App\FW\sdk\Limelight\ProductApi;


/**
 * LimeLight prospect managemnt  
 * Desc: Prospect extended with common config 
 * @copyright  2019 SketchBrain 
 * @license    SketchBrain   FW SDK License 1.0.0
 * @version    Release: V 1.0.0
 * @link       N/A
 * @since      Class available since Release 1.0.0
 */
class CrmConfig extends Config{
    // protected $request;
    protected $response;
    public function __construct(Request $request){

        $this->request = $request;
        // print_r($this->request->getParsedBody());
         // $this->requestBody = $this->requestProcessBody($this->request);// json_decode($this->request->getBody()->getContents(),true);
        parent::__construct();
        $this->request->getBody()->rewind(true); 
    }

    public function getCrmConfig(){
        try{
            $this->getProductsDetailFromCRM();
//            $shopifyDiscount = $this->getShopifyCouponDiscount();
//            $this->config['refrence_discount'] = !empty($shopifyDiscount) && is_array($shopifyDiscount) ? $shopifyDiscount : [];

            return $this->config;
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage());
        }
    }

    public function getShippingDetail(){
        try{

            $shippingIds = $this->config['crmConfig']['checkout_crm_shipping_id'] ?? $this->config['crmConfig']['checkout_crm_shipping_id'];

            $formData = [
                            "formType"=>'shipping_method_view',
                            // 'campaign_id' => isset($this->config['crmConfig']['checkout_campaign_id']) ? $this->config['crmConfig']['checkout_campaign_id'] :'all',
                            // 'search_type'=>'all',
                            'shipping_id'=>$shippingIds,
                            // "return_type"=>"shipping_method_view"
                        ];
            
                       
            $crmDetail = $this->processEndPoint($formData);

 
                        
            return isset($crmDetail['responseData']) ? [$shippingIds => $crmDetail['responseData']] :[];
           
        }catch(\Exception $ex){
            return [];
        }
    }

    public function getUpsells(){
        try{
            return  $this->getUpsellConfigTargated();
            //$this->getUpsellConfig();
            
        }catch(\Exception $ex){
            $this->pushErrorLog($ex);
            // throw new \Exception($ex->getMessage());
            return [];
        }
    }

    public function getShippingDetailFromDb(){
        try{
            
            $shippingIds = isset($this->config['crmConfig']['checkout_crm_shipping_id']) && !empty($this->config['crmConfig']['checkout_crm_shipping_id']) ? (array)$this->config['crmConfig']['checkout_crm_shipping_id']:[];
            $shippings = [];
            foreach($shippingIds as $key=>$val){
                $detail = $this->dbInstance->shipping_methods->findOne(['_id'=>new \MongoDB\BSON\ObjectID($val), "shipping_method_deleted"=>0]);

                if(empty($detail)){
                    continue;
                }
                $detail = $detail->getArrayCopy();
                $crmShipping = $this->getCrmShippingDetail($detail['shipping_method_crm_id']);
                
                if(empty($crmShipping)){
                    continue;
                }
                
                $detail = array_merge($crmShipping,$detail);
                $shippings[] = $detail; 
            }

            if(empty($shippings)){
                throw new \Exception("Shipping is not available");
            }

           return $shippings;
        }catch(\Exception $ex){
            $this->pushErrorLog($ex);
            return [];
        }
    }

    public function getCrmShippingDetail($shipping_method_crm_id){
        try{
            //get shipping detail from API
            $formData = [
                "formType"=>'shipping_method_view',
                'shipping_id'=>$shipping_method_crm_id,
                'formMethod'=>'POST'
                // "return_type"=>"shipping_method_view"
            ];  
            $crmDetail = $this->processEndPoint($formData);
            $shippingV1 =  (isset($crmDetail['responseData']) ? $crmDetail['responseData']:[]);

            $formData = [
                "formType"=>'shipping_method_view_v2',
                'param'=>$shipping_method_crm_id,
                'formMethod'=>'GET'
            ];
            $crmDetail = $this->processEndPoint($formData);
            $shippingV2 =  (isset($crmDetail['responseData']['data']) ? $crmDetail['responseData']['data']:[]);

            $res = array_merge($shippingV1,$shippingV2);
            
            return $res;
            
        }catch(\Exception $ex){
            return [];
        }
    }

    protected function getProductsDetailFromCRM(){
        try{
            $crmProductsId = isset($this->config['crmConfig']['crmProductsId']) ? $this->config['crmConfig']['crmProductsId'] : [];
            
            if(empty($crmProductsId)){
                // return false;
                throw new \Exception("Cart is empty");
            }
            
            $formData =['product_id'=>$crmProductsId];
            $formData['formType'] = 'product_index';
            $formData['formMethod'] = 'POST';
            $crmProductDetail = $this->processEndPoint($formData);
            $products = isset($crmProductDetail['responseData']['products']) ? $crmProductDetail['responseData']['products'] : [];
            if(empty($products)){
                // return false;
                throw new \Exception("Cart is empty");
            }

            $crmProductList = [];
            foreach($crmProductsId as $key=>$val){
                if(isset($products[$val]) && !empty($products[$val])){
                    $crmProductList[$key] = $products[$val];
                }
            }
            
            $this->config['crmConfig']['crmProductsId'] =  $crmProductList;          

        }catch(\Exception $ex){    
            $this->pushErrorLog($ex);
            return false;
        }
    }
   
    public function orderTotalCalculate(){
        try{

            //Apply oneClickUpsell offer
            $this->applyOneClickUpsellOffer();
            
            //get order total from API
            $offers = isset($this->config["crmConfig"]["offers"]) && !empty($this->config["crmConfig"]["offers"]) ? $this->config["crmConfig"]["offers"] : [];
            //It will git the offer discount
            if(!empty($offers)){
                foreach($offers as $offKey=>$offVal){
                    
                    $offers[$offKey]['id'] = isset($offVal['offer_id']) && !empty($offVal['offer_id']) ? $offVal['offer_id'] : '';
                    
                    //Calculate (product_price * cycle - discount) for prepaid
                    $trialPrepaidPrie = $this->fetPrepaidTrialOfferPrice($offVal);
                    if($trialPrepaidPrie!=false){
                        $offers[$offKey]['price'] = $trialPrepaidPrie;
                    }
                    //End of calculation

                    if(isset($offers[$offKey]['trial']) ){
                        unset($offers[$offKey]['price']);
                        unset($offers[$offKey]['trial']);
                        $offers[$offKey]['is_trial'] = true;
                    }
                }
            }

            if(empty($offers)){
                $products = isset($this->config['crmConfig']['products']) && !empty($this->config['crmConfig']['products']) ? $this->config['crmConfig']['products'] :[];
                
                foreach($products as $key=>$val){
                
                    if( isset($val['product_crm_id']) &&  !empty($val['product_crm_id']) ){
                        $quantity = isset($val['quantity']) && !empty($val['quantity']) ? $val['quantity'] :1;
                        $product_trial = isset($val['product_trial']) ? $val['product_trial'] : false;
                        $tempofferArray = [
                            'product_id' => (int) $val['product_crm_id'],
                            'id' =>  isset($val['product_crm_offer_id']) && !empty($val['product_crm_offer_id']) ? (int) $val['product_crm_offer_id']:  0,
                            'price' =>  isset($val['price']) && !empty($val['price']) ? (int) $val['price']: (int) '0.00',
                            'quantity' => (int) $quantity,
                            "billing_model_id" => isset($val['product_billing_model_id']) && !empty($val['product_billing_model_id']) ? (int) $val['product_billing_model_id']: '', 
                        ];

                        $preProcessProduct = $this->getPreProcessProduct($val['product_crm_id']);
                        if(!empty($preProcessProduct)){
                            $preProcessProduct['billing_model_id'] = $tempofferArray['billing_model_id'];
                            $tempofferArray= $preProcessProduct;
                        }
                        $tempofferArray['price'] = isset($tempofferArray['price']) ? number_format((float)$tempofferArray['price'],2, '.', '') : 0.00;

                        if($product_trial){
                            unset($tempofferArray['price']);
                            $tempofferArray['is_trial'] = true;
                        }

                        $offers[]= $tempofferArray;
                    }
                }
            }
            foreach($offers as &$offer){
                // unsetting variant array, as crm not respond with tax of variants
                // we also need to manage tax of diff variant of same product later
                $offer['variant'] = [];
            } 
                
            $formData = [
                "formType"=>'order_total_calculate',
                "formMethod" => "POST",
                "campaign_id" => (int) $this->config['crmConfig']['checkout_campaign_id'],
                "shipping_id" =>  ( isset($this->requestBody['shippingId']) ? (int) $this->requestBody['shippingId'] : ""),
                "use_tax_provider" => isset($this->config['crmConfig']['checkout_enable_tax']) && $this->config['crmConfig']['checkout_enable_tax'] == true ? 1:0,
                "offers" => $offers
            ];
            // remove shipping id if freeShipping set for token user, so shipping wont calculate
            if($this->getContactFreeShipping()){
                unset($formData['shipping_id']);
            }

            $state = (isset($this->requestBody['state']) ? $this->requestBody['state'] : "");
            $country = (isset($this->requestBody['country']) ? $this->requestBody['country'] : "");
            $postal_code = (isset($this->requestBody['zip']) ? $this->requestBody['zip'] : "");

            if(!empty($country) && !empty($postal_code)){
                $formData['location'] =  array (
                    'state' => $state,
                    'country' =>$country,
                    'postal_code' => $postal_code,
                );
            }
          
            if($this->requestBody['promo_code'] !== ""){
                $formData['promo'] = array ( 'code' => (isset($this->requestBody['promo_code']) ? $this->requestBody['promo_code'] : ""), 'email' => $this->requestBody['email'] );
            }
            $crmDetail = $this->processEndPoint($formData);
           
            return $crmDetail;
            // return (isset($crmDetail['responseData']) ? $crmDetail['responseData']:[]);
            
        }catch(\Exception $ex){
            $this->pushErrorLog($ex);
            throw new \Exception($ex->getMessage());
        }
    }

    protected function getPreProcessProduct($product_id){
        try{
            $product = [];
            $offers = isset($this->config["crmConfig"]["offers"]) ? $this->config["crmConfig"]["offers"] : [];

            if(empty($offers)){
                return $product;
            }

            foreach($offers as $offKey=>$offVal){
                if(isset($offVal['product_id']) && $offVal['product_id'] == $product_id){
                    $product = $offVal;
                    $product['id'] = $offVal['offer_id'];

                    //Calculate (product_price * cycle - discount) for prepaid
                    $trialPrepaidPrie = $this->fetPrepaidTrialOfferPrice($offVal);
                    if($trialPrepaidPrie!=false){
                        $product['price'] = $trialPrepaidPrie;
                    }
                    //End of calculation

                    if(isset($product['trial'])){
                        unset($product['trial']);
                    }
                    break;  
                }
            }

            return $product;
        }catch(\Exception $ex){
            $this->pushErrorLog($ex);
            return [];
        }
    }

    public function get3DSToken(){
        try{
            $checkout_enable_3d_verify = isset($this->config["crmConfig"]["checkout_enable_3d_verify"]) ? $this->config["crmConfig"]["checkout_enable_3d_verify"] : false;
            $checkout_3d_verify_apikey = isset($this->config["crmConfig"]["checkout_3d_verify_apikey"]) ? $this->config["crmConfig"]["checkout_3d_verify_apikey"] : '';
            if($checkout_enable_3d_verify==false || empty($checkout_3d_verify_apikey)){
                return '';
            }
            
            $jwt = new \JOSE_JWT(array(
                'audience' => $checkout_3d_verify_apikey,
                'issuer'=>'3dsIntegrator_Authentication_Server',
                'expiresIn'=>"12 hours",
                'header'=> ['alg'=>"HS256",'typ'=>"JWT"]
            ));
            return $jwt->toString();

        }catch(\Exception $ex){
            return '';
        }
    }
    

    public function crmCheckoutBillingModelIds(){
        try{

            $param = [];
            $XLLHash = isset($this->requestBody['X-LL-Hash']) ? $this->requestBody['X-LL-Hash'] : '';
            @parse_str($XLLHash, $param );
            $billing_id  = isset($param['billing_id']) ? explode(',',$param['billing_id']) : []; 
            
            $checkout_billing_model_ids = isset($this->config["crmConfig"]["checkout_billing_model_ids"])  ? $this->config["crmConfig"]["checkout_billing_model_ids"] :[];

            if(empty($billing_id)){
                return false;
            }

            if(!empty($checkout_billing_model_ids)){
                return false;
            }
            $checkout_billing_model_ids = [];

            foreach($billing_id as $key=>$val){

                if(isset($checkout_billing_model_ids[$val])){
                    continue;
                }

                $formData = [
                    "formType"=>'billing_model_view',
                    "formMethod" => "POST",
                    "id"=>$val
                ];

                $apiResponse = $this->processEndPoint($formData);
                $billing_name = isset($apiResponse['responseData']['data'][0]['name']) ? $apiResponse['responseData']['data'][0]['name']:'';
                if(empty($billing_name)){
                    continue;
                }
                $checkout_billing_model_ids[$val]= [
                    "key"=>$val,
                    "billing_model_title_id"=>$billing_name,
                    "billing_model_label"=>$billing_name
                ];
            }
            $this->config["crmConfig"]["checkout_billing_model_ids"] = array_values($checkout_billing_model_ids);

        }catch(\Exception $ex){
            return false;
        }
    }


    public function getBillingModels($offer_id = 'all')
    {
        try {

            $formData = [
                'formType'      => 'billing_model_view',
                'offer_id'      => $offer_id,
                'return_type'   => 'billing_model_view'
            ];

            $crmDetail = $this->processEndPoint($formData);

            $this->config['crmConfig']['all_billing_models'] = isset($crmDetail['responseData']['data']) ? $crmDetail['responseData']['data'] : [];

            return $this->config['crmConfig']['all_billing_models'];
        } catch (\Exception $ex) {
            return [];
        }
    }

    public function getPixelConfiguration(){
        try{
            $checkout_createdBy = isset($this->config["crmConfig"]["checkout_createdBy"]) ? (string) $this->config["crmConfig"]["checkout_createdBy"] : '';
            $_id = isset($this->config["crmConfig"]["_id"]) ? (string) $this->config["crmConfig"]["_id"] : '';

            if(empty($checkout_createdBy))
                throw new \Exception("Invalid checkout configuration");

            $pixels = [];
            $pixelconfiguration = $this->dbInstance->pixels->find(
                                                        ['pixel_createdBy'=>$checkout_createdBy, 
                                                            "pixel_deleted"=>0,
                                                            "pixel_active_status"=>true,
                                                        ],
                                                        [
                                                            "sort"=>['enable_targeted_upsell'=>-1]
                                                        ]
                                                    );
            foreach($pixelconfiguration as $record){
                
                if(in_array($_id,(array)$record->pixel_checkout_id)){
                    $pixels[] = $record; 
                }
            }

            // $pixelconfiguration = $this->dbInstance->pixels->findOne(['pixel_checkout_id'=>$_id]);
            return $pixels;
        }catch(\Exception $ex){
            return [];
        }
    }

    public function processPostBackUrl(){
        try{
           
            $postback_url = isset($this->requestBody['postback_url']) ? $this->requestBody['postback_url'] : '';

            $response = [];
            
            $client = new GuzzleClient();
            $res = $client->request("GET",$postback_url);
            $response['status_code'] = $res->getStatusCode();
            $response['body'] = $res->getBody();

            return $response;

        }catch(\Exception $ex){
            return ["error"=>$ex->getMessage()];
        }
    }

    
 
    
    public function getCountryBasedShippingDetailFromDb(){

        try{
            $checkout_country_based_shipping_enable = isset($this->config['crmConfig']['checkout_country_based_shipping_enable']) ? $this->config['crmConfig']['checkout_country_based_shipping_enable'] : false;

            if(!$checkout_country_based_shipping_enable) return [];

            $checkout_country_shipping = isset($this->config['crmConfig']['checkout_country_shipping']) && !empty($this->config['crmConfig']['checkout_country_shipping']) ? (array)$this->config['crmConfig']['checkout_country_shipping']:[];

            if(empty($checkout_country_shipping)) return [];

            foreach($checkout_country_shipping as $outKey=>$outVal){
                $checkout_country_shipping[$outKey]['shipping_methods'] = [];
                $shippingIds = isset($outVal['shippings']) ? $outVal['shippings'] : [];

                foreach($shippingIds as $key=>$val){
                    $detail = $this->dbInstance->shipping_methods->findOne(['_id'=>new \MongoDB\BSON\ObjectID($val), "shipping_method_deleted"=>0]);

                    if(empty($detail)){
                        continue;
                    }

                    $detail = $detail->getArrayCopy();
                    $crmShipping = $this->getCrmShippingDetail($detail['shipping_method_crm_id']);
                    if(empty($crmShipping)){
                        continue;
                    }
                    $crmShipping = array_merge($crmShipping,$detail);
                    $checkout_country_shipping[$outKey]['shipping_methods'][] = $crmShipping;
                }
            }

            return $checkout_country_shipping;

        }catch(\Exception $ex){
            $this->pushErrorLog($ex);
            return [];
        }
    }

    protected function fetPrepaidTrialOfferPrice($offVal){
        try{
            $price = isset($offVal['price']) ? $offVal['price'] : 0;
            $offer_id = isset($offVal['offer_id']) ? $offVal['offer_id'] : 0;
            
            if(empty($price) || empty($offer_id)) return false;
            
            $offerDetails = isset($this->config["crmConfig"]["offerDetails"][$offer_id]) ?$this->config["crmConfig"]["offerDetails"][$offer_id] : [];
            
            if(empty($offerDetails)) return false;
            
            $trial_flag = isset($offerDetails['trial_flag']) ? $offerDetails['trial_flag'] : false;
            
            //if trial then no need to calculate the price
            
            // if($trial_flag) return false;
            
            $prepaidCycles = isset($offerDetails['prepaid']['terms'][0]['cycles']) ? $offerDetails['prepaid']['terms'][0]['cycles'] : 0;
            $prepaidDiscount = isset($offerDetails['prepaid']['terms'][0]['discount']) ? $offerDetails['prepaid']['terms'][0]['discount'] : 0;
            
            $calculatedPrice = ($price * $prepaidCycles);
            
            if(strpos($prepaidDiscount, "%")){
                $per = str_replace("%","",$prepaidDiscount);
                $prepaidDiscount = (sprintf('%0.2f',$calculatedPrice)) / 100;
            }
            
            $calculatedPrice = $calculatedPrice - $prepaidDiscount;
            
            return $calculatedPrice;
            
        }catch(\Exception $ex){
            return false;
        }
    }

    public function getShippingMethodsFromConfiguration(){
        try{
            /*
            country
            zip
            orderTotal 
            */

            $zip = isset($this->requestBody['zip']) ? $this->requestBody['zip'] : '';
            $orderTotal = isset($this->requestBody['orderTotal']) ? $this->requestBody['orderTotal'] : 0;

            $postal_code = isset($this->config['crmConfig']['checkout_shipping']['postal_code']) ? $this->config['crmConfig']['checkout_shipping']['postal_code'] : [];
            $order_total = isset($this->config['crmConfig']['checkout_shipping']['order_total']) ? $this->config['crmConfig']['checkout_shipping']['order_total'] : [];
            $subscription = isset($this->config['crmConfig']['checkout_shipping']['subscription']) ? $this->config['crmConfig']['checkout_shipping']['subscription'] : [];

            
            $shippingsResult = $this->getLocationBasedShippingConfiguration();
           

            if(!empty($shippingsResult)){
                return $shippingsResult;
            }


            //get shipping on zip code
            $shippingsResult = [];
            if(!empty($postal_code) &&  !empty($zip)){
                $zips = isset($postal_code['zip']) ? $postal_code['zip'] : [];
                $shippings = isset($postal_code['shippings']) ? (array)$postal_code['shippings'] : [];
                $index = false;
                foreach($zips as $key=>$val){
                    if(array_search($zip,(array)$val)!==false){
                        $index = $key;
                    break;
                    }
                }
                $shippingsConfig = $index!==false && isset($shippings[$index]) ? (array)$shippings[$index] : [];

                if(!empty($shippingsConfig)){
                    $shippingsResult = $this->getShippingDBTOCRM($shippingsConfig);
                }
            }

            if(!empty($shippingsResult)){
                return $shippingsResult;
            }

            //get shipping on  order total
            $shippingsResult = [];
            $order_price = isset($order_total['order_price']) ? $order_total['order_price'] : 0;
            $condition = isset($order_total['condition']) ? (array)$order_total['condition'] : [];
            $shippings = isset($order_total['shippings']) ? (array)$order_total['shippings'] : [];

            $index = array_search('<',$condition);
            if($index!==false && $orderTotal<$order_price){
                $shippingsConfig = isset($shippings[$index]) ? (array)$shippings[$index] : [];
                if(!empty($shippingsConfig)){
                    $shippingsResult = $this->getShippingDBTOCRM($shippingsConfig);
                }
            }

            $index = array_search('>',$condition);
            if($index!==false && $orderTotal>$order_price){
                $shippingsConfig = isset($shippings[$index]) ? $shippings[$index] : [];
                if(!empty($shippingsConfig)){
                    $shippingsResult = $this->getShippingDBTOCRM($shippingsConfig);
                }
            }

            $index = array_search('=',$condition);
            if($index!==false && $orderTotal == $order_price){
                $shippingsConfig = isset($shippings[$index]) ? $shippings[$index] : [];
                if(!empty($shippingsConfig)){
                    $shippingsResult = $this->getShippingDBTOCRM($shippingsConfig);
                }
            }

            if(!empty($shippingsResult)){
                return $shippingsResult;
            }


            // for subscription on the basis of billing model
            $billing_model_id = isset($subscription['billing_model_id']) ? (array)$subscription['billing_model_id'] : [];
            $products = isset($this->config['crmConfig']['products']) ? $this->config['crmConfig']['products'] : [];
            $shippings = isset($subscription['shippings']) ? (array)$subscription['shippings'] : [];
            $shippingsResult = [];
            $shippingDBIds = [];
            foreach($billing_model_id as $key=>$val){
                $index = false;
                foreach($products as $pKey=>$pVal){
                    $product_billing_model_id = isset($pVal['product_billing_model_id']) ? $pVal['product_billing_model_id'] : "";
                    if(array_search($product_billing_model_id, (array)$val)!==false){
                        $index = $key;
                    break;
                    }
                }

                $shippingsConfig = $index!==false && isset($shippings[$index]) ? (array)$shippings[$index] : [];  
                if($index!==false && !empty($shippingsConfig)){
                    $shippingDBIds = array_merge($shippingDBIds , $shippingsConfig);
                }
            }

            if(!empty($shippingDBIds)){
                $shippingsResult = $this->getShippingDBTOCRM($shippingDBIds);
            }

            return $shippingsResult;
        }catch(\Exception $ex){
            $this->pushErrorLog($ex);
            throw new \Exception($ex->getMessage());
        }
    }

    protected function getShippingDBTOCRM($shippingIds){
        try{
            $shippings = [];
            foreach($shippingIds as $key=>$val){
                $detail = $this->dbInstance->shipping_methods->findOne(['_id'=>new \MongoDB\BSON\ObjectID($val), "shipping_method_deleted"=>0]);

                if(empty($detail)){
                    continue;
                }
                $detail = $detail->getArrayCopy();
                $crmShipping = $this->getCrmShippingDetail($detail['shipping_method_crm_id']);
                
                if(empty($crmShipping)){
                    continue;
                }
                
                $detail = array_merge($crmShipping,$detail);
                $shippings[] = $detail; 
            }

            return $shippings;


        }catch(\Exception $ex){
            return array();
        }
    }

    public function getGiftItems(){
        try{
            $checkout_gift_products = isset($this->config['crmConfig']['checkout_gift_products']) && !empty($this->config['crmConfig']['checkout_gift_products']) ? (array)$this->config['crmConfig']['checkout_gift_products']:[];
            $items = [];

            foreach($checkout_gift_products as $key=>$val){
                $productDetail = $this->dbInstance->products->findOne(['_id'=>new \MongoDB\BSON\ObjectID($val)]);

                if(!empty($productDetail)){
                    $items[] = $productDetail;
                }
            }

            return $items;
        }catch(\Exception $ex){
            $this->pushErrorLog($ex);
            return [];
        }
    }

    public function productAndOffersDetails(){
        try{

            $productApiIns = new ProductApi($this->requestBody,$this->config,$this->dbInstance);
            return $productApiIns->getProductDetail();
            
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage());
        }
    }

    protected function getLocationBasedShippingConfiguration(){
        try{

            $shippingsResult = [];
            $countryConfig = isset($this->config['crmConfig']['checkout_shipping']['country']) ? $this->config['crmConfig']['checkout_shipping']['country'] : [];
            $country = isset($this->requestBody['country']) ? $this->requestBody['country'] : '';
            $state_request = isset($this->requestBody['state']) ? $this->requestBody['state'] : '';
            $city_request = isset($this->requestBody['city']) ? $this->requestBody['city'] : '';


            

            //get shipping on the basis of country
            if(!empty($countryConfig) &&  !empty($country)){
                $countries = isset($countryConfig['countries']) ? (array)$countryConfig['countries'] : [];
                $states = isset($countryConfig['states']) ? (array)$countryConfig['states'] : [];
                $cities = isset($countryConfig['cities']) ? (array)$countryConfig['cities'] : [];


                $shippings = isset($countryConfig['shippings']) ? (array)$countryConfig['shippings'] : [];
                $index = array_search($country,(array)$countries);

                /*foreach($countries as $key=>$val){
                    if(array_search($country,(array)$countries)!==false){
                        $index = $key;
                    break;
                    }
                }*/

                //if we have state configured with contry then it should be matched with condition
                if($index!==false && !empty($states) && isset($states[$index])){
                    $selectedState = (array) $states[$index];
                    if(!empty($selectedState) && array_search( strtolower($state_request), array_map('strtolower',$selectedState) )===false){
                        return [];
                    }
                }

                //if we have city configured with contry and state then it should be matched with condition
                if($index!==false && !empty($cities) && isset($cities[$index])){
                    $selectedCity = (array) $cities[$index];
                    if(!empty($selectedCity) && array_search( strtolower($city_request), array_map('strtolower',$selectedCity) )===false){
                        return [];
                    }
                }

                $shippingsConfig = $index!==false && isset($shippings[$index]) ? (array)$shippings[$index] : [];
                $shippingsResult = [];

                if(!empty($shippingsConfig)){
                    $shippingsResult = $this->getShippingDBTOCRM($shippingsConfig);
                }

                return $shippingsResult;
            }
        }catch(\Exception $ex){
            return [];
        }
    }



    // get contact/customer all addresses using email id
    public function getContactDetails(){
        try{

            $contact_email = isset($this->requestBody['token_user_data']) && isset($this->requestBody['token_user_data']['email'])? $this->requestBody['token_user_data']['email'] : null;

            if(!$contact_email){
                return [];
            }

            $request = [
                'param'         =>  '?search[email]='.$contact_email,
                'formType'      =>  'contacts_address',
                'formMethod'    =>  'GET'
            ];

            $cont_resp = $this->processEndPoint($request);
            $contact_data = isset($cont_resp['responseData']['data'][0]) ? $cont_resp['responseData']['data'][0] : [];
           

            if($contact_data){
                // remove extra details
                unset($contact_data['custom_fields']);
                // addresses contains both billing and shipping, duplicates too
                $contact_all_addresses = isset($contact_data['addresses']) ? $contact_data['addresses'] : null;
                if($contact_all_addresses){
                    $contact_data['addresses'] = $this->getUniqueAddresses($contact_all_addresses);
                }
            }
            return $contact_data;
            
        }catch(\Exception $ex){
            $this->pushErrorLog($ex);
           return [];
        }
    }

    // contact addresses are not distributed seperately somtime it contains duplicate address as shipping and 
    // billing are same for most of customer so getting unique addresses only
    public function getUniqueAddresses($addresses){

        $unique_addresses_check = [];
        $unique_addresses = [];
        foreach($addresses as $address){
            unset($address['creator']);
            unset($address['uuid']);
            
            $curr_add = strtolower(trim($address['first_name']).trim($address['last_name']).trim($address['street']).trim($address['street_2']).trim($address['city']).trim($address['state']).trim($address['zip']).trim($address['country']));
            if(!in_array($curr_add, $unique_addresses_check)){
                $unique_addresses_check[]   =   $curr_add;
                $unique_addresses[]         =   $address;
            }
        }
        unset($unique_addresses_check);
        return $unique_addresses;
    }

    // we need previous order id to place on click order with existing user's payment methods
    // so istead of getting from contact taking it from order details
    public function getPaymentDetailsOnly(){
        try{
          
            $orders_ids                 =   [];
            $order_payments             =   [];
            $payments                   =   [];
            $billing_addresses          =   [];
            

            $customer_email = isset($this->requestBody['token_user_data']) && isset($this->requestBody['token_user_data']['email'])? $this->requestBody['token_user_data']['email'] : null;

            if(!$customer_email){
                return [];
            }

            $orders_ids = $this->getCustomerOrderIds($customer_email);

            if(!$orders_ids){
                return [];
            }

            $orderDetails = $this->getOrderDetails($orders_ids);
              
            if($orderDetails){

                if(count($orders_ids) == 1){
                    $isOrderGenuine = $this->isOrderGenuine($orderDetails);
                    if($isOrderGenuine) {
                        $used_payments = $this->getUsedPaymentDetails($orderDetails);
                        $order_payments[$orderDetails['order_id']]      = $used_payments['trimmed_payments_data'];
                        $billing_addresses[$orderDetails['order_id']]   = $used_payments['trimmed_billing_addresses'];
                    }

                }else{
                    if(isset($orderDetails['data']) && !empty($orderDetails['data'])){
                        
                        foreach($orderDetails['data'] as $orderKey => $orderVal){
                            $isOrderGenuine = $this->isOrderGenuine($orderVal);
                            
                            if($isOrderGenuine) {
                                $used_payments = $this->getUsedPaymentDetails($orderVal);
                                $order_payments[$orderVal['order_id']]      = $used_payments['trimmed_payments_data'];
                                $billing_addresses[$orderVal['order_id']]   = $used_payments['trimmed_billing_addresses'];
                            }
                        }

                    }
                }
             
                krsort($order_payments); // sort array in descending order so we will get latest order id at top
                $unique_payments = array_unique($order_payments);
                foreach($unique_payments as $paymentKey => $paymentVal){
                    $payments[$paymentKey]  =   explode('$$', $paymentVal);
                }
                foreach($billing_addresses as $billKey => $billVal){
                    $billing_addresses[$billKey]  =   explode('$$', $billVal);
                    foreach($billing_addresses[$billKey] as $k => &$val){
                        $val = ucwords($val);
                    }
                }
            }
            
            return ['payments'  =>  $payments, 'billing_addresses' => $billing_addresses];
            
        } catch(\Exception $ex){
            $this->pushErrorLog($ex);
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    // in front end we showing billing address with payment methods so adding billing addresses in same list
    public function getUsedPaymentDetails($details){
        
        $order_payments        =   strtolower(trim($details['cc_number']).'$$'.trim($details['cc_expires']).'$$'.trim($details['cc_type']));
        $billing_addresse      =   strtolower(trim($details['billing_first_name']).'$$'.trim($details['billing_last_name']).'$$'.trim($details['billing_street_address']).'$$'.trim($details['billing_street_address2']).'$$'.trim($details['billing_city']).'$$'.trim($details['billing_postcode']).'$$'.trim($details['billing_state']).'$$'.trim($details['billing_country']).'$$'.trim($details['customers_telephone']));
    
        return [
            'trimmed_payments_data'     =>  $order_payments,
            'trimmed_billing_addresses' =>  $billing_addresse           
        ];
    }

    // use to not consider orders which are marked as blacklisted or fruad
    public function isOrderGenuine($order) {
        // status 7 is Declined
        if($order['is_blacklisted'] || $order['is_fraud'] || $order['order_status'] == 7) {
            return false;
        }
        return true;
    }

}
