<?php

namespace App\FW\sdk\Limelight;

use App\FW\sdk\Config\Common;
use Psr\Http\Message\ServerRequestInterface as Request;



/**
 *  
 * Desc=> To get Lander specific details
 * @copyright  2021 SketchBrains 
 * @license    SketchBrain SDK License 2.0.0
 * @version    Release=> V 2.0.0
 * @link       N/A
 * @since      Class available since Release 2.0.0
 */

class LanderConfig extends Common
{
    // protected $request;
    protected $response;
    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct();
        $this->dbConnect();
        $this->request->getBody()->rewind(true);
    }
    public function getLanderConfig()
    {
        try {

            $lander_hash    = $this->requestBody && isset($this->requestBody['X-LL-Hash']) ? $this->requestBody['X-LL-Hash'] : null;

            //get lander details from db, using lander hash
            $detail         = $this->dbInstance->landers->findOne([
                'slug' => $lander_hash,
                'lander_deleted' => 0
            ]);
            if (!$detail) {
                throw new \Exception("Invalid lander selection");
            }

            $lander_config          = $detail['lander'];

            $offer_billing_models   = $lander_config['offer_billing_details'] ?? [];
            $lander_campaign_id     = $lander_config['campaign_id'] ?? null;
            $lander_offer_id        = $lander_config['offer_id'] ?? null;
            $lander_products        = $lander_config['products'] ?? [];
            $checkout_id            = $detail['checkout']['_id'];

            //get checkout details using the checkout assigned to lander
            $checkoutDetail         = $this->dbInstance->checkouts->findOne([
                '_id' => new \MongoDB\BSON\ObjectID($checkout_id),
                'checkout_deleted' => 0
            ]);
            if (!$checkoutDetail) {
                throw new \Exception("Invalid checkout selection");
            }

            $lander_config['checkout_hash'] = isset($checkoutDetail->checkout_label_url) && trim($checkoutDetail->checkout_label_url) != '' ? $checkoutDetail->checkout_label_url : null;

            if (!$lander_config['checkout_hash']) {
                throw new \Exception("Checkout hash not found");
            }

            // get product details from db, as we have option to edit sticky.io products into admin as well
            foreach ($lander_products as &$prod) {
                $prodDetails         = $this->dbInstance->products->findOne([
                    '_id' => new \MongoDB\BSON\ObjectID($prod['id'])
                ]);
                if ($prodDetails && $prodDetails->price) {
                    $prod['price']          = $prodDetails->price;
                    $prod['product_name']   = $prodDetails->product_name;
                }
            }

            $resp =  [
                '_id'               => $detail['_id'] ?? '',
                'lander_name'       => $lander_config['lander_name'] ?? '',
                'lander_hash'       => $detail['slug'] ?? '',
                'checkout_hash'     => $lander_config['checkout_hash'],
                'campaign_id'       => $lander_campaign_id,
                'offer_id'          => $lander_offer_id,
                'billing_models'    => $offer_billing_models,
                'products'          => $lander_products,
            ];
            return $resp;
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }
}
