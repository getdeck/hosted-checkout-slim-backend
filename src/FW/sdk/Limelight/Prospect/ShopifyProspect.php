<?php
namespace App\FW\sdk\Limelight\Prospect;
use App\FW\sdk\Config\Config;
use Psr\Http\Message\ServerRequestInterface as Request;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\RequestOptions as RequestOptions;

class ShopifyProspect extends Config{

	public $config;	

	public $requestBody;

	public function __construct($config, $requestBody){
		$this->config = $config;
		$this->requestBody = $requestBody;
	}

	public function createProspect()
	{
		try {
			$checkout_enable_back_sync_shopify = isset($this->config['crmConfig']['checkout_enable_back_sync_shopify']) ? $this->config['crmConfig']['checkout_enable_back_sync_shopify'] : false;

			if(isset($this->config['crmConfig']['shopifyStore']) && $this->config['crmConfig']['shopifyStore'] && $checkout_enable_back_sync_shopify!=false){
				$customer = [];

				$customer['customer']["first_name"] = isset($this->requestBody['firstName']) ? $this->requestBody['firstName'] : "";
				$customer['customer']["last_name"] = isset($this->requestBody['lastName']) ? $this->requestBody['lastName'] : "";
				$customer['customer']["email"] = isset($this->requestBody['email']) ? $this->requestBody['email'] : "";
				$customer['customer']["verified_email"] = true;
				$customer['customer']["phone"] = isset($this->requestBody['phone']) ? $this->requestBody['phone'] : "";
				$customer['customer']["addresses"] = array(array(
					"address1" => isset($this->requestBody['address1']) ? $this->requestBody['address1'] : "",
					"city" => isset($this->requestBody['city']) ? $this->requestBody['city'] : "",
					"province" => isset($this->requestBody['state']) ? $this->requestBody['state'] : "",
					"phone" => isset($this->requestBody['phone']) ? $this->requestBody['phone'] : "",
					"zip" => isset($this->requestBody['address1']) ? $this->requestBody['address1'] : "",
					"last_name" => $customer['customer']["first_name"],
					"first_name" => $customer['customer']["last_name"], 
					"country" => isset($this->requestBody['country']) ? $this->requestBody['country'] : ""
				));

				if($customer['customer']['first_name'] == "" || $customer['customer']['email'] == ""){
					return false;
				}
				
				// check for duplicate 
				$duplicateCustomer = $this->checkDuplicateCustomer();
				
				if(empty($duplicateCustomer['customer'])){
					// add new customer
					$client = new GuzzleClient([ "base_uri" => "https://".$this->config['crmConfig']['shopifyStore']['store_api_key'].":".$this->config['crmConfig']['shopifyStore']['store_api_password']."@".$this->config['crmConfig']['shopifyStore']['store_name']."/admin/api/"]);
				
					$guzzleResponse = $client->request("post", "2020-01/customers.json", [RequestOptions::JSON => $customer]);
					
					$statusCode = $guzzleResponse->getStatusCode();

					if($statusCode === 201 || $statusCode === 200){
						$resp = json_decode($guzzleResponse->getBody()->getContents(),true);
						$resp['exists'] = false;
					}else{
						throw new \Exception("Invalid customer request");
					}
				}else{
					// update customer
					$client = new GuzzleClient([ "base_uri" => "https://".$this->config['crmConfig']['shopifyStore']['store_api_key'].":".$this->config['crmConfig']['shopifyStore']['store_api_password']."@".$this->config['crmConfig']['shopifyStore']['store_name']."/admin/api/"]);
				
					$guzzleResponse = $client->request("put", "2020-01/customers/" . $duplicateCustomer['customers'][0]['id'] . ".json", [RequestOptions::JSON => $customer]);
					
					$statusCode = $guzzleResponse->getStatusCode();
					
					if($statusCode === 201 || $statusCode === 200){
						$resp = json_decode($guzzleResponse->getBody()->getContents(),true);
						$resp['exists'] = true;
					}else{
						throw new \Exception("Invalid customer request");
					}
				}
				
				$resp['shopify_request'] = $customer;
				
				return $resp;
			}else{
				return false;
			}
		}catch(\Exception $ex){
			return ['status'=>false,'message'=> $this->processException($ex->getMessage())];
		}
	}

	public function checkDuplicateCustomer()
	{
		try {
			$response = [ 'customer' => [] ];
			$customerEmail = isset($this->requestBody['email']) ? $this->requestBody['email'] : false;
			$customerFirstName = isset($this->requestBody['firstName']) ? $this->requestBody['firstName'] : false;

			if(!$customerEmail || !$customerFirstName){
				return false;
			}

			$client = new GuzzleClient([ "base_uri" => "https://".$this->config['crmConfig']['shopifyStore']['store_api_key'].":".$this->config['crmConfig']['shopifyStore']['store_api_password']."@".$this->config['crmConfig']['shopifyStore']['store_name']."/admin/api/"]);

			$guzzleResponse = $client->request("get", "2020-01/customers/search.json?query=" . $customerFirstName . " email:" . $customerEmail);

			$statusCode = $guzzleResponse->getStatusCode();

			if($statusCode === 201 || $statusCode === 200){
				$response = json_decode($guzzleResponse->getBody()->getContents(),true);
			}else{
				throw new \Exception("Invalid customer request");
			}
			return $response;
		}catch(\Exception $ex){
			return ['status'=>false,'message'=>$this->processException($ex->getMessage())];
		}
	}

	public function processException($errResponse)
	{
		$errResponse = strstr($errResponse, 'response');
		$errResponse = str_replace("response:","", $errResponse);
		$errResponse = trim($errResponse);
		return json_decode(($errResponse),true);
	}
}