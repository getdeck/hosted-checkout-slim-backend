<?php
namespace App\FW\sdk\Limelight\Prospect;
use App\FW\sdk\Config\Config;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\FW\sdk\Limelight\Prospect\ShopifyProspect as ShopifyProspect;

/**
 * LimeLight prospect managemnt  
 * Desc: Prospect extended with common config 
 * @copyright  2019 SketchBrain 
 * @license    SketchBrain   FW SDK License 1.0.0
 * @version    Release: V 1.0.0
 * @link       N/A
 * @since      Class available since Release 1.0.0
 */

class Prospect extends Config{
    // protected $request;
    protected $response;
    public function __construct(Request $request){

        $this->request = $request;
        // $this->requestBody = json_decode($this->request->getBody()->getContents(),true);
        parent::__construct();
        // $this->request->getBody()->rewind(true); 
    }

     /**
     * Create affiliate prospect 
     *
     * @param prospectData[]
     * 
     * @throws Exception If something interesting cannot happen
     * @author SketchBrain <sketchBrain>
     */
    public function create(){
        try{
            $this->request->getBody()->rewind(true); 
            $requestBody = $this->requestBody;
            $reqCustomFields =  isset($requestBody['custom_fields']) && count($requestBody['custom_fields']) > 0 ? $requestBody['custom_fields'] : [];
            // $requestBody = $this->request->getParsedBody();
            $requestBody['ipAddress'] = $this->getUserAgentIP($this->request);
            $requestBody['formType'] ='create_prospect';
            $requestBody['formMethod'] = 'POST';

            // check if email sent is same email which used in token
            $token_user_email = isset($this->requestBody['token_user_data']) && isset($this->requestBody['token_user_data']['email']) ? $this->requestBody['token_user_data']['email'] : null;
            if($token_user_email && $token_user_email != $this->requestBody['email']){
                throw new \Exception('The email must match with logged in user');
            }
            //Get the All detail from config file for processing the data
            $affiliateDetail = $this->getAffiliateDetail($requestBody);

            $requestBody =  array_merge($requestBody, $affiliateDetail);

            $validation  = $this->validateFormInput($requestBody);

            $shopifyRequestBody = $requestBody;
            $requestBody['prospect_id'] = $this->findProspect($requestBody);
            $prospect_id = '';
            
            //Update prospect
            if(isset($requestBody['prospect_id']) && !empty($requestBody['prospect_id'])){
                $prospect_id = $requestBody['prospect_id'];
                $requestBody['address'] = $requestBody['address1'];
                unset($requestBody['token_user_data']);                
                // unsetting/removing unwated paramter in prospect_update call
                unset($requestBody['shipping_details']);
                $requestBody =[
                    'formType'=>'prospect_update',
                    'formMethod'=>'POST',
                    'prospect_id'=>[
                        $requestBody['prospect_id'] => $requestBody
                    ]
                ];
            }
            
            $ShopifyProspect = false;
            // $ShopifyProspect = new ShopifyProspect($this->config,$shopifyRequestBody);
            // $create_prospect = $ShopifyProspect->createProspect();
            $ShopifyResponse = array( 'shopifyResponse' => $create_prospect );
            $response = array_merge($this->processEndPoint($requestBody), $ShopifyResponse);
                        
           
            $response = $this->processEndPoint($requestBody);
            //echo '$prospect_id '.$prospect_id;
            if(!$prospect_id){
                $prospect_id = $response['responseData']['prospectId'];
            }
            if($prospect_id){
                $response['responseData']['prospectId'] = $prospect_id;    
                //check if user pass custom fields then update custom fields
                // form name is in format: custom_fields[51-13-1] => crm_field_type_id - link with(order/product/contact) - type id(text,int,date,enum,etc.)
                // link with: order => 2, product => 3, contact/prospect => 13
                // data type ids: Text => 1, Numeric => 2, Date => 3, Boolean => 4, Enumeration => 5 
                if(isset($reqCustomFields) && count($reqCustomFields) > 0){
                    $cust_fields = [];
                    foreach($reqCustomFields as $cust_key => $cust_value){
                        $extracted_data = explode('-', $cust_key);
                        if($extracted_data[0] == 4 && $cust_value){
                                $cust_value = true;
                        }else if($extracted_data[0] == 4 && !$cust_value){
                                $cust_value = false;
                        }
                        if($extracted_data[1] == 13){
                            // for prospect
                            $cust_fields[] = array('id' => $extracted_data[0], 'value' => $cust_value);
                        }
                    }
                    if(count($cust_fields) > 0){
                        $body['prospect_id'] = $prospect_id;
                        $body['custom_fields'] = $cust_fields;
                        $this->updateProspectCustomFieldValue($body);
                    }  
                 }
                  
            }

            $this->requestBody['prospectId'] = isset($response['responseData']['prospectId']) ? $response['responseData']['prospectId'] : $prospect_id;
            $response["smile_io"] = $this->manageSmileIOCustomer();

            return $response;
            
        }catch(\Exception $ex){
            $this->pushErrorLog($ex);
            throw new \Exception($ex->getMessage());
        }
    }

    protected function findProspect($requestBody){
        try{
            if(isset($requestBody['prospect_id']) && !empty($requestBody['prospect_id']))
                return $requestBody['prospect_id'];
            
            
            $requestData = [
                "campaign_id"=> isset($requestBody['campaignId']) ? $requestBody['campaignId'] : '',
                "start_date"=>"01/01/1990",
                "end_date"=>date('m/d/Y'),
                "criteria"=>[
                    "email"=> isset($requestBody['email']) ? $requestBody['email'] :'',           
                ],
                "start_time"=>"",
                "end_time"=>"",
                "search_type"=>"all",
                "return_type"=>"prospect_view",
                "formType"=>'prospect_find',
                "formMethod"=>"POST"
            ];
            
            $returnData = $this->processEndPoint($requestData);
            $prospect_id =  (isset($returnData['responseData']['prospect_id']) ? $returnData['responseData']['prospect_id'] : "");
            $prospect_id = is_array($prospect_id) && !empty($prospect_id) ? $prospect_id[0] : $prospect_id;
            return $prospect_id;
            
        }catch(\Exception $ex){
            return "";
        }
    }
    // add values to the custom fields for prospect/contact, which are already created using hco admin/crm
    function updateProspectCustomFieldValue($body){
        try{
            if(!isset($body['prospect_id'])){
                throw new \Exception('prospect_id not found for updateCustomFieldValue call');
            }
            $requestData = [
                "param"=> $body['prospect_id'],
                "formType"=>'update_prospect_custom_field_value',
                "formMethod"=>"POST",
                "custom_fields" => $body['custom_fields'],
            ];
            $returnData = $this->processEndPoint($requestData);
            $status =  isset($returnData['responseData']['status']) && $returnData['responseData']['status'] == 'SUCCESS' ? true : false;
            return $status;

        }catch(\Exception $ex){
            $this->pushErrorLog($ex);
        }
    }
 
    
}