<?php
/**
 * CRM CURL HTTP call for Smile.io
 *
 *
 * @copyright  2019 SketchBrains
 * @license    SketchBrains   FW SDK License 1.0.0
 * @version    Release: V 1.0.0
 * @link       N/A
 * @since      Class available since Release 1.0.0
 */

namespace App\FW\sdk\Config;

class SmileIoCurl{

    public function __construct($config,$requestBody){
        $this->crmConfig      = $config;
        $this->requestBody       = $requestBody;

        $this->api = "https://api.sweettooth.io/v1/";
        $this->apiEndPoint =$this->api;
        $this->formData = [];
        $this->apiSecret = "int_WzzkLxh-_3szFeLrQp47";
        $this->method = "";
        $this->getApiSecretKey();
    }




    public function process(){
        try{
                $curl = curl_init();

                curl_setopt_array($curl, array(
                CURLOPT_URL => $this->apiEndPoint,//"https://api.sweettooth.io/v1/events",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => $this->method,
                CURLOPT_POSTFIELDS =>json_encode($this->formData), //"{\n  \"event\": {\n    \"topic\": \"customer/updated\",\n    \"data\": {\n      \"external_id\": \"11\",\n      \"first_name\": \"SB Demo\",\n      \"last_name\": \"test one\",\n      \"email\": \"sbtest123@gmail.com\",\n      \"external_created_at\": \"2015-11-19T21:19:30.559Z\",\n      \"external_updated_at\": \"2015-11-20T21:09:22.509Z\"\n    }\n  }\n}",
                CURLOPT_HTTPHEADER => array(
                    "Authorization: Bearer ".$this->apiSecret,
                    "Content-Type: application/json"
                ),
                ));

                $response = curl_exec($curl);

                curl_close($curl);
                return $response;

        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage());
         }
    }

    public function manageCustomer(){
        try{
            
            $prospectId = isset($this->requestBody['prospectId']) ? $this->requestBody['prospectId'] : ""; 
            $external_id = isset($this->requestBody['email']) ? $this->requestBody['email'] : "";
            
            
            $this->apiEndPoint = $this->api.'/events';
            $this->formData = [
                "event"=>[
                    "topic"=>"customer/updated",
                    "data"=> [
                        "external_id"=> $external_id,
                        "first_name"=> isset($this->requestBody['firstName']) ? $this->requestBody['firstName'] : "",
                        "last_name"=> isset($this->requestBody['lastName']) ? $this->requestBody['lastName'] : "",
                        "email"=> isset($this->requestBody['email']) ? $this->requestBody['email'] : "",
                        "external_created_at"=> date("c", time()),
                        "external_updated_at"=> date("c", time())
                    ]
                ]
            ];
            $this->method = 'POST';
            $response = $this->process();
            $response = json_decode($response,true);
            if(isset($response['errors'])){
                throw new \Exception(json_encode($response));
            }
            $customer_detail = $this->getCustomerDetail();
            $customer_detail['external_id'] = $external_id;
            $response['customer_detail'] = $customer_detail;
            $response['customer_detail']['customer_auth_digest'] = md5($external_id.$this->apiSecret);
            return $response;

        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage());
         }
    }

    protected function getApiSecretKey(){
        try{
            
            $checkout_enable_smile_config = isset($this->crmConfig['checkout_enable_smile_config']) && $this->crmConfig['checkout_enable_smile_config']==1 ? true : false;

            if($checkout_enable_smile_config!=true) throw new \Exception("Smile.io addons is not enable");

            $this->apiSecret = isset($this->crmConfig['smile_api_config']['smile_private_key']) ? $this->crmConfig['smile_api_config']['smile_private_key'] : "";

            if(empty($this->apiSecret)) throw new \Exception("Invalid API secrect");

        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage());
         }
    }

    public function getCustomerDetail(){
        try{
            $email = isset($this->requestBody['email']) ? $this->requestBody['email'] : "";
            $prospectId = isset($this->requestBody['prospectId']) ? $this->requestBody['prospectId'] : "";

            $this->apiEndPoint = $this->api.'/customers?email='.$email;
            $this->formData =[];
            $this->method = 'GET';
            $response = $this->process();
            $response = json_decode($response,true);
            ;
            $validCustomer = (isset($response['customers'][0]) ? $response['customers'][0] : [] );
            return $validCustomer;
        }catch(\Exception $ex){
            return ["message"=>$ex->getMessage()];
        }
    }
}
