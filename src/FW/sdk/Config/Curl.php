<?php
/**
 * CRM CURL HTTP call
 *
 *
 * @copyright  2019 SketchBrain
 * @license    SketchBrain   FW SDK License 1.0.0
 * @version    Release: V 1.0.0
 * @link       N/A
 * @since      Class available since Release 1.0.0
 */

namespace App\FW\sdk\Config;

use App\FW\sdk\Logger\CustomLog;

class Curl
{

    protected $crmConfig;
    protected $formData;
    protected $apiResponse;
    protected $apiErr;
    protected $apiEndpoint;

    public function __construct($crmConfig, $formData)
    {
        $this->crmConfig      = $crmConfig;
        $this->formData       = $formData;
        $this->apiResponse    = [];
        $this->apiErr         = [];
        $this->apiEndpoint    = '';
        $this->apiResponseTag = json_decode(file_get_contents(__DIR__ . '../../../apiResponseTag.json'), true);
        $this->processPrepaidOffer();
    }

    /**
     * Process FormInput as per the crm selection
     * @param $this ->crmConfig, $this->formData
     *
     * @return CRM response LL/KON
     *
     * @throws Curl_excpetion Invalid curl request will throw the exception
     * @author Sketchbrain TL
     */
    public function process()
    {
        try {

            if (empty($this->formData)) {
                throw new \Exception("Invalid form");
            }

            $response = [];

            $response = $this->limeLightCurl();
            $response = [
                "status"       => true,
                "message"      => "Request has been processed successfully",
                "responseData" => $response,
                "statusCode"   => 200,
            ];

            if (empty($response)) {
                throw new \Exception("Please select a vaild CRM configuration for processing your request");
            }

            //We will process the request as per the CRM Selection
            return $response;
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    /**
     * CURL call for LL CRM
     * @param $this ->crmConfig, $this->formData
     *
     * @return CRM response LL
     *
     * @throws Curl_excpetion Invalid curl request will throw the exception
     * @author Sketchbrain TL
     */
    protected function limeLightCurl()
    {
        try {

            $this->applyRefrenceDiscount(); // for shopify coupon
            $this->applyRedeemCouponDiscount();
            $this->validateOfferPrice();
           
            $method = $this->getLimeLightProcessMethod();
            $type   = $this->formData['formMethod'];

            if (isset($this->formData['formType'])) {
                unset($this->formData['formType']);
            }

            if (isset($this->formData['formMethod'])) {
                unset($this->formData['formMethod']);
            }

            $curl              = curl_init();
            $endPoint          = 'https://' . $this->crmConfig['checkout_crm_admin'] . '.limelightcrm.com/api/';
            $this->apiEndpoint = $endPoint . $method;

            curl_setopt_array($curl, [
                CURLOPT_URL            => $this->apiEndpoint, //EndPoint should be end with '/'
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING       => "",
                CURLOPT_MAXREDIRS      => 10,
                CURLOPT_TIMEOUT        => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST  => $type, //GET , POST, PUT
                CURLOPT_POSTFIELDS     => json_encode($this->formData),
                CURLOPT_HTTPHEADER     => [
                    "authorization: Basic " . base64_encode($this->crmConfig['checkout_api_username'] . ':' . $this->crmConfig['checkout_api_password']),
                    "cache-control: no-cache",
                    "content-type: application/json",
                ],
            ]);
            $response = curl_exec($curl);
            
            $err      = curl_error($curl);

            if (in_array('v2', explode("/", $method))) {
                $response          = json_decode($response, true);
                $this->apiResponse = $response;
                $responseCode = isset($response['response_code']) ? $response['response_code'] : "";

                if($responseCode==100){
                    return $response;
                }

                if (isset($response['status']) && $response['status'] == 'FAILURE' && (!isset($this->formData['returnError']) || $this->formData['returnError'])) {
                    $errMessage = isset($response['message']) ? $response['message'] : "We can't process your request, Please contact to our support";
                    throw new \Exception($errMessage);
                }

                return $response;
            }

            if ($err) {
                $err          = json_decode($err, true);
                $this->apiErr = $err;
                throw new \Exception($err);
            } else {
                $isJson            = isset($this->formData['isJson']) ? $this->formData['isJson'] : true;
                $jsonDecode        = json_decode($response, true);
                $this->apiResponse = $jsonDecode;
                if (!$isJson) {
                    if (is_array($jsonDecode) && isset($this->formData['alt_pay_return_url']) && !empty($this->formData['alt_pay_return_url'])) {
                        header("Location: " . $this->formData['alt_pay_return_url'] . '?' . http_build_query($jsonDecode));

                    } else {
                        echo $response;
                    }
                    exit;
                }

                if (isset($jsonDecode['response_code']) && ($jsonDecode['response_code'] == 100 || $jsonDecode['response_code'] == 911 || $jsonDecode['response_code']== 333)) {
                    if(!isset($this->formData['returnError']) || $this->formData['returnError']) {
                        return $jsonDecode;
                    } else {
                        $successResponse = [];

                        $successResponse['status'] = 'SUCCESS';
                        $successResponse['data'] = $jsonDecode;
                        $successResponse['data']['errorFound']   = 0;
                        $successResponse['data']['transactionID']   = "Not Available";
                        $successResponse['data']['authId']   = "Not Available";
                        $successResponse['data']['responseCode']   = $jsonDecode['response_code'];

                        return $successResponse;
                    }
                } else {
                    $error = [];
                    $error['status']  = 'FAILURE';
                    $error['code']    = isset($this->apiResponse['response_code']) ? $this->apiResponse['response_code'] : 500;

                    if (isset($jsonDecode['decline_reason'])) {
                        if(!isset($this->formData['returnError']) || $this->formData['returnError']) {
                            $resposneData = isset($this->apiResponseTag[$jsonDecode['response_code']]) ? $this->apiResponseTag[(string) $jsonDecode['response_code']] : 'Something went wrong, we are not able to process your request';
                            throw new \Exception($resposneData, 202);
                        } else {
                            $error['message'] = $jsonDecode['decline_reason'];
                            return $error;  
                        }
                    } else {
                        $message = isset($jsonDecode['error_message']) && !empty($jsonDecode['error_message']) ? $jsonDecode['error_message'] : '';
                        $message = isset($jsonDecode['response_message']) && !empty($jsonDecode['response_message']) ? $jsonDecode['response_message'] : $message;
                        $message = empty($message) ? 'Something went wrong, we are not able to process your request' : $message;
                        $message = isset($this->apiResponseTag[(string) $jsonDecode['response_code']]) ? $this->apiResponseTag[(string) $jsonDecode['response_code']] : $message;
                        
                        if(!isset($this->formData['returnError']) || $this->formData['returnError']) {
                            throw new \Exception($message);
                        } else {
                            $error['message'] = $message;
                            return $error;  
                        }
                    }
                }
            }
        } catch (\Exception $ex) {
            $this->pushErrorLog($ex);
            if(!isset($this->formData['returnError']) || $this->formData['returnError']) {
                throw new \Exception($ex->getMessage());
            } else {
                return $ex->getMessage();
            }
        }
    }


    /**
     * Get the method name on the basis of process name
     * @param $this ->formData
     *
     * @return String CRM method
     *
     * @throws Excpetion Invalid curl request will throw the exception
     * @author Sketchbrain TL
     */
    protected function getLimeLightProcessMethod()
    {
        try {
            $process = [
                'create_prospect'            => 'v1/new_prospect',
                'create_prospect_with_order' => 'v1/new_order_with_prospect',
                'create_upsell_order'        => 'v1/new_order_card_on_file',
                'new_order_with_user'        => 'v1/new_order_card_on_file',
                'onepage_checkout'           => 'v1/new_order',
                'campaign_view'              => 'v1/campaign_view',
                'shipping_method_view'       => 'v1/shipping_method_view',
                'shipping_method_view_v2'    => 'v2/shipping/'.(isset($this->formData['param']) ? $this->formData['param']: ""),
                'coupon_validate'            => 'v1/coupon_validate',
                'prospect_update'            => 'v1/prospect_update',
                'shipping_method_find'       => 'v1/shipping_method_find',
                'order_total_calculate'      => 'v2/order_total/calculate',
                'product_index'              => 'v1/product_index',
                'billing_model_view'         => 'v1/billing_model_view',
                'order_view'                 => 'v1/order_view',
                'prospect_find'              => 'v1/prospect_find',
                'order_find'                 => 'v1/order_find',
                'product_image'              => 'v2/products/'.(isset($this->formData['param']) ? $this->formData['param']: "").'/images',
                'order_refund'               => 'v1/order_refund',
                'subscription_start'         => 'v2/subscriptions/'.(isset($this->formData['param']) ? $this->formData['param']: "").'/start',
                'subscription_stop'          => 'v2/subscriptions/'.(isset($this->formData['param']) ? $this->formData['param']: "").'/stop',
                'subscription_pause'         => 'v2/subscriptions/'.(isset($this->formData['param']) ? $this->formData['param']: "").'/pause',
                'subscription_reset'         => 'v2/subscriptions/'.(isset($this->formData['param']) ? $this->formData['param']: "").'/reset',
                'skip_next_billing'          => 'v1/skip_next_billing',
                'order_update'               => 'v1/order_update',
                'subscription_recur_at'      => 'v2/subscriptions/'.(isset($this->formData['param']) ? $this->formData['param']: "").'/recur_at',
                'contacts'                   => 'v2/contacts/'.(isset($this->formData['param']) ? $this->formData['param']: ""),
                'member_update'              => 'v1/member_update',
                'member_login'               => 'v1/member_login',
                'member_forgot_password'     => 'v1/member_forgot_password',
                'member_reset_password'      => 'v1/member_reset_password',
                'member_check_session'       => 'v1/member_check_session',
                'order_calculate_refund'     => 'v1/order_calculate_refund',
                'subscription_order_update'  => 'v1/subscription_order_update',
                'member_logout'              => 'v1/member_logout',
                'member_create'              => 'v1/member_create',
                'offer_view'                 => 'v1/offer_view',
                'product_bundle_view'        => 'v1/product_bundle_view',
                'contacts_address'           => 'v2/contacts'.(isset($this->formData['param']) ? $this->formData['param']: ""),
                'customer_find'              => 'v1/customer_find',
                'offer_view_v2'              => 'v2/offers/'.(isset($this->formData['param']) ? $this->formData['param']: ""), 
                'update_prospect_custom_field_value' => 'v2/prospects/'.(isset($this->formData['param']) ? $this->formData['param']: "").'/custom_fields', 
                'update_order_custom_field_value' => 'v2/orders/'.(isset($this->formData['param']) ? $this->formData['param']: "").'/custom_fields', 
                'update_customer_custom_field_value' => 'v2/customers/'.(isset($this->formData['param']) ? $this->formData['param']: "").'/custom_fields', 
                'change_recurring_product'   => 'v2/subscriptions/'.(isset($this->formData['param']) ? $this->formData['param']: "").'/product',
            ];

            return $process[$this->formData['formType']];

        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    /**
     * @param \Exception $ex
     * @return bool
     */
    protected function pushErrorLog(\Exception $ex)
    {
        try {
            (new CustomLog())
                ->writeLog([
                    'type'      => "ERROR",
                    'message'   => $ex->getMessage(),
                    'client_id' => isset($this->crmConfig['checkout_createdBy']) ? $this->crmConfig['checkout_createdBy'] : '',
                    'app_key'   => isset($this->crmConfig['checkout_crm_admin']) ? $this->crmConfig['checkout_crm_admin'] : '',
                    'context'   => [
                        "file" => $ex->getFile(),
                        "line" => $ex->getLine(),
                    ],
                    'data'      => [
                        'request'      => $_REQUEST,
                        'session'      => $_SESSION,
                        'api_endpoint' => $this->apiEndpoint,
                        'api_response' => $this->apiResponse,
                        'api_error'    => $this->apiErr,
                    ],
                ]);
        } catch (\Exception $ex) {
            return false;
        }

        return true;
    }


    protected function amazonPayProcess($param)
    {
        try {
            if (empty($param)) {
                return $param;
            }

            $creditCardType = isset($param['creditCardType']) ? strtolower($param['creditCardType']) : '';
            $requiredIndex  = [
                'creditCardType',
                'tranType',
                'access_token',
                'order_reference_id',
                'billing_agreement_id',
                'shippingId',
                'ipAddress',
                'campaignId',
                'offers',
            ];

            if ($creditCardType != 'amazonpay') {
                return $param;
            }

            $amazonProcessParam = [];
            foreach ($requiredIndex as $key => $val) {
                $amazonProcessParam[$val] = isset($param[$val]) ? $param[$val] : '';
            }


            $amazonProcessParam['tranType'] = 'sale';
            // echo json_encode($amazonProcessParam);
            // die;
            return $amazonProcessParam;
        } catch (\Exception $ex) {
            return $param;
        }
    }
    
    protected function processPrepaidOffer(){
        try{
            if(!in_array($this->formData['formType'],["create_prospect_with_order","onepage_checkout"])) return false;
            
            $offerDetails = isset($this->crmConfig['offerDetails']) ? $this->crmConfig['offerDetails'] : [];
            $shipping_details =  isset($this->formData['shipping_details']) ? $this->formData['shipping_details'] : [];
            
            if(empty($offerDetails) || empty($shipping_details)) return false;
            
            $shipping_price = isset($shipping_details['initial_amount']) ? $shipping_details['initial_amount'] : 0;
            
            
            $totalCycle = 0;
            
            $offers = isset($this->formData['offers']) ? $this->formData['offers'] : [];
            
            if(empty($offers)) return false;
            
            foreach($offers as $key=>$val){
                $offer_id = $val['offer_id'];
                $cycle = isset($offerDetails[$offer_id]['prepaid_profile']['terms'][0]['cycles']) ?$offerDetails[$offer_id]['prepaid_profile']['terms'][0]['cycles'] : 0;
                if($cycle<1) continue;
                
                $offers[$key]['prepaid_cycles']  = $cycle;
                $totalCycle = $totalCycle + $cycle;
            }
            
           
            if(!empty($offers)){
                $this->formData['offers'] = $offers;
            }
            
            $customShipping = $shipping_price * $totalCycle;
            
            if($customShipping >0){
                $this->formData['dynamic_shipping_charge'] =$customShipping;
            }
            
            return true;
        }catch(\Exception $ex){
            return false;
        }
    }

    protected function applyRefrenceDiscount(){
        try{
            
            $formType = isset($this->formData["formType"]) ? $this->formData["formType"] :"";
            if(!in_array($formType,['create_prospect_with_order','onepage_checkout'])) return false;
            
            if($this->crmConfig->checkout_store_type!=2) return false;
            
            $refrence_discount = isset($this->crmConfig['refrence_discount']) ?$this->crmConfig['refrence_discount'] : [];
            $status  = isset($refrence_discount['status']) ? $refrence_discount['status'] : false;
            $discount_type = isset($refrence_discount['discount_type']) ? $refrence_discount['discount_type'] : "";
            $discount_value = isset($refrence_discount['discount_value']) ? $refrence_discount['discount_value'] : 0;
            $offers = isset($this->formData['offers']) ? $this->formData['offers'] : [];
            $couponValue = $discount_value;
            if(empty($offers)) return false;
            
            $discountApplied = false;
            foreach($offers as $key=>$val){
                if($discount_value==0 || $discount_value<0) break;
                
                if(!$status) continue;
                
                if($discount_type == 'fixed_amount'){
                    $tempPrice = $offers[$key]["price"] - $discount_value;
                    $discount_value = $tempPrice<0 ? ($discount_value  - $offers[$key]["price"]) : 0.00;
                    $offers[$key]["price"] = $tempPrice<0 ? 0.00 : $tempPrice;
                    $discountApplied = true;
                }
                
                if($discount_type == 'percentage'){
                    $discount = ($offers[$key]["price"] * $discount_value) / 100;
                    $tempPrice = $offers[$key]["price"] - $discount;
                    $offers[$key]["price"] = $tempPrice<0 ? 0.00 : $tempPrice;
                    $discountApplied = true;
                }
                
            }
            
            if($discountApplied){
                $this->formData["notes"] = "Shopify coupon applied ".(isset($refrence_discount['discount_code']) ? $refrence_discount['discount_code'] : "").", Coupon type: ".$discount_type. ", value: ".$couponValue;
            }
            
            $this->formData['offers'] = empty($offers) ? $this->formData['offers'] : $offers;
            
        }catch(\Exception $ex) {
            return false;
        }
    }

    protected function validateOfferPrice(){
        try{
            $offers = isset($this->formData['offers']) ? $this->formData['offers'] : [];
            
            if(empty($offers)) return false;
            
            foreach($offers as $key=>$val){

                /*
                Desc : swap product id if we are processing variant
                */
                $shopify_variant_id = isset($this->formData['offers'][$key]['shopify_variant_id']) ? $this->formData['offers'][$key]['shopify_variant_id'] : "";
                $shopify_variants_to_sticky_relation = isset($this->formData['offers'][$key]['shopify_variants_to_sticky_relation']) ? $this->formData['offers'][$key]['shopify_variants_to_sticky_relation'] : [];
                $sticky_prodcut_id = !empty($shopify_variants_to_sticky_relation) && !empty($shopify_variant_id) && isset($shopify_variants_to_sticky_relation[$shopify_variant_id]["product_crm_id"]) ? $shopify_variants_to_sticky_relation[$shopify_variant_id]["product_crm_id"] : "";

                if(!empty($sticky_prodcut_id)){
                    $this->formData['offers'][$key]['product_id'] = $sticky_prodcut_id;
                    unset($this->formData['offers'][$key]['variant']);
                }

                if(isset($this->formData['offers'][$key]['price'])){
                    $this->formData['offers'][$key]['price'] = sprintf('%0.2f',$this->formData['offers'][$key]['price']); 
                }
            }

            return true;
        }catch(\Exception $ex){
            return false;
        }
    }


    /**
     * Apply redeem point value on prodcut price
     * @param $this ->crmConfig, $this->formData
     * 
     * @return updated prodcut price 
     *
     * @throws Exception
     * @author Sketchbrain TL
     */
    protected function applyRedeemCouponDiscount(){
        try{
            $formType = isset($this->formData["formType"]) ? $this->formData["formType"] :"";
            if(!in_array($formType,['create_prospect_with_order','onepage_checkout','order_total_calculate'])) return false;

            $checkout_enable_smile_config = isset($this->crmConfig['checkout_enable_smile_config']) ? $this->crmConfig['checkout_enable_smile_config'] : false;

            if($checkout_enable_smile_config == false) return [];

            $redeem_coupons = isset($this->crmConfig['redeem_coupons']) ? $this->crmConfig['redeem_coupons'] : [];

            if(empty($redeem_coupons)) return [];

            $totalDiscount = 0 ; 
            $offers = isset($this->formData['offers']) ? $this->formData['offers'] : [];
            //Get total discount amount
            foreach($redeem_coupons as $disKey=>$disVal){
                $totalDiscount = $totalDiscount + $disVal['discount'];
            }

            if($totalDiscount > 0){
                //apply on discount
                foreach($offers as $key=>$val){
                    $price = $val['price'];
                    if($price>0){
                        $new_product_price = $price;
                        $new_discount_price = $totalDiscount;
                        if($price>=$totalDiscount){
                            $new_product_price = $price - $totalDiscount;
                            $new_discount_price = $totalDiscount - ($price - $new_product_price);
                        }else{
                            $new_product_price = 0;
                            $new_discount_price = $totalDiscount - $price; 
                        }
                        
                        $this->formData['offers'][$key]['price'] = $new_product_price;
                        $totalDiscount = $new_discount_price;
                    }
                }
            }
            
            return true;

        }catch(\Exception $ex){
            return false;
        }
    }


}
