<?php
/**
 * CRM Configuraton Setting
 *
 *
 * @copyright  2019 SketchBrain
 * @license    SketchBrain   FW SDK License 1.0.0
 * @version    Release: V 1.0.0
 * @link       N/A
 * @since      Class available since Release 1.0.0
 */

namespace App\FW\sdk\Config;

use App\FW\sdk\Config\Request;
use App\FW\sdk\Limelight\Order\Shopify as Shopify;
use App\FW\sdk\Config\Common;
use App\FW\sdk\Config\Curl;
use App\FW\sdk\Logger\CustomLog;
use App\FW\sdk\Magento\MagentoProcess;
use App\FW\sdk\Config\SmileIoCurl;


class Config extends Common
{

    public $config;
    public $logError;

    public function __construct()
    {

        parent::__construct();
        $this->dbConnect();
        $this->config   = $this->importCollectionConfig(); //$this->importConfig();
        $this->logError = true;
        // $this->getOfferDetails();
        $this->getOfferDetailsV2();
        $this->getOneClickUpsellProductDetails();
        
    }


    /**
     * Persist CRM configure return
     * @param void
     *
     * @return Config
     * @throws Exception_Class If something interesting cannot happen
     * @author Sketchbrain TL
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Manage Persist CRM configure data to class var
     * @param condfigData Array
     *
     * @return Config
     * @throws Exception_Class If something interesting cannot happen
     * @author Sketchbrain TL
     */
    public function setConfig($configData = [])
    {
        try {
            $this->config = $configData;

            return $this->config;
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }


    /**
     * Parse user input and convert it as per the CRM selection into common request
     * @param Request $request
     *
     * @return Request $request
     * @throws Request_Exception_Class If something interesting cannot happen
     * @author Sketchbrain TL
     */
    public function request($param = [])
    {
        try {
            return Request::processRequest($param);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    /**
     * Parse user response and convert it as per the CRM selection into common response
     * @param Request $request
     *
     * @return Request $request
     * @throws Request_Exception_Class If something interesting cannot happen
     * @author Sketchbrain TL
     */
    public function response()
    {
        try {
            return Response::processResponse($param);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    /**
     * validate form input
     * @param Request $request
     *
     * @return boolean true
     * @throws Custom_Form_Error If the form elements and it's value are invalid as per the form validator
     * @author Sketchbrain TL
     */
    public function validateFormInput($formData = [])
    {
        try {
            if (empty($formData)) {
                throw new \Exception("Invalid form data");
            }


            $formType  = isset($formData['formType']) ? $formData['formType'] : '';
            $validator = isset($this->config['validator'][$formType]) ? array_change_key_case($this->config['validator'][$formType]) : '';

            if ($validator == "") {
                throw new \Exception("Invalid form request, Please enter a valid 'formType'");
            }

            $errors   = [];
            $formData = array_change_key_case($formData);

            foreach ($validator as $key => $val) {

                //ignoring if the form field is optional
                if (empty($val)) {
                    continue;
                }

                foreach ($val as $inKey => $inVal) {

                    //Error already register, then ignore in second iteration
                    if (isset($errors[$key])) {
                        continue;
                    }

                    //required Validation
                    if ($inVal == 'required' && (!isset($formData[$key]) || empty($formData[$key]))) {
                        $errors[$key] = "Please enter a valid " . $key;
                        continue;
                        // break;
                    }

                    //for pregMatch value
                    if (isset($formData[$key]) && !empty($inVal) && $inVal != 'required' && @preg_match($inVal, $formData[$key])) {
                        $errors[$key] = "Please enter a valid " . $key;
                        // break;
                        continue;
                    }

                }
            }


            if (!empty($errors)) {
                throw new \Exception(implode(':,:', $errors));
            }

            return true;

        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }

    }

    /**
     * @param array $formData
     * @return CRM|array
     * @throws \Exception
     */
    public function processEndPoint($formData = [])
    {
        try {
            $this->logError = false;
            $curl           = new Curl($this->config['crmConfig'], $formData);

            return $curl->process();

        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    /**
     * @param array $formParam
     * @return array
     * @throws \Exception
     */
    public function getAffiliateDetail($formParam = [])
    {
        try {

            if (!isset($formParam['formType']) && empty($formParam)) {
                throw new \Exception("Invalid form selection for getting the affiliate detail");
            }

            return $this->getLLAffiliateDetail($formParam);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    /**
     * @param array $param
     * @return array
     * @throws \Exception
     */
    public function getLLAffiliateDetail($param = [])
    {
        try {
            $response = [];
            $formType = isset($param['formType']) ? $param['formType'] : '';
            $tranType = isset($this->requestBody['tranType']) ? $this->requestBody['tranType'] : '';
            $tranType = isset($this->config['crmConfig']['tranType']) && empty($tranType) ? $this->config['crmConfig']['tranType'] : 'SaLe';
            
            if (empty($formType)) {
                throw new \Exception("Invalid form type");
            }

            if ($formType == 'create_prospect') {
                $response = [
                    'campaignId' => $this->config['crmConfig']['checkout_campaign_id'],
                ];
            }
            //Two step checkout
            if ($formType == 'create_prospect_with_order' || $formType == 'new_order_with_user') {

                $response = [
                    'campaignId' => $this->config['crmConfig']['checkout_campaign_id'],
                    'shippingId' => (isset($this->requestBody['shippingId']) ? $this->requestBody['shippingId'] : ''),
                    //$this->config['crmConfig']['checkout_crm_shipping_id'],
                    'tranType'   => $tranType,
                    //$this->config['crmConfig']['tranType'] ? $this->config['crmConfig']['tranType'] : 'SaLe',
                    'offers'     => $this->getLLCheckoutOfferDetail(),
                    // 'offers'=>$this->getConfigOffers($param)
                ];
                // check if freeShipping is enable for this user then send dynamic_shipping_charge 0
                $isFreeShipping = $this->getContactFreeShipping();
                if($isFreeShipping){
                    $response['dynamic_shipping_charge'] = 0;
                }

            }

            //process upsell order
            if ($formType == 'create_upsell_order') {
                $response = [
                    'campaignId' => $this->config['crmConfig']['checkout_campaign_id'],
                    'shippingId' => (isset($this->requestBody['shippingId']) ? $this->requestBody['shippingId'] : ''),
                    //$this->config['crmConfig']['checkout_crm_shipping_id'],
                    'tranType'   => $tranType,
                    'dynamic_shipping_charge' => 0,
                    //$this->config['crmConfig']['tranType'] ? $this->config['crmConfig']['tranType'] : 'Sale',
                    'offers'     => [$this->getLLUpsellOfferDetail()]
                    // 'offers'=>$this->getConfigOffers($param)
                ];
            }

            //full page checkout
            if ($formType == 'onepage_checkout') {
                $response = [
                    'campaignId' => $this->config['crmConfig']['checkout_campaign_id'],
                    'shippingId' => (isset($this->requestBody['shippingId']) ? $this->requestBody['shippingId'] : ''),
                    //$this->config['crmConfig']['checkout_crm_shipping_id'],
                    'tranType'   => $tranType,
                    //$this->config['crmConfig']['tranType'],
                    'offers'     => $this->getLLCheckoutOfferDetail(),
                ];
            }

            if ($formType == 'coupon_validate') {
                $response = [
                    'campaign_id' => $this->config['crmConfig']['checkout_campaign_id'],
                    'shipping_id' => (isset($this->requestBody['shippingId']) ? $this->requestBody['shippingId'] : ''),
                    //$this->config['crmConfig']['checkout_crm_shipping_id'],
                    'products'    => $this->getLLCheckoutProductDetail(),
                ];
            }
            $response['shipping_details'] = $this->getCrmShippingDetail( ((isset($this->requestBody['shippingId']) ? $this->requestBody['shippingId'] : '')) );
            return $response;
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    /**
     * @return array|mixed
     * @throws \Exception
     */
    protected function getLLCheckoutOfferDetail()
    {
        try {
            $offers = isset($this->config['crmConfig']['offers']) ? $this->config['crmConfig']['offers'] : [];

            if(empty($offers)){
                foreach ($this->config['crmConfig']['products'] as $key => $val) {
                    $trial_main_subscription_product = isset($val['trial_main_subscription_product']) ? $val['trial_main_subscription_product'] : "";
    
                    if (isset($val['product_crm_id']) && !empty($val['product_crm_id'])) {
                        $quantity   = isset($val['quantity']) && !empty($val['quantity']) ? $val['quantity'] : 1;
                        $offerIndex = [
                            'product_id' => $val['product_crm_id'],
                            'offer_id'   => isset($val['product_offer_id']) && !empty($val['product_offer_id']) ? $val['product_offer_id'] : 1,
                            'price'      => isset($val['price']) && !empty($val['price']) ? $val['price'] : '',
                            'quantity'   => $quantity,
                        ];
    
                        if (isset($val['product_billing_model_id']) && !empty($val['product_billing_model_id'])) {
                            $offerIndex['billing_model_id'] = $val['product_billing_model_id'];
                        }
    
                        $product_trial = isset($val['product_trial']) ? $val['product_trial'] : false;
                        if($product_trial){
                            $offerIndex['trial'] = [
                                'product_id' => $val['product_crm_id'],
                                'trial_main_subscription_product'=>$trial_main_subscription_product,
                                // 'price' =>  isset($val['price']) && !empty($val['price']) ? $val['price'] : '',
                                // 'variant'=>$variant
                            ];
                        }
    
                        $offers[] = $offerIndex;
                    }
                }
            }

            $offers = $this->swapTrialMainProduct($offers);
            

            $oneClickUpsellOffer = $this->oneClickUpsellOffers();
            if(is_array($oneClickUpsellOffer) && !empty($oneClickUpsellOffer)){
                $offers =  array_merge($offers , $oneClickUpsellOffer);
            }

            return $offers;

        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

    }

    /**
     * @return array
     * @throws \Exception
     */
    protected function getLLUpsellOfferDetail()
    {
        try {
            $product_crm_id       = isset($this->config['configProduct']['product_crm_id']) ? $this->config['configProduct']['product_crm_id'] : '1';
            $product_crm_offer_id = isset($this->config['configProduct']['product_offer_id']) && !empty($this->config['configProduct']['product_offer_id']) ? $this->config['configProduct']['product_offer_id'] : '1';

            if (!empty($product_crm_id)) {
                return [
                    'product_id' => $product_crm_id,
                    'offer_id'   => $product_crm_offer_id,
                    'price'      => isset($this->config['configProduct']['price']) && !empty($this->config['configProduct']['price']) ? $this->config['configProduct']['price'] : '',
                ];
            }

            return [];

        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    /**
     * This function is not in use, keep for future
     * @param array $formParam
     * @return array
     * @throws \Exception
     */
    protected function getConfigOffers($formParam = [])
    {
        try {
            $paramArray = [
                'product_id' => isset($formParam['productIdes']) ? explode(',', $formParam['productIdes']) : [],
                'offer_id'   => isset($formParam['offerIdes']) ? explode(',', $formParam['offerIdes']) : [],
            ];

            if (empty($paramArray['product_id']) && empty($paramArray['offer_id'])) {
                throw new \Exception("Invalid product selection, Please provide product/offer ID");
            }

            $offers      = [];
            $configOffer = isset($this->config['crmConfig']['offers']) ? $this->config['crmConfig']['offers'] : [];


            foreach ($paramArray as $key => $val) { //$key will be search key
                foreach ($val as $intKey => $intVal) {
                    foreach ($configOffer as $conKey => $conVal) {
                        if ($conVal[$key] == $intVal) {
                            $offers[] = $conVal;
                        }
                    }
                }
            }

            if (empty($offers)) {
                throw new \Exception("Please provide valid product/offer ID");
            }

            return $offers;
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    /**
     * @return mixed
     */
    public function getUserAgentIP()
    {
        return $_SERVER['REMOTE_ADDR'];
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getLLCheckoutProductDetail()
    {
        try {
            $offers = [];

            foreach ($this->config['crmConfig']['products'] as $key => $val) {

                if (isset($val['product_crm_id']) && !empty($val['product_crm_id'])) {
                    $quantity = isset($val['quantity']) && !empty($val['quantity']) ? $val['quantity'] : 1;

                    $offers[] = [
                        'product_id' => $val['product_crm_id'],
                        'quantity'   => $quantity,
                    ];
                }
            }

            return $offers;

        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function processShopifyOrder()
    {
        try {

            
            $profileResponse = $this->createMembershipProfile();
            $magentoProcessResponse = $this->processMagentoOrderSync();
            
            $shopifyResponse = (new Shopify($this->config, $this->requestBody))
                ->processOrder();
            return ['profileResponse'=>$profileResponse, 'shopifyResponse'=>$shopifyResponse,"magentoProcessResponse"=>$magentoProcessResponse];

        }catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function pushErrorLog(\Exception $ex)
    {
        try {

            if ($this->logError != true) {
                return false;
            }
            $ll_hash  = isset($this->requestBody['X-LL-Hash']) ? $this->requestBody['X-LL-Hash'] : '';
            $logParam = [
                'type'      => "ERROR",
                'message'   => $ex->getMessage(),
                'client_id' => isset($this->config['crmConfig']['checkout_createdBy']) ? $this->config['crmConfig']['checkout_createdBy'] : $ll_hash,
                'app_key'   => isset($this->config['crmConfig']['checkout_crm_admin']) ? $this->config['crmConfig']['checkout_crm_admin'] : '',
                'context'   => "File: " . $ex->getFile() . " Line: " . $ex->getLine(),
                'data'      => [
                    'request'      => $this->requestBody,
                    'session'      => $_SESSION,
                    'api_endpoint' => '',
                    'api_response' => [],
                    'api_error'    => [],
                ],
            ];
            (new CustomLog())
                ->writeLog($logParam);
        } catch (\Exception $ex) {
            return false;
        }
    }

    public function createMembershipProfile(){
        try{
            
            $customerId = isset($this->requestBody['customerId']) ? $this->requestBody['customerId'] : "";
            $email = isset($this->requestBody['email']) ? $this->requestBody['email'] : "";
           
            if(empty($customerId) || empty($email)) return false;
            /*
            //Logic commented and converted into addon
            $checkoutId = (string) $this->config['crmConfig']['_id'];
            $memberConfig = $this->dbInstance->members_portals->findOne([
                'member_portal_checkout_id' => $checkoutId,
                'member_portal_deleted'=>0
            ]);
            //check if we have configured profile or not 
            $memberConfigID = isset($memberConfig['_id']) ? (string) $memberConfig['_id'] : '';
            if(empty($memberConfigID)) return false ;
            */
            
            $checkout_enable_member_create = isset($this->config["crmConfig"]['checkout_enable_member_create']) ? $this->config["crmConfig"]['checkout_enable_member_create'] : false;

            if($checkout_enable_member_create == false) return false;

            $request = [
                "customer_id" => $customerId,
                "email" => $email,
                "event_id" =>1,
                "formType"=>'member_create',
                "formMethod"=>'POST'
            ];

            $response = $this->processEndPoint($request);
            return ["status"=>true, "message"=>"success", 'response'=>$response];

        }catch(\Exception $ex){
            return ["status"=>false, "message"=>$ex->getMessage(), 'response'=>[]];
        }
    }

    public function getOfferDetails(){
        try{
            
            $offerDetails = [];
            
            $offers = isset($this->config["crmConfig"]["products"]) ? $this->config["crmConfig"]["products"] : [];
            
            if(empty($offers)) return [];

            foreach($offers as $key=>$val){
                $offer_id = isset($val['product_offer_id']) ? $val['product_offer_id'] : "";
                $product_crm_id = isset($val['product_crm_id']) ? $val['product_crm_id'] : "";
                
                if(empty($offer_id) || isset($offerDetails[$offer_id])) continue;
                
                $formData = [
                    "formType"=>'offer_view',
                    "formMethod" => "POST",
                    "offer_id"=>$offer_id
                ];

                $apiResponse = $this->processEndPoint($formData);
                
                $responseCode = isset($apiResponse['responseData']['response_code']) ?$apiResponse['responseData']['response_code'] : 500;
                $offerData = $responseCode ==100 && isset($apiResponse['responseData']['data'][0]) ? $apiResponse['responseData']['data'][0] : [];
                
                if(empty($offerData)) continue;
                $offerDetails[$offer_id] = $offerData;

                //trial logic
                $trial_flag = isset($offerData['trial_flag'])  && $offerData['trial_flag'] == 1 ? true : false;
                $product_trial = isset($this->config["crmConfig"]["products"][$key]["product_trial"])  ? $this->config["crmConfig"]["products"][$key]["product_trial"]: false;
                $inherit_product_price = isset($offerData['trial']['price']['inherit_product_price']) && $offerData['trial']['price']['inherit_product_price']!=0 ? true : false;
                $custom_price = isset($offerData['trial']['price']['custom_price']) ? $offerData['trial']['price']['custom_price'] : 0.00;

                if($trial_flag && $product_trial && $inherit_product_price!=true){
                    $this->config["crmConfig"]["products"][$key]["price"] = $custom_price;
                }
                
                //End of trial logic

                $cycle = isset($offerData['prepaid']['terms'][0]['cycles']) ? $offerData['prepaid']['terms'][0]['cycles'] : 0;
                $discount = isset($offerData['prepaid']['terms'][0]['discount']) ? $offerData['prepaid']['terms'][0]['discount'] : 0;
                
                $is_prepaid_applied = isset($this->config["crmConfig"]["products"][$key]["is_prepaid_applied"])  ? $this->config["crmConfig"]["products"][$key]["is_prepaid_applied"]: false;
                
                if($is_prepaid_applied!=true){
                        if($cycle>0){
                           $this->config["crmConfig"]["products"][$key]["is_prepaid_applied"] = true;
                            
                            $this->config["crmConfig"]["products"][$key]["price"] = sprintf('%0.2f',$this->config["crmConfig"]["products"][$key]["price"]) * $cycle;
                           
                            if(strpos($discount, "%")){
                                $per = str_replace("%","",$discount);
                                $discount = (sprintf('%0.2f',$this->config["crmConfig"]["products"][$key]["price"]) * $per) / 100;
                            }
                        }
                        
                        if($discount>0){
                            $this->config["crmConfig"]["products"][$key]["price"] = $this->config["crmConfig"]["products"][$key]["price"] - $discount;
                        }
                }

            }
             
            $this->config["crmConfig"]["offerDetails"] = $offerDetails;

        }catch(\Exception $ex){
            
            return [];
        }
    }
    
    public function getCrmShippingDetail($shipping_method_crm_id){
        try{
            //get shipping detail from API
            $formData = [
                "formType"=>'shipping_method_view',
                'shipping_id'=>$shipping_method_crm_id,
                'formMethod'=>'POST'
                // "return_type"=>"shipping_method_view"
            ];
                
            $crmDetail = $this->processEndPoint($formData);
            return (isset($crmDetail['responseData']) ? $crmDetail['responseData']:[]);
            
        }catch(\Exception $ex){
            return [];
        }
    }
    
    public function getShopifyCouponDiscount(){
        try{
           
            $shopifyResponse = (new Shopify($this->config, $this->requestBody))
                ->getCouponDiscouunt();
            return $shopifyResponse;
        }catch(\Exception $ex){
            // discount_on => shipping/cart
            //discount_type => fixed / percentage
            return [
                        "status"=>false,
                        "error"=>$ex->getMessage(), 
                        "refrence"=>"shopify",
                        "discount_code"=>"",
                        "discount_on"=>"", 
                        "discount_type"=>"",
                        "discount_amount"=>0
                    ];
        }
    }

    public function processMagentoOrderSync(){
        try{
            // $this->config["crmConfig"]["magentoStore"]
            $checkout_enable_back_sync_shopify = isset($this->config['crmConfig']['checkout_enable_back_sync_shopify']) ? $this->config['crmConfig']['checkout_enable_back_sync_shopify'] : false;
            if($checkout_enable_back_sync_shopify==false) throw new \Exception("Order back-sync is not enable");

            $instance = new MagentoProcess($this->config,$this->requestBody);
            $response = $instance->orderSync();
            return ["status"=>false,"message"=>"Order has been processed", 'order_id'=>$response];
        }catch (\Exception $ex) {
            return ["status"=>false,"message"=>$ex->getMessage(), 'order_id'=>""];
        }
    }

    protected function oneClickUpsellOffers(){
        try{
            
            $oneClickUpsells = isset($this->requestBody['oneClickUpsells']) ? $this->requestBody['oneClickUpsells'] : [];
            
            if(empty($oneClickUpsells)){
                return [];
            }
            $offers = [];
            foreach($oneClickUpsells as $key=>$val){
                
                $product_mongodb_id = isset($val['product_mongodb_id']) ? $val['product_mongodb_id'] : "";
                if(empty($product_mongodb_id)) continue;

                $price = isset($val['price']) && !empty($val['price']) ? $val['price']: '';
                $price = isset($val['product_discount_price']) && !empty($val['product_discount_price']) && $val['product_discount_price'] > 0 ? $val['product_discount_price']: $price;


                
                $productDetail = $this->dbInstance->products->findOne(['_id'=>new \MongoDB\BSON\ObjectID($product_mongodb_id)]);
                if(empty($productDetail)) continue;

                //Apply product price if not configured in upsell configuration
                if(empty($price)){
                    $price = isset($productDetail['price']) && !empty($productDetail['price']) ? $productDetail['price']: '';
                    $price = isset($productDetail['product_discount_price']) && !empty($productDetail['product_discount_price']) && $productDetail['product_discount_price'] > 0 ? $productDetail['product_discount_price']: $price;
                }
                
                $offers[] = [
                                'product_id' => isset($productDetail['product_crm_id']) && !empty($productDetail['product_crm_id']) ? $productDetail['product_crm_id']:'',
                                'offer_id' =>  isset($productDetail['product_offer_id']) && !empty($productDetail['product_offer_id']) ? $productDetail['product_offer_id']: '',
                                'price' =>  sprintf('%0.2f',$price),
                                'quantity' => isset($val['quantity']) ? $val['quantity'] :1,
                                'billing_model_id' => isset($productDetail['product_billing_model_id']) && !empty($productDetail['product_billing_model_id']) ? $productDetail['product_billing_model_id']: '',
                            ];
            }

            return $offers;
        }catch(\Exception $ex) {
            return [];
        }
    }

    public function validateShopifyCustomerProcess(){
        try{
            $shopifyResponse = (new Shopify($this->config, $this->requestBody))
            ->validateCustomer();
        return $shopifyResponse;

        }catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function createShopifyCustomerProcess(){
        try{
            $shopifyResponse = (new Shopify($this->config, $this->requestBody))
            ->createShopifyCustomer();
        return $shopifyResponse;

        }catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }
    
    protected function swapTrialMainProduct($offers){
        try{
            foreach($offers as $key=>$val){
                if(isset($val['trial']) && isset($val['trial']['trial_main_subscription_product'])){
                    $detail = $this->dbInstance->products->findOne(['_id'=>new \MongoDB\BSON\ObjectID($val['trial']['trial_main_subscription_product'])]);
                    
                    if(empty($detail)) continue;

                    $offers[$key]['product_id'] = isset($detail['product_crm_id']) ? $detail['product_crm_id'] : $val['product_id'];
                    $offers[$key]['price']  = isset($detail['price']) ? $detail['price'] : $val['price'];

                }
            }
            
            return $offers;
        }catch(\Exception $ex){
            return $offers;
        }
    }

    public function getOfferDetailsV2(){
        try{
            
            $offerDetails = [];
            
            $offers = isset($this->config["crmConfig"]["products"]) ? $this->config["crmConfig"]["products"] : [];
            
            if(empty($offers)) return [];

            foreach($offers as $key=>$val){
                $offer_id = isset($val['product_offer_id']) ? $val['product_offer_id'] : "";
                $product_crm_id = isset($val['product_crm_id']) ? $val['product_crm_id'] : "";
                
                if(empty($offer_id) || isset($offerDetails[$offer_id])) continue;
                
                $formData = [
                    "formType"=>'offer_view_v2',
                    "formMethod" => "GET",
                    "param"=>$offer_id
                ];

                $apiResponse = $this->processEndPoint($formData);
                
                $offerData = isset($apiResponse['responseData']['data']) ? $apiResponse['responseData']['data'] : [];
                
                if(empty($offerData)) continue;
                $offerDetails[$offer_id] = $offerData;

                //trial logic
                $trial_flag = isset($offerData['is_trial'])  && $offerData['is_trial'] == 1 ? true : false;
                $product_trial = isset($this->config["crmConfig"]["products"][$key]["product_trial"])  ? $this->config["crmConfig"]["products"][$key]["product_trial"]: false;
                $is_custom_price = isset($offerData['trial']['is_custom_price']) && $offerData['trial']['is_custom_price']==1 ? true : false;
                $custom_price = isset($offerData['trial']['price']) && !empty($offerData['trial']['price']) ? $offerData['trial']['price'] : 0.00;

                if($trial_flag && $product_trial && $is_custom_price == true){
                    $this->config["crmConfig"]["products"][$key]["price"] = $custom_price;
                }
                
                //End of trial logic

                $cycle = isset($offerData['prepaid_profile']['terms'][0]['cycles']) ? $offerData['prepaid_profile']['terms'][0]['cycles'] : 0;
                $discount = isset($offerData['prepaid_profile']['terms'][0]['discount_value']) ? $offerData['prepaid']['terms'][0]['discount_value'] : 0;
                $discount_type = isset($offerData['prepaid_profile']['terms'][0]['discount_type']['name']) ? $offerData['prepaid_profile']['terms'][0]['discount_type']['name'] : "Amount";

                $is_prepaid = isset($offerData['is_prepaid']) && $offerData['is_prepaid'] ==1 ? true : false;


                $is_prepaid_applied = isset($this->config["crmConfig"]["products"][$key]["is_prepaid_applied"])  ? $this->config["crmConfig"]["products"][$key]["is_prepaid_applied"]: false;
                
                if($is_prepaid_applied!=true){
                        if($is_prepaid == true && $cycle>0){
                           $this->config["crmConfig"]["products"][$key]["is_prepaid_applied"] = true;
                            
                            $this->config["crmConfig"]["products"][$key]["price"] = sprintf('%0.2f',$this->config["crmConfig"]["products"][$key]["price"]) * $cycle;
                           
                            if(strtoupper($discount_type) == "percent"){
                                $discount = (sprintf('%0.2f',$this->config["crmConfig"]["products"][$key]["price"]) * $discount) / 100;
                            }
                        }
                        
                        if($discount>0){
                            $this->config["crmConfig"]["products"][$key]["price"] = $this->config["crmConfig"]["products"][$key]["price"] - $discount;
                        }
                }

            }
             
            $this->config["crmConfig"]["offerDetails"] = $offerDetails;

        }catch(\Exception $ex){
            
            return [];
        }
    }

    public function getStickyOrderTotal(){
        try{
            $oneClickUpsellOffer = $this->applyOneClickUpsellOffer();
            $offers = isset($this->config['crmConfig']['offers']) ? $this->config['crmConfig']['offers'] :[];
            $checkout_campaign_id = isset($this->config['crmConfig']['checkout_campaign_id']) ? (int) $this->config['crmConfig']['checkout_campaign_id']:"";
            $shippingId = ( isset($this->requestBody['shippingId']) ? (int) $this->requestBody['shippingId'] : "");

            if(empty($offers) || empty($checkout_campaign_id) || empty($shippingId)){
                return false;
            }

            foreach($offers as $key=>$val){
            $offers[$key]['id'] = isset($val['offer_id']) ? $val['offer_id'] : "";
            }



            $formData = [
                "formType"=>'order_total_calculate',
                "formMethod" => "POST",
                "campaign_id" =>$checkout_campaign_id,
                "shipping_id" =>$shippingId,
                "offers" => $offers
            ];

          return $this->processEndPoint($formData);
  
        }catch(\Exception $ex){
           return false;
        }
    }
    
    public function getOneClickUpsellProductDetails(){
        try{
            $checkout_enable_one_click_upsell_products = isset($this->config['crmConfig']['checkout_enable_one_click_upsell_products']) ? $this->config['crmConfig']['checkout_enable_one_click_upsell_products'] : false;
            $checkout_one_click_upsell_products = isset($this->config['crmConfig']['checkout_one_click_upsell_products']) ? $this->config['crmConfig']['checkout_one_click_upsell_products'] : [];

            if($checkout_enable_one_click_upsell_products != true){
                return false;
            }

            foreach($checkout_one_click_upsell_products as $key=>$val){
                $product_mongodb_id = isset($val['product_mongodb_id']) ? $val['product_mongodb_id'] : "";
                $this->config['crmConfig']['checkout_one_click_upsell_products'][$key]['product_name'] = isset($val['name']) ? $val['name'] : "";
                

                $price = isset($val['price']) && !empty($val['price']) ? $val['price']: '';
                $price = isset($val['product_discount_price']) && !empty($val['product_discount_price']) && $val['product_discount_price'] > 0 ? $val['product_discount_price']: $price;

                $productDetail = $this->dbInstance->products->findOne(['_id'=>new \MongoDB\BSON\ObjectID($product_mongodb_id)]);

                if(empty($productDetail)) continue;

                $this->config['crmConfig']['checkout_one_click_upsell_products'][$key]['product_offer_id'] = isset($productDetail['product_offer_id']) && !empty($productDetail['product_offer_id']) ? $productDetail['product_offer_id']: '';
                $this->config['crmConfig']['checkout_one_click_upsell_products'][$key]['product_billing_model_id'] = isset($productDetail['product_billing_model_id']) && !empty($productDetail['product_billing_model_id']) ? $productDetail['product_billing_model_id']: '';

                $this->config['crmConfig']['checkout_one_click_upsell_products'][$key]['quantity'] = 1;

                
                // need variant id for shopify upsell back sync so add check if product is shopify based then
                // check for its variant id, for now take first variant id as we dont have setting in admin or front end to select variant for upsell
                $first_var_id = null;
                // we doing this foreach coz the shopify objects saving in key => val format where key is shop var id
                foreach($productDetail['variants'] as $var_key => $var_row){
                    if(!$first_var_id){
                        $first_var_id = isset($var_row['shopify_variant_id']) ? $var_row['shopify_variant_id'] : $var_row['id'];
                    }
                }
                // we have to remove this and manage variants either from HCO or from front end to user select which one they want
                // if product level shopify variant id present then take it, means its a shopify product without variants
                // if not present then check for variants array and take 0th position object and take its  shopify_variant_id
                // if not present then its a sticky product then use use id from 0th object
                $this->config['crmConfig']['checkout_one_click_upsell_products'][$key]['variant_id'] = isset($productDetail['shopify_variant_id']) ? $productDetail['shopify_variant_id'] : $first_var_id;
                

                //Apply product price if not configured in upsell configuration
                $price = isset($this->config['crmConfig']['checkout_one_click_upsell_products'][$key]['price']) ? $this->config['crmConfig']['checkout_one_click_upsell_products'][$key]['price'] : "";
                $product_discount_price = isset($this->config['crmConfig']['checkout_one_click_upsell_products'][$key]['product_discount_price']) ? $this->config['crmConfig']['checkout_one_click_upsell_products'][$key]['product_discount_price'] : "";
                if(empty($price)){
                    $this->config['crmConfig']['checkout_one_click_upsell_products'][$key]['price'] = isset($productDetail['price']) && !empty($productDetail['price']) ? $productDetail['price']: '0';
                }

                if(empty($product_discount_price)){
                    $this->config['crmConfig']['checkout_one_click_upsell_products'][$key]['product_discount_price'] = isset($productDetail['product_discount_price']) && !empty($productDetail['product_discount_price']) ? $productDetail['product_discount_price']: '0';
                }

                $price = isset($this->config['crmConfig']['checkout_one_click_upsell_products'][$key]['price']) ? $this->config['crmConfig']['checkout_one_click_upsell_products'][$key]['price'] : "";
                $product_discount_price = isset($this->config['crmConfig']['checkout_one_click_upsell_products'][$key]['product_discount_price']) ? $this->config['crmConfig']['checkout_one_click_upsell_products'][$key]['product_discount_price'] : "";
                $this->config['crmConfig']['checkout_one_click_upsell_products'][$key]['item_price'] = $price;
                
                if(!empty($product_discount_price) && $product_discount_price>0){
                    $this->config['crmConfig']['checkout_one_click_upsell_products'][$key]['price'] = $product_discount_price;
                }

                $this->config['crmConfig']['checkout_one_click_upsell_products'][$key]['description'] = $this->manageOnceClickUpsellDescTemplate($this->config['crmConfig']['checkout_one_click_upsell_products'][$key]);
            }

        }catch(\Exception $ex){
            return false;
        }
    }

    public function applyOneClickUpsellOffer(){
        try{
            $oneClickUpsells = isset($this->requestBody['oneClickUpsells']) ? $this->requestBody['oneClickUpsells'] : [];

            foreach($oneClickUpsells as $key=>$val){
                $productDetail = $this->dbInstance->products->findOne(['_id'=>new \MongoDB\BSON\ObjectID($val['product_mongodb_id'])]);
                if(isset($productDetail['product_crm_id']) && $productDetail['product_crm_id'] !== NULL){
                    $this->config["crmConfig"]["offers"][] = [
                                'product_id' => (int) $productDetail['product_crm_id'],
                                'id' =>  isset($val['product_offer_id']) && !empty($val['product_offer_id']) ? (int) $val['product_offer_id']:  0,
                                'offer_id'=>isset($val['product_offer_id']) && !empty($val['product_offer_id']) ? (int) $val['product_offer_id']:  0,
                                'price' =>  isset($val['price']) && !empty($val['price']) ? $val['price']: 0,
                                'quantity' => 1,
                                "billing_model_id" => isset($val['product_billing_model_id']) && !empty($val['product_billing_model_id']) ? (int) $val['product_billing_model_id']: '', 
                    ];    
                }else{
                    // We need product_crm_id to process order, that is why we are skipping add offer in offers array  
                }
            }

            return $this->config["crmConfig"]["offers"];
        }catch(\Exception $ex){
            return [];
        }
    }

    public function manageOnceClickUpsellDescTemplate($desc)
    {
        $description = (isset($desc['description'])) ? $desc['description'] : "";
        try {
            // Check if on click products exists or not
            if(!isset($desc) || sizeof($desc) == 0){
                return false;
            }

            $imgUrl = (isset($desc['image_url']) && sizeof($desc['image_url']) > 0) ? $desc['image_url'][0] : "";

            // Tag replacement
            $description = str_replace('[product_name]', $desc['name'], $description);
            $description = str_replace('[product_price]', '<span class="product-currency">$</span> '.$desc['item_price'], $description);
            $description = str_replace('[product_discount_price]', '<span class="discount-currency">$</span> '.$desc['product_discount_price'], $description);
            $description = str_replace('[product_default_img_url]', $imgUrl, $description);
            $description = str_replace('[product_default_img]', '<img src="'.$imgUrl.'" class="product-image"/>', $description);
            
            return $description;
        }catch(\Exception $ex){
            return $description;
        }
    }

     public function manageSmileIOCustomer(){
         try{
             
            $smile = new SmileIoCurl($this->config['crmConfig'], $this->requestBody);
            return $smile->manageCustomer();

         }catch(\Exception $ex){
            return ["status"=>false, "message"=>$ex->getMessage()];
         }
     }


    // get order ids for a given email id
    public function getCustomerOrderIds($customer_email){
        try{

            $orders_ids     = [];
          
            $filters = [
                "campaign_id"   =>  "all",
                "start_date"    =>  date(DATE_FORMAT,strtotime(date(DATE_FORMAT).'-1 year')),
                "end_date"      =>  date(DATE_FORMAT),
                "start_time"    =>  "",
                "end_time"      =>  "",
                "search_type"   =>  "",
                "criteria"      =>  ["email" => $customer_email],
                "formType"      =>  "order_find",
                "formMethod"    =>  "POST"
            ];

            $orders = $this->processEndPoint($filters);
     
            if(isset($orders) && isset($orders['responseData']) && isset($orders['responseData']['order_id']) && !empty($orders['responseData']['order_id'])){
                $orders_ids = $orders['responseData']['order_id'];
            }
            
            return $orders_ids;
            
        } catch(\Exception $ex){
            $this->pushErrorLog($ex);
            return [];
        }
    }


    // get order details using order id
    public function getOrderDetails($order_ids){
        try{

            $filters = [
                "order_id"      =>  $order_ids,
                "formType"      =>  "order_view",
                "formMethod"    =>  "POST"
            ];

            $orderDetails = $this->processEndPoint($filters);
        
            if(isset($orderDetails) && isset($orderDetails['responseData'])){
                return $orderDetails['responseData'];
            }else{
                return null;
            }
            
        } catch(\Exception $ex){
            return null;
        }
    }

}
