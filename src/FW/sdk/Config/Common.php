<?php

/**
 * Common helper class for providing the common function like date conversion  
 * Data manipulation etc....
 *
 * @copyright  2019 SketchBrain 
 * @license    SketchBrain   FW SDK License 1.0.0
 * @version    Release: V 1.0.0
 * @link       N/A
 * @since      Class available since Release 1.0.0
 */

namespace App\FW\sdk\Config;

use App\Controller\JWTController;
use Exception;
use InvalidArgumentException;
use Symfony\Component\Yaml\Yaml;

define("DATE_FORMAT", "m/d/Y");
class Common
{
    protected $validatorPath;
    protected $dbName;
    public $dbInstance;
    public $request;
    public $requestBody;

    public function __construct()
    {

        $this->curlConfig = [];
        $this->validatorPath = __DIR__ . '../../../validate.json';
        $this->crmConfigPath =  __DIR__ . '/../../config.yaml';
        $this->dbName = 'hosted_checkout';
        $this->collection = 'checkouts';
        $this->productCollection = 'products';
        $this->upsellsCollection = 'upsells';
        $this->landersCollection = 'landers';

        $this->mongoUSName = '';
        $this->mongoPassword = '';

        $this->requestBody  = $this->requestProcessBody();
        $body = $this->requestBody; //json_decode($this->request->getBody()->getContents(),true);
        $this->collectionConfigHash = $body['X-LL-Hash'] ?? $body['X-LL-Hash']; //'5e0adc12c0c6ac447560f4a7';

        if (empty($this->collectionConfigHash) && isset($body['store'])) {
            $this->collectionConfigHash = "slug=" . $body['store'];
        }
    }

    public function requestProcessBody()
    {
        try {
            $header = $this->request->getHeader('Content-Type'); //getParsedBody();
            $contentType = isset($header[0]) ? $header[0] : '';
            $body = [];

            if (stripos($contentType, 'json') > -1) {
                //json header
                $body = json_decode($this->request->getBody()->getContents(), true);
                $body['isJson'] = true;
            } else {
                $body = $this->request->getParsedBody();
                $body['products'] = isset($body['products']) ? json_decode($body['products'], true) : [];
                $body['oneClickUpsells'] = isset($body['oneClickUpsells']) ? json_decode($body['oneClickUpsells'], true) : [];
                $body['isJson'] = false;
            }

            $params = $this->request->getQueryParams();
            $body = !empty($params) ? array_merge($body, $params) : $body;

            if (!empty($body)) {
                $this->request->getBody()->rewind(true);
            }

            return  $body;
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function dbConnect()
    {
        try {

            $user = $this->mongoUSName;
            $pwd = $this->mongoPassword;
            $conString = $_ENV['MONGO_CON']; //"mongodb://localhost:27017";
            // if(!empty($user) && !empty($pwd)){
            //     $conString = "mongodb://${user}:${pwd}@127.0.0.1:27017";
            // }

            $con = new \MongoDB\Client($conString);
            $this->dbInstance = $con->{$this->dbName};
            return $this->dbInstance;
        } catch (\Exception $ex) {
            throw new \Exception("Something went wrong while connecting the instance");
        }
    }

    public function importCollectionConfig()
    {
        try {

            if (empty($this->collectionConfigHash)) {
                throw new \Exception("Invalid 'X-ll-Hash' header, Please try again later", 202);
            }

            //1. Get collection config

            // check if URL contians login token, then set it into object
            @parse_str($this->collectionConfigHash, $param);
            if(isset($param['token'])){
                $this->requestBody['token'] = $param['token'];
                // using login token extract details from it like email, freeShipping, etc.
                $this->getUserDetailsFromToken();
            }
 
            //1.0 Get the config on the basis of store selection
            $data = $this->getConfirgForMagento();
            $data = empty($data) ? $this->getConfirgForShopify() : $data; //for shopify
            $data = empty($data) ? $this->getDynamicProductsConfiguration() : $data;

            $productDetail = [];
            if (!$data) {
                //Hosted checkout
                $data = $this->dbInstance->{$this->collection}->findOne(array(
                    // '_id' => new \MongoDB\BSON\ObjectID($this->collectionConfigHash)
                    'checkout_label_url' => $this->collectionConfigHash,
                    'checkout_deleted' => 0
                ));


                if (!$data) {
                    throw new \Exception("Invalid CRM selection, Please connect with support", 202);
                }

                //2. Get config product

                $products = [];
                $crmProductsId = [];
                $data['checkout_products'] = isset($data['checkout_products']) && !empty($data['checkout_products']) ? (array) $data['checkout_products'] : [];


                if (is_array($data['checkout_products']) && !empty($data['checkout_products'])) {
                    $crmProductsId = [];
                    foreach ($data['checkout_products'] as $key => $val) {
                        $detail = $this->dbInstance->products->findOne(['_id' => new \MongoDB\BSON\ObjectID($val)]);

                        if (!$detail) {
                            continue;
                        }

                        //unset product_body_html for resolving newrelic issue
                        if (isset($detail['product_body_html'])) {
                            unset($detail['product_body_html']);
                        }
                        //End of newrelic issue

                        //apply discount
                        if (isset($detail->product_discount_price) && $detail->product_discount_price > 0) {
                            $detail->price = $detail->product_discount_price;
                        }

                        $product_img_url = isset($detail['product_img_url']) ? $detail['product_img_url'] : '';

                        if (!empty($product_img_url) && !filter_var($product_img_url, FILTER_VALIDATE_URL)) {

                            $detail['product_img_url'] = $_ENV['ll_MEDIA_URL'] . 'uploads/products/' . $product_img_url;

                            if (!empty($product_img_url) && !filter_var($product_img_url, FILTER_VALIDATE_URL)) {

                                $detail['product_img_url'] = $_ENV['ll_MEDIA_URL'] . 'uploads/products/' . $product_img_url;
                            }

                            $detail['quantity'] = $this->getCartQuantity($val);
                            $products[$val] =  $detail; //$this->dbInstance->products->findOne(['_id'=>new \MongoDB\BSON\ObjectID($val)]);
                        }

                        $detail['quantity'] = $this->getCartQuantity($val);
                        $products[$val] =  $detail; //$this->dbInstance->products->findOne(['_id'=>new \MongoDB\BSON\ObjectID($val)]);
                        $products[$val]['variant_id'] = (isset($detail['shopify_variant_id']) && $detail['shopify_variant_id'] != NULL) ? $detail['shopify_variant_id'] : ""; //Need to update that value on the basis of shopify import value
                        $crmProductsId[$val] = isset($detail['product_crm_id']) ? $detail['product_crm_id'] : '';
                    }
                }
                $data->products = $products;
                $data->processUpsell = true;
                $data->crmProductsId = $crmProductsId;
            }

            //check store onLine offLine
            if ($data &&  !$data->checkout_online_status) {
                throw new \Exception("Store is not available, please try again later ", 202);
            }

            $ll_product = isset($this->requestBody['ll_product']) ? $this->requestBody['ll_product'] : '';
            if (!empty($ll_product)) {
                $productDetail = $this->dbInstance->{$this->productCollection}->findOne(['_id' => new \MongoDB\BSON\ObjectID($ll_product)]);
            }

            //3. Get upsell product 
            // $data->upsells = $this->dbInstance->{$this->upsellsCollection}->findOne(['upsell_checkout_id'=>(string)$data->_id]);

            //4. get all country
            $countrys = [];
            $checkout_countries = isset($data['checkout_countries']) && !empty($data['checkout_countries']) ? (array) $data['checkout_countries'] : [];
            $checkout_default_country = $data['checkout_default_country'] ?? 'United States';

            if (!empty($checkout_countries)) {
                foreach ($checkout_countries as $key => $val) {
                    $countrys[] = $this->dbInstance->countries->findOne(['name' => $val])->iso2;
                }
            }

            $data->checkout_default_country  = $this->dbInstance->countries->findOne(['name' => $checkout_default_country])->iso2;
            $data->countries = $countrys;

            $checkout_ll_creds_id = isset($data->checkout_ll_creds_id) && !empty($data->checkout_ll_creds_id) ? $data->checkout_ll_creds_id : '';

            if (empty($checkout_ll_creds_id)) {
                throw new \Exception("Store is not available, please try again later ", 202);
            }

            //
            $llStoreDetail = $this->dbInstance->store_credentials->findOne(['_id' => new \MongoDB\BSON\ObjectID($data->checkout_ll_creds_id), "store_credential_deleted" => 0]);

            if (empty($llStoreDetail)) {
                throw new \Exception("Invalid store selection");
            }

            $data->checkout_crm_admin = $llStoreDetail->store_name;
            $data->checkout_api_username = $llStoreDetail->store_api_key;
            $data->checkout_api_password = $llStoreDetail->store_api_password;

            $validator = json_decode(file_get_contents($this->validatorPath), true);

            return [
                "crmConfig" => $data,
                "validator" => $validator['LimeLight'],
                "configProduct" => $productDetail
            ];
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage(), 202);
        }
    }

    public function manageDate()
    {
        // busincess logic
    }

    /**
     * Import or INIT CRM,Validator setting   
     * @param Request NULL
     * 
     * @throws File_Import_Exception If somthing happed wrong while importinh the config file   
     * @author Sketchbrain TL
     * @return Array Having the value of crmConfig and validator
     */
    public function importConfig()
    {
        try {
            //import form validator
            $validator = json_decode(file_get_contents($this->validatorPath), true);
            $crmConfig = Yaml::parseFile($this->crmConfigPath); //import config detail

            $crmType = isset($crmConfig['crmType']) ? $crmConfig['crmType'] : '';

            if (empty($crmType)) {
                throw new \Exception("Invalid CRM selection, Please contact your site admin");
            }

            return [
                "crmConfig" => $crmConfig,
                "validator" => $validator[$crmType]
            ];
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function getConfirgForShopify()
    {
        try {
            $param = [];
            $crmProductsId = [];
            @parse_str($this->collectionConfigHash, $param);

            $checkout_store_url = isset($param['slug']) ? $param['slug'] : '';
            $paramProducts = isset($param['products']) ? $param['products'] : '';
            $quantity = isset($param['quantity']) ? explode(',', $param['quantity']) : [];
            $paramVariant = isset($param['variant']) ? explode(',', $param['variant']) : [];
            $billingIds = isset($param['billing_id']) && !empty($param['billing_id']) ? explode(',', $param['billing_id']) : [];
            $ref_coupon = isset($param['ref_coupon']) ? $param['ref_coupon'] : '';
            $coupon_code = isset($param['coupon_code']) ? $param['coupon_code'] : '';
            $coupon_code_e = isset($param['coupon_code_e']) ? $param['coupon_code_e'] : '';
            $removed_items = isset($this->requestBody['removed_items']) ? $this->requestBody['removed_items'] : [];
            $porducts = isset($this->requestBody['porducts']) ? $this->requestBody['porducts'] : [];


            if (empty($checkout_store_url)) {
                return false;
            }

            $paramProducts = explode(',', $param['products']);
            $type = isset($param['type']) ? $param['type'] : '1';

            //get the store detail from store colection
            $store = $data = $this->dbInstance->store_credentials->findOne(array(
                'store_name' => $checkout_store_url,
                'store_credential_deleted' => 0,
                'store_type' => 2
            ));

            if (empty($store)) {
                $store = $data = $this->dbInstance->store_credentials->findOne(array(
                    'store_custom_domain' => $checkout_store_url,
                    'store_credential_deleted' => 0,
                    'store_type' => 2
                ));
            }

            $storeId = (isset($store->_id) ? (string) $store->_id : null);



            if (empty($storeId)) {
                throw new \Exception("Invalid store selection");
            }

            $data = $this->dbInstance->{$this->collection}->findOne(array(
                'checkout_store_url' => $storeId,
                'checkout_deleted' => 0
            ));

            if (empty($data)) {
                throw new \Exception("Invalid selection");
            }

            $checkout_createdBy = $data->checkout_createdBy;
            $products = [];
            $quantityIndex = 0;
            $offers = [];
            if (is_array($paramProducts) && !empty($paramProducts)) {
                foreach ($paramProducts as $key => $val) {
                    $producVariants = isset($paramVariant[$key]) ? explode('-', $paramVariant[$key]) : [];
                    $processBillingId = isset($billingIds[$key]) ? $billingIds[$key] : '';

                    $detail = $this->dbInstance->products->findOne(['product_shopify_id' => $val, 'product_createdBy' => $checkout_createdBy]);
                    if (!$detail) {
                        continue;
                    }

                    if (in_array($val, $removed_items)) {
                        continue;
                    }

                    //unset product_body_html for resolving newrelic issue
                    if (isset($detail->product_body_html)) {
                        unset($detail->product_body_html);
                    }
                    //End of newrelic issue

                    //apply discount
                    if (isset($detail->product_discount_price) && $detail->product_discount_price > 0) {
                        $detail->price = $detail->product_discount_price;
                    }

                    $product_img_url = isset($detail['product_img_url']) ? $detail['product_img_url'] : '';


                    if (!empty($product_img_url) && !filter_var($product_img_url, FILTER_VALIDATE_URL)) {

                        $detail['product_img_url'] = $_ENV['ll_MEDIA_URL'] . 'uploads/products/' . $product_img_url;
                    }

                    $variants = isset($detail->variants) ? (array) $detail->variants : [];
                    $product_trial = isset($detail['product_trial']) ? $detail['product_trial'] : false;
                    $trial_main_subscription_product = isset($detail['trial_main_subscription_product']) ? $detail['trial_main_subscription_product'] : "";
                    $shopify_variants_to_sticky_relation = isset($detail['shopify_variants_to_sticky_relation']) ? $detail['shopify_variants_to_sticky_relation'] : [];


                    //for variant process
                    if (isset($variants) && !empty($variants) && !empty($producVariants)) {
                        $processVariantBilling = !empty($processBillingId) ? explode('-', $processBillingId) : [];
                        $copydetail = (array) $detail;
                        $variantProcess = false;
                        foreach ($producVariants as $variantKey => $varantVal) {

                            $processVariant = isset($variants[$varantVal]) ? $variants[$varantVal] : [];
                            $crm_variant_id =  isset($processVariant['id']) ? $processVariant['id'] : "";
                            if (empty($processVariant)) {
                                continue;
                            }

                            $billing_id = isset($processVariantBilling[$variantKey]) ? $processVariantBilling[$variantKey] : '';

                            $variantProcess = true;
                            $products[$varantVal] = [];
                            $products[$varantVal] = $copydetail;

                            $products[$varantVal]['product_billing_model_id'] = isset($billing_id) && !empty($billing_id) ? $billing_id : (isset($copydetail['product_billing_model_id']) && !empty($copydetail['product_billing_model_id']) ? $copydetail['product_billing_model_id'] : '');



                            $products[$varantVal]['variants'] = [];
                            $products[$varantVal]['crm_variant_id'] = $crm_variant_id;
                            $products[$varantVal]['variant_id'] = $varantVal;
                            $products[$varantVal]['quantity'] = isset($quantity[$quantityIndex]) ? $quantity[$quantityIndex] : 1;
                            $products[$varantVal]['attributes'] = isset($variants[$varantVal]->attributes) ? $variants[$varantVal]->attributes : [];

                            if (isset($processVariant->price) && !empty($processVariant->price)) {
                                $products[$varantVal]['price'] = $processVariant->price;
                            }
                            //process for prodcut variant image
                            if (isset($processVariant->product_img_url) && !empty($processVariant->product_img_url) && filter_var($processVariant->product_img_url, FILTER_VALIDATE_URL)) {
                                $products[$varantVal]['product_img_url'] = $processVariant->product_img_url;
                            }

                            $quantityIndex = $quantityIndex + 1;
                            $variant = [];
                            foreach ($products[$varantVal]['attributes'] as $attrKey => $attrVal) {
                                $attribute_name = isset($attrVal['attribute']['name']) ? $attrVal['attribute']['name'] : '';
                                $attribute_value = isset($attrVal['attribute']['option']['name']) ? $attrVal['attribute']['option']['name'] : '';
                                if (!empty($attribute_name) && !empty($attribute_value)) {
                                    $variant[] = [
                                        'attribute_name' => $attribute_name,
                                        'attribute_value' => $attribute_value
                                    ];
                                }
                            }

                            $crmProductsId[$varantVal] = isset($products[$varantVal]['product_crm_id']) && !empty($products[$varantVal]['product_crm_id']) ? $products[$varantVal]['product_crm_id'] : '';

                            $tempOffer = [
                                'product_id' => isset($products[$varantVal]['product_crm_id']) && !empty($products[$varantVal]['product_crm_id']) ? $products[$varantVal]['product_crm_id'] : '',
                                'offer_id' =>  isset($copydetail['product_offer_id']) && !empty($copydetail['product_offer_id']) ? $copydetail['product_offer_id'] : '',
                                'price' =>  isset($products[$varantVal]['price']) && !empty($products[$varantVal]['price']) ? $products[$varantVal]['price'] : '',
                                'quantity' => isset($products[$varantVal]['quantity']) ? $products[$varantVal]['quantity'] : 1,
                                'variant' => $variant,
                                'shopify_variant_id' => $varantVal,
                                'shopify_variants_to_sticky_relation' => $shopify_variants_to_sticky_relation
                            ];

                            $products[$varantVal]['variant_product_crm_id'] = isset($shopify_variants_to_sticky_relation[$varantVal]['product_crm_id']) ? $shopify_variants_to_sticky_relation[$varantVal]['product_crm_id'] : "";


                            if (isset($products[$varantVal]['product_billing_model_id']) && !empty($products[$varantVal]['product_billing_model_id'])) {
                                $tempOffer['billing_model_id'] = $products[$varantVal]['product_billing_model_id'];
                            }

                            //for trial

                            if ($product_trial) {
                                $tempOffer['trial'] = [
                                    'product_id' => isset($products[$varantVal]['product_crm_id']) && !empty($products[$varantVal]['product_crm_id']) ? $products[$varantVal]['product_crm_id'] : '',
                                    'variant' => $variant,
                                    'trial_main_subscription_product' => $trial_main_subscription_product
                                ];
                            }

                            // Validate removed_items
                            if (in_array($tempOffer['shopify_variant_id'], $removed_items)) {
                                continue;
                            }

                            $offers[] = $tempOffer;
                        }

                        if ($variantProcess == true) {
                            continue;
                        }
                    }

                    //end of process variant

                    $detail['quantity'] = isset($quantity[$quantityIndex]) ? $quantity[$quantityIndex] : 1;
                    $quantityIndex = $quantityIndex + 1;
                    $detail['product_billing_model_id'] = !empty($processBillingId) && strpos($processBillingId, '-') == false ? $processBillingId : (isset($detail['product_billing_model_id']) && !empty($detail['product_billing_model_id']) ? $detail['product_billing_model_id'] : '');

                    $products[$val] =  $detail;
                    $products[$val]['variant_id'] = (isset($detail['shopify_variant_id']) && $detail['shopify_variant_id'] != NULL) ? $detail['shopify_variant_id'] : "";
                    $crmProductsId[$val] = isset($detail['product_crm_id']) ? $detail['product_crm_id'] : '';
                    $tempOffer = [
                        'product_id' => isset($detail['product_crm_id']) && !empty($detail['product_crm_id']) ? $detail['product_crm_id'] : '',
                        'offer_id' =>  isset($detail['product_offer_id']) && !empty($detail['product_offer_id']) ? $detail['product_offer_id'] : '',
                        'price' =>  isset($detail['price']) && !empty($detail['price']) ? $detail['price'] : '',
                        'quantity' => isset($detail['quantity']) ? $detail['quantity'] : 1,
                    ];

                    if (isset($detail['product_billing_model_id']) && !empty($detail['product_billing_model_id'])) {
                        $tempOffer['billing_model_id'] = $detail['product_billing_model_id'];
                    }

                    if ($product_trial) {
                        $tempOffer['trial'] = [
                            'product_id' => isset($detail['product_crm_id']) && !empty($detail['product_crm_id']) ? $detail['product_crm_id'] : '',
                            'trial_main_subscription_product' => $trial_main_subscription_product
                        ];
                    }

                    // Validate removed_items
                    if (in_array($tempOffer['shopify_variant_id'], $removed_items)) {
                        continue;
                    }

                    $offers[] = $tempOffer;
                }
                // $detail['quantity'] = isset($quantity[$key]) ? $quantity[$key] : 1;
                // $products[$val] =  $detail;
                // $crmProductsId[$val] = isset($detail['product_crm_id']) ? $detail['product_crm_id'] : '';
            }

            $data->shopifyStore = $store;
            $data->products = $products;
            $data->offers = $offers;
            $data->crmProductsId = $crmProductsId;
            $data->processUpsell = false;
            $data->coupon_code = $coupon_code;
            $data->ref_coupon = $ref_coupon;
            $data->coupon_code_e = $coupon_code_e;
            $data->refrence_discount = $this->getRefrenceCouponDiscount(["crmConfig" => $data], $this->requestBody);

            return $data;
        } catch (\Exception $ex) {
            return false;
        }
    }


    public function getUpsellConfig()
    {
        try {
            $_id = isset($this->config['crmConfig']->_id) ? $this->config['crmConfig']->_id : '';
            $upsells = $this->dbInstance->upsells->findOne(['upsell_checkout_id' => (string)$_id, "upsell_deleted" => 0]);
            // $upsells = isset($this->config['crmConfig']['upsells']['upsell_product_ids']) && is_array($this->config['crmConfig']['upsells']['upsell_product_ids']) ? iterator_to_array($this->config['crmConfig']['upsells']['upsell_product_ids']) : [];
            $products = [];


            if (empty($upsells)) {
                throw new \Exception('Invalid upsell selection');
            }

            $upsell_code_templates = isset($upsells->upsell_code_templates) ? $upsells->upsell_code_templates : [];
            $upsell_product_ids = isset($upsells->upsell_product_ids) && !empty($upsells->upsell_product_ids) ? (array) $upsells->upsell_product_ids : [];

            foreach ($upsell_product_ids as $key => $val) {

                $detail = $this->dbInstance->products->findOne(['_id' => new \MongoDB\BSON\ObjectID($val)]);

                if (empty($detail)) {
                    continue;
                }

                //unset product_body_html for resolving newrelic issue
                if (isset($detail['product_body_html'])) {
                    unset($detail['product_body_html']);
                }
                //End of newrelic issue

                $detail->_id = (string) $detail->_id;
                $product_img_url = isset($detail['product_img_url']) ? $detail['product_img_url'] : '';
                if (!empty($product_img_url) && !filter_var($product_img_url, FILTER_VALIDATE_URL)) {

                    $detail['product_img_url'] = $_ENV['ll_MEDIA_URL'] . 'uploads/products/' . $product_img_url;
                }

                $html = isset($upsell_code_templates[$key]) ? $upsell_code_templates[$key] : '';

                $detail->template = $this->upsellTemplateView($detail, $html);
                $products[] =  $detail;
            }

            if (!empty($products)) {
                $this->config['crmConfig']['upsells']['products'] = $products;
                $this->config['crmConfig']['upsells']['totalUpsell'] = count($products);
                $this->config['crmConfig']['upsells']['status'] = true;
                $this->config['crmConfig']['upsells']['message'] = '';
            }


            return $this->config['crmConfig']['upsells'];
        } catch (\Exception $ex) {
            // return [];
            throw new \Exception($ex->getMessage());
        }
    }

    //funtion trying to get the product quantity 
    public function getCartQuantity($_id = '')
    {
        try {
            if (empty($_id)) {
                throw new \Exception("invalid product selection");
            }
            return (isset($this->requestBody['products'][$_id]['quantity']) && !empty($this->requestBody['products'][$_id]['quantity']) ? $this->requestBody['products'][$_id]['quantity'] : 1);
        } catch (\Exception $ex) {
            return 1;
            // throw new \Exception("Invalid selection");
        }
    }


    public function upsellTemplateView($data, $html)
    {
        try {
            //
            $defaultConfig =[
                "token"=>[
                    '[product_name]'=>'product_name',
                    '[product_price]'=>'price',
                    '[add_to_order_btn]'=>'<button class="btn-primary" id="process-upsell">Add To My Order</button>',
                    '[no_thankyou_link]'=>'<a class="btn btn-default btn-sm btn-block no-thank-you-a" id="no-thanks">No Thanks!</a>',
                    '[product_image_src]'=>'product_img_url'
                ],
                "html"=>'<meta name="viewport" content="width=device-width, initial-scale=1.0" /> <style>.no-thank-you-a{cursor: pointer; text-decoration: underline;}.product-image{width:30%}.product-content{width:70%}.product-content,.product-image{float:left;padding:0 15px;box-sizing:border-box}.container{max-width:1170px;padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto}.product-image img{width:100%;border:1px solid #ddd;margin-bottom:20px}h2.product-title{margin:0}.text-center{text-align:center}.footer{width:100%;float:left;bottom:0;position:fixed;background:#fff;text-align:center}.product-price{font-size:18px}.btn-primary{padding:8px 12px;background:#242527;color:#fff;cursor:pointer;font-size:18px;text-decoration:none;border-radius:.2rem;margin-bottom:20px;display:inline-block;position:relative}description-p{line-height:1.6;letter-spacing:.5px;color:#666}.product-wrapper{width:100%;float:left;padding:30px 0}.tooltip{position:relative;display:block}.tooltiptext{display:none;background-color:#000;color:#fff;text-align:center;border-radius:6px;padding:5px;position:absolute;z-index:1}.tooltip:hover+.tooltiptext{display:block}@media (max-width:767px){.product-content,.product-image{width:100%;float:left}}h2{width:300px;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;padding:5px;font-size:1.3rem;margin:0;background:#fff;resize:none}</style><div class="container"><div class="product-wrapper"><div class="product-image"> <img src="[product_image_src]"></div><div class="product-content"><h2 class="tooltip">[product_name]</h2> <span class="tooltiptext">[product_name]</span><p class="product-price">$ [product_price]</p><form name="is-upsell" class="is-upsell"> <input type="hidden" name="prospectId" value="">[add_to_order_btn]</form><div> <a class="no-thank-you-p"> [no_thankyou_link] </a></div></div></div></div>'
            ];

            $defaultConfig["html"] = !empty($html) ? $html : $defaultConfig["html"];
            $defaultConfig["html"] = str_replace('<body ', '<div ', $defaultConfig["html"]);
            $defaultConfig["html"] = str_replace('</body>', '</div>', $defaultConfig["html"]);
            $productNoImage = isset($_SERVER['HTTP_HOST']) ? "//".$_SERVER['HTTP_HOST'].'/images/no-product-image.png':'';
            
            foreach($defaultConfig["token"] as $key=>$val){
                $tempVal = (isset($data->{$val}) && !empty($data->{$val})  ? $data->{$val}: $val );
                if($val == 'product_img_url' && ($tempVal == 'product_img_url' || empty($tempVal))){
                    $tempVal = $productNoImage;
                }

                if ($val == 'price' && !empty($tempVal)) {
                    $tempVal = number_format($tempVal, 2);
                }

                $defaultConfig["html"] = str_replace($key, $tempVal, $defaultConfig["html"]);
            }

            return $defaultConfig["html"];
        } catch (\Exception $ex) {
            return $html;
        }
    }

    public function getDynamicProductsConfiguration()
    {
        try {
            $param = [];
            $crmProductsId = [];
            $offers = [];
            @parse_str($this->collectionConfigHash, $param);

            $hash = array_key_first($param);
            $lander_hash    = isset($param['lander_hash']) ? $param['lander_hash'] : null;
            $paramProducts  = isset($param['products']) ? explode(',', $param['products']) : [];
            $paramVariant   = isset($param['variant']) ? explode(',', $param['variant']) : [];
            $billingModels  = isset($param['billing_id']) ? explode(',', $param['billing_id']) : [];
            $quantity       = isset($param['quantity']) ? explode(',', $param['quantity']) : [];
            $removed_items  = isset($this->requestBody['removed_items']) ? $this->requestBody['removed_items'] : [];

            //validate hash
            if (empty($hash)) {
                throw new \Exception("Invalid hash");
            }

            //validate param product


            //Hosted checkout
            $data = $this->dbInstance->{$this->collection}->findOne(array(
                'checkout_label_url' => $hash,
                'checkout_deleted' => 0
            ));
            //find checkout created by and get its product from crm 
            $checkout_created_by = isset($data['checkout_createdBy']) ? $data['checkout_createdBy'] : '';

            //validate checkout 
            if (!$data) {
                return false;
            }

            if (empty($paramProducts)) {
                $paramProducts = isset($data['checkout_products']) ? (array) $data['checkout_products'] : [];
            }


            $lander_data = [];

            //Lander Data checkout
            if ($lander_hash) {
                $lander_data = $this->dbInstance->{$this->landersCollection}->findOne(array(
                    'slug' => $lander_hash,
                    'lander_createdBy' => $checkout_created_by,
                    'lander_deleted' => 0
                ));
                //override offer and campaing id from lander
                if (isset($lander_data) && $lander_data['lander'] && !empty($lander_data['lander'])) {
                    $lander_camp_id = isset($lander_data['lander']['campaign_id']) && $lander_data['lander']['campaign_id'] > 0 ? $lander_data['lander']['campaign_id'] : $data['checkout_campaign_id'];
                    $lander_offer_id = isset($lander_data['lander']['offer_id']) && $lander_data['lander']['offer_id'] > 0 ? $lander_data['lander']['offer_id'] : $data['checkout_offer_id'];
                    $lander_shipping_ids = isset($lander_data['lander']['shipping_ids']) && count($lander_data['lander']['shipping_ids']) > 0 ? $lander_data['lander']['shipping_ids'] : $data['checkout_crm_shipping_id'];

                    //get products shipping id, if its there then overide with lander shipping ids
                    //$paramProducts
                    $lander_products = $lander_data['lander']['products'];
                    $hco_prod = $paramProducts[0]; // we taking only first product as the feature is buy now so each time one product will be there
                    // when cart feature is there we have to change this logic
                    foreach ($lander_products as $prod) {
                        if ($prod['crm_id'] == $hco_prod || $prod['id'] == $hco_prod) {
                            // match for product crm id OR product mongo id
                            if (isset($prod['shipping_id']) && trim($prod['shipping_id']) != '') {
                                //check if product level shipping id is present
                                $lander_shipping_ids = array($prod['shipping_id']);
                            }
                        }
                    }

                    $data['checkout_is_lander'] = true;
                    $data['checkout_campaign_id'] = $lander_camp_id;
                    $data['checkout_offer_id'] = $lander_offer_id;
                    $data['checkout_crm_shipping_id'] = isset($lander_shipping_ids) && count($lander_shipping_ids) > 0 ? $lander_shipping_ids : [];
                }
            }


            //manage Variant object
            $variants = [];

            foreach ($paramVariant as $varKey => $varVal) {
                $varVal = str_replace(" ", "_", $varVal);
                if (!isset($param[$varVal])) {
                    continue;
                }
                $variants[$varVal] = !empty($param[$varVal]) ? explode(',', $param[$varVal]) : [];
            }


            $products = [];
            $crmProductsId = [];

            if (is_array($paramProducts) && !empty($paramProducts)) {
                foreach ($paramProducts as $key => $val) {

                    //validate the remove item
                    if (in_array($val, $removed_items)) {
                        continue;
                    }

                    //checking manually if given string is mongo (24 char long) or crm id(usually 4-5 char long)
                    $detail = null;
                    if (strlen($val) < 10) {
                        $detail = $this->dbInstance->products->findOne([
                            'product_crm_id' => array('$in' => array((int) $val, strval($val))),
                            'product_createdBy' => $checkout_created_by
                        ]);
                        // update $val, as it now contains crm id, we need _id. as it creating issue in remove product feature
                        if($detail){
                            $val = strval($detail['_id']);
                        }
                    } else {
                        $detail = $this->dbInstance->products->findOne([
                            '_id' => new \MongoDB\BSON\ObjectID($val)
                        ]);
                    }
                    if (!$detail) {
                        continue;
                    }
                    $product_trial = isset($detail['product_trial']) ? $detail['product_trial'] : false;
                    $trial_main_subscription_product = isset($detail['trial_main_subscription_product']) ? $detail['trial_main_subscription_product'] : "";

                    //unset product_body_html for resolving newrelic issue
                    if (isset($detail['product_body_html'])) {
                        unset($detail['product_body_html']);
                    }
                    //End of newrelic issue

                    //apply discount
                    if (isset($detail->product_discount_price) && $detail->product_discount_price > 0) {
                        $detail->price = $detail->product_discount_price;
                    }


                    $product_img_url = isset($detail['product_img_url']) ? $detail['product_img_url'] : '';

                    if (!empty($product_img_url) && !filter_var($product_img_url, FILTER_VALIDATE_URL)) {
                        $detail['product_img_url'] = $_ENV['ll_MEDIA_URL'] . 'uploads/products/' . $product_img_url;
                    }
                    // logic to retrive billing and offer ids
                    // if checkout is lander based then take offer id from lander and billing from parameter
                    // if checkout is liked based then check if product has both things
                    // if either thing missing then take from checkout
                    
                    // 1. offer id
                    $prod_offer_id          = null;
                  
                    // offer id from product level 
                    if(isset($detail) && isset($detail['product_offer_id']) && $detail['product_offer_id'] > 0){
                        $prod_offer_id      = $detail['product_offer_id'];
                    }

                    // if product level empty then check on checkout level
                    if(isset($data) && isset($data['checkout_offer_id']) && $data['checkout_offer_id'] > 0 && !$prod_offer_id){
                        $prod_offer_id      = $data['checkout_offer_id'];
                    }

                    // check on lander level, highest priority, for lander products only
                    if($lander_hash && isset($lander_data) && isset($lander_data['offer_id']) && $lander_data['offer_id'] > 0){
                        $prod_offer_id      = $lander_data['offer_id'];
                    }

                    $products[$val]['product_offer_id'] = $detail['product_offer_id'] = $prod_offer_id;

                    // 2.Billing id      
                    $prod_billing_id        = null;

                    // check on product level
                     if(isset($detail) && isset($detail['product_billing_model_id']) && $detail['product_billing_model_id'] > 0){
                        $prod_billing_id    = $detail['product_billing_model_id'];
                    }

                    // if product level empty then check on checkout level
                    if(isset($data) && isset($data['checkout_billing_model_id']) && $data['checkout_billing_model_id'] > 0 && !$prod_billing_id){
                        $prod_billing_id    = $data['checkout_billing_model_id'];
                    }

                    // if passed via parameter give priority to that
                    if(isset($billingModels[$key]) && !empty($billingModels[$key])){
                        $prod_billing_id    = $billingModels[$key];
                    }

                    $products[$val]['product_billing_model_id'] = $detail['product_billing_model_id'] = $prod_billing_id;

                    $detail['quantity'] = isset($quantity[$key]) && !empty($quantity[$key]) ? $quantity[$key] : $this->getCartQuantity($val);
                    $products[$val] =  $detail;
                    $products[$val]['variant_id'] = (isset($detail['shopify_variant_id']) && $detail['shopify_variant_id'] != NULL) ? $detail['shopify_variant_id'] : ""; //Need to update that value on the basis of shopify import value
                    $crmProductsId[$val] = isset($detail['product_crm_id']) ? $detail['product_crm_id'] : '';

                    $offerIndex = [
                        'product_id' => isset($products[$val]['product_crm_id']) && !empty($products[$val]['product_crm_id']) ? $products[$val]['product_crm_id'] : '',
                        'offer_id' => $products[$val]['product_offer_id'],
                        'price' =>  isset($products[$val]['price']) && !empty($products[$val]['price']) ? $products[$val]['price'] : '0.00',
                        'quantity' => isset($products[$val]['quantity']) ? $products[$val]['quantity'] : 1
                    ];

                    if (isset($products[$val]['product_billing_model_id']) && !empty($products[$val]['product_billing_model_id'])) {
                        $offerIndex['billing_model_id'] = $products[$val]['product_billing_model_id'];
                    }


                    $variant = $this->getDynamicProductVariant($variants, $key, $products[$val]);
                    if (!empty($variant)) {
                        $offerIndex['variants'] = isset($variant['offerVariant']) ? $variant['offerVariant'] : [];
                        $products[$val]['variants'] = isset($variant['offerVariant']) ? $variant['offerVariant'] : [];
                        $products[$val]['price'] = isset($variant['prodcutVariant']['price']) ? $variant['prodcutVariant']['price'] : $products[$val]['price'];
                        $products[$val]['variant_id'] = isset($variant['prodcutVariant']['shopify_variant_id']) ? $variant['prodcutVariant']['shopify_variant_id'] : $products[$val]['variant_id'];
                        $offerIndex['price'] = $products[$val]['price'];
                    }

                    if ($product_trial) {
                        $offerIndex['trial'] = [
                            'product_id' => isset($products[$val]['product_crm_id']) && !empty($products[$val]['product_crm_id']) ? $products[$val]['product_crm_id'] : '',
                            'trial_main_subscription_product' => $trial_main_subscription_product
                        ];

                        if (isset($offerIndex['variants'])) {
                            $offerIndex['trial']['variants'] = $offerIndex["variants"];
                        }
                    }

                    $offers[] = $offerIndex;
                }
            }

            $data->offers = $offers;
            $data->products = $products;
            $data->processUpsell = true;
            $data->crmProductsId = $crmProductsId;
            return $data;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /*retrun [
                                        'attribute_name'=> $attribute_name,
                                        'attribute_value'=>$attribute_value
                                    ];
    */
    protected function getDynamicProductVariant($variant, $productIndex, $prodcutDetails)
    {
        try {

            if (empty($variant)) {
                return [];
            }
            $productVariant = [];


            foreach ($variant as $key => $val) {

                if (isset($val[$productIndex]) && $val[$productIndex] != 'null') {
                    $productVariant[] = [
                        'attribute_name' => $key,
                        'attribute_value' => $val[$productIndex]
                    ];
                }
            }

            $prodcut_variants = isset($prodcutDetails->variants) && !empty($prodcutDetails->variants) ? $prodcutDetails->variants : [];
            $variantForProcess = [];
            if (!empty($prodcut_variants)) {
                foreach ($prodcut_variants as $vKey => $vVal) {
                    $attributes = isset($vVal['attributes']) ? $vVal['attributes'] : [];

                    if (empty($attributes)) continue;
                    $resultArray = []; // All the value should be true
                    foreach ($attributes as $attrKey => $attrVal) {
                        $llAttrName = isset($attrVal['attribute']['name']) ? $attrVal['attribute']['name'] : "";

                        $attrName = str_replace(" ", "_", $attrVal['attribute']['name']);
                        $attrValue = isset($attrVal['attribute']['option']['name']) ? $attrVal['attribute']['option']['name'] : "";

                        foreach ($productVariant as $pvKey => $pvVal) {
                            if ($pvVal['attribute_name'] == $attrName && $pvVal['attribute_value'] == $attrValue) {
                                $productVariant[$pvKey]['attribute_name'] = $llAttrName;
                                $resultArray[] = true;
                            } else {
                                $resultArray[] = false;
                            }
                        }
                    }

                    if (!empty($resultArray) && in_array(false, $resultArray) == false) $variantForProcess = $vVal;
                    break;
                }
            }

            return ["offerVariant" => $productVariant, 'prodcutVariant' => $variantForProcess];
        } catch (\Exception $ex) {
            return [];
        }
    }


    //targated upsell update
    public function getUpsellConfigTargated()
    {
        try {

            $_id = isset($this->config['crmConfig']->_id) ? $this->config['crmConfig']->_id : '';
            $upsells = $this->dbInstance->upsells->find(
                [
                    'upsell_checkout_id' => (string)$_id,
                    "upsell_deleted" => 0,
                    "upsell_online_status" => true,
                ],
                [
                    "sort" => ['enable_targeted_upsell' => -1]
                ]
            );
            $products = [];

            foreach ($upsells as $record) {

                $upsell_code_templates = isset($record->upsell_code_templates) ? $record->upsell_code_templates : [];
                $upsell_product_ids = isset($record->upsell_product_ids) && !empty($record->upsell_product_ids) ? (array) $record->upsell_product_ids : [];
                $enable_targeted_upsell = isset($record->enable_targeted_upsell) ? $record->enable_targeted_upsell : false;
                $upsell_targeted_product_ids = isset($record->upsell_targeted_product_ids) ? $record->upsell_targeted_product_ids : [];

                if (empty($upsell_product_ids)) {
                    continue;
                }

                foreach ($upsell_product_ids as $upKey => $upVal) {
                    $upVal = (array) $upVal;
                    $upsell_targeted_product_id = isset($upsell_targeted_product_ids[$upKey]) ? $upsell_targeted_product_ids[$upKey] : '';
                    $processStatus = $enable_targeted_upsell == true && !empty($upsell_targeted_product_id) ? $this->verifyProdcutForTargated($upsell_targeted_product_id) : true;

                    if ($processStatus != true) {
                        continue;
                    }

                    foreach ($upVal as $inKey => $inVal) {

                        $prodcutResponse = $this->getUpsellProductWithTemplate($inVal, $upKey, $upsell_code_templates);
                        if (!empty($prodcutResponse)) {
                            $prodcutResponse['upsell_id'] = (string) $record->_id;
                            $prodcutResponse['upsell_label'] = (string) $record->upsell_label;
                            $products[] = $prodcutResponse;
                        }
                    }
                }
            }

            if (!empty($products)) {
                $this->config['crmConfig']['upsells']['products'] = $products;
                $this->config['crmConfig']['upsells']['totalUpsell'] = count($products);
                $this->config['crmConfig']['upsells']['status'] = true;
                $this->config['crmConfig']['upsells']['message'] = '';
            }


            return $this->config['crmConfig']['upsells'];
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    /**
     * @param prodcutID String, templateIndex String
     * 
     * @throws [] throw empty string    
     * @author Sticky.io TL
     * @return Array Having the value with prodcut template
     */
    protected function getUpsellProductWithTemplate($productID, $templateIndex, $upsell_code_templates)
    {
        try {
            if (empty($productID)) {
                return [];
            }

            $detail = $this->dbInstance->products->findOne(['_id' => new \MongoDB\BSON\ObjectID($productID)]);

            if (empty($detail)) {
                return [];
            }

            $detail->_id = (string) $detail->_id;
            $product_img_url = isset($detail['product_img_url']) ? $detail['product_img_url'] : '';
            if (!empty($product_img_url) && !filter_var($product_img_url, FILTER_VALIDATE_URL)) {
                $detail['product_img_url'] = $_ENV['ll_MEDIA_URL'] . 'uploads/products/' . $product_img_url;
            }
            $html = isset($upsell_code_templates[$templateIndex]) ? $upsell_code_templates[$templateIndex] :'';
            $detail->is_default_template = (isset($html) && $html === '')? true : false;
            $detail->template = $this->upsellTemplateView($detail,$html);
            
            return $detail; 

            $detail->template = $this->upsellTemplateView($detail, $html);
            return $detail;
        } catch (\Exception $ex) {
            return [];
        }
    }


    protected function verifyProdcutForTargated($upsellTargatedProdcut)
    {
        try {

            $returnStatus = false;
            $checkoutProducts = isset($this->config['crmConfig']["products"]) ? $this->config['crmConfig']["products"] : [];
            if (empty($checkoutProducts)) {
                return $returnStatus;
            }

            foreach ($checkoutProducts as $proKey => $proVal) {

                if (((string) $proVal['_id']) == $upsellTargatedProdcut) {
                    $returnStatus = true;
                    break;
                }
            }

            return $returnStatus;
        } catch (\Exception $ex) {
            return false;
        }
    }

    public function getConfirgForMagento()
    {
        try {
            $param = [];
            $crmProductsId = [];
            @parse_str($this->collectionConfigHash, $param);

            $checkout_store_url = isset($param['slug']) ? $param['slug'] : '';
            $paramProducts = isset($param['products']) ? $param['products'] : '';
            $quantity = isset($param['quantity']) ? explode(',', $param['quantity']) : [];
            $paramVariant = isset($param['variant']) ? explode(',', urldecode($param['variant'])) : [];
            $billingIds = isset($param['billing_id']) && !empty($param['billing_id']) ? explode(',', $param['billing_id']) : [];
            $quote_id = isset($param['quote_id']) ? $param['quote_id'] : '';
            if (empty($checkout_store_url)) {
                return false;
            }

            $paramProducts = explode(',', $param['products']);
            $type = isset($param['type']) ? $param['type'] : '1';

            //get the store detail from store colection
            $store = $this->dbInstance->store_credentials->findOne(array(
                'store_name' => $checkout_store_url,
                'store_credential_deleted' => 0,
                'store_type' => 4

            ));

            if (empty($store)) {
                $store =  $this->dbInstance->store_credentials->findOne(array(
                    'store_custom_domain' => $checkout_store_url,
                    'store_credential_deleted' => 0,
                    'store_type' => 4
                ));
            }

            $storeId = (isset($store->_id) ? (string) $store->_id : null);



            if (empty($storeId)) {
                throw new \Exception("Invalid store selection");
            }

            $data = $this->dbInstance->{$this->collection}->findOne(array(
                'checkout_store_url' => $storeId,
                'checkout_deleted' => 0
            ));

            if (empty($data)) {
                throw new \Exception("Invalid selection");
            }

            $checkout_createdBy = $data->checkout_createdBy;
            $products = [];
            $quantityIndex = 0;
            $offers = [];
            if (is_array($paramProducts) && !empty($paramProducts)) {
                foreach ($paramProducts as $key => $val) {
                    $producVariants = isset($paramVariant[$key]) ? explode(')(', $paramVariant[$key]) : [];
                    $processBillingId = isset($billingIds[$key]) ? $billingIds[$key] : '';

                    $detail = $this->dbInstance->products->findOne(['reference_product_id' => (int) $val, 'product_createdBy' => $checkout_createdBy]);

                    if (!$detail) {
                        continue;
                    }

                    //apply discount
                    if (isset($detail->product_discount_price) && $detail->product_discount_price > 0) {
                        $detail->price = $detail->product_discount_price;
                    }

                    //unset product_body_html for resolving newrelic issue
                    if (isset($detail->product_body_html)) {
                        unset($detail->product_body_html);
                    }
                    //End of newrelic issue

                    $product_img_url = isset($detail['product_img_url']) ? $detail['product_img_url'] : '';


                    if (!empty($product_img_url) && !filter_var($product_img_url, FILTER_VALIDATE_URL)) {

                        $detail['product_img_url'] = $_ENV['ll_MEDIA_URL'] . 'uploads/products/' . $product_img_url;
                    }

                    $variants = isset($detail->variants) ? $detail->variants : [];
                    $product_trial = isset($detail['product_trial']) ? $detail['product_trial'] : false;
                    $trial_main_subscription_product = isset($detail['trial_main_subscription_product']) ? $detail['trial_main_subscription_product'] : "";


                    //for variant process
                    if (isset($variants) && !empty($variants) && !empty($producVariants)) {
                        $processVariantBilling = !empty($processBillingId) ? explode('-', $processBillingId) : [];
                        $copydetail = (array) $detail;
                        $variantProcess = false;
                        foreach ($producVariants as $variantKey => $varantVal) {

                            $processVariant = $this->getMagentoVariantBySku($variants, $varantVal); //isset($variants[$varantVal]) ? $variants[$varantVal] :[];
                            $varantVal = isset($processVariant['id']) ? $processVariant['id'] : $varantVal;
                            if (empty($processVariant)) {
                                continue;
                            }

                            $billing_id = isset($processVariantBilling[$variantKey]) ? $processVariantBilling[$variantKey] : '';

                            $variantProcess = true;
                            $products[$varantVal] = [];
                            $products[$varantVal] = $copydetail;

                            $products[$varantVal]['product_billing_model_id'] = isset($billing_id) && !empty($billing_id) ? $billing_id : (isset($copydetail['product_billing_model_id']) && !empty($copydetail['product_billing_model_id']) ? $copydetail['product_billing_model_id'] : '');



                            $products[$varantVal]['variants'] = [];
                            $products[$varantVal]['variant_id'] = $varantVal;
                            $products[$varantVal]['quantity'] = isset($quantity[$quantityIndex]) ? $quantity[$quantityIndex] : 1;
                            $products[$varantVal]['attributes'] = isset($processVariant['attributes']) ? $processVariant['attributes'] : [];

                            if (isset($processVariant['price']) && !empty($processVariant['price'])) {
                                $products[$varantVal]['price'] = sprintf('%0.2f', $processVariant['price']);
                            }
                            //process for prodcut variant image
                            if (isset($processVariant['product_img_url']) && !empty($processVariant['product_img_url']) && filter_var($processVariant['product_img_url'], FILTER_VALIDATE_URL)) {
                                $products[$varantVal]['product_img_url'] = $processVariant['product_img_url'];
                            }

                            $quantityIndex = $quantityIndex + 1;
                            $variant = [];
                            foreach ($products[$varantVal]['attributes'] as $attrKey => $attrVal) {
                                $attribute_name = isset($attrVal['attribute']['name']) ? $attrVal['attribute']['name'] : '';
                                $attribute_value = isset($attrVal['attribute']['option']['name']) ? $attrVal['attribute']['option']['name'] : '';
                                if (!empty($attribute_name) && !empty($attribute_value)) {
                                    $variant[] = [
                                        'attribute_name' => $attribute_name,
                                        'attribute_value' => $attribute_value
                                    ];
                                }
                            }

                            $crmProductsId[$varantVal] = isset($products[$varantVal]['product_crm_id']) && !empty($products[$varantVal]['product_crm_id']) ? $products[$varantVal]['product_crm_id'] : '';

                            $tempOffer = [
                                'product_id' => isset($products[$varantVal]['product_crm_id']) && !empty($products[$varantVal]['product_crm_id']) ? $products[$varantVal]['product_crm_id'] : '',
                                'offer_id' =>  isset($copydetail['product_offer_id']) && !empty($copydetail['product_offer_id']) ? $copydetail['product_offer_id'] : '',
                                'price' =>  isset($products[$varantVal]['price']) && !empty($products[$varantVal]['price']) ? sprintf('%0.2f', $products[$varantVal]['price']) : '',
                                'quantity' => isset($products[$varantVal]['quantity']) ? $products[$varantVal]['quantity'] : 1,
                                'variant' => $variant
                            ];

                            if (isset($products[$varantVal]['product_billing_model_id']) && !empty($products[$varantVal]['product_billing_model_id'])) {
                                $tempOffer['billing_model_id'] = $products[$varantVal]['product_billing_model_id'];
                            }

                            if ($product_trial) {
                                $tempOffer['trial'] = [
                                    'product_id' => isset($products[$varantVal]['product_crm_id']) && !empty($products[$varantVal]['product_crm_id']) ? $products[$varantVal]['product_crm_id'] : '',
                                    'variant' => $variant,
                                    'trial_main_subscription_product' => $trial_main_subscription_product
                                ];
                            }

                            $offers[] = $tempOffer;
                        }

                        if ($variantProcess == true) {
                            continue;
                        }
                    }

                    //end of process variant

                    $detail['quantity'] = isset($quantity[$quantityIndex]) ? $quantity[$quantityIndex] : 1;
                    $quantityIndex = $quantityIndex + 1;
                    $detail['product_billing_model_id'] = !empty($processBillingId) && strpos($processBillingId, '-') == false ? $processBillingId : (isset($detail['product_billing_model_id']) && !empty($detail['product_billing_model_id']) ? $detail['product_billing_model_id'] : '');

                    $products[$val] =  $detail;
                    $products[$val]['variant_id'] = (isset($detail['shopify_variant_id']) && $detail['shopify_variant_id'] != NULL) ? $detail['shopify_variant_id'] : "";
                    $crmProductsId[$val] = isset($detail['product_crm_id']) ? $detail['product_crm_id'] : '';
                    $tempOffer = [
                        'product_id' => isset($detail['product_crm_id']) && !empty($detail['product_crm_id']) ? $detail['product_crm_id'] : '',
                        'offer_id' =>  isset($detail['product_offer_id']) && !empty($detail['product_offer_id']) ? $detail['product_offer_id'] : '',
                        'price' =>  isset($detail['price']) && !empty($detail['price']) ? sprintf('%0.2f', $detail['price']) : '',
                        'quantity' => isset($detail['quantity']) ? $detail['quantity'] : 1,
                    ];

                    if (isset($detail['product_billing_model_id']) && !empty($detail['product_billing_model_id'])) {
                        $tempOffer['billing_model_id'] = $detail['product_billing_model_id'];
                    }

                    if ($product_trial) {
                        $tempOffer['trial'] = [
                            'product_id' => isset($detail['product_crm_id']) && !empty($detail['product_crm_id']) ? $detail['product_crm_id'] : '',
                            'trial_main_subscription_product' => $trial_main_subscription_product
                        ];
                    }

                    $offers[] = $tempOffer;
                }
                // $detail['quantity'] = isset($quantity[$key]) ? $quantity[$key] : 1;
                // $products[$val] =  $detail;
                // $crmProductsId[$val] = isset($detail['product_crm_id']) ? $detail['product_crm_id'] : '';
            }


            $data->magentoStore = $store;
            $data->products = $products;
            $data->offers = $offers;
            $data->crmProductsId = $crmProductsId;
            $data->processUpsell = false;
            $data->quote_id = $quote_id;

            return $data;
        } catch (\Exception $ex) {
            return false;
        }
    }

    protected function getMagentoVariantBySku($variantArray, $variantSky)
    {
        try {
            if (empty($variantArray) || empty($variantSky)) return [];

            $selectedVaraiant = [];
            foreach ($variantArray as $key => $val) {
                if ($val["sku_num"] == $variantSky) {
                    $selectedVaraiant = $val;
                    break;
                }
            }

            return $selectedVaraiant;
        } catch (\Exception $ex) {
            return [];
        }
    }

    protected function validateShopifyRefCoupon($res_coupon)
    {
        try {
            if (empty($res_coupon)) return [];
        } catch (\Exception $ex) {
            return [];
        }
    }

    public function getRefrenceCouponDiscount($config, $requestBody)
    {
        try {

            $shopifyResponse = (new \App\FW\sdk\Limelight\Order\Shopify($config, $requestBody))
                ->getCouponDiscouunt();
            return $shopifyResponse;
        } catch (\Exception $ex) {
            // discount_on => shipping/cart
            //discount_type => fixed / percentage
            return [
                "status" => false,
                "error" => $ex->getMessage(),
                "refrence" => "shopify",
                "discount_code" => "",
                "discount_on" => "",
                "discount_type" => "",
                "discount_amount" => 0
            ];
        }
    }

    public function getRedeemCouponsDetail($data){
        try{
            $couponDetails = []; 
            $redeemCoupons = isset($this->requestBody['redeemCoupons']) ?$this->requestBody['redeemCoupons'] : [];
            $checkout_id = isset($data['_id']) ? (string) $data['_id'] : "";
            if(empty($redeemCoupons) || empty($checkout_id)) return [];


            $checkout_smile_config = $this->dbInstance->checkout_smile_config->findOne(array(
                'checkout_id'=>$checkout_id
            ));

            if(empty($checkout_smile_config)){
                return [];
            }

            $coupons = isset($checkout_smile_config->checkout_smile_config->coupons) ? (array) $checkout_smile_config->checkout_smile_config->coupons : [];
            

            foreach($coupons as $key=>$val){
                
                $discount_price = isset($val['discount_price']) ? $val['discount_price'] : 0;
                $coupon_codes = isset($val['coupon_codes']) ? (array)$val['coupon_codes'] : [];

                foreach($redeemCoupons as $reKey=>$reVal ){
                    if(in_array($reVal, $coupon_codes)){
                        $couponDetails[] = ["coupon"=>$reVal, "discount"=>$discount_price];
                    }
                }

            }

            return $couponDetails;

        }catch(\Exception $ex){
            return [];
        }
    }

    // Using authentication token from URL we will let a user login and fetch addresses and payment methods to do quick checkout
    public function getUserDetailsFromToken(){
        try{

            $login_token = isset($this->requestBody['token']) ? $this->requestBody['token'] : null;

           if(!$login_token){
                throw new InvalidArgumentException('Login token missing');
            }

            //processing on token, validate token if its validated then use data from it
            $decodedData =  JWTController::decodeToken($login_token);
            if($decodedData && $decodedData['status'] && $decodedData['token'] && $decodedData['token']->email){
                $token_user_data = array(
                    'login_token'   =>  $login_token,
                    'email'         =>  $decodedData['token']->email,
                    'freeShipping'  =>  $decodedData['token']->freeShipping
                );
            }else{
                throw new InvalidArgumentException('Login token missing/invalid');
            }

            $this->requestBody['token_user_data'] = $token_user_data;

            return $this->requestBody['token_user_data'];
            
            
        }catch(\Exception $ex){
            return [];
        }
    }
    // Using token data which is saved into object using getUserDetailsFromToken we will fetch free shipping status for current token
    public function getContactFreeShipping(){
        try{

            $token_user_data = isset($this->requestBody['token']) && $this->requestBody['token_user_data'] ? $this->requestBody['token_user_data'] : null;

           if(!$token_user_data){
                throw new InvalidArgumentException('Login token missing');
            }

            return  $token_user_data['freeShipping'];
            
        }catch(\Exception $ex){
            return false;
        }
    }

    
    // Using token data which is saved into object using getUserDetailsFromToken we will fetch free shipping status for current token
    public function getContactLoginValidation(){
        try{

            $token_user_data = isset($this->requestBody['token']) && $this->requestBody['token_user_data'] ? $this->requestBody['token_user_data'] : null;

            if(!$token_user_data){
                throw new InvalidArgumentException('Login token missing');
            }

            return  $token_user_data['email'];
            
        }catch(\Exception $ex){
            return false;
        }
    }

}
