<?php

namespace App\FW\sdk\Logger;

/**
 * Class CustomLog
 * @package App\FW\sdk\Looger
 */
class CustomLog
{
    /**
     * @var string[]
     */
    protected $logType = [
        'E_ERROR'             => 'ERROR',
        'E_CORE_ERROR'        => 'ERROR',
        'E_COMPILE_ERROR'     => 'ERROR',
        'E_PARSE'             => 'ERROR',
        'E_USER_ERROR'        => 'ERROR',
        'E_RECOVERABLE_ERROR' => 'ERROR',
        'E_WARNING'           => 'WARNING',
        'E_CORE_WARNING'      => 'WARNING',
        'E_USER_WARNING'      => 'WARNING',
        'E_NOTICE'            => 'NOTICE',
        'E_USER_NOTICE'       => 'NOTICE',
        'E_STRICT'            => 'DEBUG',
        'E_COMPILE_WARNING'   => 'WARNING',
    ];

    /**
     * @var array
     */
    protected $requestParam = [];

    /**
     * @var string
     */
    protected $fileDir = __DIR__ . '/../../../../log';

    /**
     * @var string
     */
    protected $fileName = 'debug.log';

    /**
     * @var string
     */
    protected $filePath = '';

    function __construct()
    {
        $this->filePath = "{$this->fileDir}/{$this->fileName}";
    }

    /**
     * 1. check the log file if not created then create. file must be read write permission
     * 2. log folder would be /log/debug.log
     * 3. file always will be open into append mode
     * 4. file name , file path instance
     *
     * @param array $paramData
     * @return bool|false|int
     */
    public function writeLog($paramData = [])
    {
        try {
            $this->requestParam = $paramData;

            return $this->manageLogFile()->saveLog();
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * @return $this
     */
    protected function manageLogFile()
    {
        if (file_exists($this->filePath)) {
            return $this;
        }

        //check directory exist ot not
        if (!file_exists(dirname($this->filePath))) {
            mkdir(dirname($this->filePath), 0777, true);
        }

        //check file exist
        if (!file_exists($this->filePath)) {
            file_put_contents($this->filePath, '');
        }

        //create a file into path log

        return $this;
    }

    /**
     * @return false|int
     * @throws \Exception
     */
    protected function saveLog()
    {
        if (empty($this->requestParam)) {
            throw new \Exception("Invalid lag param");
        }

        $logger = [
            'timestamp'  => date('m-d-Y H:i:s'),
            'type'       => $this->requestParam['type'],
            'msg'        => isset($this->requestParam['message']) ? $this->requestParam['message'] : '',
            //message,
            'session_id' => session_id(),
            'client_id'  => isset($this->requestParam['client_id']) ? $this->requestParam['client_id'] : 0,
            //hosted checkout createdBy
            'app_key'    => isset($this->requestParam['app_key']) ? $this->requestParam['app_key'] : '',
            //api_key
            'context'    => isset($this->requestParam['context']) ? $this->requestParam['context'] : [],
            'data'       => isset($this->requestParam['data']) ? $this->requestParam['data'] : [],
        ];

        $data = (getenv('APPLICATION_ENV') != 'production') ? json_encode($logger) : json_encode($logger, JSON_PRETTY_PRINT);

        return file_put_contents($this->filePath, $data . PHP_EOL, FILE_APPEND);
    }
}
