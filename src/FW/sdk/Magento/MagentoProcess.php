<?php
namespace App\FW\sdk\Magento;
use App\FW\sdk\Magento\HttpClient;

//process class

class MagentoProcess extends HttpClient{
    public function __construct($config,$requestBody)
    {
        $this->config   = $config;
        $this->requestBody = $requestBody;
        $store = isset($this->config["crmConfig"]["magentoStore"]) ? $this->config["crmConfig"]["magentoStore"] : [];
        $this->accessCredentials = [
            "consumer_key"=> isset($store['store_consumer_key']) ? $store['store_consumer_key'] : "",
            "consumer_secret"=>isset($store['store_consumer_secret']) ? $store['store_consumer_secret'] : "",
            "access_token"=>isset($store['store_access_token']) ? $store['store_access_token'] : "",
            "token_secret"=>isset($store['store_token_secret']) ? $store['store_token_secret'] : ""
        ];

    }

    public function orderSync(){
        try{
            set_time_limit(0);
            $store = isset($this->config["crmConfig"]["magentoStore"]) ? $this->config["crmConfig"]["magentoStore"] : [];
            $quote_id = isset($this->config["crmConfig"]["quote_id"]) ? $this->config["crmConfig"]["quote_id"] : "673";
            $store_type = isset($store['store_type']) ?$store['store_type'] : "";

            if(empty($store)) throw new \Exception("Invalid store selection");

            if(empty($quote_id)) throw new \Exception("Invalid quote_id for processing order sync");
            
            if($store_type!=4) throw new \Exception("Magento order sync will not be applicable");

            $domain = isset($store['store_custom_domain']) ? $store['store_custom_domain'] : "";
            $domain = empty($domain) && isset($store['store_name']) ? $store['store_name'] : $domain;
            $this->apiUri = "https://".$domain."/rest/V1/";

            $estimateShippingMethodResponse = $this->estimateShippingMethod($quote_id);
            
            $shippingInformationResponse = $this->manageShippingInformation($estimateShippingMethodResponse,$quote_id);
            
            $method = isset($shippingInformationResponse["payment_methods"][0]['code']) ? $shippingInformationResponse["payment_methods"][0]['code'] : "purchaseorder";

            // $this->accessCredentials = $store;
            $this->param = json_encode(["paymentMethod"=>["method"=>$method]]);
            $this->method = 'PUT';
            $this->apiEndPoint = $this->apiUri.'carts/'.$quote_id.'/order';
            // print_r($this->apiEndPoint); die;
            $magentoResponse = $this->processEndPoint();
           
            if(isset($magentoResponse["message"])) throw new \Exception($magentoResponse["message"]);

            return $magentoResponse;

        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), 202);
        }
    }

    //
    protected function estimateShippingMethod($quote_id){
        try{

            // $this->accessCredentials = $store;
            $requestBody = $this->requestBody;
            $street = [];
            $address1 = isset($requestBody['address1']) ? $requestBody['address1'] : "";
            $address2 = isset($requestBody['address2']) ? $requestBody['address2'] : "";

            if(!empty($address1)){ $street[] = $address1;}


            if(!empty($address2)) {$street[] = $address2;}
            

            $this->param = json_encode([
                "address"=>[
                    "region"=> isset($requestBody['state']) ? $requestBody['state'] : "",
                    // "region_id"=>isset($requestBody['state']) ? $requestBody['state'] : "",
                    "region_code"=> isset($requestBody['state']) ? $requestBody['state'] : "",
                    "country_id"=>isset($requestBody['country']) ? $requestBody['country'] : "",
                    "street"=>$street,
                    "postcode"=>isset($requestBody['zip']) ? $requestBody['zip'] : "",
                    "city"=>isset($requestBody['city']) ? $requestBody['city'] : "",
                    "firstname"=>isset($requestBody['firstName']) ? $requestBody['firstName'] : "",
                    "lastname"=>isset($requestBody['lastName']) ? $requestBody['lastName'] : "",
                    // "customer_id"=>isset($requestBody['state']) ? $requestBody['state'] : "",,
                    "email"=>isset($requestBody['email']) ? $requestBody['email'] : "",
                    "telephone"=>isset($requestBody['phone']) ? $requestBody['phone'] : "",
                    "same_as_billing"=>isset($requestBody['billingSameAsShipping']) && $requestBody['billingSameAsShipping']!='YES' ? 0 : 1,
                ]
            ]);

            $this->method = 'POST';
            $this->apiEndPoint = $this->apiUri.'carts/'.$quote_id.'/estimate-shipping-methods';
           
            $magentoResponse = $this->processEndPoint();
            
            if(isset($magentoResponse["message"])) throw new \Exception($magentoResponse["message"]);

            return $magentoResponse;

        }catch(\Exception $ex){
            
            return [];
        }
    }

    protected function manageShippingInformation($estimateShippingMethodResponse,$quote_id){
        try{
            $shipping_carrier_code = isset($estimateShippingMethodResponse[0]['carrier_code']) ?$estimateShippingMethodResponse[0]['carrier_code'] : "";
            $shipping_method_code = isset($estimateShippingMethodResponse[0]['method_code']) ?$estimateShippingMethodResponse[0]['method_code'] : "";
            
            $requestBody = $this->requestBody;
            $street = [];
            $address1 = isset($requestBody['address1']) ? $requestBody['address1'] : "";
            $address2 = isset($requestBody['address2']) ? $requestBody['address2'] : "";

            if(!empty($address1)){ $street[] = $address1;}
            if(!empty($address2)) {$street[] = $address2;}

            $billingSameAsShipping = isset($requestBody['billingSameAsShipping']) && $requestBody['billingSameAsShipping']!='YES' ? 0 : 1;
            $shippingAddress = [
                "region"=> isset($requestBody['state']) ? $requestBody['state'] : "",
                // "region_id"=>isset($requestBody['state']) ? $requestBody['state'] : "",
                "region_code"=> isset($requestBody['state']) ? $requestBody['state'] : "",
                "country_id"=>isset($requestBody['country']) ? $requestBody['country'] : "",
                "street"=>$street,
                "postcode"=>isset($requestBody['zip']) ? $requestBody['zip'] : "",
                "city"=>isset($requestBody['city']) ? $requestBody['city'] : "",
                "firstname"=>isset($requestBody['firstName']) ? $requestBody['firstName'] : "",
                "lastname"=>isset($requestBody['lastName']) ? $requestBody['lastName'] : "",
                // "customer_id"=>isset($requestBody['state']) ? $requestBody['state'] : "",,
                "email"=>isset($requestBody['email']) ? $requestBody['email'] : "",
                "telephone"=>isset($requestBody['phone']) ? $requestBody['phone'] : "",
                "same_as_billing"=>isset($requestBody['billingSameAsShipping']) && $requestBody['billingSameAsShipping']!='YES' ? 0 : 1,
            ];

            $billingAddress = $shippingAddress;

            if($billingSameAsShipping ==0){
                $billingStreet = [];
                $Baddress1 = isset($requestBody['billingAddress1']) ? $requestBody['billingAddress1'] : "";
                $Baddress2 = isset($requestBody['billingAddress2']) ? $requestBody['billingAddress2'] : "";

                if(!empty($Baddress1)){ $billingStreet[] = $Baddress1;}
                if(!empty($Baddress2)) {$billingStreet[] = $Baddress2;}

                $billingAddress = [
                    "region"=> isset($requestBody['billingState']) ? $requestBody['billingState'] : $shippingAddress["region"],
                    // "region_id"=>isset($requestBody['state']) ? $requestBody['state'] : "",
                    "region_code"=> isset($requestBody['billingState']) ? $requestBody['billingState'] : $shippingAddress["region"],
                    "country_id"=>isset($requestBody['billingCountry']) ? $requestBody['billingCountry'] : $shippingAddress["country_id"],
                    "street"=>empty($billingStreet) ? $street : $billingStreet,
                    "postcode"=>isset($requestBody['billingZip']) ? $requestBody['billingZip'] : $shippingAddress["postcode"],
                    "city"=>isset($requestBody['billingCity']) ? $requestBody['billingCity'] : $shippingAddress["city"],
                    "firstname"=>isset($requestBody['billingFirstName']) ? $requestBody['billingFirstName'] : $shippingAddress["firstname"],
                    "lastname"=>isset($requestBody['billingLastName']) ? $requestBody['billingLastName'] : $shippingAddress["lastname"],
                    // "customer_id"=>isset($requestBody['state']) ? $requestBody['state'] : "",,
                    "email"=>isset($requestBody['email']) ? $requestBody['email'] : $shippingAddress["email"],
                    "telephone"=>isset($requestBody['phone']) ? $requestBody['phone'] : $shippingAddress["phone"]
                ];
            }


            $this->param = json_encode([
                "addressInformation" =>
                    [
                        "shipping_address"=>$shippingAddress,
                        "billing_address" =>$billingAddress,
                        "shipping_carrier_code"=>$shipping_carrier_code,
                        "shipping_method_code"=>$shipping_method_code
                    ]
                ]);
            
            $this->method = 'POST';
            $this->apiEndPoint = $this->apiUri.'carts/'.$quote_id.'/shipping-information';
           
            $magentoResponse = $this->processEndPoint();
            
            if(isset($magentoResponse["message"])) throw new \Exception($magentoResponse["message"]);

            return $magentoResponse;

        }catch(\Exception $ex){
            
            return [];
        }
    }
}