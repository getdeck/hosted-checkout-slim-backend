<?php
namespace App\FW\sdk\Magento;

class HttpClient{
    public $accessCredentials; //All the key
    public $param; //Searching field that will pass as form data, always will be array
    public $method; // GET, POST , PUT
    public $apiEndPoint; // APIEndpoint
    public $apiUri;
    public $storeUrl;
    public $accessEndpoint; //API path with URL param Data
    
    public $oauthSignature; //internal use
    public $outhNonce; //internal use test
    public $errorResponse;
    public $currentErrindex =0;

    public function __construct(){
        
    }

    public function processEndPoint(){
        try{
            $this->getOauthSignature();
            if($this->method!="PUT"){
                //http_build_query($this->param)
            }
            // paymentMethod%5Bmethod%5D=purchaseorder
            // paymentMethod%5Bmethod%5D=purchaseorder

            
            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_ENCODING=>"",
                CURLOPT_MAXREDIRS=>10,
                CURLOPT_TIMEOUT=>0,
                CURLOPT_FOLLOWLOCATION=>true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => $this->method,
                // CURLOPT_SSL_VERIFYHOST => 0,
                // CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_URL => $this->apiEndPoint,
                CURLOPT_POSTFIELDS=>$this->param,
                CURLOPT_HTTPHEADER => [
                    // 'Authorization: Basic '. base64_encode('not:telling'),
                    'Authorization: OAuth ' 
                    . 'oauth_consumer_key="' . $this->outhNonce['oauth_consumer_key'] . '",'
                    . 'oauth_nonce="' . $this->outhNonce['oauth_nonce'] . '",'
                    . 'oauth_signature_method="' . $this->outhNonce['oauth_signature_method'] . '",'
                    . 'oauth_signature="' . rawurlencode($this->oauthSignature) . '",'
                    . 'oauth_timestamp="' . $this->outhNonce['oauth_timestamp'] . '",'
                    . 'oauth_version="' . $this->outhNonce['oauth_version'] . '",'
                    . 'oauth_token="' . rawurlencode($this->outhNonce['oauth_token']).'"',
                    'Content-Type: application/json'
                ]
            ]);

            
            $result = curl_exec($curl);
            $err = curl_error($curl);

            if($err) throw new \Exception("We are not able to process your request");

            $respone = json_decode($result,true);

            if(isset($respone['message'])) throw new \Exception($respone['message']);
            
            return ($respone ? $respone : $result);
        }catch(\Eception $ex){
            throw new \Exception($ex->getMessage());
        }
    }

    protected function getOauthSignature(){
        try{
           
            $this->outhNonce =  [
                'oauth_consumer_key' => $this->accessCredentials['consumer_key'],
                'oauth_nonce' => md5(uniqid(rand(), true)),
                'oauth_signature_method' => 'HMAC-SHA1',
                'oauth_timestamp' => time(),
                'oauth_token' => $this->accessCredentials['access_token'],
                'oauth_version' => '1.0',
            ];
            $this->method = strtoupper($this->method);
            $url = $this->apiEndPoint;
           
            $params = $this->outhNonce;//array_merge($this->outhNonce,$this->param); 
            
            ksort($params);
            $sortedParamsByKeyEncodedForm = array();
            foreach ($params as $key => $value) {
                $sortedParamsByKeyEncodedForm[] = rawurlencode($key) . '=' . rawurlencode($value);
            }
           
            
            $strParams = implode('&', $sortedParamsByKeyEncodedForm);
           
            $signatureData = strtoupper($this->method) // HTTP method (POST/GET/PUT/...)
                . '&'
                . rawurlencode($url)// base resource url - without port & query params & anchors, @see how Zend extracts it in Zend_Oauth_Signature_SignatureAbstract::normaliseBaseSignatureUrl()
                . '&'. rawurlencode($strParams);
                $secret = implode('&', [$this->accessCredentials['consumer_secret'], $this->accessCredentials['token_secret']]);
            
            $this->oauthSignature = base64_encode(hash_hmac("SHA1",$signatureData,$secret,true));
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage());
        }
    }
}