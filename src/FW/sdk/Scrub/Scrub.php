<?php
namespace App\FW\sdk\Scrub;
use App\FW\sdk\Scrub\ScrubConfig;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * LimeLight Scrub managemnt Init Class  
 * Desc: Prospect extended with common config 
 * @copyright  2019 SketchBrain 
 * @license    SketchBrain   FW SDK License 1.0.0
 * @version    Release: V 1.0.0
 * @link       N/A
 * @since      Class available since Release 1.0.0
 */

class Scrub extends ScrubConfig{
    protected $request;
    protected $response;
    protected $scResponse;
    //dbInstance inherit from parent

    public function __construct(Request $request , Response $response){
        parent::__construct();
        
        if(!$this->dbInstance){
            throw new \Exception("DB Connection error !");
        }
        // $scResponse = parent::getScrubPercentage();
        $this->request = $request;
        $this->response = $response;
        
    }

    public function decide(){
        try{
            
        $canScrub= false;
        
        $tableStatus = $this->checkTableExist($this->tableName);

        if(!$tableStatus){
            $this->createTable();
        }

        $rows = $this->selectData($this->tableName); // related to per

        if(!$rows){
            $data = $this->insertTable();
            $rows = $this->selectData($this->tableName); 
        }
        
        $rows['current_order_count'] = $rows['current_order_count'] +1;

        if($rows['current_order_count']<=$rows['scrub_value']){
            $canScrub = true;
        }
        return ['status'=>true,"message"=>"Request has been processed successfully", "responseData"=>["isScrub"=>$canScrub]];

        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage());
        }
    }

    public function completeScrub(){
        try{
            $rows = $this->selectData($this->tableName); 
            $rows = array_intersect_key($rows,array_flip($this->columns));
            $rows['current_order_count'] = $rows['current_order_count'] +1;
            $rows['total_order'] = $rows['total_order'] - 1;

            $actionResponse = [];
            if($rows['total_order'] <1){
                //Delete the row
                $actionResponse = $this->deleteRow($rows['id']);
            }else{
                //update
                $actionResponse = $this->updateTable($rows);
            }

             return ["status"=>true,"message"=>"Request has been processed successfully","responseData"=>$actionResponse];

        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage());
        }
    }

   

   
    

 
    
}