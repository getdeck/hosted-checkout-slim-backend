<?php
namespace App\FW\sdk\Scrub;
use App\FW\sdk\Config\Config;

class ScrubConfig extends Config{
    public $dbInstance;
    
    public $scrubPercentage;
    public $dbIns;
    public $tableName;
    public $randomCount;
    public $columns;
    public $tableConfigName;
    public $configColumns;

    public function __construct(){
        parent::__construct();
        
        $this->dbInstance = new \SQLite3(__DIR__.'/scrubDb.db');

        //For scrub row
        $this->randomCount = rand(5,50);
        $this->tableName = 'SCRUB_CONFIG';
        $this->columns = ['id','total_order','current_order_count','order_per','scrub_value'];

        //For config detail
        $this->tableConfigName = 'SCRUB_CONFIG_DETAIL';
        $this->configColumns = ['id','scrubbing_percentage'];

        $scResponse = $this->getScrubPercentage();
        $this->scrubPercentage = isset($scResponse['scrubbing_percentage']) ? $scResponse['scrubbing_percentage'] : 10;
        
    }

    public function getDBInstance(){
        return $this->dbInstance;
    }


    //for SCRUB_CONFIG
    public function processDone(){
        try{
            $rows = $this->selectData(); 
            $rows = array_intersect_key($rows,array_flip($this->columns));
        
            $rows['current_order_count'] = $rows['current_order_count'] +1;

            $rows['total_order'] = $rows['total_order'] - 1;

            if($rows['total_order'] <1){
                //Delete the row
               return $this->deleteRow($rows['id']);
            }else{
                //update
                return $this->updateTable($rows);
            }

        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage());
        }
    }

    //for SCRUB_CONFIG
    public function createTable(){
        try{
            $sql = <<<EOF
                    CREATE TABLE $this->tableName
                    (   id INTEGER PRIMARY KEY AUTOINCREMENT,
                        total_order INT NOT NULL DEFAULT 0, 
                        current_order_count INT NOT NULL DEFAULT 0,
                        order_per INT NOT NULL DEFAULT 0,
                        scrub_value INT NOT NULL DEFAULT 0
                    );

EOF;
            return $this->dbInstance->query($sql);
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage());
        }
    }

    //for SCRUB_CONFIG
    public function insertTable(){
        try{
            $scrub_value = ceil($this->randomCount * $this->scrubPercentage / 100);

            $sql = <<<EOF
                    INSERT INTO  $this->tableName(`total_order`,`current_order_count`,`order_per`,`scrub_value`) values($this->randomCount,0,$this->scrubPercentage,$scrub_value);
EOF;

            return $this->dbInstance->query($sql);
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage());
        }
    }

    ////for SCRUB_CONFIG
    public function updateTable($param = []){
        try{
            $arrCount = count($param);
            if(!$param || $arrCount<1){
                throw new \Exception("Invalid update param");
            }
            $updateStr = '';
            $inc = 0;
            
            foreach($param as $key=>$val){
                $updateStr.= ($inc++ < ($arrCount -1)) ?  '`'.$key.'`='."'".$val."', " : '`'.$key.'`='."'".$val."' ";
            }

            $sql = <<<EOF
                    UPDATE $this->tableName SET $updateStr;
EOF;
            

            return $this->dbInstance->query($sql);
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage());
        }
    }

    //for SCRUB_CONFIG
    public function checkTableExist($tableName = ''){
        try{
            $sql =<<<EOF
            SELECT name FROM sqlite_master WHERE type='table' AND name='$tableName' 
EOF;
            $ret = $this->dbInstance->query($sql);
            return $ret->fetchArray();
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage());
        }
       
    }

    //for SCRUB_CONFIG
    public function selectData($tableName = ''){
        try{
            $sql =<<<EOF
            SELECT * from $tableName LIMIT 1 
EOF;
            
            $ret = $this->dbInstance->query($sql);
           
            return $ret->fetchArray();
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage());
        }
       
    }

    //for SCRUB_CONFIG
    public function deleteRow($id = NULL){
        try{
            if(!$id){
                throw new \Exception("Invalid row ID");
            }

            $sql =<<<EOF
            DELETE from $this->tableName where id=$id
EOF;
            return $this->dbInstance->query($sql);
        
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage());
        }
    }

    //for SCRUB_CONFIG
    public function deleteAll(){
        try{
            if(!$id){
                throw new \Exception("Invalid row ID");
            }

            $sql =<<<EOF
            DELETE from $this->tableName
EOF;
            return $this->dbInstance->query($sql);
        
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage());
        }
    }

    //for SCRUB_CONFIG_DETAIL
    public function getScrubPercentage(){

        try{

            $tableStatus = $this->checkTableExist($this->tableConfigName);

            if(!$tableStatus){
                $this->createConfigTable();
            }

            $rows = $this->selectData($this->tableConfigName);

            if(!$rows){
                $data = $this->insertTableIntoConfig();
                $rows = $this->selectData($this->tableConfigName); 
            }

            return $rows;
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage());
        }
        
    }


    public function createConfigTable(){
        try{
            $sql = <<<EOF
                    CREATE TABLE $this->tableConfigName
                    (   id INTEGER PRIMARY KEY AUTOINCREMENT,
                        scrubbing_percentage INT NOT NULL DEFAULT 0
                    );
EOF;
            return $this->dbInstance->query($sql);
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage());
        }
    }

    public function insertTableIntoConfig(){
        try{
            $scrub_value = 10;

            $sql = <<<EOF
                    INSERT INTO  $this->tableConfigName(`scrubbing_percentage`) values($scrub_value);
EOF;

            return $this->dbInstance->query($sql);
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage());
        }
    }

}
