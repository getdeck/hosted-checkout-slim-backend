<?php
namespace App\Middleware;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use Slim\Psr7\Response;

class HeaderAuthMiddleware
{
	private $headerAuthToken;

	public function __construct()
	{
		$this->headerAuthToken = $_ENV['AUTHORIZATION_TOKEN'];
	}

    /**
     * {@inheritdoc}
     */
    public function __invoke(Request $request, RequestHandler $handler)
    {
        $headerToken = $request->getHeader('Token');
        if(isset($headerToken[0]) && !empty($headerToken[0]) && $headerToken[0] == $this->headerAuthToken)
        {
            return $handler->handle($request);
        }
        $response = new Response();
        $response->getBody()->write(json_encode([
            'status' => 401,
            'message' => 'Invalid access token'
        ]));
        return $response->withHeader('Content-type', 'application/json')->withStatus(401);
    }
}
