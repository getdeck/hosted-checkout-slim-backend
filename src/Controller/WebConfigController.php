<?php

namespace App\Controller;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use App\FW\sdk\Limelight\CrmConfig;
use App\FW\sdk\Limelight\LanderConfig;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\RequestOptions as RequestOptions;
use TaxJar\Client as tacJarClient;

final class WebConfigController extends BaseController
{

    public function listUpsellDetail(Request $request, Response $response, array $args = [])
    {
        try {

            $CrmConfig = new CrmConfig($request);
            $detail = $CrmConfig->getUpsells();


            return $this->toJSON($request, $response, (!empty($detail) ? ($detail) : []));
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage(), 202);
        }
    }
    public function landerConfig(Request $request, Response $response, array $args = [])
    {
        // get chekout hash, products for current lander, campaign id, offer id, shippings ids(methods) for products of current lander
        // using checkout hash redirect user to HCO, 
        if (extension_loaded('newrelic') && function_exists('newrelic_disable_autorum')) { // Ensure PHP agent is available
            newrelic_disable_autorum();
        }
        $resp = ['status' => false, 'data' => [], 'message' => ''];
        try {
            $lander_config = [];
            $landerConfig = new LanderConfig($request);
            $resp['status'] = true;
            $resp['data'] = $landerConfig->getLanderConfig();
        } catch (\Exception $ex) {
            $resp['message'] = $ex->getMessage();
        }
        return $this->toJSON($request, $response, $resp);
    }
    public function llConfig(Request $request, Response $response, array $args = [])
    {
        try {
            if (extension_loaded('newrelic') && function_exists('newrelic_disable_autorum')) { // Ensure PHP agent is available
                newrelic_disable_autorum();
            }

            $CrmConfig = new CrmConfig($request);
            $token_based_user_addresses = $CrmConfig->getContactDetails();
            $token_based_user_free_shipping = $CrmConfig->getContactFreeShipping();
            $token_based_user_login = $CrmConfig->getContactLoginValidation();

            $detail = $CrmConfig->getCrmConfig();
            $crmShipping = $CrmConfig->getShippingDetailFromDb();
            $countryBasedShippingDetailFromDb = $CrmConfig->getCountryBasedShippingDetailFromDb();
            $pixelConfig = $CrmConfig->getPixelConfiguration();
            $giftItems = $CrmConfig->getGiftItems();
            // $offerDetails = $CrmConfig->getOfferDetails();
           // $CrmConfig->crmCheckoutBillingModelIds(); //it will show the billing label for dynamic cart based system for link based

            $offer_id = isset($detail['crmConfig']['checkout_offer_id']) ? $detail['crmConfig']['checkout_offer_id'] : 'all';
            // get billing models as per offer selected for link based checkout or as per selected on lander
             $billingModels = $CrmConfig->getBillingModels($offer_id);

            $logo = isset($detail['crmConfig']['store_logo']) ? $detail['crmConfig']['store_logo'] : '';

            if (!empty($logo) && is_string($logo)) {
                $logo = $logo;
            }else{
                $logo = null; 
            }
            //checkout_enable_tax
            $checkout_enable_one_click_upsell_products = isset($detail['crmConfig']['checkout_enable_one_click_upsell_products']) ? $detail['crmConfig']['checkout_enable_one_click_upsell_products'] : false;
            $checkout_enable_3d_verify = isset($detail['crmConfig']['checkout_enable_3d_verify']) ? $detail['crmConfig']['checkout_enable_3d_verify'] : false;
            $checkout_enable_single_page = isset($detail['crmConfig']['checkout_enable_single_page']) ? $detail['crmConfig']['checkout_enable_single_page'] : false;
            $cart = array(
                'cartInfo' => array(
                    'shopDetails'           => array(
                        'name'              => isset($detail['crmConfig']['checkout_label']) ? $detail['crmConfig']['checkout_label'] : '',
                        'logo'              => isset($logo) && !empty($logo) ? $logo : '',
                        'products'          => isset($detail['crmConfig']['products']) ? $detail['crmConfig']['products'] : '',
                        'label_url'         => isset($detail['crmConfig']['checkout_label_url']) ? $detail['crmConfig']['checkout_label_url'] : '',
                        'promo_code'        => isset($detail['crmConfig']['checkout_enable_promo_code']) && !empty($detail['crmConfig']['checkout_enable_promo_code']) ? $detail['crmConfig']['checkout_enable_promo_code'] : false,
                        'custom_css'        => isset($detail['crmConfig']['checkout_custom_css']) && !empty($detail['crmConfig']['checkout_custom_css']) ? $detail['crmConfig']['checkout_custom_css'] : '',
                        'crm_product_details' => isset($detail['crmConfig']['crmProductsId']) && !empty($detail['crmConfig']['crmProductsId']) ? $detail['crmConfig']['crmProductsId'] : [],
                        'checkout_billing_model_ids' => isset($detail['crmConfig']['checkout_billing_model_ids']) ? $detail['crmConfig']['checkout_billing_model_ids'] : [],
                        'amazon_pay_details' => [
                            'sellerId' => isset($detail['crmConfig']['checkout_amazon_pay_seller_id']) ? $detail['crmConfig']['checkout_amazon_pay_seller_id'] : '',
                            'clientId' => isset($detail['crmConfig']['checkout_amazon_pay_client_id']) ? $detail['crmConfig']['checkout_amazon_pay_client_id'] : '',
                            'sandbox'  => isset($detail['crmConfig']['checkout_amazon_pay_sandbox']) ? $detail['crmConfig']['checkout_amazon_pay_sandbox'] : false,
                            'region'   => isset($detail['crmConfig']['checkout_amazon_pay_region']) ? $detail['crmConfig']['checkout_amazon_pay_region'] : 'us',
                        ]
                    ),
                    'cart'                  => array(),
                    'shippingMethods'       => $crmShipping,
                    'billingModels'         => isset($this->config['crmConfig']['all_billing_models']) ? $this->config['crmConfig']['all_billing_models'] : $billingModels,
                    'country_based_shipping' => $countryBasedShippingDetailFromDb,
                    'checkout_country_based_shipping_enable' => isset($detail['crmConfig']['checkout_country_based_shipping_enable']) ? $detail['crmConfig']['checkout_country_based_shipping_enable'] : false,

                    'application_id' => isset($detail['crmConfig']['checkout_square_application_id']) ? $detail['crmConfig']['checkout_square_application_id'] : '',
                    'location_id' => isset($detail['crmConfig']['checkout_square_location_id']) ? $detail['crmConfig']['checkout_square_location_id'] : '',
                    'checkout_is_lander' => isset($detail['crmConfig']['checkout_is_lander']) ? $detail['crmConfig']['checkout_is_lander'] : false,

                    'store_fevicon' => isset($detail['crmConfig']['store_favicon']) ? $detail['crmConfig']['store_favicon'] : false,
                    'checkout_enable_exit_popup' => isset($detail['crmConfig']['checkout_enable_exit_popup']) ? $detail['crmConfig']['checkout_enable_exit_popup'] : false,
                    'countries'             => isset($detail['crmConfig']['countries']) ? $detail['crmConfig']['countries'] : ['US', 'CA'],
                    'checkout_default_country'             => isset($detail['crmConfig']['checkout_default_country']) ? $detail['crmConfig']['checkout_default_country'] : 'US',
                    'checkout_enable_single_page'             => isset($detail['crmConfig']['checkout_enable_single_page']) ? $detail['crmConfig']['checkout_enable_single_page'] : false,
                    'checkout_exit_popup_html' => isset($detail['crmConfig']['checkout_exit_popup_html']) ? $detail['crmConfig']['checkout_exit_popup_html'] : '',
                    'checkout_order_receipt_html' => isset($detail['crmConfig']['checkout_order_receipt_html']) ? $detail['crmConfig']['checkout_order_receipt_html'] : '',
                    'checkout_online_status' => isset($detail['crmConfig']['checkout_online_status']) ? $detail['crmConfig']['checkout_online_status'] : false,
                    'checkout_card_type_accepted' => isset($detail['crmConfig']['checkout_card_type_accepted']) ? $detail['crmConfig']['checkout_card_type_accepted'] : [],
                    'processUpsell'         =>  true,
                    'checkout_enable_3d_verify' => $checkout_enable_3d_verify,
                    'checkout_enable_kount' => isset($detail['crmConfig']['checkout_enable_kount']) ? $detail['crmConfig']['checkout_enable_kount'] : false,
                    'checkout_enable_tax' => isset($detail['crmConfig']['checkout_enable_tax']) ? $detail['crmConfig']['checkout_enable_tax'] : false,

                    'checkout_kount_mid' => isset($detail['crmConfig']['checkout_kount_mid']) ? $detail['crmConfig']['checkout_kount_mid'] : '',
                    'checkout_kount_testmode' => isset($detail['crmConfig']['checkout_kount_testmode']) ? $detail['crmConfig']['checkout_kount_testmode'] : false,
                    'checkout_3d_verify_apikey' => isset($detail['crmConfig']['checkout_3d_verify_apikey']) && $checkout_enable_3d_verify != false ? $detail['crmConfig']['checkout_3d_verify_apikey'] : '',
                    'checkout_3d_verify_sandbox' => isset($detail['crmConfig']['checkout_3d_verify_sandbox']) ? $detail['crmConfig']['checkout_3d_verify_sandbox'] : false,

                    'checkout_custom_theme' => array(
                        'sidebar-background-color' => "#e9ecef",
                        'sidebar-text-color' => "#323232",
                        'background-color' => "#ffffff",
                        'text-color' =>  isset($detail['crmConfig']['checkout_custom_theme']) && !empty($detail['crmConfig']['checkout_custom_theme']) ? $detail['crmConfig']['checkout_custom_theme'] : "#242527"
                    ),
                    'checkout_font' => (isset($detail['crmConfig']['checkout_font']) && $detail['crmConfig']['checkout_font'] !== "") ? $detail['crmConfig']['checkout_font'] : "",
                    'google_id' => isset($detail['crmConfig']['checkout_google_snippet']) ? trim($detail['crmConfig']['checkout_google_snippet']) : false,
                    'three_ds_jwt_token' => $CrmConfig->get3DSToken(),
                    'pixelConfig' => $pixelConfig,
                    'offerDetails' => isset($detail['crmConfig']['offerDetails']) ? $detail['crmConfig']['offerDetails'] : [],
                    'refrence_discount' => isset($detail['crmConfig']['refrence_discount']) && !empty($detail['crmConfig']['refrence_discount']) ? $detail['crmConfig']['refrence_discount'] : [
                        "status" => false,
                        "error" => "Something went wrong",
                        "refrence" => "shopify",
                        "discount_code" => "",
                        "discount_on" => "",
                        "discount_type" => "",
                        "discount_amount" => 0
                    ],
                    'checkout_square_sandbox' => isset($detail['crmConfig']['checkout_square_sandbox']) ? ($detail['crmConfig']['checkout_square_sandbox']) : true,
                    'checkout_enable_tandc' => isset($detail['crmConfig']['checkout_enable_tandc']) ? ($detail['crmConfig']['checkout_enable_tandc']) : false,
                    'tandc_details' => isset($detail['crmConfig']['tandc_details']['termConditionDetails']) ? $detail['crmConfig']['tandc_details']['termConditionDetails'] : "",
                    'tandc_iagree_detail' => isset($detail['crmConfig']['tandc_details']['iAgreeDetail']) ? $detail['crmConfig']['tandc_details']['iAgreeDetail'] : "",
                    'checkout_one_click_upsell_products' => isset($detail['crmConfig']['checkout_one_click_upsell_products']) && $checkout_enable_one_click_upsell_products == true ? ($detail['crmConfig']['checkout_one_click_upsell_products']) : [],
                    'checkout_enable_gift_notification' => isset($detail['crmConfig']['checkout_enable_gift_notification']) ? ($detail['crmConfig']['checkout_enable_gift_notification']) : false,
                    'checkout_shipping_config' => isset($detail['crmConfig']['checkout_enable_shipping_config']) ? $detail['crmConfig']['checkout_enable_shipping_config'] : false,
                    'checkout_enable_shopify_profile_creation' => $token_based_user_login ? false : (isset($detail['crmConfig']['checkout_enable_shopify_profile_creation']) ? ($detail['crmConfig']['checkout_enable_shopify_profile_creation']) : false),
                    'account_created_after_order_status' => isset($detail['crmConfig']['account_created_after_order_status']) ? ($detail['crmConfig']['account_created_after_order_status']) : false,
                    'checkout_enable_google_analytics' => isset($detail['crmConfig']['checkout_enable_google_analytics']) ? ($detail['crmConfig']['checkout_enable_google_analytics']) : false,
                    'checkout_enable_gift' => isset($detail['crmConfig']['checkout_enable_gift']) ? ($detail['crmConfig']['checkout_enable_gift']) : false,
                    'checkout_enable_custom_fields'=>  isset($detail['crmConfig']['checkout_enable_custom_fields']) ? ($detail['crmConfig']['checkout_enable_custom_fields']) : false,
                    'checkout_custom_fields'=>  isset($detail['crmConfig']['checkout_custom_fields']) ? ($detail['crmConfig']['checkout_custom_fields']) : false,
                    'checkout_gift_products'=> $giftItems,
                    'checkout_enable_smile_config' => isset($detail['crmConfig']['checkout_enable_smile_config']) ? ($detail['crmConfig']['checkout_enable_smile_config']) : false,
                    'checkout_smile_public_key' => isset($detail['crmConfig']['smile_api_config']['smile_public_key']) ? ($detail['crmConfig']['smile_api_config']['smile_public_key']) : "",
                    'redeem_coupons'=>isset($detail['crmConfig']['redeem_coupons']) ? $detail['crmConfig']['redeem_coupons'] : [],
                    //isset($detail['crmConfig']['processUpsell']) ? $detail['crmConfig']['processUpsell'] : false
                    'token_based_user_addresses'        => $token_based_user_addresses,
                    'token_based_user_free_shipping'    => $token_based_user_free_shipping,
                    'token_based_user_login'            => $token_based_user_login,
                ),
            );

            return $this->toJSON($request, $response, $cart);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage(), 202);
        }
    }
    public function getTokenPayments(Request $request, Response $response, array $args = [])
    {
        try {
            $CrmConfig      =   new CrmConfig($request);
            $paymentDetail  =   $CrmConfig->getPaymentDetailsOnly();
            return $this->toJSON($request, $response, $paymentDetail);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage(), 202);
        }
    }
    public function orderTotalCalculate(Request $request, Response $response, array $args = [])
    {
        try {

            $CrmConfig = new CrmConfig($request);
            $detail = $CrmConfig->orderTotalCalculate();
            return $this->toJSON($request, $response, $detail);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage(), 202);
        }
    }

    public function taxjarOrderTotal(Request $request, Response $response, array $args = [])
    {
        try {
            $api_key = "Bearer 025343f3938d6f2e2fba528bb44ddef1";

            $client = new GuzzleClient(["base_uri" => "https://api.taxjar.com/"]);

            $line_items = [];

            $CrmConfig = new CrmConfig($request);

            $products = $CrmConfig->getCrmConfig();

            foreach ($products['crmConfig']['products'] as $key => $value) {
                array_push($line_items, [
                    'id' => json_decode(json_encode($value['_id']), true)['$oid'],
                    'quantity' => $value['quantity'],
                    'product_tax_code' => '20010',
                    'unit_price' => $value['price'],
                    'discount' => 0
                ]);
            }

            /*
            * from address : Store Address
            * to address : User Shipping / Biling Address
            */
            $orderData = [
                'from_country' => 'US',
                'from_zip' => '92093',
                'from_state' => 'CA',
                'from_city' => 'La Jolla',
                'from_street' => '9500 Gilman Drive',

                /*'from_country' => 'US',
                'from_zip' => '90002',
                'from_state' => 'CA',
                'from_city' => 'Los Angeles',
                'from_street' => '1335 E 103rd St',*/

                'to_country' => 'US',
                'to_zip' => '90002',  //The to_zip parameter is required when to_country is ‘US’.
                'to_state' => 'CA', //The to_state parameter is required when to_country is 'US’ or 'CA’.
                'to_city' => 'Los Angeles',
                'to_street' => '1335 E 103rd St',
                'shipping' => 0,
                'line_items' => $line_items
            ];

            $guzzleResponse = $client->request("post", "v2/taxes", [RequestOptions::JSON => $orderData, 'headers' => ["Authorization" =>  $api_key, "Content-Type" => "application/json"]]);

            $statusCode = $guzzleResponse->getStatusCode();

            if ($statusCode === 200) {
                $resp = json_decode($guzzleResponse->getBody()->getContents(), true);
            } else {
                throw new \Exception("Invalid order request");
            }
            return $this->toJSON($request, $response, $resp);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage(), 202);
        }
    }

    public function manageSession(Request $request, Response $response, array $args = [])
    {
        try {

            $currentCookieParams = session_get_cookie_params();
            $just_domain = preg_replace("/^(.*\.)?([^.]*\..*)$/", "$2", $_SERVER['HTTP_HOST']);
            $rootDomain = '.' . $just_domain;


            @session_set_cookie_params(0, '/', $rootDomain);

            @session_start();
            @setcookie('PHPSESSID', session_id(), time() + 3600, '/', $rootDomain);


            $session = [session_id()];

            return $this->toJSON($request, $response, $session);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage(), 202);
        }
    }

    public function getTheme(Request $request, Response $response, array $args = [])
    {
        try {

            $data = [
                "gjs-css" => "* { box-sizing: border-box; } body {margin: 0;}*{box-sizing:border-box;}body{margin-top:0px;margin-right:0px;margin-bottom:0px;margin-left:0px;}.clearfix{clear:both;}.header-banner{padding-top:35px;padding-bottom:100px;color:rgb(255, 255, 255);font-family:Helvetica, serif;font-weight:100;background-image:url(\"//grapesjs.com/img/bg-gr-v.png\"), url(\"//grapesjs.com/img/work-desk.jpg\");background-attachment:scroll, scroll;background-position-x:left, center;background-position-y:top, center;background-repeat-x:no-repeat, no-repeat;background-repeat-y:repeat, no-repeat;background-size:contain, cover;}.container-width{width:90%;max-width:1150px;margin-top:0px;margin-right:auto;margin-bottom:0px;margin-left:auto;}.logo-container{float:left;width:50%;}.logo{background-color:rgb(255, 255, 255);border-top-left-radius:5px;border-top-right-radius:5px;border-bottom-right-radius:5px;border-bottom-left-radius:5px;width:130px;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;min-height:30px;text-align:center;line-height:30px;color:rgb(77, 17, 79);font-size:23px;}.menu{float:right;width:50%;}.menu-item{float:right;font-size:15px;color:rgb(238, 238, 238);width:130px;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;min-height:50px;text-align:center;line-height:30px;font-weight:400;}.lead-title{margin-top:150px;margin-right:0px;margin-bottom:30px;margin-left:0px;font-size:40px;}.sub-lead-title{max-width:650px;line-height:30px;margin-bottom:30px;color:rgb(198, 198, 198);}.flex-sect{background-color:rgb(250, 250, 250);padding-top:100px;padding-right:0px;padding-bottom:100px;padding-left:0px;font-family:Helvetica, serif;}.flex-title{margin-bottom:15px;font-size:2em;text-align:center;font-weight:700;color:rgb(85, 85, 85);padding-top:5px;padding-right:5px;padding-bottom:5px;padding-left:5px;}.flex-desc{margin-bottom:55px;font-size:1em;color:rgba(0, 0, 0, 0.5);text-align:center;padding-top:5px;padding-right:5px;padding-bottom:5px;padding-left:5px;}.cards{padding-top:20px;padding-right:0px;padding-bottom:20px;padding-left:0px;display:flex;justify-content:space-around;flex-direction:initial;flex-wrap:wrap;}.card{background-color:white;height:300px;width:300px;margin-bottom:30px;box-shadow:rgba(0, 0, 0, 0.2) 0px 1px 2px 0px;border-top-left-radius:2px;border-top-right-radius:2px;border-bottom-right-radius:2px;border-bottom-left-radius:2px;transition-duration:0.5s;transition-timing-function:ease;transition-delay:0s;transition-property:all;font-weight:100;overflow-x:hidden;overflow-y:hidden;}.card:hover{margin-top:-5px;box-shadow:rgba(0, 0, 0, 0.2) 0px 20px 30px 0px;}.card-header{height:155px;background-image:url(\"//placehold.it/350x250/78c5d6/fff/image1.jpg\");background-size:cover;background-position-x:center;background-position-y:center;}.card-header.ch2{background-image:url(\"//placehold.it/350x250/459ba8/fff/image2.jpg\");}.card-header.ch3{background-image:url(\"//placehold.it/350x250/79c267/fff/image3.jpg\");}.card-header.ch4{background-image:url(\"//placehold.it/350x250/c5d647/fff/image4.jpg\");}.card-header.ch5{background-image:url(\"//placehold.it/350x250/f28c33/fff/image5.jpg\");}.card-header.ch6{background-image:url(\"//placehold.it/350x250/e868a2/fff/image6.jpg\");}.card-body{padding-top:15px;padding-right:15px;padding-bottom:5px;padding-left:15px;color:rgb(85, 85, 85);}.card-title{font-size:1.4em;margin-bottom:5px;}.card-sub-title{color:rgb(179, 179, 179);font-size:1em;margin-bottom:15px;}.card-desc{font-size:0.85rem;line-height:17px;}.am-sect{padding-top:100px;padding-bottom:100px;font-family:Helvetica, serif;}.img-phone{float:left;}.am-container{display:flex;flex-wrap:wrap;align-items:center;justify-content:space-around;}.am-content{float:left;padding-top:7px;padding-right:7px;padding-bottom:7px;padding-left:7px;width:490px;color:rgb(68, 68, 68);font-weight:100;margin-top:50px;}.am-pre{padding-top:7px;padding-right:7px;padding-bottom:7px;padding-left:7px;color:rgb(177, 177, 177);font-size:15px;}.am-title{padding-top:7px;padding-right:7px;padding-bottom:7px;padding-left:7px;font-size:25px;font-weight:400;}.am-desc{padding-top:7px;padding-right:7px;padding-bottom:7px;padding-left:7px;font-size:17px;line-height:25px;}.am-post{padding-top:7px;padding-right:7px;padding-bottom:7px;padding-left:7px;line-height:25px;font-size:13px;}.blk-sect{padding-top:100px;padding-bottom:100px;background-color:rgb(34, 34, 34);font-family:Helvetica, serif;}.blk-title{color:rgb(255, 255, 255);font-size:25px;text-align:center;margin-bottom:15px;}.blk-desc{color:rgb(177, 177, 177);font-size:15px;text-align:center;max-width:700px;margin-top:0px;margin-right:auto;margin-bottom:0px;margin-left:auto;font-weight:100;}.price-cards{margin-top:70px;display:flex;flex-wrap:wrap;align-items:center;justify-content:space-around;}.price-card-cont{width:300px;padding-top:7px;padding-right:7px;padding-bottom:7px;padding-left:7px;float:left;}.price-card{margin-top:0px;margin-right:auto;margin-bottom:0px;margin-left:auto;min-height:350px;background-color:rgb(217, 131, 166);border-top-left-radius:5px;border-top-right-radius:5px;border-bottom-right-radius:5px;border-bottom-left-radius:5px;font-weight:100;color:rgb(255, 255, 255);width:90%;}.pc-title{font-weight:100;letter-spacing:3px;text-align:center;font-size:25px;background-color:rgba(0, 0, 0, 0.1);padding-top:20px;padding-right:20px;padding-bottom:20px;padding-left:20px;}.pc-desc{padding-top:75px;padding-right:0px;padding-bottom:75px;padding-left:0px;text-align:center;}.pc-feature{color:rgba(255, 255, 255, 0.5);background-color:rgba(0, 0, 0, 0.1);letter-spacing:2px;font-size:15px;padding-top:10px;padding-right:20px;padding-bottom:10px;padding-left:20px;}.pc-feature:nth-of-type(2n){background-color:transparent;}.pc-amount{background-color:rgba(0, 0, 0, 0.1);font-size:35px;text-align:center;padding-top:35px;padding-right:0px;padding-bottom:35px;padding-left:0px;}.pc-regular{background-color:rgb(218, 120, 160);}.pc-enterprise{background-color:rgb(214, 106, 150);}.footer-under{background-color:rgb(49, 40, 51);padding-bottom:100px;padding-top:100px;min-height:500px;color:rgb(238, 238, 238);position:relative;font-weight:100;font-family:Helvetica, serif;}.copyright{background-color:rgba(0, 0, 0, 0.15);color:rgba(238, 238, 238, 0.5);bottom:0px;padding-top:1em;padding-right:0px;padding-bottom:1em;padding-left:0px;position:absolute;width:100%;font-size:0.75em;}.made-with{float:left;width:50%;padding-top:5px;padding-right:0px;padding-bottom:5px;padding-left:0px;}.foot-social-btns{display:none;float:right;width:50%;text-align:right;padding-top:5px;padding-right:0px;padding-bottom:5px;padding-left:0px;}.footer-container{display:flex;flex-wrap:wrap;align-items:stretch;justify-content:space-around;}.foot-list{float:left;width:200px;}.foot-list-title{font-weight:400;margin-bottom:10px;padding-top:0.5em;padding-right:0px;padding-bottom:0.5em;padding-left:0px;}.foot-list-item{color:rgba(238, 238, 238, 0.8);font-size:0.8em;padding-top:0.5em;padding-right:0px;padding-bottom:0.5em;padding-left:0px;}.foot-list-item:hover{color:rgb(238, 238, 238);}.foot-form-cont{width:300px;float:right;}.foot-form-title{color:rgba(255, 255, 255, 0.75);font-weight:400;margin-bottom:10px;padding-top:0.5em;padding-right:0px;padding-bottom:0.5em;padding-left:0px;text-align:right;font-size:2em;}.foot-form-desc{font-size:0.8em;color:rgba(255, 255, 255, 0.55);line-height:20px;text-align:right;margin-bottom:15px;}.sub-input{width:100%;margin-bottom:15px;padding-top:7px;padding-right:10px;padding-bottom:7px;padding-left:10px;border-top-left-radius:2px;border-top-right-radius:2px;border-bottom-right-radius:2px;border-bottom-left-radius:2px;color:rgb(255, 255, 255);background-color:rgb(85, 76, 87);border-top-width:initial;border-right-width:initial;border-bottom-width:initial;border-left-width:initial;border-top-style:none;border-right-style:none;border-bottom-style:none;border-left-style:none;border-top-color:initial;border-right-color:initial;border-bottom-color:initial;border-left-color:initial;border-image-source:initial;border-image-slice:initial;border-image-width:initial;border-image-outset:initial;border-image-repeat:initial;}.sub-btn{width:100%;margin-top:15px;margin-right:0px;margin-bottom:15px;margin-left:0px;background-color:rgb(120, 85, 128);border-top-width:initial;border-right-width:initial;border-bottom-width:initial;border-left-width:initial;border-top-style:none;border-right-style:none;border-bottom-style:none;border-left-style:none;border-top-color:initial;border-right-color:initial;border-bottom-color:initial;border-left-color:initial;border-image-source:initial;border-image-slice:initial;border-image-width:initial;border-image-outset:initial;border-image-repeat:initial;color:rgb(255, 255, 255);border-top-left-radius:2px;border-top-right-radius:2px;border-bottom-right-radius:2px;border-bottom-left-radius:2px;padding-top:7px;padding-right:10px;padding-bottom:7px;padding-left:10px;font-size:1em;cursor:pointer;}.sub-btn:hover{background-color:rgb(145, 105, 154);}.sub-btn:active{background-color:rgb(87, 63, 92);}.bdg-sect{padding-top:100px;padding-bottom:100px;font-family:Helvetica, serif;background-color:rgb(250, 250, 250);}.bdg-title{text-align:center;font-size:2em;margin-bottom:55px;color:rgb(85, 85, 85);}.badges{padding-top:20px;padding-right:20px;padding-bottom:20px;padding-left:20px;display:flex;justify-content:space-around;align-items:flex-start;flex-wrap:wrap;}.badge{width:290px;font-family:Helvetica, serif;background-color:white;margin-bottom:30px;box-shadow:rgba(0, 0, 0, 0.2) 0px 2px 2px 0px;border-top-left-radius:3px;border-top-right-radius:3px;border-bottom-right-radius:3px;border-bottom-left-radius:3px;font-weight:100;overflow-x:hidden;overflow-y:hidden;text-align:center;}.badge-header{height:115px;background-image:url(\"//grapesjs.com/img/bg-gr-v.png\"), url(\"//grapesjs.com/img/work-desk.jpg\");background-position-x:left, center;background-position-y:top, center;background-attachment:scroll, fixed;overflow-x:hidden;overflow-y:hidden;}.badge-name{font-size:1.4em;margin-bottom:5px;}.badge-role{color:rgb(119, 119, 119);font-size:1em;margin-bottom:25px;}.badge-desc{font-size:0.85rem;line-height:20px;}.badge-avatar{width:100px;height:100px;border-top-left-radius:100%;border-top-right-radius:100%;border-bottom-right-radius:100%;border-bottom-left-radius:100%;border-top-width:5px;border-right-width:5px;border-bottom-width:5px;border-left-width:5px;border-top-style:solid;border-right-style:solid;border-bottom-style:solid;border-left-style:solid;border-top-color:rgb(255, 255, 255);border-right-color:rgb(255, 255, 255);border-bottom-color:rgb(255, 255, 255);border-left-color:rgb(255, 255, 255);border-image-source:initial;border-image-slice:initial;border-image-width:initial;border-image-outset:initial;border-image-repeat:initial;box-shadow:rgba(0, 0, 0, 0.2) 0px 1px 1px 0px;margin-top:-75px;position:relative;}.badge-body{margin-top:35px;margin-right:10px;margin-bottom:35px;margin-left:10px;}.badge-foot{color:rgb(255, 255, 255);background-color:rgb(162, 144, 165);padding-top:13px;padding-bottom:13px;display:flex;justify-content:center;}.badge-link{height:35px;width:35px;line-height:35px;font-weight:700;background-color:rgb(255, 255, 255);color:rgb(162, 144, 165);display:block;border-top-left-radius:100%;border-top-right-radius:100%;border-bottom-right-radius:100%;border-bottom-left-radius:100%;margin-top:0px;margin-right:10px;margin-bottom:0px;margin-left:10px;}@media (max-width: 768px){.foot-form-cont{width:400px;}}@media (max-width: 480px){.foot-lists{display:none;}}",
                "gjs-html" => "<header class=\"header-banner\"><div class=\"container-width\"><div id=\"ione\" class=\"logo-container\"><div id=\"iz0f\" class=\"logo\">GrapesJS\n        </div></div> <div id=\"root\">demo</div> </div><nav class=\"menu\"><div class=\"menu-item\">BUILDER\n        </div><div class=\"menu-item\">TEMPLATE\n        </div><div class=\"menu-item\">WEB\n        </div></nav><div class=\"clearfix\">\n      </div><div class=\"lead-title\">Build your templates without coding\n      </div><div class=\"sub-lead-title\"><div id=\"cart\">All text blocks could be edited easily with double clicking on it. You can create new text blocks with the command from the left panel\n      </div><div id=\"ikyxa\">Hover me\n      </div></div></header><section class=\"flex-sect\"><div class=\"container-width\"><div class=\"flex-title\">Flex is the new black\n      </div><div class=\"flex-desc\">With flexbox system you're able to build complex layouts easily and with free responsivity\n      </div><div class=\"cards\"><div class=\"card\"><div class=\"card-header\">\n          </div><div class=\"card-body\"><div class=\"card-title\">Title one\n            </div><div class=\"card-sub-title\">Subtitle one\n            </div><div class=\"card-desc\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore\n            </div></div></div><div class=\"card\"><div class=\"card-header ch2\">\n          </div><div class=\"card-body\"><div class=\"card-title\">Title two\n            </div><div class=\"card-sub-title\">Subtitle two\n            </div><div class=\"card-desc\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore\n            </div></div></div><div class=\"card\"><div class=\"card-header ch3\">\n          </div><div class=\"card-body\"><div class=\"card-title\">Title three\n            </div><div class=\"card-sub-title\">Subtitle three\n            </div><div class=\"card-desc\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore\n            </div></div></div><div class=\"card\"><div class=\"card-header ch4\">\n          </div><div class=\"card-body\"><div class=\"card-title\">Title four\n            </div><div class=\"card-sub-title\">Subtitle four\n            </div><div class=\"card-desc\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore\n            </div></div></div><div class=\"card\"><div class=\"card-header ch5\">\n          </div><div class=\"card-body\"><div class=\"card-title\">Title five\n            </div><div class=\"card-sub-title\">Subtitle five\n            </div><div class=\"card-desc\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore\n            </div></div></div><div class=\"card\"><div class=\"card-header ch6\">\n          </div><div class=\"card-body\"><div class=\"card-title\">Title six\n            </div><div class=\"card-sub-title\">Subtitle six\n            </div><div class=\"card-desc\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore\n            </div></div></div></div></div></section><section class=\"am-sect\"><div class=\"container-width\"><div class=\"am-container\"><img onmousedown=\"return false\" src=\"./img/phone-app.png\" class=\"img-phone\"/><div class=\"am-content\"><div class=\"am-pre\">ASSET MANAGER\n          </div><div class=\"am-title\">Manage your images with Asset Manager\n          </div><div class=\"am-desc\">You can create image blocks with the command from the left panel and edit them with double click\n          </div><div class=\"am-post\">Image uploading is not allowed in this demo\n          </div></div></div></div></section><section class=\"blk-sect\"><div class=\"container-width\"><div class=\"blk-title\">Blocks\n      </div><div class=\"blk-desc\">Each element in HTML page could be seen as a block. On the left panel you could find different kind of blocks that you can create, move and style\n      </div><div class=\"price-cards\"><div class=\"price-card-cont\"><div class=\"price-card\"><div class=\"pc-title\">Starter\n            </div><div class=\"pc-desc\">Some random list\n            </div><div class=\"pc-feature odd-feat\">+ Starter feature 1\n            </div><div class=\"pc-feature\">+ Starter feature 2\n            </div><div class=\"pc-feature odd-feat\">+ Starter feature 3\n            </div><div class=\"pc-feature\">+ Starter feature 4\n            </div><div class=\"pc-amount odd-feat\">$ 9,90/mo\n            </div></div></div><div class=\"price-card-cont\"><div class=\"price-card pc-regular\"><div class=\"pc-title\">Regular\n            </div><div class=\"pc-desc\">Some random list\n            </div><div class=\"pc-feature odd-feat\">+ Regular feature 1\n            </div><div class=\"pc-feature\">+ Regular feature 2\n            </div><div class=\"pc-feature odd-feat\">+ Regular feature 3\n            </div><div class=\"pc-feature\">+ Regular feature 4\n            </div><div class=\"pc-amount odd-feat\">$ 19,90/mo\n            </div></div></div><div class=\"price-card-cont\"><div class=\"price-card pc-enterprise\"><div class=\"pc-title\">Enterprise\n            </div><div class=\"pc-desc\">Some random list\n            </div><div class=\"pc-feature odd-feat\">+ Enterprise feature 1\n            </div><div class=\"pc-feature\">+ Enterprise feature 2\n            </div><div class=\"pc-feature odd-feat\">+ Enterprise feature 3\n            </div><div class=\"pc-feature\">+ Enterprise feature 4\n            </div><div class=\"pc-amount odd-feat\">$ 29,90/mo\n            </div></div></div></div></div></section><section class=\"bdg-sect\"><div class=\"container-width\"><h1 class=\"bdg-title\">The team\n      </h1><div class=\"badges\"><div class=\"badge\"><div class=\"badge-header\">\n          </div><img src=\"img/team1.jpg\" class=\"badge-avatar\"/><div class=\"badge-body\"><div class=\"badge-name\">Adam Smith\n            </div><div class=\"badge-role\">CEO\n            </div><div class=\"badge-desc\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore ipsum dolor sit\n            </div></div><div class=\"badge-foot\"><span class=\"badge-link\">f</span><span class=\"badge-link\">t</span><span class=\"badge-link\">ln</span></div></div><div class=\"badge\"><div class=\"badge-header\">\n          </div><img src=\"img/team2.jpg\" class=\"badge-avatar\"/><div class=\"badge-body\"><div class=\"badge-name\">John Black\n            </div><div class=\"badge-role\">Software Engineer\n            </div><div class=\"badge-desc\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore ipsum dolor sit\n            </div></div><div class=\"badge-foot\"><span class=\"badge-link\">f</span><span class=\"badge-link\">t</span><span class=\"badge-link\">ln</span></div></div><div class=\"badge\"><div class=\"badge-header\">\n          </div><img src=\"img/team3.jpg\" class=\"badge-avatar\"/><div class=\"badge-body\"><div class=\"badge-name\">Jessica White\n            </div><div class=\"badge-role\">Web Designer\n            </div><div class=\"badge-desc\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore ipsum dolor sit\n            </div></div><div class=\"badge-foot\"><span class=\"badge-link\">f</span><span class=\"badge-link\">t</span><span class=\"badge-link\">ln</span></div></div></div></div></section><footer class=\"footer-under\"><div class=\"container-width\"><div class=\"footer-container\"><div class=\"foot-lists\"><div class=\"foot-list\"><div class=\"foot-list-title\">About us\n            </div><div class=\"foot-list-item\">Contact\n            </div><div class=\"foot-list-item\">Events\n            </div><div class=\"foot-list-item\">Company\n            </div><div class=\"foot-list-item\">Jobs\n            </div><div class=\"foot-list-item\">Blog\n            </div></div><div class=\"foot-list\"><div class=\"foot-list-title\">Services\n            </div><div class=\"foot-list-item\">Education\n            </div><div class=\"foot-list-item\">Partner\n            </div><div class=\"foot-list-item\">Community\n            </div><div class=\"foot-list-item\">Forum\n            </div><div class=\"foot-list-item\">Download\n            </div><div class=\"foot-list-item\">Upgrade\n            </div></div><div class=\"clearfix\">\n          </div></div><div class=\"form-sub\"><div class=\"foot-form-cont\"><div class=\"foot-form-title\">Subscribe\n            </div><div class=\"foot-form-desc\">Subscribe to our newsletter to receive exclusive offers and the latest news\n            </div><input name=\"name\" placeholder=\"Name\" class=\"sub-input\"/><input name=\"email\" placeholder=\"Email\" class=\"sub-input\"/><button type=\"button\" class=\"sub-btn\">Submit</button></div></div></div></div><div class=\"copyright\"><div class=\"container-width\"><div class=\"made-with\">\n          made with GrapesJS\n        </div><div class=\"foot-social-btns\">facebook twitter linkedin mail\n        </div><div class=\"clearfix\">\n        </div></div></div></footer>"
            ];
            return $this->toJSON($request, $response, $data);
        } catch (\Exception $ex) {
            die($ex->getMessage());
        }
    }

    public function productSubscriptionDetail(Request $request, Response $response, array $args = [])
    {
        try {
            if (extension_loaded('newrelic') && function_exists('newrelic_disable_autorum')) { // Ensure PHP agent is available
                newrelic_disable_autorum();
            }

            $CrmConfig = new CrmConfig($request);
            $detail = $CrmConfig->getCrmConfig();

            $cart = [
                "products" => isset($detail['crmConfig']['products']) ? $detail['crmConfig']['products'] : [],
                "checkout_enable_redirect_subscription" => isset($detail['crmConfig']['checkout_enable_redirect_subscription']) ? $detail['crmConfig']['checkout_enable_redirect_subscription'] : false
            ];
            return $this->toJSON($request, $response, $cart);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage(), 202);
        }
    }

    public function postBackUrlCall(Request $request, Response $response, array $args = [])
    {
        try {

            $CrmConfig = new CrmConfig($request);
            $detail = $CrmConfig->processPostBackUrl();
            return $this->toJSON($request, $response, $detail);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage(), 202);
        }
    }

    public function postBackUrlCheck(Request $request, Response $response, array $args = [])
    {
        try {
            $_request = $_REQUEST;
            return $this->toJSON($request, $response, $_request);
        } catch (\Exception $ex) {
            echo $ex->getMessage();
            die;
        }
    }

    public function getShippingMethodsFromConfiguration(Request $request, Response $response, array $args = [])
    {
        try {
            $CrmConfig = new CrmConfig($request);
            $detail = $CrmConfig->getShippingMethodsFromConfiguration();
            return $this->toJSON($request, $response, $detail);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage(), 202);
        }
    }


    public function productAndOffersDetails(Request $request, Response $response, array $args = [])
    {
        try {
            $CrmConfig = new CrmConfig($request);
            $detail = $CrmConfig->productAndOffersDetails();
            return $this->toJSON($request, $response, $detail);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage(), 202);
        }
    }

    public function validateRedeemDiscount(Request $request, Response $response, array $args = []){
        try{
            $CrmConfig = new CrmConfig($request);
            $detail = $CrmConfig->getCrmConfig();
            $couponDetail = isset($detail['crmConfig']['redeem_coupons']) ? $detail['crmConfig']['redeem_coupons'] : [];
            return $this->toJSON($request,$response,  ['redeem_coupons'=>$couponDetail]);
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), 202);
        }
    }
}
