<?php
namespace App\Controller;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use App\FW\sdk\Scrub\Scrub;
use App\FW\sdk\Limelight\Order\Order;


final class ScrubController extends BaseController
{
    
    /**
     * ScrubController decide scrub 
     * @param Request $request
     * 
     * @throws Exception If something wrong
     * @author SketchBrain <sketchBrain>
     * @return Response $response
     */
    public function decideScrub(Request $request, Response $response, array $args = []){
        try{
            
            $scrubIns =  new Scrub($request,$response);
            $decideReturn = $scrubIns->decide();
            return $this->toJSON($request,$response,$decideReturn);
            
        }catch(\Exception $ex){
            return $this->toJSON($request,$response,
                                            [
                                                'status'=>false,
                                                'status_code'=>500,
                                                'message'=>$ex->getMessage()
                                            ]
                                );
        }
    }

    /**
     * ScrubController complete scrub 
     * @param Request $request
     * 
     * @throws Exception If something wrong
     * @author SketchBrain <sketchBrain>
     * @return Response $response
     */
    public function scrubCompleted(Request $request, Response $response, array $args = []){
        try{
            
            $scrubIns =  new Scrub($request,$response);
            $return = $scrubIns->completeScrub();
            return $this->toJSON($request,$response,$return);
            
        }catch(\Exception $ex){
            return $this->toJSON($request,$response,
                                            [
                                                'status'=>false,
                                                'status_code'=>500,
                                                'message'=>$ex->getMessage()
                                            ]
                                );
        }
    }
   
    
    
}
