<?php
namespace App\Controller;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use App\FW\sdk\Limelight\Prospect\Prospect;
use App\FW\sdk\Limelight\Order\Order;
use App\FW\sdk\Limelight\Order\Shopify as Shopify;
use App\FW\sdk\Magento\MagentoProcess;

final class HomeController extends BaseController
{
    public function index(Request $request, Response $response, array $args = []): Response
    {
        $this->logger->info("Home page action dispatched");
        $p = new Prospect();
        $p->checkFunction();
       
        return $this->toJSON($request,$response,['data'=>12345]);
        
        //return $this->render($request, $response, 'index.twig');
    }


    /**
     * HomeController create prospect 
     * @param Request $request
     * 
     * @throws Exception If something wrong
     * @author SketchBrain <sketchBrain>
     * @return Response $response
     */
    public function createProspect(Request $request, Response $response, array $args = []){
        try{

            $prospect = new Prospect($request);
            $responseData = $prospect->create();
            return $this->toJSON($request,$response,$responseData);

            
        }catch(Exception $ex){
            throw new Exception($ex->getMessage(), 202);
        }
    }

    /**
     * HomeController Action order with prospect 
     * @param prospectData[]
     * 
     * @throws Exception If something wrong
     * @author SketchBrain <sketchBrain>
     * @return Response $response
     */

     public function createOrderWithProspect(Request $request, Response $response, array $args = []){
        try{
            
            $order = new Order($request);
            $responseData = $order->createWithOrder();
            return $this->toJSON($request,$response,$responseData);

        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getCode());
        }
     }

    /**
     * HomeController Action order for upsell 
     * @param upsellDetail[]
     * 
     * @throws Exception If something wrong
     * @author SketchBrain <sketchBrain>
     * @return Response $response
     */
     public function processUpsellOrder(Request $request, Response $response, array $args = []){
        try{
           
            $order = new Order($request);
            $responseData = $order->processUpsell();
            return $this->toJSON($request,$response,$responseData);

        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getCode());
        }
     }

     /**
     * HomeController Action for full page checkout 
     * @param Request fullPageCheckout
     * 
     * @throws Exception If something wrong
     * @author SketchBrain <sketchBrain>
     * @return Response $response
     */
     public function processOnepageCheckout(Request $request, Response $response, array $args = []){
         try{
           
            $order = new Order($request);
            $responseData = $order->fullPageCheckout();
            
            return $this->toJSON($request,$response,$responseData);

         }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getCode());
         }
     }


     public function processCoupon(Request $request, Response $response, array $args = []){
         try{
            
            $order = new Order($request);
            $responseData = $order->applyCoupon();
            
            return $this->toJSON($request,$response,$responseData);
         }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getCode());
         }
     }
     
     //paypalSuccess

     public function paypalSuccess(Request $request, Response $response, array $args = []){
        try{
            echo json_encode($_REQUEST);
            exit;
        }catch(\Exception $ex){
           throw new \Exception($ex->getMessage(), $ex->getCode());
        }
    }
    
    public function backSyncPaypalOrder(Request $request, Response $response, array $args = [])
    {
        try{
            $order = new Order($request);
            $responseData = $order->processShopifyOrderPaypal();
            return $this->toJSON($request,$response,$responseData);        
        }catch(\Exception $ex){
           throw new \Exception($ex->getMessage(), $ex->getCode());
        }
    }  
    
    public function getOrderDetail(Request $request, Response $response, array $args = []){
        try{
            
            $order = new Order($request);
            $responseData = $order->orderDetail();
            return $this->toJSON($request,$response,$responseData); 
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getCode());
        }
    }

    public function orderPostBack(Request $request, Response $response, array $args = []){
        try{
           
            $sub = new Order($request);
            $responseData = $sub->orderPostBack();

            return $this->toJSON($request,$response,$responseData);

        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }
    
    public function getDomain(Request $request, Response $response, array $args = []){
        try{
            $dnsr = checkdnsrr('sketchbrains.com','MX');//dns_get_record('sketchbrains.com', DNS_ALL - DNS_PTR);
            print_r($dnsr);
            die;
        }catch(\Exception $ex){
            die($ex->getMessage());
        }
    }

    public function validateShopifyCustomer(Request $request, Response $response, array $args = []){
        try{
            $order = new Order($request);
            $responseData = $order->validateShopifyCustomer();
            return $this->toJSON($request,$response,$responseData); 

        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    public function createShopifyCustomer(Request $request, Response $response, array $args = []){
        try{
            $order = new Order($request);
            $responseData = $order->createShopifyCustomer();
            return $this->toJSON($request,$response,$responseData); 
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

}
