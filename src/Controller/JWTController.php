<?php
namespace App\Controller;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Firebase\JWT\JWT;
use DateTime;
use Exception;

final class JWTController extends BaseController
{
    public function getToken(Request $request, Response $response){
        try{
            $requestBody = json_decode($request->getBody()->getContents(), true);

            $validationErr = '';
            
            if(!is_array($requestBody))
            {
                $validationErr = 'Invalid request';
            }

            if(is_array($requestBody) && (!isset($requestBody["email"])) || (isset($requestBody["email"]) && !filter_var($requestBody["email"], FILTER_VALIDATE_EMAIL)))
            {
                $validationErr = 'Invalid email';
            }

            if(is_array($requestBody) && filter_var($requestBody["freeShipping"], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) === NULL)
            {
                $validationErr = 'Invalid free shipping value';
            }

            if(!empty($validationErr))
            {
                $data['status'] = 422;
                $data["token"] = '';
                $data["message"] = $validationErr;
                return $this->toJSON($request,$response,$data);
            }

            $future = new DateTime("now +60 minutes");

            $payload = [
                "email" => $requestBody['email'],
                "freeShipping" => $requestBody["freeShipping"],
                "exp" => $future->getTimeStamp()
            ];

            $secret = $_ENV["JWT_SECRET"];
            $token = JWT::encode($payload, $secret, "HS256");

            $data['status'] = 200;
            $data["token"] = $token;
            return $this->toJSON($request,$response,$data);
        }catch(Exception $ex){
            $data['status'] = 500;
            $data["token"] = '';
            $data["message"] = $ex->getMessage();
            return $this->toJSON($request,$response,$data);
        }
    }

    public static function decodeToken($token){
        try{
            $secret = $_ENV["JWT_SECRET"];
            $decodedToken = JWT::decode($token, $secret, ["HS256"]);
            return [
                'status' => true,
                'message' => 'token decoded',
                'token' => $decodedToken
            ];
        }catch(Exception $ex){
            return [
                'status' => false,
                'message' => $ex->getMessage(),
                'token' => []
            ];
        }
    }
}
