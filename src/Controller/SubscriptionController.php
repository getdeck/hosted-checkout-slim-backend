<?php
namespace App\Controller;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use App\FW\Subscription\Subscriptions;


final class SubscriptionController extends BaseController
{

    public function listSubscriptionOrders(Request $request, Response $response, array $args = []){
        try{
            
            $sub = new Subscriptions($request);
            $responseData = $sub->orderList();
            return $this->toJSON($request,$response,$responseData);

        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    public function orderRefund(Request $request, Response $response, array $args = []){
        try{
            $sub = new Subscriptions($request);
            $responseData = $sub->orderRefund();
            return $this->toJSON($request,$response,$responseData);
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    public function orderCancel(Request $request, Response $response, array $args = []){
        try{
            $sub = new Subscriptions($request);
            $responseData = $sub->orderCancel();
            return $this->toJSON($request,$response,$responseData);
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    public function orderStartSubscription(Request $request, Response $response, array $args = []){
        try{
            $sub = new Subscriptions($request);
            $responseData = $sub->orderStartSubscription();
            return $this->toJSON($request,$response,$responseData);
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    public function orderStopSubscription(Request $request, Response $response, array $args = []){
        try{
            $sub = new Subscriptions($request);
            $responseData = $sub->orderStopSubscription();
            return $this->toJSON($request,$response,$responseData);
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    public function orderPauseSubscription(Request $request, Response $response, array $args = []){
        try{
            $sub = new Subscriptions($request);
            $responseData = $sub->orderPauseSubscription();
            return $this->toJSON($request,$response,$responseData);
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    public function orderResetSubscription(Request $request, Response $response, array $args = []){
        try{
            $sub = new Subscriptions($request);
            $responseData = $sub->orderResetSubscription();
            return $this->toJSON($request,$response,$responseData);
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    public function orderSkipSubscription(Request $request, Response $response, array $args = []){
        try{
            $sub = new Subscriptions($request);
            $responseData = $sub->orderSkipSubscription();
            return $this->toJSON($request,$response,$responseData);
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }
    

    public function orderUpdate(Request $request, Response $response, array $args = []){
        try{
            $sub = new Subscriptions($request);
            $responseData = $sub->orderUpdate();
            return $this->toJSON($request,$response,$responseData);
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    public function activeMemberProfile(Request $request, Response $response, array $args = []){
        try{
            $sub = new Subscriptions($request);
            $responseData = $sub->activeMemberProfile();
            return $this->toJSON($request,$response,$responseData);
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    public function memberLogin(Request $request, Response $response, array $args = []){
        try{
            $sub = new Subscriptions($request);
            $responseData = $sub->memberLogin();
            return $this->toJSON($request,$response,$responseData);
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    public function validateLogin(Request $request, Response $response, array $args = []){
        try{
            $sub = new Subscriptions($request);
            $responseData = $sub->validateLogin();
            return $this->toJSON($request,$response,$responseData);
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    public function memberForgotPassword(Request $request, Response $response, array $args = []){
        try{
            $sub = new Subscriptions($request);
            $responseData = $sub->memberForgotPassword();

            if(isset($responseData['responseData']['data']['temp_password'])){
                unset($responseData['responseData']['data']['temp_password']);
            }

            return $this->toJSON($request,$response,$responseData);
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    public function memberResetPassword(Request $request, Response $response, array $args = []){
        try{
            $sub = new Subscriptions($request);
            $responseData = $sub->memberResetPassword();
            return $this->toJSON($request,$response,$responseData);
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    
    public function getMemberConfiguration(Request $request, Response $response, array $args = []){
        try{
            $sub = new Subscriptions($request);
            $responseData = $sub->memberConfiguration();
            return $this->toJSON($request,$response,$responseData);
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    public function getSubscriptionFrequency(Request $request, Response $response, array $args = []){
        try{
            $sub = new Subscriptions($request);
            $responseData = $sub->subscriptionFrequencyByOffer();
            return $this->toJSON($request,$response,$responseData);
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }
    
    public function getOrdersProdcutsImage(Request $request, Response $response, array $args = []){
        try{
            $sub = new Subscriptions($request);
            $responseData = $sub->getOrdersProdcutsImage();
            return $this->toJSON($request,$response,$responseData);

        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    public function subscriptionOrderUpdate(Request $request, Response $response, array $args = []){
        try{
            $sub = new Subscriptions($request);
            $responseData = $sub->subscriptionOrderUpdate();
            return $this->toJSON($request,$response,$responseData);
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }
    

    public function buyNow(Request $request, Response $response, array $args = []){
        try{
            $sub = new Subscriptions($request);
            $responseData = $sub->buyNowProdcut();
            return $this->toJSON($request,$response,$responseData);

        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    public function memberLogout(Request $request, Response $response, array $args = []){
        try{
            $sub = new Subscriptions($request);
            $responseData = $sub->memberLogout();
            return $this->toJSON($request,$response,$responseData);
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }
    
    public function getProductsBundle(Request $request, Response $response, array $args = []){
        try{
            $sub = new Subscriptions($request);
            $responseData = $sub->getProductsBundle();
            return $this->toJSON($request,$response,$responseData);

        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    public function getADProducts(Request $request, Response $response, array $args = []){
        try{
            $sub = new Subscriptions($request);
            $responseData = $sub->getADProducts();
            return $this->toJSON($request,$response,$responseData);

        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    public function getCatalogProducts(Request $request, Response $response, array $args = []){
        try{
            $sub = new Subscriptions($request);
            $responseData = $sub->getCatalogProducts();
            return $this->toJSON($request,$response,$responseData);

        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    public function getNotification(Request $request, Response $response, array $args = []){
        try{
            $sub = new Subscriptions($request);
            $responseData = $sub->getNotification();
            return $this->toJSON($request,$response,$responseData);

        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    public function changeSubscriptionDate(Request $request, Response $response, array $args = []){
        try{
            $sub = new Subscriptions($request);
            $responseData = $sub->orderUpdateSubscriptionRecurringDate();
            return $this->toJSON($request,$response,$responseData);

        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    public function swapSubscriptionProduct(Request $request, Response $response, array $args = []){
        try{
            $sub = new Subscriptions($request);
            $responseData = $sub->swapSubscriptionProduct();
            return $this->toJSON($request,$response,$responseData);
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }
    
    public function changeNotificationStatus(Request $request, Response $response, array $args = []){
        try{
            $sub = new Subscriptions($request);
            $responseData = $sub->changeNotificationStatus();
            return $this->toJSON($request,$response,$responseData);

        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

    public function getAccountDetails(Request $request, Response $response, array $args = []){
        try{
            $sub = new Subscriptions($request);
            $responseData = $sub->getAccountDetails();
            return $this->toJSON($request,$response,$responseData);

        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }
    
    public function orderPaymentUpdate(Request $request, Response $response, array $args = []){
        try{
            $sub = new Subscriptions($request);
            $responseData = $sub->orderPaymentUpdate();
            return $this->toJSON($request,$response,$responseData);

        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getcode());
        }
    }

}
