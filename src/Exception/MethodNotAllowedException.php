<?php
namespace App\Exception;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use Slim\Psr7\Response;

class MethodNotAllowedException
{
    /**
     * {@inheritdoc}
     */
    public function __invoke()
    {
        $response = new Response();
        $response->getBody()->write(json_encode([
            'status' => 405,
            'message' => 'Method not allowed'
        ]));
        return $response->withHeader('Content-type', 'application/json')->withStatus(405);
    }
}
