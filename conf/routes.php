<?php

declare(strict_types=1);

use Slim\App;
use App\Middleware\HeaderAuthMiddleware;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;

return function (App $app) {

    $app->options('/{routes:.+}', function ($request, $response, $args) {
        return $response;
    });

    $app->get('/', function () {
        print_r("Sticky.Io");
        trigger_error("Cannot divide by zero", E_WARNING);
        exit;
    })->setName('home');
    $app->post('/create-prospect', 'App\Controller\HomeController:createProspect')->setName('createProspect');
    $app->post('/api/customer/session', 'App\Controller\JWTController:getToken')->setName('getJwtToken')->add(new HeaderAuthMiddleware());
    //createOrderWithProspect
    $app->post('/process-order', 'App\Controller\HomeController:createOrderWithProspect')->setName('createOrderWithProspect');
    $app->post('/process-upsell-order', 'App\Controller\HomeController:processUpsellOrder')->setName('processUpsellOrder');
    $app->post('/process-onepage-checkout', 'App\Controller\HomeController:processOnepageCheckout')->setName('processOnepageCheckout');
    $app->post('/order-detail', 'App\Controller\HomeController:getOrderDetail')->setName('getOrderDetail');



    //process-coupon
    $app->post('/process-coupon', 'App\Controller\HomeController:processCoupon')->setName('processCoupon');

    //getWebUIDetail
    //get-upsells
    $app->post('/get-upsells', 'App\Controller\WebConfigController:listUpsellDetail')->setName('listUpsellDetail');
    $app->post('/get-ll-config', 'App\Controller\WebConfigController:llConfig')->setName('llConfig');
    $app->post('/get-product-subscription', 'App\Controller\WebConfigController:productSubscriptionDetail')->setName('productSubscriptionDetail');
    // for landers
    $app->post('/get-lander-config', 'App\Controller\WebConfigController:landerConfig')->setName('landerConfig');

    //for scrub
    $app->post('/decide-scrub', 'App\Controller\ScrubController:decideScrub')->setName('decideScrub');
    $app->post('/scrub-completed', 'App\Controller\ScrubController:scrubCompleted')->setName('scrubCompleted');

    //paypal
    $app->get('/paypal-success', 'App\Controller\HomeController:paypalSuccess')->setName('paypalSuccess');
    $app->get('/get-domain', 'App\Controller\HomeController:getDomain')->setName('get-domain');

    $app->post('/back-sync-paypal-order', 'App\Controller\HomeController:backSyncPaypalOrder')->setName('paypalSuccess');
    $app->post('/process-order-total', 'App\Controller\WebConfigController:orderTotalCalculate')->setName('orderTotalCalculate');
    $app->post('/get-token-payments', 'App\Controller\WebConfigController:getTokenPayments')->setName('getTokenPayments');
    $app->get('/session', 'App\Controller\WebConfigController:manageSession')->setName('manageSession');
    $app->get('/get-theme', 'App\Controller\WebConfigController:getTheme')->setName('getTheme');


    // $app->post('/taxjar-order-total', 'App\Controller\WebConfigController:taxjarOrderTotal')->setName('taxjarOrderTotal');

    $app->post('/post-back', 'App\Controller\WebConfigController:postBackUrlCall')->setName('postBackUrlCall');
    $app->get('/post-back-check', 'App\Controller\WebConfigController:postBackUrlCheck')->setName('postBackUrlCheck');


    $app->post('/get-shipping-methods-from-configuration', 'App\Controller\WebConfigController:getShippingMethodsFromConfiguration')->setName('getShippingMethodsFromConfiguration');






    $app->post('/subscription/order-list', 'App\Controller\SubscriptionController:listSubscriptionOrders')->setName('listSubscriptionOrders');
    $app->post('/subscription/order-refund', 'App\Controller\SubscriptionController:orderRefund')->setName('orderRefund');
    $app->post('/subscription/order-cancel', 'App\Controller\SubscriptionController:orderCancel')->setName('orderCancel');
    $app->post('/subscription/order-start-subscription', 'App\Controller\SubscriptionController:orderStartSubscription')->setName('orderStartSubscription');
    $app->post('/subscription/order-stop-subscription', 'App\Controller\SubscriptionController:orderStopSubscription')->setName('orderStopSubscription');
    $app->post('/subscription/order-pause-subscription', 'App\Controller\SubscriptionController:orderPauseSubscription')->setName('orderPauseSubscription');
    $app->post('/subscription/order-reset-subscription', 'App\Controller\SubscriptionController:orderResetSubscription')->setName('orderResetSubscription');
    $app->post('/subscription/order-skip-subscription', 'App\Controller\SubscriptionController:orderSkipSubscription')->setName('orderSkipSubscription');
    $app->post('/subscription/order-update-subscription-date', 'App\Controller\SubscriptionController:orderUpdateSubscriptionRecurringDate')->setName('orderUpdateSubscriptionRecurringDate');
    $app->post('/subscription/order-update', 'App\Controller\SubscriptionController:orderUpdate')->setName('orderUpdate');
    $app->post('/subscription/active-member-profile', 'App\Controller\SubscriptionController:activeMemberProfile')->setName('activeMemberProfile');
    $app->post('/subscription/member-login', 'App\Controller\SubscriptionController:memberLogin')->setName('memberLogin');
    $app->post('/subscription/validate-login', 'App\Controller\SubscriptionController:validateLogin')->setName('validateLogin');
    $app->post('/subscription/member-forgot-password', 'App\Controller\SubscriptionController:memberForgotPassword')->setName('memberForgotPassword');
    $app->post('/subscription/member-reset-password', 'App\Controller\SubscriptionController:memberResetPassword')->setName('memberResetPassword');
    $app->post('/subscription/buy-now', 'App\Controller\SubscriptionController:buyNow')->setName('buyNow');
    $app->post('/subscription/get-config', 'App\Controller\SubscriptionController:getMemberConfiguration')->setName('getMemberConfiguration');
    $app->post('/subscription/get-subscription-frequency', 'App\Controller\SubscriptionController:getSubscriptionFrequency')->setName('getSubscriptionFrequency');
    $app->post('/subscription/subscription-order-update', 'App\Controller\SubscriptionController:subscriptionOrderUpdate')->setName('subscriptionOrderUpdate');
    $app->post('/subscription/member-logout', 'App\Controller\SubscriptionController:memberLogout')->setName('memberLogout');



    $app->post('/subscription/get-orders-products-image', 'App\Controller\SubscriptionController:getOrdersProdcutsImage')->setName('getOrdersProdcutsImage');
    $app->post('/subscription/get-products-bundle', 'App\Controller\SubscriptionController:getProductsBundle')->setName('getProductsBundle');
    $app->post('/subscription/get-ad-products', 'App\Controller\SubscriptionController:getADProducts')->setName('getADProducts');
    $app->post('/subscription/change-subscription-date', 'App\Controller\SubscriptionController:changeSubscriptionDate')->setName('changeSubscriptionDate');

    $app->post('/order-post-back', 'App\Controller\HomeController:orderPostBack')->setName('orderPostBack');
    $app->post('/subscription/get-catalog-products', 'App\Controller\SubscriptionController:getCatalogProducts')->setName('getCatalogProducts');
    $app->post('/subscription/get-notification', 'App\Controller\SubscriptionController:getNotification')->setName('getNotification');
    $app->post('/subscription/change-notification-status', 'App\Controller\SubscriptionController:changeNotificationStatus')->setName('changeNotificationStatus');

    $app->post('/subscription/my-account', 'App\Controller\SubscriptionController:getAccountDetails')->setName('getAccountDetails');
    $app->post('/subscription/order-payment-update', 'App\Controller\SubscriptionController:orderPaymentUpdate')->setName('orderPaymentUpdate');

    $app->post('/validate-shopify-customer', 'App\Controller\HomeController:validateShopifyCustomer')->setName('validateShopifyCustomer');
    $app->post('/create-shopify-customer', 'App\Controller\HomeController:createShopifyCustomer')->setName('createShopifyCustomer');
    $app->post('/api/shopify/product', 'App\Controller\WebConfigController:productAndOffersDetails')->setName('productAndOffersDetails');
    $app->post('/subscription/swap-subscription-product','App\Controller\SubscriptionController:swapSubscriptionProduct')->setName('swapSubscriptionProduct');
    
    //memberForgotPassword


};
