<?php
// error_reporting(0);
declare(strict_types=1);
// error_reporting(E_ALL & ~E_NOTICE);
header('Access-Control-Allow-Origin:*'); 
header('Access-Control-Allow-Headers:X-Request-With');
header('Access-Control-Allow-Credentials:true');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With, Auth, Member-Hash');

$header = getallheaders();
$authSession = isset($header['Authorization']) ? $header['Authorization'] :"";
@session_id($authSession);


use DI\ContainerBuilder;
use Slim\Factory\AppFactory;
use Slim\Factory\ServerRequestCreatorFactory;
use App\Exception\MethodNotAllowedException;

// Set the absolute path to the root directory.
$rootPath = realpath(__DIR__ . '/..');

// Include the composer autoloader.
include_once($rootPath . '/vendor/autoload.php');
include_once($rootPath . '/env.php');
require_once __DIR__.'/helper.php';
// Instantiate PHP-DI ContainerBuilder
$containerBuilder = new ContainerBuilder();

// Set up settings
$settings = require $rootPath . '/conf/settings.php';
$settings($containerBuilder);

// Set up dependencies
$dependencies = require $rootPath . '/conf/dependencies.php';
$dependencies($containerBuilder);

// Build PHP-DI Container instance
$container = $containerBuilder->build();

$settings = $container->get('settings');

// Instantiate the app
$app = AppFactory::createFromContainer($container);
$app->setBasePath($settings['base_path']);

// Register middleware
$middleware = require $rootPath . '/conf/middleware.php';
$middleware($app);

// Register routes
$routes = require $rootPath . '/conf/routes.php';
$routes($app);

// Set the cache file for the routes. Note that you have to delete this file
// whenever you change the routes.
if (!$settings['debug']) {
    $app->getRouteCollector()->setCacheFile($settings['route_cache']);
}

// Add the routing middleware.
$app->addRoutingMiddleware();

// Add error handling middleware.
$errorMiddleware = $app->addErrorMiddleware($settings['debug'], !$settings['debug'], false);
$errorMiddleware->setErrorHandler(Slim\Exception\HttpMethodNotAllowedException::class, MethodNotAllowedException::class);

if (extension_loaded('newrelic') && function_exists('newrelic_disable_autorum')){ // Ensure PHP agent is available
    newrelic_disable_autorum();
}

// Run the app
$app->run();

if(!empty($authSession)){
    $currentCookieParams = @session_get_cookie_params();
    $just_domain = preg_replace("/^(.*\.)?([^.]*\..*)$/", "$2", $_SERVER['HTTP_HOST']);
    $rootDomain = '.'.$just_domain;
    // session_write_close();
    @session_set_cookie_params(0, '/', $rootDomain);
    @setcookie('PHPSESSID', $authSession, time() + 3600, '/', $rootDomain);
}

