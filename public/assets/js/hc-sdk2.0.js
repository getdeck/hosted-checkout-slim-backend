class StickyIOCart {
    constructor(params) {
      // need to pass while creating instance
      this.apiKey = params.apiKey ?? "";
      this.landerHash = params.landerHash ?? "z0k-1lh-k5d";
      this.checkoutDomain =
        params.checkoutDomain ?? "https://checkout.ll-demo.sketchbrains.com/app/";
  
      //default variable/function to be assigned/execute after initiate object
      this.checkoutURL =
        this.checkoutDomain + "?lander_hash=" + this.landerHash + "/";
      this.apiURL = params.apiURL ?? window.location.origin + "/"; // as the slim backend will be api url to get data and sdk hosted on same
      this.init();
    
    }
  
    generateBaseCheckoutURL = () => {
      //checkout URL
      if (this.getSessionStorageValue("hc_lander_config")) {
        this.checkoutURL =
          this.checkoutDomain +
          this.getSessionStorageValue("hc_lander_config").checkout_hash +
          "&lander_hash=" +
          this.landerHash;
      }
    };
    setSessionStorageValue = (key, value) => {
      sessionStorage.setItem(key, JSON.stringify(value));
    };
    getSessionStorageValue = (key) => {
      return JSON.parse(sessionStorage.getItem(key));
    };
  
    // in this function we will updated prices like offer price, actual price of product, discounted amount, shipping cost
    updatePrices = (params) => {
      let body = $("body");
  
      if (!params || !params.crm_id) {
        console.log("CRM ID not found to update Single product");
      }
      let ele_to_update = body.find(
        '[data-hc-product-crm-id="' + params.crm_id + '"]'
      );
  
      let price = params.price ?? 0.00;
      let product_name = params.product_name ?? '';
      let billing_discount = params.billing_discount ?? 0.00;
      let offer_price = price - billing_discount ?? 0.00;
      let shipping_cost = params.shipping_cost ?? null;
  
      //we also need to check if this element has any selected billing model id, then display other prices as per that billing model id
      let bill_ele = ele_to_update.find(
        "[data-hc-product-billing-model-id-selection]"
      );
      //get billing id , we either use it as drop down(in this case we will get its val)
      // or button/link/checkbox (in this case id is attached to data attr itself)
      let bill_id =
        bill_ele.length > 0 //if element exists
          ? bill_ele.val() > 0
            ? bill_ele.val() // if value exists
            : bill_ele.attr("data-hc-product-billing-model-id-selection") > 0 //else if attr value exists
            ? bill_ele.attr("data-hc-product-billing-model-id-selection")
            : 0
          : 0;
  
      if (bill_ele.length > 0 && bill_id > 0) {
        // it means we have theme which has product selection and also it
        // show all billing models discounted price and shipping amount
        // let bill_id = bill_ele.attr('data-hc-product-billing-model-id-selection');
        // get discounted amount and shipping price for selected billing model id
        // from params.billing_discounts & params.shipping_costs respct.
        billing_discount =
          params.billing_discounts && params.billing_discounts[bill_id]
            ? params.billing_discounts[bill_id]
            : 0.0;
        offer_price = price - billing_discount;
        shipping_cost =
          params.shipping_costs && params.shipping_costs[bill_id]
            ? params.shipping_costs[bill_id]
            : 0.0;
      } else {
        console.log("billing not found ");
      }
  
      //update name
      ele_to_update.find("[data-hc-product-name]").html(product_name);
  
      //update values of its child divs
      price ?
      ele_to_update.find("[data-hc-product-price]").html(parseFloat(price).toFixed(2)) : '';
  
      //offer price
      offer_price
        ? ele_to_update.find("[data-hc-product-offer-price]").html(parseFloat(offer_price).toFixed(2))
        : ele_to_update.find("[data-hc-product-offer-price]").html(parseFloat(price).toFixed(2));
  
      //user saved amount
      billing_discount
        ? ele_to_update
            .find("[data-hc-product-discounted-amount]")
            .html(billing_discount)
        : // else hide element removing hide show logic as it will be theme dependent and also
          ele_to_update.find("[data-hc-product-discounted-amount]").html("0.00");
  
      //shipping cost
      shipping_cost
        ? ele_to_update.find("[data-hc-shipping-cost]").html("Shipping $" + parseFloat(shipping_cost).toFixed(2))
        : // else add text free
          ele_to_update.find("[data-hc-shipping-cost]").html("Free Shipping");
    };
  
    renderProducts = (params) => {
      // here we can pass filter parameter wether to render single to all products
      // we can also show skeleton loader according to that if single then only that perticular area else whole data-hc-multiple-products-area area
      let body = $("body");
      let __this = this;
  
      //if passed in paramter
      let products = params.products ?? null;
      if (!products) {
        //check if present in session storage
        products = this.getSessionStorageValue("hc_lander_config").products;
      }
      if (!products) {
        console.log("Please provide products data to render");
      }
  
      //traverse through all prducts div, using the crm product id and assign initial values
      for (var i = 0; i < products.length; i++) {
        let ele_to_update = body.find(
          '[data-hc-product-crm-id="' + products[i].crm_id + '"]'
        );
        if (ele_to_update) {
          //default set, so even if user not do any event we will get default data while doing buy now
  
          ele_to_update.attr("data-hc-product-offer-id", params.offer_id);
          ele_to_update.attr(
            "data-hc-product-shipping-id",
            products[i].shipping_id
          );
          ele_to_update.attr("data-hc-product-name", products[i].product_name);
          ele_to_update.attr("data-hc-product-price", products[i].price);
  
          //update billing model drop down
          let billing_dp = "";
          if (
            ele_to_update.find("[data-hc-product-billing-model-id-selection]") &&
            ele_to_update
              .find("[data-hc-product-billing-model-id-selection]")
              .is("select")
          ) {
            if (
              params.billing_models &&
              Object.keys(params.billing_models).length > 0
            ) {
              let master_billing = params.billing_models;
  
              for (var k = 0; k < products[i].billing_model_ids.length; k++) {
                for (var j = 0; j < master_billing.length; j++) {
                  if (
                    products[i].billing_model_ids[k] == master_billing[j]["id"]
                  ) {
                    billing_dp +=
                      '<option value="' +
                      products[i].billing_model_ids[k] +
                      '">' +
                      master_billing[j]["name"] +
                      "</option>";
                  }
                }
              }
            }
  
            ele_to_update
              .find("[data-hc-product-billing-model-id-selection]")
              .html(billing_dp);
            // hide billing model drop down when only one billing model is there
              if(products[i].billing_model_ids.length == 1){
                ele_to_update
                .find("[data-hc-product-billing-model-id-selection]").parent().hide();
              }
          }
  
          // update prices of current product
          __this.updatePrices(products[i]);
          __this.initializeEvents();
        } else {
          console.log("Product with id " + products[0].crm_id + " not fuond");
        }
      }
    };
  
    updateParentAttributes = (_this, params) => {
      var elementDiv = _this.closest("[data-hc-product]");
      params.billing_model_id
        ? elementDiv.attr(
            "data-hc-product-billing-model-id",
            params.billing_model_id
          )
        : "";
      params.product_crm_id
        ? elementDiv.attr("data-hc-product-crm-id", params.product_crm_id)
        : "";
    };
  
    updateDisplayPrice = (params) => {
      this.getProducts(params, this.renderProducts);
    };
    clearSessionStorate = () => {
      sessionStorage.removeItem("hc_lander_config");
      sessionStorage.clear();
    };
  
    initializeEvents = () => {
      let __this = this;
  
      $(document).on("click", "[data-hc-buy-it-now-btn]", function () {
        __this.generateBaseCheckoutURL();
  
        let _this = $(this).closest("[data-hc-product]");
        // we will send lander_hash in URL using that on the checkout page we will get
        // lander related campaign,offer,billing ids and other data
  
        let product_crm_id = _this.attr("data-hc-product-crm-id")
          ? _this.attr("data-hc-product-crm-id")
          : 0;
        let product_billing_model_id = parseInt(
          _this.attr("data-hc-product-billing-model-id")
            ? _this.attr("data-hc-product-billing-model-id")
            : 0
        );
        //handle page back event, if user goes to checkout using diff billing id and come back
  
        var bill_id = _this
          .find("[data-hc-product-billing-model-id-selection]")
          .val();
  
        if (bill_id && bill_id > 0) {
          product_billing_model_id = bill_id;
        }
  
        let product_quantity = parseInt(
          _this.find("[data-hc-product-qty-select]").val() > 0
            ? _this.find("[data-hc-product-qty-select]").val()
            : 1
        );
  
        let processUrl =
          "products=" +
          product_crm_id +
          "&billing_id=" +
          product_billing_model_id +
          "&quantity=" +
          product_quantity;
        // clears session storage and remove products, so when user press back or come back on site we will load fresh data from server
        __this.clearSessionStorate();
  
        window.location = __this.checkoutURL + "&" + processUrl;
      });
  
      $(document).on(
        "change",
        "[data-hc-product-billing-model-id-selection]",
        function () {
          let params = {};
          if ($(this).is("select") == true) {
            // for drop down
            params.billing_model_id = $(this).val();
            params.product_crm_id = $(this)
              .closest("[data-hc-product]")
              .attr("data-hc-product-crm-id");
            // update price/shipping/offer-price of product
            __this.updateParentAttributes($(this), params);
            let products = __this.getProducts();
  
            let product = products.find(
              (element) => element.crm_id == params.product_crm_id
            );
            if (!product) {
              console.log("Product not found in response");
              return true;
            }
            __this.updatePrices(product);
          }
        }
      );
    };
  
    init = () => {
      this.updateDisplayPrice();
      //  this.initializeEvents();
    };
  
    getProducts(params = [], callback = "") {
      //params may contains filter criteria, we can also use same function to get one or many products using filters
      let products = {};
      let hc_lander_config = {};
      let __this = this;
  
      var XHRData = {};
      XHRData.url = "get-lander-config";
      XHRData.data = {
        "X-LL-Hash": this.landerHash,
      };
  
      hc_lander_config = this.getSessionStorageValue("hc_lander_config");
  
      if (hc_lander_config && hc_lander_config.products) {
        if (callback) {
          callback(hc_lander_config);
        }
        products = hc_lander_config.products;
      } else {
        $.when(this.proceessXHR(XHRData)).done(function (resp1) {
          // the code here will be executed when ajax requests resolved.
          let resp = resp1 ?? null;
          if (resp.status && resp.data && Object.keys(resp.data).length > 0) {
            __this.setSessionStorageValue("hc_lander_config", resp.data);
            products = resp.data.products;
            if (callback) {
              callback(resp.data);
              __this.generateBaseCheckoutURL();
            }
          }
        });
      }
      return products;
    }
  
    proceessXHR = (params) => {
      // used return so that calling function will wait for this ajax call to be done and we can use the ajax response there
      return $.ajax({
        url: this.apiURL + params.url ?? "404",
        type: "post",
        data: params.data ?? { "X-LL-Hash": this.landerHash },
        dataType: "json",
        success: function (resp) {},
        error: (resp) => {
          console.log("Error in proceessXHR ", resp);
          console.log(" \n Request was ", params);
        },
      });
    };
  }
  
