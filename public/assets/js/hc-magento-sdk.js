window.addEventListener("load", function() {

    var locationRedirect = "";
    function __init(){
             var env = jQuery(jQuery('[data-hc-url]')).attr('data-hc-url');
             env = env ? env : 'https://checkout.myhostedcheckout.com/';
             var checkoutURL = env+'app/slug='+window.location.host;

             var billingModelIds = [];

             //For catalog page
             jQuery('.tocart').click(function(e){
                 try{
                    // e.preventDefault();
                    // e.stopPropagation();
                    var postData = jQuery(this).data("post");
                    let product = postData && postData.data && postData.data.product ? postData.data.product : "";
                    var allProducts = JSON.parse(window.localStorage.getItem('offer_products'));
                    allProducts = allProducts ? allProducts : [];
                    let productDetail = {
                        product_id:product,
                        type:"simple",
                        variants:[],
                        billing_model_id: 0
                    }
                    let productIndex = -1;
                    console.log(allProducts);
                    for(let i=0;i<Object.keys(allProducts).length;i++){
                        console.log("in loop",allProducts[i]['product_id']);
                        if(allProducts[i]['product_id'] == product){
                            productIndex = i;
                            break;
                        }
                    }

                    if(productIndex>-1){
                        allProducts[productIndex] = productDetail;
                    }else{
                        allProducts.push(productDetail)
                    }

                    window.localStorage.setItem('offer_products',JSON.stringify(allProducts));
                    
                 }catch(e){
                     console.log("Error:",e);
                 }
                
            });

            
             jQuery("#product-addtocart-button").click(function(e){
                 try{   
                    // e.preventDefault();
                    // e.stopPropagation();
                    var allProducts = JSON.parse(window.localStorage.getItem('offer_products'));
                    allProducts = allProducts ? allProducts : [];
                    let product = jQuery('input[name="product"]').val();
                    let options = [];
                    let ind = 0
                    let variantIndex = "";
                    let productType = "simple";
                    let selectedConigOption = jQuery("input[name='selected_configurable_option']").val()

                    let attrCode = "";
                    let optionSelected = "" ;
                    let attribute_id = "";
                    jQuery(".swatch-attribute").each(function(){
                        productType = "configurable";
                        let ele = jQuery(this)
                        attrCode = ele.attr("attribute-code");
                        optionSelected = ele.attr("option-selected");
                        attribute_id = ele.attr("attribute-id");
                        
                        if(optionSelected){
                            options[ind] = {
                                attribute_code:attrCode,
                                option_selected:optionSelected,
                                attribute_id:attribute_id,  
                            }
                            variantIndex+=attribute_id+optionSelected
                            ind++ ;
                        } 
                    });

                    //for input type select option 
                    if(variantIndex=="" && selectedConigOption){
                        options = [];
                        ind = 0

                        jQuery(".super-attribute-select").each(function(){
                            productType = "configurable";
                            let attrName = jQuery(this).attr("name");
                            attribute_id = attrName.split('[').pop().split(']')[0];
                            optionSelected = jQuery(this).children("option:selected").val()
                            let id = jQuery(this).attr("id");
                            attrCode = jQuery('[for="'+id+'"] span').html()

                            if(optionSelected){
                                options[ind] = {
                                    attribute_code:attrCode,
                                    option_selected:optionSelected,
                                    attribute_id:attribute_id,  
                                }
                                variantIndex+=attribute_id+optionSelected
                                ind++ ;
                            } 

                        });
                    }

                    if(productType=="configurable" && variantIndex=="") return true; 
                   
                    let billing_model_id = jQuery("[name=crm_billing_model]").val() //we will get that value from html selector
                    billing_model_id = billing_model_id ? billing_model_id:0;
                    let productIndex = -1;
                    
                    for(let i=0;i<Object.keys(allProducts).length;i++){
                        console.log("in loop",allProducts[i]['product_id']);
                        if(allProducts[i]['product_id'] == product){
                            productIndex = i;
                            break;
                        }
                    }
                    

                    let productDetail = {
                        product_id:product,
                        type:"simple",
                        variants:[],
                        billing_model_id: billing_model_id
                    }
                    
                    if(variantIndex!=""){
                        productDetail["type"] = "configurable";
                        productDetail["billing_model_id"] = "";
                        
                    }

                    if(productIndex<0){
                        productDetail["variants"] = {};
                        productDetail["variants"][variantIndex] = {options:options, billing_model_id:billing_model_id};
                        allProducts.push(productDetail)
                    }else{
                        if(variantIndex!=""){
                            allProducts[productIndex]["variants"][variantIndex] = {options:options, billing_model_id:billing_model_id};
                        }
                        
                        allProducts[productIndex]["billing_model_id"] = productDetail["billing_model_id"];
                        allProducts[productIndex]["type"] = productDetail["type"];
                    }

                    window.localStorage.setItem('offer_products',JSON.stringify(allProducts));
                    return true;

                 }catch(e){
                    console.log(e);
                 }
             });
             
             //id="product-addtocart-button"

             //Add to cart action tocart


             //for checkout process
            //data-role="proceed-to-checkout"
            //id="top-cart-btn-checkout"

            function getQuoteID(){
                try{
                    return  window.checkoutConfig && window.checkoutConfig.quoteData && window.checkoutConfig.quoteData.entity_id ? window.checkoutConfig.quoteData.entity_id : "" ;

                }catch(e){
                    return "";
                }
            }
            function processCheckout(){
                try{
                    
                    let product = [];
                    let cart = JSON.parse(window.localStorage.getItem("mage-cache-storage"))//require('Magento_Customer/js/customer-data').get('cart')();
                    cart = cart ? cart.cart : [];
                    let items = cart && cart.items;
                    console.log(items, Object.keys(items).length);
                    let products = [];
                    let skus = [];
                    let qtys = [];
                    let billingIds = [];
                    if(items && Object.keys(items).length>0){
                            for(var i=0;i<Object.keys(items).length;i++){
                               
                               let product_id = getPathVariable("product_id",items[i]['configure_url']);
                               console.log(product_id);
                               items[i]['product_id'] = product_id;
                               let productIndex = products.indexOf(product_id);
                               let billing_model_id = getBillingModelID(items[i]);

                               if(productIndex>-1){
                                //update
                                skus[productIndex] = skus[productIndex]+")("+items[i]['product_sku'];
                                qtys.push(items[i]['qty']);
                                billingIds[productIndex] = billingIds[productIndex]+"-"+billing_model_id
                               }else{
                                //add
                                products.push(product_id);
                                skus.push(items[i]['product_sku']);
                                qtys.push(items[i]['qty']);
                                billingIds.push(billing_model_id)
                               }
                               console.log("loop");
                            }
                    }
                    checkoutURL+='&products='+products.join()+"&quantity="+qtys.join()+"&variant="+encodeURI(skus.join())+"&billing_id="+billingIds.join();
                    let quote_id = getQuoteID();
                    window.location = checkoutURL+"&quote_id="+quote_id;
                }catch(e){
                    console.log("Error",e);
                }
            }

            function checkoutAction(){
                try{
                    if(window.location.pathname == "/checkout/"){
                        processCheckout();
                    }
                }catch(e){
                    console.log(e);
                }
            }

            

            function getPathVariable(variable, url) {
                let origin = window.location.origin;
                var path = url.replace(origin,url);
      
                var parts = path.substr(1).split('/'), value;
                while(parts.length) {
                  if (parts.shift() === variable) value = parts.shift();
                  else parts.shift();
                }
              
                return value;
            }

            function getBillingModelID(itemDetail){
                try{
                    let billing_model_id = 0
                    if(!itemDetail) return billing_model_id;

                    let offerProducts = JSON.parse(window.localStorage.getItem('offer_products'));

                    if(!offerProducts) return billing_model_id;

                    for(let i=0;i<Object.keys(offerProducts).length ; i++){
                        if(offerProducts[i]["product_id"] == itemDetail["product_id"] && itemDetail["product_type"]!="configurable" && offerProducts[i]["type"] != "configurable"){
                            billing_model_id = offerProducts[i]["billing_model_id"];
                        }

                        if(offerProducts[i]["product_id"] == itemDetail["product_id"] && itemDetail["product_type"] == "configurable" && offerProducts[i]["type"] == "configurable"){
                            let variants = offerProducts[i]['variants']; //local
                            
                            for(let j=0;j<Object.keys(variants).lengh;j++){
                                let options = variants[j]['options'];
                                let cartOptons = itemDetail['options'];
                                let matchedTrue = [];

                                for(let c=0;c<Object.keys(cartOptons).lengh ; c++){
                                    let option_id = cartOptons[c]['option_id'];
                                    let option_value =cartOptons[c]['option_value'];

                                    for(let k=0; k<Object.keys(options).lengh ; k++){
                                        if(option_id == options[k]['attribute_id'] && option_value == options[k]['option_selected']){
                                            matchedTrue.push(true);
                                            break;
                                        }
                                    }
                                }

                                if(Object.keys(cartOptons).lengh == matchedTrue.lengh){
                                    billing_model_id = variants[j];
                                    break;
                                }
                                //attripute-id option-selected // from local
                                //option_id    option_value   // from cart 
                            }
                        }
                    }
                    
                    return billing_model_id;
                }catch(e){
                    return 0;
                }
            }

            jQuery(document).ready(function(){
                console.log(window.checkoutConfig);
                checkoutAction();
            });
    }
 
    if(typeof(jQuery) === "undefined"){
         var script = document.createElement('script');
         script.type = "text/javascript";
         if(script.readyState) {  // only required for IE <9
             script.onreadystatechange = function() {
             if ( script.readyState === "loaded" || script.readyState === "complete" ) {
                 script.onreadystatechange = null;
                 __init();
             }
             };
         } else {  //Others
             script.onload = function() {
                 __init();
             };
         }
 
         script.src = "https://code.jquery.com/jquery-3.5.1.min.js";
 
         document.head.appendChild(script);
     }else{
         __init()
     }
     
 });