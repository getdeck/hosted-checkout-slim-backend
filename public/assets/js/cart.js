$(function(event) {
    var env = jQuery(jQuery('[data-hc-url]')).attr('data-hc-url');
    env = env ? env : 'https://checkout.myhostedcheckout.com/';

    cart_sdk = {
        hc_ulr:env+"app/",
        product: {},
        defaultProductImage : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAilBMVEX///+qqqqlpaWjo6MxMTGnp6fV1dVDQ0OJiYk2NjY7OzsvLy/g4ODKyso4ODiXl5fr6+sqKiqysrL4+Pjy8vK+vr7ExMTt7e3k5OTR0dGwsLA+Pj5ERES4uLhqamqDg4NMTEx9fX10dHQdHR2Hh4dWVlZgYGARERFbW1sEBASSkpIXFxdubm4kJCS41QjUAAAPD0lEQVR4nO1d2WKqOhRlDGWUWRSp1qrt8bb//3s3IzMaFRS06+GcqoFkJXtKdgiC8Ic//OEPz4MgnLsJgTsPg0c3pz+ELljEmSjJUhWyJGbxAiThoxt4AwIXxL4qSaqqiu2Av0Cufgzc6Q1pYMVLxK1MBkPCjDFKv0nSMramM5qBZYsyI0BIiX4U2wsALAtqoWUBsLDjyBel0gDDcmJsTWAsA5DBZrM2y2JmA7d7cMIE2JEos8GGnZGBUZMMgF+wUyOQ8LUWKmykFiz90ZK0Mpk1Urxcq0IrFln3yJk1SAtvQmhDXmTwMnCtzQihiJOhlFR7XIYnocOnytGNxiKwInorKUt6at3tsESpTw3KtVkSxyGsYAC5KmQe9HbPa0H4weHru7stMpCP5miJKtE+d4Cbz4lGqg+UVXcpYX7xUGYvjDFHaTlEB55HEMnD8sOVUI7RA4IAgKuWo6HdVkg7cjFwPXXMl7hr/XuIj+tjZVjO71BXDhv3q3ovE2Bhgy3bd6oODqB45wpZl4p3GsaFfH+hoWpxF20MMuleVVWBO1bKBjeqLoo01OUjAv8QDaMqDRyP4468qwaWgbVxWPGJJGRCHzerSZBRlaLB7h9gMfEfucYQ+FhJBmrC/O4+og3EbwxiyBOsBI+fk1q4HQNoChiu7y4EkaXep43IiA4m/xcC24O+TaqNjKjf7z1vQIZMaq8WARMczkhfjqhnioigFPd3vx4QS31SHCHBfikueu2u3oD7vRdzg9zE6EYQAY1iH04DOXp1jAQhRbUP1z+XR2ZFy0AWVb4xCAnUUfnBOpBfVG8LQ5YwQFr21JwhcHP7otv7aFhgGbtBiZCfuFXOBwayE9f7DGRGRzBdOg00mbrWoAYoGB2fp6/DRiHqdZoEDdWIzWgBH7Yzu+ZCpISjtjIMyNpco4rzG+T7zsD24nKDKE5CCQmQKorXXDRmV1/F8vLhwDI6rj07pxBeLqeoU+6ffLkei0tF7uILHg40JBfMFYPrjNMjgdWK37fBgHsydpQBmkb+ENxFs94hWzMI0GyYd+MElGlp7AF3E5bEbTtQ0SnEo3X43AMjXjDcYwJSLq7IBlyisqMCMpA8HkOdVDRTRshnIdEQjnN59DxirkFUL/Kc40LAM4jWhIeQDOI5cypOVgsRwvPmNJGmakgJoDk9s2kqg15zir6QwYXRyslVKTTKUwxnCvhntAwG6BOMSMuAIefJaZE6xUlFFacpnOuAKeC0GCI7M11XQRCesjXB5O0Mgn8iKAOcsfm4cYrFKfbTwQlJDCYezzBkale2DXCErVOA1Smm2dWZxnEh6LKmnT9MDl1Dhdz99C0pAuhw+nD2OHl3TxB2hGbiFVnGkaKdCpw4TXj5ogooji2O3XoSX4HQzuV51JAoYlMelzdODUNg231u3bBse3H1/doSvJwhm/if98XM1OLrK0+ER3tPURRTOTbEIPv6hxd+5E/v85h/u/31vgrnm/3n6ZWL7B90O8X0tjGry8vx77wyRS2Bm8vnDX1npe3p30BR6BXBQTEUY78xHcOsS0ekm5ihBK802JeBqa30okMPqaaUdg0EP6ahm5v9RlF0ZRWSulaOQvHfeYbII9YX1Nq+a2eYKnGd4cbQf1C1gWhqXo1iwVDbK2z4fX2vFQxdT9sYH8Ul8HYHGw8BOHq/FqnLiRKL4nxo2TZe7Qa2hWH6k25qDD8MfUZ/TxzNq24AKBgaor6lX+510SgYys4+1s289h9DmeWXh+9WpS4+BC2uz+fz976jW4pONIjVannpe17AVtKPyhUFQyX0KI3EM1ylYKg5kmDqTC0XZvUOQbkuXojNOSLn3NB3TGFnaEK51nVqljTjPfUq1qZg6Ak/Cql2Bi2SmTNcKLDEzmHqvU3N5jaQSxmifZXVb7pCuTogwyD09GW5VkXblEpEjl7pqjLDWCE0YJfMC4YfzgHdzKS+wSxJRI5LGdoN755wTiwQQ+FoOKVaXdM4lkokSuVjhaGge4iGrWzgVYxhQAR05RDlsxRDbtYL67oo4GoaTk5TShiGpqMKOcOF4lS8q5duyx8rDN+wDflQ1BJDH/2AzA3xJbHutKgLULSVRpDumz830ByxBWfMhhkKM0cPcoa2XhVLryplFYbATNGgQXNbMNwba/SfaxJfEuk6VZfgOENQCcPcH5objnYiravuy0NRKceFlGGgOHKJoVNZGvDSn06GwsZcCL7yLhQMLZM6+4OxpQypnQ+/IB9HPxCGjg8YeBraiEwzzskhYQhFSgkYQ1h52WwFNWNfZSgpH8K3EpUYHo0VLeiYSIqKDsNjuCcScamlQe4iq3/BtdpNGQq6c2S1hl66LpWoW4oqw7mnWyaKQXOGTnrAwjg7pg7q5KRquH6M6xj69dibd6mUMRQdL2C1rjSnVEJ1lIrbqTKE7nJjvAkFw1jXmIKlxOukLGbCeL+SYVRXO94JPmMopMbRorXKhl66dqVVpwk1hpGumcTHEIY/6X5GsTNw5HA0ymSuZYii0PLngDetljP0oQV1SK2hqWl5UCk7etWd1RgKDhkpynDuOYX+G2R0vXIEcS1DuzZ/ahrXDuQMhVW60WitopJ+02+XZkXGhCZDCspQdUqB+tHAw6/C2+VfXsuw7v7mvGulBUMobitW605JdRm4VnRQUqcWVJ5muEoPxXeARuNvZqrsYstNwPJg6JRhelTZGcs80Q0KYcotQfMprqhoaf6ysd8Yqcd6RTQdHdoK3TEP9bghMn/JHF/5rTD8NSEb4Jllm74izk/IdB2aH9NE9ofEcsBLdTYD/uRZma8HaQkvQ3u9ZgwX67d1fk0oHQxP2aybor5YrzHpeFdxk+EalfV363KPZLsPevdsu1E8ffPBDk2y1m85WippwqptrOFmOBm0MBz6/KU7o87oj+H00MLw+fXwyRly+8PJoO4PuWOayaAe03DHpZNBPS7lnltMBvW5Bff8cDKozw+55/iTQWOOz7tOMxk01ml419qE+Mv7JH/tPv+VVfn99z/yx/Hzt7Rg6n2SKcW/z+ZKdq0kgVlMUOKvSjI0LmdJzy1fN9baeNdLhW2q0VUZyywtQAihZ9CUmK5pZkFdwysTsP1O8/61klqKp3+64dFFc1uvLOXb+iqfI36dY9hYL+Vd8w5NbcNWfL/LCxaiQzMrMZz7l6ifYFgvmX6gHCjI9ilNRzYY6jFvlrTp/njzFqKzWug0++U7ZhEl7NNv8sePs5adTdHuboYdJeG9NI9SqjFUuGcHzbwFb+5p48wEg65xB0reJiSyxFLNTcVOzIJ6N8OukrjrcB82GXJHls0R48wfAtO0hBnr+I80Xxo9GnSFSkQppL2TU+9m2FUSr+/fyrCZP+R0iG8oU2uZdI1toeRLwQ7babBxjoKwdMx6u5sMu0qiniOL6DcwbOaAOfP4Jk4V7llbUmZ0Yp0mkICC1q1LKexOhi0lSb4inCme30YJfuR+JKvFv3PtxYh0nKPPO142aM7+J03JN2/GBv23NZij62TYLKnhJGiqOPsop1RlWGRJWxLhZbTtxeAypu8kxRd6tONdjxgd6AypP6c+MqbLwCcYNktSfwgFHnQwzP2hdxBOom0/Dc+eKNekerc1aA3vxCWKjkeaSQcZ6qUjn2bYUjLdWgCARfaueB3+0IlYkvSMQraNF8++NpZqh3WZjBG29hsmNTSPKwgzlvfsYthdEn1FFtOv94dt+9p49iZq6bdMk5lMKnXULMDMRWKmW5IpW6fU9HQwPFESgu46ut6Wth4+c35/6UIpJTPJpiFoL3T0j0I+yQbLLugpzXV3MDxRUkAB0uYmhu37S8/vEf4wNiyZCTkx7wBVU2dtgzaOldimXnCC4YmSApL61U0M27mc3ecd5LMH3Bzm4dMtrJlFAMUeyrlHNhy0MzxVEjlJotdXM+xwfef26sNosagAekIWpaUHNsdgsQjGwfgut9urbNg4VRJZaLL/rWZa+C1NB5Vzivhd3oyUsEg79FYai8S98i4Kn0yncobpNs9wBu0l03f02k557aR0F5+tGywtKvuVj6ezpF1B9plnZqxfpTwKe53y/XGMX9IxS9MrXR96CmJhKDv88TfN9/h+Be0lDVJC1z26e8Uu0qLIxdvlLOkphQIdD+Wfee4p2q3LK6zx7oN8BOsdVc/Z7q18Afn4tiNm+6OU4ewoSTHLWD2gSIvu5MrHt/Wp6KTzEbWnf3btBZ4/fP5nSJ//OeAXeJb7+Z/Hf5YzFU6cKvz852I8/9kmL3A+zfOfMfT850S9wFlfz39e2wucuff85ya+wNmXz39+6QucQfv85wi/wFnQz3+e9wucyf785+q/wLsRkMd47vdbvMA7Sl7gPTPP/66gF3jf0wu8s+v537v2Au/Oe4H3H+L4dBrvsLx6svf07yF9gXfJvsD7gF/gnc7P/17uF3i3OpwrIms1RooxsvS97KxAPmOEFG3pNj/RuNfYKKIRlHqLuEZIsV+ChOKoLCqyon0SpBTH4xf93glCc4OcxnIcAVywRG6i94Uy5DREdQxh+BytWfTjJqpArn8MkykLt2OQ9RXSd4+eEttDyhKWf9V/pDIGyMYMaQ8iZFLVx63AJSoyooO6rYX8SEnFEtq/Ea3CVbGYPGLBP8RKMvx5VkEm3aEjW4DFR8ruYQVwVeryvq5xvlTv2LFz8e5+g/gI8X69Siq82/Mnlnp/V0yERvLvse/G9aUHqAWKU3G/RkNb1TDC8jJEHHoOQYS7Vo6H5BjEtCMfE0i5y4E5hoSftHzcJjQLW1VVjoZoghthfqr42PkMwGZOlfy+m2H5Er7zCJ7EAiqSVVFS7f6ENbTJTcfAD8ESJTqQoA+DEAAyfKL0YPksI8mwykCNzDje5HMKgVXcalz7JJBcEb2RMnCtuIYgk1S1d5nvDaz3obiKsXVpA0MrJsJOJGGQFt6OXIPQUKoRSPgkNnBBpNLB602bB0MABa1gKYuZDZLu4QwTYEeiXLCDIj5qegSBZYsybTOkiQ56EP0othcAWFaSJJYFwMKOIx8dEqEWBWUx5hz0MSCw4qXEhoYyxSCkMEq/SdIyvtEIPwJQvWJfLQ1TA4SxHwN3euwKhC5YxNlSkqUqZGmZxYtTSjo5BOHcTQjceTjlQfvDH/7whzr+BzDd7U2Tey7UAAAAAElFTkSuQmCC",
        mustacheHtml : '<table class="table striped">{{#showTitle}}<thead><th>Product Image</th><th>Product Name</th><th>Product Price</th><th>Product Quantity</th><th>Action</th></thead>{{/showTitle}}<tbody>{{#products}}<tr><td><img src="{{product_image}}" height="100" width="100"/></td><td>{{product_name}} <br/><br/> {{#variant}} {{ option }}<br> {{/variant}} </td><td>{{price}}</td><td class="col-md-3"><input name="updateQty" style="width: 40px !important;" type="number" value="{{quantity}}" readonly/></td><td><button type="button" data-hc-remove-product="{{id}}" >Remove Product</button></td></tr>{{/products}}</tbody></table>',
        popUpHtml : '<div class="modal" cart-hc-invalid-qty role="dialog"><div class="modal-dialog modal-sm"><div class="modal-content"><div class="modal-header" style="display: table-cell;"><button type="button" class="close" data-hc-close-popup>&times;</button><h4 class="modal-title">Notification</h4></div><div class="modal-body"><p>Please add valid quantity.</p></div><div class="modal-footer"><button type="button" class="btn btn-default" data-hc-close-popup>Close</button></div></div></div></div>',
        addTocart: function(product, isVariant, _btn_this, event, toggle) {
            var cartInfo = cart_sdk.manageCartArray(product, isVariant, _btn_this, event, toggle)
            cart_sdk.setLocalStorageValue('cart', cartInfo)    
        },
        setLocalStorageValue: function(key, value) {
            localStorage.setItem(key, JSON.stringify(value))
        },
        getLocalStorageValue: function(key) {
            return JSON.parse(localStorage.getItem(key))
        },
        manageQuantitySelector : function (_this, value) {
            _this.each(function () {
                if($(this).is('select') == true){
                   $(this).val(value) 
                }

                if($(this).is('input') == true){
                    switch($(this).attr('type')){
                        // case "radio":
                        //     $(this).each(function () {
                        //         if(parseInt(value) == parseInt($(this).val())){
                        //             $(this).prop("checked", true);
                        //         }
                        //     })
                        // break;

                        case "number":
                            // console.log(value)
                            $(this).val(value)
                        break;

                        case "text":
                            $(this).val(value)
                        break;

                        default:
                            return false;
                        break;
                    }
                }
                
            })
        },
        manageCartArray: function(product, isVariant, _btn_this, event, toggle) {
            var cartInfo = cart_sdk.getLocalStorageValue('cart')
            var newCartInfo = {};
            if(isVariant){
                if (cartInfo && cartInfo.cartInfo !== null && Object.keys(cartInfo.cartInfo).length > 0) {
                    var checkUpdate = false;
                    if(toggle == true){
                        if(product.id in cartInfo.cartInfo){
                            if(JSON.stringify(cartInfo.cartInfo[product.id].variant) == JSON.stringify(product.variant)){
                                delete cartInfo.cartInfo[product.id]
                            }else{
                                cartInfo.cartInfo[product.id].variant = product.variant
                            }
                            newCartInfo = cartInfo
                        }else{
                            cartInfo.cartInfo[product.id] = product
                            newCartInfo = cartInfo
                        }
                        return newCartInfo
                    }
                    $.each(cartInfo.cartInfo, function(index, value) {
                        if (value.id == product.id) {
                            checkUpdate = true;
                            if(JSON.stringify(value.variant) == JSON.stringify(product.variant)){
                                if(event.type == "click"){
                                    value.quantity = parseInt(product.quantity);
                                }
                                if(event.type == "change"){
                                    value.quantity = parseInt(product.quantity);    
                                } 
                            }else{
                                if(event.type == "click"){
                                    value.quantity = parseInt(product.quantity);
                                }
                                if(event.type == "change"){
                                    value.quantity = parseInt(product.quantity);    
                                }
                            }
                            value.variant = {}
                            value.variant = product.variant
                            cart_sdk.manageQuantitySelector(_btn_this.find('[data-hc-product-qty-select]'),value.quantity)
                        }
                    });

                    if (checkUpdate == true) {
                        // Updates QTY if product exists in cart
                        newCartInfo = cartInfo
                    } else {
                        // Add product in cart
                        cartInfo.cartInfo[product.id] = product
                        newCartInfo = cartInfo
                    }
                } else {
                    // Add product in cart
                    var cartInfo = {}
                    cartInfo.cartInfo = {}
                    cartInfo.cartInfo[product.id] =  product
                    newCartInfo = cartInfo
                    cart_sdk.manageQuantitySelector(_btn_this.find('[data-hc-product-qty-select]'),product.quantity)
                }
            }else{
                if (cartInfo && cartInfo.cartInfo !== null && Object.keys(cartInfo.cartInfo).length > 0) {
                    var checkUpdate = false;
                    if(toggle == true){
                        if(product.id in cartInfo.cartInfo){
                            delete cartInfo.cartInfo[product.id]
                            newCartInfo = cartInfo
                        }else{
                            cartInfo.cartInfo[product.id] = product
                            newCartInfo = cartInfo
                        }
                        return newCartInfo
                    }
                    $.each(cartInfo.cartInfo, function(index, value) {
                        if (value.id == product.id) {
                            checkUpdate = true;
                            if(event.type == "click"){
                                value.quantity = parseInt(product.quantity);
                            }
                            if(event.type == "change"){
                                value.quantity = parseInt(product.quantity);    
                            }
                                
                            cart_sdk.manageQuantitySelector(_btn_this.find('[data-hc-product-qty-select]'),value.quantity)
                        }
                    });

                    if (checkUpdate == true) {
                        // Updates QTY if product exists in cart
                        newCartInfo = cartInfo
                    } else {
                        // Add product in cart
                        cartInfo.cartInfo[product.id] = product
                        newCartInfo = cartInfo
                    }
                } else {
                    // Add product in cart
                    var cartInfo = {}
                    cartInfo.cartInfo = {}
                    cartInfo.cartInfo[product.id] =  product
                    newCartInfo = cartInfo
                    cart_sdk.manageQuantitySelector(_btn_this.find('[data-hc-product-qty-select]'),product.quantity)
                }
            }

            
            return newCartInfo
        },
        renderCart : function () {
            var body = $("body")
            var cartInfo = cart_sdk.getLocalStorageValue('cart') 
            if(cartInfo != null && cartInfo.cartInfo != null && Object.keys(cartInfo.cartInfo).length > 0){
                cartInfo = cartInfo.cartInfo
                var result = cart_sdk.manageMustageInfo(cartInfo);
                // console.log(result)
                if(body.find('[data-hc-cart-count]').length > 0){
                    var tag = body.find('[data-hc-cart-count]').get(0).tagName
                    var totalCartItem = 0;
                    if(tag == 'INPUT' || tag == 'SELECT'){
                        Object.keys(result).forEach((key)=>{
                            totalCartItem = parseInt(parseInt(totalCartItem) + parseInt(result[key].quantity))
                        })
                        body.find('[data-hc-cart-count]').val(totalCartItem)     
                    }else{
                        Object.keys(result).forEach((key)=>{
                            totalCartItem = parseInt(parseInt(totalCartItem) + parseInt(result[key].quantity))
                        })
                        body.find('[data-hc-cart-count]').html(totalCartItem)
                    }
                }
                if(body.find('[data-hc-render-cart]').length > 0){
                    var dataHcRenderCart = body.find('[data-hc-render-cart]')
                    var renderData = {};
                    renderData['products'] = result
                    if(dataHcRenderCart.attr('data-hc-cart-title') == "true"){
                        renderData['showTitle'] = true
                    }

                    if(dataHcRenderCart.attr('data-hc-cart-title') == "false"){
                        renderData['showTitle'] = false
                    }
                    // Default html for cart
                    var cartProductDetails = cart_sdk.mustacheHtml;

                    // Set custom html for cart 
                    if(body.find('[data-cart-details]').length > 0){
                        cartProductDetails = body.find('[data-cart-details]').html()
                    }
                    var rendered = Mustache.render(cartProductDetails, renderData);
                    body.find('[data-hc-render-cart]').html(rendered);
                }
            }else{
                body.find('[data-hc-render-cart]').html("Cart is Empty");
            }
        },
        manageMustageInfo : function (data) {
            var arr=[];
            for(var i in data ){
                var o = data[i];
                //if you want to preserve ids
                //o.price = "$ " + o.price.toFixed(2)
                o.price = o.price.toFixed(2)
                o.price_total = (o.price * o.quantity).toFixed(2)
                if(Object.keys(o.variant).length > 0){
                    var tempVar = [];
                    
                    Object.keys(o.variant).forEach((key) => {
                        tempVar.push({ option : key + " : " + o.variant[key] })
                    })
                    o.variant = tempVar
                }
                o.id = i;
                arr.push(data[i]);
            }
            return arr
        },
        addProductIntoCart : function (_this, event) {
            var productObj = _this.closest('[data-hc-product]')

            switch(event.type){
                case "click":
                    cart_sdk.clickEvent(productObj,event)
                break;

                case "change":
                    if(productObj.find('[data-cart-button]').length > 0){
                        return false
                    }else{
                        cart_sdk.clickEvent(productObj,event)
                    }
                break;

                default:
                    return false;
                break;
            }
        },
        clickEvent: function (_this, event) {
            try{
                var product = {};

                product.id = ((_this.attr('data-hc-product')) ? _this.attr('data-hc-product') : 0)

                product.product_name = (_this.attr('data-hc-name')) ? _this.attr('data-hc-name') : 0

                product.product_billing_model_id = parseInt((_this.attr('data-hc-product-billing-model-id')) ? _this.attr('data-hc-product-billing-model-id') : 0)

                product.product_offer_id = parseInt((_this.attr('data-hc-product-offer-id')) ? _this.attr('data-hc-product-offer-id') : 0)

                product.price = parseInt((_this.attr('data-hc-product-price')) ? _this.attr('data-hc-product-price') : 0)

                product.product_image = ((_this.find('[data-hc-product-image]').attr('src')) ? _this.find('[data-hc-product-image]').attr('src') : cart_sdk.defaultProductImage)

                product.quantity = parseInt((_this.find('[data-hc-product-qty-select]').val() > 0) ? _this.find('[data-hc-product-qty-select]').val() : 0)

                var toggle = false;
                if(_this.attr('data-hc-toggle') == "true"){
                    toggle = true
                }

                if(_this.find('[data-hc-product-qty-select]').length == 0){
                    product.quantity = parseInt(1)
                }

                if(product.quantity == 0){
                    if($('body').find('[cart-hc-invalid-qty]').length > 0){
                        $('body').find('[cart-hc-invalid-qty]').modal('show');     
                    }
                    return false
                }

                product.variant = {}
                
                if(_this.find('[data-variation]').length > 0){
                    var variantArr = {};
                    variantArr = {}
                    _this.find('[data-variation]').each(function (event) {
                        variantArr[$(this).attr('data-variation')] = $(this).val()
                    })
                }

                product.variant = (variantArr) ? variantArr : {}
                // Check product is valid or not

                if (product.id && product.id != "") {
                    if(Object.keys(product.variant).length == 0){
                        cart_sdk.addTocart(product, false, _this, event, toggle)
                    }else{
                        cart_sdk.addTocart(product, true, _this, event, toggle)
                    }
                    cart_sdk.renderCart()
                }

            }catch(e){
                console.log("Cart Process Error", e);
            }
        },
        onloadSetQtyInproduct : function (_this) {
            var cartInfo = cart_sdk.getLocalStorageValue('cart')
            if(cartInfo && cartInfo.cartInfo != null && Object.keys(cartInfo.cartInfo).length > 0){
                $('body').find('[data-hc-product]').each(function () {
                    if($(this).attr('data-hc-product') in cartInfo.cartInfo){
                        $(this).find('[data-hc-product-qty-select]').val(cartInfo.cartInfo[$(this).attr('data-hc-product')].quantity)
                    }
                })
            }
        },
        clearCart  : function () {
            cart_sdk.setLocalStorageValue('cart', { "cartInfo" : {} })
            cart_sdk.renderCart()
        },
        init: function() {
            // Set cart Initially blank
            // localStorage.setItem('cart', JSON.stringify({
            //     "cartInfo": []
            // }));
            

            var body = $("body");


            body.find('[data-new-page-redirect]').each(function(e){
                $(this).bind({
                    click : function (e) {
                        window.location = $(this).attr('href');
                    }
                })
            });

         
            

            cart_sdk.renderCart()

            cart_sdk.onloadSetQtyInproduct()

            body.append(cart_sdk.popUpHtml)

            body.find('[data-hc-product-qty-select]').each(function (e) {

                if($(this).is('select') == true){
                    $(this).bind({
                        click : function (e) {
                            e.stopPropagation()
                            return false;
                        },
                        change : function (e) {
                            e.stopPropagation()
                            cart_sdk.addProductIntoCart($(this), e)
                        }
                    })    
                }

                if($(this).is('input') == true){
                    switch($(this).attr('type')){
                        // case "radio":
                        //    $(this).each(function () {
                        //        $(this).bind({
                        //         click : function (e) {
                        //             e.stopPropagation()
                        //         },
                        //         change : function (e) {
                        //             e.stopPropagation()
                        //             cart_sdk.addProductIntoCart($(this), e)
                        //         }
                        //        })
                        //    })
                        // break;

                        case "number":
                           $(this).each(function () {
                               $(this).bind({
                                click : function (e) {
                                    e.stopPropagation()
                                },
                                change : function (e) {
                                    e.stopPropagation()
                                    cart_sdk.addProductIntoCart($(this), e)
                                }
                               })
                           })
                        break;

                        case "text":
                           $(this).each(function () {
                               $(this).bind({
                                click : function (e) {
                                    e.stopPropagation()
                                },
                                change : function (e) {
                                    e.stopPropagation()
                                    cart_sdk.addProductIntoCart($(this), e)
                                }
                               })
                           })
                        break;

                        default:
                        return;
                        break;
                    }
                }    
            })

            body.find('[data-variation]').each(function (e) {
                $(this).bind({
                    click : function (e) {
                        e.stopPropagation()
                        return false;
                    },
                    change : function (e) {
                        e.stopPropagation()
                        cart_sdk.addProductIntoCart($(this), e)
                    }
                })
            })

            body.find('[data-cart-button]').each(function (e) {
                $(this).bind({
                    click : function (e) {
                        e.stopPropagation()
                        if($(this).find('[data-cart-button]').length > 0){
                            return false
                        }
                        cart_sdk.addProductIntoCart($(this), e)    
                    },
                    change : function (e) {
                        e.stopPropagation()
                        return false
                    }
                })
            })

            body.on('click', '[data-hc-remove-product]', function(e){
                e.stopPropagation()
                        var cartInfo = cart_sdk.getLocalStorageValue('cart')
                        if(cartInfo && cartInfo.cartInfo  != null && Object.keys(cartInfo.cartInfo).length > 0){
                            if($(this).attr('data-hc-remove-product') in cartInfo.cartInfo){
                                delete cartInfo.cartInfo[$(this).attr('data-hc-remove-product')]
                                cart_sdk.setLocalStorageValue('cart',cartInfo)
                                cart_sdk.renderCart()
                            }
                        }

            })

            body.find('[data-hc-close-popup]').each(function(){
                $(this).bind({
                    click : function(e){
                        e.stopPropagation()
                        $(this).closest('.modal').modal('hide')
                    }
                })
            })

            $(document).on('click','[data-hc-process-order]', function (e) {
                var cartInfo = cart_sdk.getLocalStorageValue('cart')
                if(cartInfo && cartInfo.cartInfo != null && Object.keys(cartInfo.cartInfo).length > 0){
                    cart_sdk.cartProcess($(this))
                }
                return false
            })

            
            // Disable process-order if cart empty
            
        },
        manageVariantArray:function(variant,variantArray,productIndex ){
            try{
                
                variantArray = variantArray ?variantArray:[];

                $(Object.keys(variant)).each(function(index,val){
                    if(variantArray[val]){
                        variantArray[val][productIndex] = variant[val];
                    }else{
                        variantArray[val] = [];
                        variantArray[val][productIndex]= variant[val]; 
                    } 
                });

                return variantArray;
            }catch(e){
                console.log(e);
            }
        },
        convertVariantToQueryString:function(variant){
            try{
                let variantOptionString = '';
                let variantString = [];
                $(Object.keys(variant)).each(function(ind,val){
                    
                    variantString.push(val);
                    
                    for(i=0;i< variant[val].length; i++){
                        variant[val][i] = variant[val][i] ?variant[val][i] : 'null'; 
                    }
                    variantOptionString = variantOptionString + val+'='+variant[val].join() + '&';
                });

                variantOptionString = variantOptionString.replace(/&([^&]*)$/, '$1')
                variantString = variantString.join()+'&'+ variantOptionString;
                
                return variantString;
            }catch(e){
                console.log(e);
            }
        },
        cartProcess:function(_this){
            try{
                let dataHcUrl = _this.attr('data-hc-hash');
                let cart = cart_sdk.getLocalStorageValue('cart');
                let cartInfo = cart.cartInfo ? cart.cartInfo :[];
                if(Object.keys(cart.cartInfo).length <1){
                    return false;
                }

                let products = [];
                let billing_id = [];
                let variant = []
                let quantity = [];
                $(Object.keys(cart.cartInfo)).each(function(index,value){
                    let tempCart = cart.cartInfo[value];

                    products.push(tempCart.id);
                    billing_id.push(tempCart.product_billing_model_id);
                    quantity.push(tempCart.quantity);
                    variant = cart_sdk.manageVariantArray(tempCart.variant,variant, index);
                    // console.log(variantProcess,variant);
                    
                });

                let tempVariant = cart_sdk.convertVariantToQueryString(variant);
                // console.log(variant['color'].join().toString());
                let processUrl = 'products='+products.join()+'&billing_id='+billing_id.join()+'&quantity='+quantity.join()+'&variant='+tempVariant;
                
                window.location =cart_sdk.hc_ulr+dataHcUrl+'&'+processUrl;
                // console.log(cart_sdk.hc_ulr+dataHcUrl+'?'+processUrl);
                
            }catch(e){
                console.log("Cart Process Error", e);
            }
        }
        
    }
    cart_sdk.init();
})