<?php

use App\FW\sdk\Logger\CustomLog;


set_error_handler('errorHandler');
register_shutdown_function('shutdownHandler');

/**
 * @param $error_level
 * @param $error_message
 * @param $error_file
 * @param $error_line
 * @param $error_context
 */
function errorHandler($error_level, $error_message, $error_file, $error_line, $error_context)
{
    switch ($error_level) {
        case E_ERROR:
        case E_CORE_ERROR:
        case E_COMPILE_ERROR:
        case E_PARSE:
            $type = 'EMERGENCY';
        break;
        case E_USER_ERROR:
        case E_RECOVERABLE_ERROR:
            $type = 'ERROR';
        break;
        case E_WARNING:
        case E_CORE_WARNING:
        case E_COMPILE_WARNING:
        case E_USER_WARNING:
            $type = 'WARNING';
        break;
        case E_NOTICE:
        case E_USER_NOTICE:
            $type = 'NOTICE';
        break;
        case E_STRICT:
            $type = 'DEBUG';
        break;
        default:
            $type = 'WARNING';
    }

    (new CustomLog())
        ->writeLog([
            'type'    => $type,
            'message' => $error_message,
            'context' => array_merge($error_context, [
                'file' => $error_file,
                'line' => $error_line,
            ]),
            'data'    => [
                'request'      => $_REQUEST,
                'session'      => $_SESSION,
                'api_endpoint' => '',
                'api_response' => [],
                'api_error'    => [],
            ],
        ]);
}

/**
 * @return bool
 */
function shutdownHandler() //will be called when php script ends.
{
    $lasterror = error_get_last();

    if (empty($lasterror)) {
        return false;
    }

    switch ($lasterror['type']) {
        case E_ERROR:
        case E_CORE_ERROR:
        case E_COMPILE_ERROR:
        case E_USER_ERROR:
        case E_RECOVERABLE_ERROR:
        case E_PARSE:
            $type = 'EMERGENCY';
        break;
        case E_CORE_WARNING:
        case E_COMPILE_WARNING:
            $type = 'WARNING';
        break;
        default:
            $type = 'WARNING';
    }

    $message = isset($lasterror['message']) ? $lasterror['message'] : '';
    $file    = isset($lasterror['file']) ? $lasterror['file'] : '';
    $line    = isset($lasterror['line']) ? $lasterror['line'] : '';

    (new CustomLog())
        ->writeLog([
            'type'    => $type,
            'message' => $message,
            'context' => [
                'file' => $file,
                'line' => $line,
            ],
            'data'    => [
                'request'      => $_REQUEST,
                'session'      => $_SESSION,
                'api_endpoint' => '',
                'api_response' => [],
                'api_error'    => [],
            ],
        ]);

    return true;
}



